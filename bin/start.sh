#!/bin/bash
#获取当前路径
currentPath=$(pwd)
phoenix=${currentPath}/linux/phoenix
echo "${phoenix}"
#判断文件是否存在
if [ ! -f "${phoenix}" ]; then
    echo "phoenix not exist"
    exit 1
fi
#判断程序是否已经启动
if [ ! -f "${currentPath}"/linux/phoenix.pid ]; then
    $phoenix -P "${currentPath}"/linux/phoenix.pid
fi