chcp 65001
@echo off
set programName=dwys
::获取当前路径
set currentPath=%~dp0
echo %currentPath%
set phoenixExe=%currentPath%win/%programName%.exe
echo %phoenixExe%
::判断程序是否存在
if not exist %phoenixExe% (
    echo %phoenixExe% not found
    exit 1
    )
::判断程序是否运行,不存在就运行
set natsRun=0
tasklist | find /i "nats-server.exe" && %natsRun%=1 || %natsRun%=0
if %natsRun%==0 (
    echo 启动nats_server
    start "" cmd /k call %currentPath%../tools/nats-server/start_win_nopass.bat
    )
timeout /nobreak /t 3
echo 启动%programName%
::判断程序是否运行,不存在就运行
tasklist | find /i %programName%.exe && taskkill /im %programName%.exe || start /b %phoenixExe%