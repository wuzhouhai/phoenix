# phoenix

## 介绍
phoenix 是使用golang语言开发的一个简单,快速, 轻量级游戏服务器框架，
它为多人游戏服务器端应用程序提供了一个基本的开发框架。

## 使用说明
1. 使用git clone下载phoenix项目
2. 配置环境变量 (这里假设放在D盘)
   ![实例](./doc/img/env.png)
   - PHOENIX_BIN 为phoenix项目可执行文件  PHOENIX_BIN=%PHOENIX_ROOT%\bin
   - PHOENIX_RES_PATH 资源文件目录可以用逗号分隔多个目录 PHOENIX_RES_PATH=%PHOENIX_ROOT%\res;%PHOENIX_ROOT%\项目路径\res
   - PHOENIX_ROOT 为phoenix项目根目录  PHOENIX_ROOT=D:\phoenix
3. 创建项目(需要安装python3,版本3.12以上)
   - cd tools
   - cd operate
   - 执行脚本(projectName是项目名称) python ./scripts/phoenix.py new projectName
   - 新建项目会位于PHOENIX_ROOT\apps\projectName下面
4. 项目配置
