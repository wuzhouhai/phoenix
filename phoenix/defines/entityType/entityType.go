// Package entityType -----------------------------
// @file      : entityType.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/8/20 15:05
// @desc      :
// -------------------------------------------
package entityType

const (
	Phoenix_EntityType_Account     = 1
	Phoenix_EntityType_Packet      = 2
	Phoenix_EntityType_Bundle      = 3
	Phoenix_EntityType_Nats_Packet = 4

	Phoenix_APPEntityType_Start = 10000
)
