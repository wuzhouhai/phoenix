// Package opcode -----------------------------
// @file      : opcode.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/15 18:28
// @desc      :
// -------------------------------------------
package opcode

import "phoenix/phoenix/defines/dataType"

const (
	Opcode_SC_DEFINE_START     dataType.PhoenixTypeOpCode = 10000 // 服务器应用程序定义服务器发送发客户端消息码起始值
	Opcode_CS_DEFINE_START     dataType.PhoenixTypeOpCode = 20000 // 客户端应用程序定义客户端发送服务器消息码起始值
	Opcode_CS_DEFINE_LOGIN_END dataType.PhoenixTypeOpCode = 200   // 200 以下消息会被转发到login服务,其他会被转发到game服务
)

const (
	Opcode_SC_LOGIN dataType.PhoenixTypeOpCode = 1101
	Opcode_SC_KICK  dataType.PhoenixTypeOpCode = 1102
)

const (
	Opcode_CS_LOGIN dataType.PhoenixTypeOpCode = 101
)

const (
	Opcode_Nats_Gate2X_ClientMessage   dataType.PhoenixTypeOpCode = 40001
	Opcode_Nats_X2Gate_ClientMessage   dataType.PhoenixTypeOpCode = 40002
	Opcode_Nats_Login2Game_ClientLogin dataType.PhoenixTypeOpCode = 40003
	Opcode_Nats_Gate2Game_Disconnect   dataType.PhoenixTypeOpCode = 40004
)
