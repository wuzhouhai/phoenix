// Package opcode -----------------------------
// @file      : opcodeType.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/15 18:59
// @desc      :
// -------------------------------------------
package opcode

type OpCodeType uint8

const (
	OpCodeType_Text     OpCodeType = iota + 1 // 文本
	OpCodeType_Binary                         // 二进制
	OpCodeType_Json                           // Json
	OpCodeType_Protobuf                       // Protobuf
	OpCodeType_Unknown                        // 未知
)
