// Package manager -----------------------------
// @file      : menagerDefine.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/5 17:45
// @desc      :
// -------------------------------------------
package manager

const (
	PhoenixManagerConnect     = iota + 1 // 连接管理器
	PhoenixManagerDataBase               // 数据库管理器
	PhoenixManagerEntity                 // 实体管理器
	PhoenixManagerEvent                  // 事件管理器
	PhoenixManagerLogicConfig            // 逻辑配置管理器
	PhoenixManagerModule                 // 模块管理器
	PhoenixManagerObjects                // 对象管理器
	PhoenixManagerRes                    // 资源管理器
	PhoenixManagerRpc                    // rpc管理器
	PhoenixManagerTimer                  // 定时器管理器
	PhoenixManagerWork                   // 工作管理器
	PhoenixManagerWorld                  // 世界管理器
)
