package dataType

type PhoenixTypeConnectId = int32                   // 用户连接ID
type PhoenixTypeEntityId = uint64                   // 实体ID
type PhoenixTypeEntityType = int32                  // 实体类型ID
type PhoenixTypeDataBaseId = int                    // 数据库ID
type PhoenixTypeTimerID = uint64                    // 定时器ID
type PhoenixTypeModuleId = uint64                   // 模块ID
type PhoenixTypeUserId = uint64                     // 用户ID
type PhoenixTypeModuleData = map[string]interface{} //模块数据
type PhoenixTypeResValue = int64                    // 资源数据
type PhoenixTypeOpCode = uint16
