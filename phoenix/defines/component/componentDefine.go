// Package component -----------------------------
// @file      : componentDefine.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2023/12/21 16:51
// @desc      :
// -------------------------------------------
package component

const (
	COMPONENT_NETWORK    = "proxyComponent"
	COMPONENT_NATS       = "natsComponent"
	COMPONENT_WEB        = "webComponent"
	COMPONENT_WEB_SOCKET = "websocketComponent"
	COMPONENT_RPC_SERVER = "rpc_server"
	COMPONENT_RPC_CLIENT = "rpc_client"
)
