// Package normal -----------------------------
// @file      : normal.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/17 18:43
// @desc      :
// -------------------------------------------
package normal

const (
	HeaderSize  = 16               //包头长度
	MaxBodySize = 1024 * 1024 * 64 // 最大包体长度 64K

	ConnectTypeSocket    = "socket"
	ConnectTypeWebSocket = "websocket"

	DefaultGroupName = "default"
)
