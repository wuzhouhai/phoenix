// Package routine -----------------------------
// @file      : routineDefine.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2023/12/22 18:37
// @desc      :
// -------------------------------------------
package services

const (
	PHOENIX_SERVICE_GATE  = "gate"
	PHOENIX_SERVICE_WEB   = "web"
	PHOENIX_SERVICE_GAME  = "game"
	PHOENIX_SERVICE_LOGIN = "login"
)
