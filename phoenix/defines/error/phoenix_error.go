// Package error -----------------------------
// @file      : phoenix_error.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/7/1 16:36
// @desc      :
// -------------------------------------------
package error

const (
	PHOENIX_ERROR_START                   = 1000
	PHOENIX_ERROR_REFECT_FIELD_INVALID    = 1001 // 反射字段KEY无效
	PHOENIX_ERROR_REFECT_VALUE_INVALID    = 1002 // 反射字段VALUE无效
	PHOENIX_ERROR_REFECT_VALUE_TPYE       = 1003 // 反射设置字段值类型错误
	PHOENIX_ERROR_ENTITY_PROPERTY_INVALID = 1101 // 实体属性无效
	PHOENIX_ERROR_ENTITY_MODULE_NO_MODEL  = 1102 // 实体模块没有数据模型
	PHOENIX_ERROR_NET_MSG_INVALID         = 1201
	PHOENIX_ERROR_ACCOUNT_ONLINE          = 1301 // 账号已登录
	PHOENIX_ERROR_ACCOUNT_KICK            = 1302 //
	PHOENIX_ERROR_ACCOUNT_LOGINING        = 1303 // 账号正在登录
	PHOENIX_ERROR_DATA_TYPE_ERROR         = 1401 // 数据类型错误
	PHOENIX_ERROR_SERVICES_NOT_READY      = 1501 // 服务还没有准备好
	PHOENIX_ERROR_SERVICES_NOT_FIND       = 1502
	PHOENIX_ERROR_SERVICES_NO_GAME        = 1503 // 没有游戏服务
	PHOENIX_ERROR_END                     = 5000

	PHOENIX_CUSTOMER_ERROR_START = 10000 // 应用自定义错误开始
)
