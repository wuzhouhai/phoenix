// Package env -----------------------------
// @file      : envDefine.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2023/12/18 15:58
// @desc      : 环境变量定义
// -------------------------------------------
package env

const (
	ENV_PHOENIX_ROOT     = "PHOENIX_ROOT"
	ENV_PHOENIX_BIN      = "PHOENIX_BIN"
	ENV_PHOENIX_RES_PATH = "PHOENIX_RES_PATH"
)
