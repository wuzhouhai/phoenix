// Package funcType -----------------------------
// @file      : funcType.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/4/16 18:24
// @desc      :
// -------------------------------------------
package funcType

type PhoenixTypeMessageHandleFunc = func(i interface{}, d interface{}, agentHandleName string)
type PhoenixTypeNatsRpcMessageHandleFunc = func(i interface{}, d interface{}, agentHandleName string, serviceType string,
	serviceId string)
