// Package eventType -----------------------------
// @file      : eventType.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/29 14:58
// @desc      :
// -------------------------------------------
package eventType

const (
	PhoenixEvent_QuitApp      = "PhoenixEvent_QuitApp"
	PhoenixEvent_AppWillClose = "PhoenixEvent_AppWillClose"
)
