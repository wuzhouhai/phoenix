// Package files -----------------------------
// @file      : files.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2023/12/18 16:04
// @desc      : 文件名定义
// -------------------------------------------
package files

const (
	FILE_CONFIG_FILE      = "default_phoenix.yaml" // 默认配置文件
	FILE_USER_CONFIG_FILE = "phoenix.yaml"         // 用户配置文件,会覆盖默认配置文件
)
