// Package paths Package files -----------------------------
// @file      : paths.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2023/12/18 16:04
// @desc      : 路径定义
// -------------------------------------------
package paths

const (
	Res_Path         = "/res"
	Config_Path      = "/phoenix"
	LogicConfig_Path = "/logicConfig"
)
