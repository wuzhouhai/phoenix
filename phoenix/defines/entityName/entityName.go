// Package entityName -----------------------------
// @file      : entityName.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/8/20 15:09
// @desc      :
// -------------------------------------------
package entityName

const (
	Phoenix_EntityName_Account       = "Account"
	Phoenix_EntityName_Packet        = "Packet"
	Phoenix_EntityName_NatsRpcPacket = "NatsRpcPacket"
	Phoenix_EntityName_Bundle        = "Bundle"
)
