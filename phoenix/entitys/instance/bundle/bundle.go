// Package bundle -----------------------------
// @file      : bundle.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/16 14:40
// @desc      :
// -------------------------------------------
package bundle

import (
	"bytes"
	"github.com/gobwas/ws/wsutil"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/defines/entityName"
	"phoenix/phoenix/defines/entityType"
	"phoenix/phoenix/defines/normal"
	"phoenix/phoenix/entitys/entityBase"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/connectManager"
	"phoenix/phoenix/managers/objectsManager"
	"phoenix/phoenix/network/writer"
)

type Bundle struct {
	entityBase.EntityBase
	Receivers    []dataType.PhoenixTypeConnectId // 接收的连接ID
	packetWriter *writer.PacketWriter
	datas        bytes.Buffer // 数据包
}

func NewBundle() *Bundle {
	e, _ := objectsManager.PhoenixManagerObjects().CreateObject(entityName.Phoenix_EntityName_Bundle)
	return e.(*Bundle)
}

func (this *Bundle) Init() {

}

func (this *Bundle) AddendReceiver(receiver dataType.PhoenixTypeConnectId) {
	this.Receivers = append(this.Receivers, receiver)
}

func (this *Bundle) GetLength() int {
	return this.datas.Len()
}

func (this *Bundle) GetData() []byte {
	return this.datas.Bytes()
}

func (this *Bundle) AppendPacket(packet interfaces.IPacket) {
	err := this.packetWriter.Writer(&this.datas, packet)
	if err != nil {
		return
	}
}

func (this *Bundle) SendData() error {
	for _, receiver := range this.Receivers {
		// 发送数据
		connector := connectManager.PhoenixManagerConnect().GetConnect(receiver)
		if connector == nil {
			continue
		}
		switch connector.GetConType() {
		case normal.ConnectTypeSocket:
			err := this.SendSocketData(connector)
			if err != nil {
				return err
			}
		case normal.ConnectTypeWebSocket:
			err := this.SendWebSocketData(connector)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (this *Bundle) SendSocketData(con interfaces.IConnector) error {
	c := con.GetConn()
	if c != nil {
		err := c.AsyncWrite(this.datas.Bytes(), nil)
		if err != nil {
			return err
		}
	}
	return nil
}

func (this *Bundle) SendWebSocketData(con interfaces.IConnector) error {
	c := con.GetConn()
	if c != nil {
		err := wsutil.WriteServerMessage(c, con.GetWebSocketOpcode(), this.datas.Bytes())
		if err != nil {
			return err
		}
	}
	return nil
}

func (this *Bundle) ResetObject() {
	this.EntityBase.ResetObject()
	clear(this.Receivers)
	this.datas.Reset()
}

func NewBundleInPool() *Bundle {
	e := &Bundle{
		EntityBase:   entityBase.NewEntityBase(entityName.Phoenix_EntityName_Bundle, entityType.Phoenix_EntityType_Bundle),
		Receivers:    make([]dataType.PhoenixTypeConnectId, 0),
		packetWriter: writer.NewPacketWriter(),
		datas:        bytes.Buffer{},
	}
	return e
}

func BundleObjectCreate() (interface{}, error) {
	return NewBundleInPool(), nil
}

func BundleObjectDestroy(i interface{}) {
}
