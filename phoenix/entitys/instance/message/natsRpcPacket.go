// Package message -----------------------------
// @file      : natsRpcPacket.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/11 14:09
// @desc      :
// -------------------------------------------
package message

import (
	"errors"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/defines/entityName"
	"phoenix/phoenix/defines/entityType"
	"phoenix/phoenix/entitys/entityBase"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/objectsManager"
	"phoenix/phoenix/register"
	"reflect"
)

type NatsRpcPacket struct {
	Packet
}

func NewRpcPacket(opcode dataType.PhoenixTypeOpCode) *NatsRpcPacket {
	e, _ := objectsManager.PhoenixManagerObjects().CreateObject(entityName.Phoenix_EntityName_NatsRpcPacket)
	p := e.(*NatsRpcPacket)
	p.InitRpcPacket(opcode)
	return p
}

func (this *NatsRpcPacket) InitRpcPacket(opcode dataType.PhoenixTypeOpCode) {
	handler := register.GetNatsRpcHandler(opcode)
	this.Opcode = opcode
	this.Msg = reflect.New(handler.MsgProtoType).Interface()
}

func (this *NatsRpcPacket) EncodeMsg() (error, []byte) {
	if this.Msg == nil {
		return errors.New("packet is nil"), nil
	}
	handler := register.GetNatsRpcHandler(this.Opcode)
	codecRefType := register.GetCodec(handler.OpCodecType)
	codecObj := reflect.New(codecRefType).Interface().(interfaces.ICodec)
	msgData, err := codecObj.Encode(this.Msg)
	if err != nil {
		return err, nil
	} else {
		return nil, msgData
	}
}

func (this *NatsRpcPacket) DecodeMsg(msgData []byte) error {
	if len(msgData) == 0 {
		return nil
	}
	handler := register.GetNatsRpcHandler(this.Opcode)
	codecRefType := register.GetCodec(handler.OpCodecType)
	codecObj := reflect.New(codecRefType).Interface().(interfaces.ICodec)
	err := codecObj.Decode(msgData, this.Msg)
	return err
}

func NewNatsRpcPacketInPool() *NatsRpcPacket {
	e := &NatsRpcPacket{
		Packet: Packet{
			EntityBase: entityBase.NewEntityBase(entityName.Phoenix_EntityName_NatsRpcPacket, entityType.Phoenix_EntityType_Nats_Packet),
			Size:       0,
			Opcode:     0,
			Msg:        nil,
		},
	}
	return e
}

func NatsRpcPacketObjectCreate() (interface{}, error) {
	return NewNatsRpcPacketInPool(), nil
}

func NatsRpcPacketObjectDestroy(i interface{}) {
}
