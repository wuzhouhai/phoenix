// Package header -----------------------------
// @file      : header.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/15 18:25
// @desc      :
// -------------------------------------------
package header

import (
	"phoenix/phoenix/defines/dataType"
	opcode2 "phoenix/phoenix/defines/opcode"
)

type Header struct {
	Length     int32                      // 包长度
	VerifyID   uint32                     // 验证ID 只增不减少
	Opcode     dataType.PhoenixTypeOpCode // 消息号码
	OpcodeType opcode2.OpCodeType         // 消息类型,决定解包
	Masked     bool                       // 是否有掩码
	Mask       [4]byte                    // 掩码数组
}

func NewHeader() *Header {
	return &Header{
		Length:     -1,
		VerifyID:   0,
		Opcode:     0,
		OpcodeType: 0,
		Masked:     false,
		Mask:       [4]byte{0},
	}
}

func (this *Header) Reset() {
	this.Length = -1
	this.VerifyID = 0
	this.Opcode = 0
	this.OpcodeType = 0
	this.Masked = false
	for i := 0; i < 4; i++ {
		this.Mask[i] = 0
	}
}

func (this *Header) SetLength(bodyLength int32) {
	this.Length = bodyLength
}

func (this *Header) SetMast(index int, value byte) {
	this.Mask[index] = value
	this.Masked = true
}
