// Package message Packet message -----------------------------
// @file      : packet.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/15 19:12
// @desc      :
// -------------------------------------------
package message

import (
	"errors"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/defines/entityName"
	"phoenix/phoenix/defines/entityType"
	"phoenix/phoenix/entitys/entityBase"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/objectsManager"
	"phoenix/phoenix/register"
	"reflect"
)

type Packet struct {
	entityBase.EntityBase
	Size   int32
	Opcode dataType.PhoenixTypeOpCode
	Msg    interface{}
}

func NewPacket(opcode dataType.PhoenixTypeOpCode) *Packet {
	e, _ := objectsManager.PhoenixManagerObjects().CreateObject(entityName.Phoenix_EntityName_Packet)
	p := e.(*Packet)
	p.InitPacket(opcode)
	return p
}

func (this *Packet) Init() {

}

func (this *Packet) InitPacket(opcode dataType.PhoenixTypeOpCode) {
	handler := register.GetSocketHandler(opcode)
	this.Opcode = opcode
	this.Msg = reflect.New(handler.MsgProtoType).Interface()
}

func (this *Packet) SetMsg(msg interface{}) {
	this.Msg = msg
}

func (this *Packet) GetMsg() interface{} {
	return this.Msg
}

func (this *Packet) SetOpCode(opcode dataType.PhoenixTypeOpCode) {
	this.Opcode = opcode
}

func (this *Packet) GetOpcode() dataType.PhoenixTypeOpCode {
	return this.Opcode
}

func (this *Packet) GetSize() int32 {
	return this.Size
}

func (this *Packet) EncodeMsg() (error, []byte) {
	if this.Msg == nil {
		return errors.New("packet is nil"), nil
	}
	handler := register.GetSocketHandler(this.Opcode)
	codecRefType := register.GetCodec(handler.OpCodecType)
	codecObj := reflect.New(codecRefType).Interface().(interfaces.ICodec)
	msgData, err := codecObj.Encode(this.Msg)
	if err != nil {
		return err, nil
	} else {
		return nil, msgData
	}
}

func (this *Packet) DecodeMsg(msgData []byte) error {
	if len(msgData) == 0 {
		return nil
	}
	handler := register.GetSocketHandler(this.Opcode)
	codecRefType := register.GetCodec(handler.OpCodecType)
	codecObj := reflect.New(codecRefType).Interface().(interfaces.ICodec)
	err := codecObj.Decode(msgData, this.Msg)
	return err
}

func (this *Packet) ResetObject() {
	this.EntityBase.ResetObject()
	this.Size = 0
	this.Opcode = 0
	this.Msg = nil
}

func NewPacketInPool() *Packet {
	e := &Packet{
		EntityBase: entityBase.NewEntityBase(entityName.Phoenix_EntityName_Packet, entityType.Phoenix_EntityType_Packet),
		Size:       0,
		Opcode:     0,
		Msg:        nil,
	}
	return e
}

func PacketObjectCreate() (interface{}, error) {
	return NewPacketInPool(), nil
}

func PacketObjectDestroy(i interface{}) {
}
