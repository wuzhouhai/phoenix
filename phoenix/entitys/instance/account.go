// Package entitys -----------------------------
// @file      : account.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/8/20 11:21
// @desc      :
// -------------------------------------------
package instance

import (
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	"phoenix/apps/examples/phoenix_app_dwys/protocol/golang/protos"
	"phoenix/phoenix/app"
	"phoenix/phoenix/common/fixCache"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/entityName"
	"phoenix/phoenix/defines/entityType"
	error2 "phoenix/phoenix/defines/error"
	"phoenix/phoenix/defines/opcode"
	"phoenix/phoenix/defines/services"
	"phoenix/phoenix/entitys/agentEntity"
	entityBase2 "phoenix/phoenix/entitys/entityBase"
	"phoenix/phoenix/entitys/entityModel"
	"phoenix/phoenix/entitys/instance/message"
	"phoenix/phoenix/managers/cacheManager"
	"phoenix/phoenix/managers/entityManager"
	"phoenix/phoenix/managers/objectsManager"
	"phoenix/phoenix/network/protocol/nrpc/golang/natsRpc"
	"phoenix/phoenix/network/protocol/nrpc/golang/service"
	"phoenix/phoenix/network/protocol/webToServer"
	"phoenix/phoenix/utils"
)

type Account struct {
	agentEntity.AgentEntity
}

func newAccount() *Account {
	e := &Account{
		AgentEntity: agentEntity.AgentEntity{
			ConnectId:  0,
			EntityBase: entityBase2.NewEntityBase(entityName.Phoenix_EntityName_Account, entityType.Phoenix_EntityType_Account),
		},
	}
	entityManager.PhoenixManagerEntity().AddEntity(e)
	return e
}

func (this *Account) Init() {
	logger.PhoenixLoggerIns().Debug("Account init")
	this.AgentEntity.Init()
}

func (this *Account) Release() {
	logger.PhoenixLoggerIns().Debugf("Account release %d", this.Id)
	objectsManager.PhoenixManagerObjects().DestroyObject(this)
	entityManager.PhoenixManagerEntity().RemoveEntity(this.Id)
}

func (this *Account) OnLogin(d interface{}, natsRpcBase *natsRpc.RpcBase) error {
	logger.PhoenixLoggerIns().Debug("Account OnLogin")
	if msg, ok := d.(*protos.C2SLogin); ok {
		msgInfo, err := utils.AESDecryptFromString(msg.Credential, app.PhoenixIns().GetServerKey(), msg.Iv)
		if err != nil {
			return err
		}
		loginData := &webToServer.LoginData{}
		err = loginData.DecodeFromJsonString(msgInfo)
		if err != nil {
			logger.PhoenixLoggerIns().Error("account OnLogin DecodeFromJsonString err : " + err.Error())
			return err
		}
		entityDataModel := this.EntityModel.GetEntityData().(*entityModel.AccountModelData)
		entityDataModel.AvatarId = loginData.AvatarId
		entityDataModel.AccountName = loginData.AccountName
		err = this.LoadEntityData()
		if err != nil {
			return err
		}
		this.SetAccountDBID(entityDataModel.Id)
		//处理登录时间更新
		entityDataModel.LoginTime = utils.GetTimeStrFromTimestamp(loginData.LoginTime)
		err = this.EntityModel.UpdateEntityData(true)
		if err != nil {
			return err
		}
		// 构造发送到Game登录消息
		packet := message.NewRpcPacket(opcode.Opcode_Nats_Login2Game_ClientLogin)
		if packet != nil {
			defer objectsManager.PhoenixManagerObjects().DestroyObject(packet)
			natsComponent := this.Service.GetNatsComponent()
			selectGameService := ""
			if natsComponent != nil {
				loginMsg := packet.GetMsg().(*service.Login2GameClientLogin)
				loginMsg.Channel = &natsRpc.RpcBase{
					FromSubscribe:   "",
					FromServiceType: "",
					FromServiceId:   "",
					ToSubscribe:     "",
					ConnectorId:     0,
				}
				selectGameService, _ = natsComponent.SelectGameService(this.AccountDBID)
				err := natsComponent.CreateRpcBaseWithServiceId(services.PHOENIX_SERVICE_GAME, selectGameService,
					loginMsg.Channel, this.ConnectId)
				if err != nil {
					return err
				}
				loginMsg.AvatarId = int32(loginData.AvatarId)
				loginMsg.AvatarName = loginData.AvatarName
				loginMsg.AccountName = loginData.AccountName
				loginMsg.GateServiceId = natsRpcBase.FromServiceId
			}
			err := this.Service.SendNatsPacket(services.PHOENIX_SERVICE_GAME, selectGameService, packet)
			if err != nil {
				return err
			}
			gateCacheKey := fixCache.GetAvatarGateCacheKey(int32(this.AccountDBID))
			_ = cacheManager.PhoenixManagerCache().Put(gateCacheKey, natsRpcBase.FromServiceId, 0)
			this.SaveToDB()
		}
		return nil
	}
	return gerror.NewCodef(gcode.New(error2.PHOENIX_ERROR_NET_MSG_INVALID, "", nil),
		"msg invalid")
}

func (this *Account) LoadEntityData() error {
	err := this.GetEntityModel().LoadEntityDataFromDB(nil)
	if err != nil {
		logger.PhoenixLoggerIns().Errorf("load entity %d data error %s", this.Id, err.Error())
		return err
	}
	this.DataBaseID = this.GetEntityModel().GetEntityDBID()
	return nil
}

func AccountObjectCreate() (interface{}, error) {
	return newAccount(), nil
}

func AccountObjectDestroy(i interface{}) {
}
