// Package entityModule -----------------------------
// @file      : entityModuleBase.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/7/4 17:14
// @desc      :
// -------------------------------------------
package entityModule

import (
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	"phoenix/phoenix/defines/dataType"
	error2 "phoenix/phoenix/defines/error"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/register"
)

type EntityModuleBase struct {
	Name       string // 模块名称
	EntityDBID dataType.PhoenixTypeDataBaseId
	Model      interfaces.IEntityModuleModel
}

func (this *EntityModuleBase) Init() {

}

func (this *EntityModuleBase) GetEntityModuleName() string {
	return this.Name
}

func (this *EntityModuleBase) GetModuleModel() interfaces.IEntityModuleModel {
	return this.Model
}

func (this *EntityModuleBase) SetEntityDBId(entityID dataType.PhoenixTypeDataBaseId) {
	this.EntityDBID = entityID
	this.Model.SetEntityDBID(entityID)
}

func (this *EntityModuleBase) GetEntityDBId() dataType.PhoenixTypeDataBaseId {
	return this.EntityDBID
}

func (this *EntityModuleBase) LoadData() error {
	if this.Model == nil {
		return gerror.NewCodef(gcode.New(error2.PHOENIX_ERROR_ENTITY_MODULE_NO_MODEL, "", nil),
			"模块%s没有数据模型", this.Name)
	}
	if this.Model.IsPersistent() {
		return this.Model.LoadModule()
	}
	return nil
}

func (this *EntityModuleBase) SavaData() error {
	if this.Model == nil {
		return gerror.NewCodef(gcode.New(error2.PHOENIX_ERROR_ENTITY_MODULE_NO_MODEL, "", nil),
			"模块%s没有数据模型", this.Name)
	}
	if this.Model.IsPersistent() {
		return this.Model.SaveModuleToDB()
	}
	return nil
}

func (this *EntityModuleBase) SyncToDB() error {
	if this.Model == nil {
		return gerror.NewCodef(gcode.New(error2.PHOENIX_ERROR_ENTITY_MODULE_NO_MODEL, "", nil),
			"模块%s没有数据模型", this.Name)
	}
	if this.Model.IsPersistent() {
		return this.Model.SyncModuleToDB()
	}
	return nil
}

func (this *EntityModuleBase) Reset() {
	if this.Model == nil {
		return
	}
	this.Model.Reset()
}

func NewEntityModuleBase(entityName string, moduleName string, entityDBID dataType.PhoenixTypeDataBaseId) EntityModuleBase {
	return EntityModuleBase{
		Name:       moduleName,
		EntityDBID: entityDBID,
		Model:      register.GetModelModule(entityName, moduleName, entityDBID),
	}
}
