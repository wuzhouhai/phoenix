package agentEntity

import (
	"fmt"
	"phoenix/apps/examples/phoenix_app_dwys/protocol/golang/protos"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	error2 "phoenix/phoenix/defines/error"
	"phoenix/phoenix/defines/opcode"
	"phoenix/phoenix/defines/services"
	"phoenix/phoenix/entitys/entityBase"
	"phoenix/phoenix/entitys/instance/message"
	"phoenix/phoenix/managers/objectsManager"
	"phoenix/phoenix/managers/worldManager"
)

type AgentEntity struct {
	entityBase.EntityBase
	GateServiceId string
	ConnectId     dataType.PhoenixTypeConnectId
	AccountDBID   dataType.PhoenixTypeDataBaseId
}

func (this *AgentEntity) Init() {
	this.EntityBase.Init()
}

func (this *AgentEntity) SetAccountDBID(dbid dataType.PhoenixTypeDataBaseId) {
	this.AccountDBID = dbid
}

func (this *AgentEntity) GetAccountDBID() dataType.PhoenixTypeDataBaseId {
	return this.AccountDBID
}

func (this *AgentEntity) SetConnectId(connId dataType.PhoenixTypeConnectId) {
	this.ConnectId = connId
}

func (this *AgentEntity) GetConnectId() dataType.PhoenixTypeConnectId {
	return this.ConnectId
}

func (this *AgentEntity) DisConnect() {
	logger.PhoenixLoggerIns().Debug(fmt.Sprintf("AgentEntity DisConnect PhoenixTypeConnectId:%v, entityId:%d",
		this.ConnectId, this.Id))
	this.SaveToDB()
	worldManager.PhoenixManagerWorld().Offline(this.AccountDBID)
}

func (this *AgentEntity) Release() {
	panic("implement me")
}

func (this *AgentEntity) ResetObject() {
	this.EntityBase.ResetObject()
	this.ConnectId = 0
}

func (this *AgentEntity) SetGateService(gateId string) {
	this.GateServiceId = gateId
}

func (this *AgentEntity) GetGateService() string {
	return this.GateServiceId
}

func (this *AgentEntity) SendPacket(p *message.Packet) error {
	natsPacket := this.Service.CreateX2GateRpcClientPacket(this.GateServiceId, p, this.ConnectId)
	err := this.Service.SendNatsPacket(services.PHOENIX_SERVICE_GATE, this.GateServiceId, natsPacket)
	if err != nil {
		return err
	}
	defer objectsManager.PhoenixManagerObjects().DestroyObject(p)
	defer objectsManager.PhoenixManagerObjects().DestroyObject(natsPacket)
	return nil
}

func (this *AgentEntity) BeKick(msg string) {
	// 返回登录失败
	packet := message.NewPacket(opcode.Opcode_SC_KICK)
	if packet != nil {
		sendMsg := packet.GetMsg().(*protos.S2CKick)
		sendMsg.Base = &protos.S2CBase{
			Code: error2.PHOENIX_ERROR_ACCOUNT_KICK,
			Msg:  msg,
		}
		err := this.SendPacket(packet)
		if err != nil {
			logger.PhoenixLoggerIns().Error(fmt.Sprintf("SendPacket error:%v", err))
		}
	}
	this.ConnectId = 0
	this.GateServiceId = ""
}

func (this *AgentEntity) OnAvatarLogin(avatarId dataType.PhoenixTypeDataBaseId, avatarName string, accountName string) error {
	panic("implement me")
}

func (this *AgentEntity) ReAvatarLogin(avatarId dataType.PhoenixTypeDataBaseId, avatarName string, accountName string) error {
	panic("implement me")
}
