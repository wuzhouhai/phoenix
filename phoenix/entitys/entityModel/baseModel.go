// Package entityModel -----------------------------
// @file      : BaseModel.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/7/1 17:36
// @desc      :
// -------------------------------------------
package entityModel

import (
	"phoenix/phoenix/defines/dataType"
)

type BaseModel struct {
	Name       string
	EntityDBID dataType.PhoenixTypeDataBaseId
	Template   interface{} // 数据模版,由子类设置
}

func (this *BaseModel) Create() error {
	panic("BaseModel.Create() not implement")
}

func (this *BaseModel) IsPersistent() bool {
	return true
}

func (this *BaseModel) SetEntityDBID(entityDBID dataType.PhoenixTypeDataBaseId) {
	this.EntityDBID = entityDBID
}

func (this *BaseModel) GetEntityDBID() dataType.PhoenixTypeDataBaseId {
	return this.EntityDBID
}

func (this *BaseModel) SetTemplate(t interface{}) {
	this.Template = t
}

func (this *BaseModel) Reset() {
	this.Name = ""
	this.EntityDBID = 0
}

func NewBaseModel(name string, entityDBID dataType.PhoenixTypeDataBaseId) BaseModel {
	return BaseModel{
		Name:       name,
		EntityDBID: entityDBID,
		Template:   nil,
	}
}
