// Package entityModel -----------------------------
// @file      : moduleSingleModel.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/7/18 17:37
// @desc      :
// -------------------------------------------
package entityModel

import (
	"phoenix/phoenix/defines/dataType"
)

type ModuleSingleModel struct {
	ModuleModel
}

func (this *ModuleSingleModel) SetModuleSingleDataDBID(dbid dataType.PhoenixTypeDataBaseId) {
	this.SingleModuleDataDBID = dbid
}

func (this *ModuleSingleModel) GetModelData(dbId dataType.PhoenixTypeDataBaseId) interface{} {
	if this.Datas.Contains(this.SingleModuleDataDBID) {
		return this.Datas.Get(this.SingleModuleDataDBID)
	}
	return nil
}

func (this *ModuleSingleModel) Reset() {
	this.ModuleModel.Reset()
	this.SingleData = true
}

func NewModuleSingleModel(name string, entityDBID dataType.PhoenixTypeDataBaseId) ModuleSingleModel {
	m := ModuleSingleModel{
		ModuleModel: NewModuleModel(name, entityDBID),
	}
	m.SingleData = true
	return m
}
