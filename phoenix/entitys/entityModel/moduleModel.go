// Package entityModel -----------------------------
// @file      : moduleModel.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/7/18 17:36
// @desc      :
// -------------------------------------------
package entityModel

import (
	"github.com/gogf/gf/v2/container/garray"
	"github.com/gogf/gf/v2/container/gmap"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/managers/databaseManager"
)

type ModuleModel struct {
	BaseModel
	SingleData           bool
	SingleModuleDataDBID dataType.PhoenixTypeDataBaseId // 单条模块数据库ID
	SingleUpdateFlag     bool                           // 更新标识
	Datas                *gmap.IntAnyMap
	UpdateDatas          *garray.IntArray
}

func (this *ModuleModel) IsSingleData() bool {
	return this.SingleData
}
func (this *ModuleModel) SetModuleSingleDataDBID(dbid dataType.PhoenixTypeDataBaseId) {
	panic("not support")
}

func (this *ModuleModel) SetUpdateSingleData() {
	this.SetModelDataUpdate(this.SingleModuleDataDBID)
}

func (this *ModuleModel) Reset() {
	this.SingleData = false
	this.SingleModuleDataDBID = 0
	this.SingleUpdateFlag = false
	this.BaseModel.Reset()
	this.Datas.Clear()
	this.UpdateDatas.Clear()
}

func (this *ModuleModel) SetModelData(dbId dataType.PhoenixTypeDataBaseId, data interface{}) {
	this.Datas.Set(dbId, data)
}

func (this *ModuleModel) GetModelData(dbId dataType.PhoenixTypeDataBaseId) interface{} {
	if this.Datas.Contains(dbId) {
		return this.Datas.Get(dbId)
	}
	return nil
}

func (this *ModuleModel) GetModelDatas() *gmap.IntAnyMap {
	return this.Datas
}

func (this *ModuleModel) SetModelDataUpdate(dbId dataType.PhoenixTypeDataBaseId) {
	if !this.UpdateDatas.Contains(dbId) {
		this.UpdateDatas.Append(dbId)
	}
}

func (this *ModuleModel) SyncModuleToDB() error {
	base, err := databaseManager.PhoenixManagerDatabase().DefaultDataBase()
	if err != nil {
		return err
	}
	this.UpdateDatas.Iterator(func(k int, v int) bool {
		uData := this.GetModelData(v)
		if uData != nil {
			_, err := base.GetDb().Model(this.Template).Data(uData).OmitEmpty().Save()
			if err != nil {
				logger.PhoenixLoggerIns().Errorf("update model %s error dbid:%s", this.Name, v)
			}
		}
		return true
	})
	this.UpdateDatas.Clear()
	return nil
}

func (this *ModuleModel) UpdateDataModule(dbId dataType.PhoenixTypeDataBaseId, isSyncToDB bool) error {
	if isSyncToDB {
		recordData := this.GetModelData(dbId)
		if recordData != nil {
			base, err := databaseManager.PhoenixManagerDatabase().DefaultDataBase()
			if err != nil {
				return err
			}
			_, err = base.GetDb().Model(this.Template).Data(recordData).OmitEmpty().Save()
			if err != nil {
				logger.PhoenixLoggerIns().Errorf("UpdateData model %s error dbid:%s", this.Name, dbId)
			}
		}
	} else {
		this.SetModelDataUpdate(dbId)
	}
	return nil
}

func (this *ModuleModel) SaveModuleToDB() error {
	base, err := databaseManager.PhoenixManagerDatabase().DefaultDataBase()
	if err != nil {
		return err
	}
	this.Datas.Iterator(func(k int, v interface{}) bool {
		_, err := base.GetDb().Model(this.Template).Data(v).OmitEmpty().Save()
		if err != nil {
			logger.PhoenixLoggerIns().Errorf("save model %s error dbid:%s", this.Name, k)
		}
		return true
	})
	return nil
}

func (this *ModuleModel) LoadModuleFromDB(records interface{}) error {
	base, err := databaseManager.PhoenixManagerDatabase().DefaultDataBase()
	if err != nil {
		return err
	}
	err = base.GetDb().Model(this.Template).Where("ownerId", this.EntityDBID).Scan(records)
	if err != nil {
		return err
	}
	return nil
}

func NewModuleModel(name string, entityDBID dataType.PhoenixTypeDataBaseId) ModuleModel {
	return ModuleModel{
		BaseModel:            NewBaseModel(name, entityDBID),
		SingleData:           false,
		SingleUpdateFlag:     false,
		SingleModuleDataDBID: 0,
		UpdateDatas:          garray.NewIntArray(true),
		Datas:                gmap.NewIntAnyMap(true),
	}
}
