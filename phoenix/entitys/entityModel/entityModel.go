// Package entityModel -----------------------------
// @file      : entityModel.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/7/18 17:36
// @desc      :
// -------------------------------------------
package entityModel

import (
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/databaseManager"
)

type EntityModel struct {
	BaseModel
	EntityData interfaces.IEntityModelData // 数据
	UpdateFlag bool                        // 更新标识
}

func (this *EntityModel) LoadEntityDataFromDB(records interface{}) error {
	panic("implement me")
}

func (this *EntityModel) UpdateEntityData(isSyncToDB bool) error {
	if isSyncToDB {
		err := this.SaveEntityDataToDB()
		if err != nil {
			return err
		}
	} else {
		this.UpdateFlag = true
	}
	return nil
}

func (this *EntityModel) SetEntityData(d interfaces.IEntityModelData) {
	this.EntityData = d
}

func (this *EntityModel) GetEntityData() interfaces.IEntityModelData {
	return this.EntityData
}

func (this *EntityModel) SyncEntityDataToDB() error {
	if this.UpdateFlag {
		err := this.SaveEntityDataToDB()
		if err != nil {
			return err
		}
		this.UpdateFlag = false
	}
	return nil
}

func (this *EntityModel) CreateDataToDB() error {
	base, err := databaseManager.PhoenixManagerDatabase().DefaultDataBase()
	if err != nil {
		return err
	}
	dbid, err := base.GetDb().Model(this.Template).Data(this.EntityData).OmitEmpty().InsertAndGetId()
	this.SetEntityDBID(dataType.PhoenixTypeDataBaseId(dbid))
	if err != nil {
		logger.PhoenixLoggerIns().Errorf("CreateDataToDB model %s error", this.Name)
	}
	return err
}

func (this *EntityModel) SaveEntityDataToDB() error {
	if this.EntityData == nil || this.EntityDBID == 0 {
		return nil
	}
	base, err := databaseManager.PhoenixManagerDatabase().DefaultDataBase()
	if err != nil {
		return err
	}
	_, err = base.GetDb().Model(this.Template).Data(this.EntityData).OmitEmpty().Save()
	if err != nil {
		logger.PhoenixLoggerIns().Errorf("SyncEntityDataToDB model %s error dbid:%s", this.Name, this.EntityDBID)
	}
	return err
}

func (this *EntityModel) Reset() {
	this.BaseModel.Reset()
	this.EntityData.Reset()
	this.UpdateFlag = false
}

func NewEntityModel(name string) EntityModel {
	return EntityModel{
		BaseModel:  NewBaseModel(name, 0),
		EntityData: nil,
		UpdateFlag: false,
	}
}
