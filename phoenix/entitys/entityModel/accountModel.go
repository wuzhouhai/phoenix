// Package entityModel -----------------------------
// @file      : accountModel.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/8/20 14:31
// @desc      :
// -------------------------------------------
package entityModel

import (
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/util/gmeta"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	error2 "phoenix/phoenix/defines/error"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/databaseManager"
	"phoenix/phoenix/utils"
)

type AccountModelData struct {
	gmeta.Meta  `orm:"table: account"`
	Id          dataType.PhoenixTypeDataBaseId `orm:"id"`          // 账号ID
	AccountName string                         `orm:"account"`     // 账号
	Password    string                         `orm:"password"`    // 密码
	AvatarId    dataType.PhoenixTypeDataBaseId `orm:"avatar_id"`   // 角色ID
	LoginTime   string                         `orm:"login_time"`  // 登录时间
	CreateTime  string                         `orm:"create_time"` // 创建时间
}

func (this *AccountModelData) Reset() {
	this.Id = 0
	this.AccountName = ""
	this.Password = ""
	this.AvatarId = 0
	this.LoginTime = ""
	this.CreateTime = ""
}

type AccountModel struct {
	EntityModel
}

func (this *AccountModel) LoadEntityDataFromDB(records interface{}) error {
	base, err := databaseManager.PhoenixManagerDatabase().DefaultDataBase()
	if err != nil {
		return err
	}
	if entityData, ok := this.EntityData.(*AccountModelData); ok {
		one, err := base.GetDb().Model(this.Template).Where("account", entityData.AccountName).One()
		if err != nil {
			return err
		}
		if one == nil {
			return this.Create()
		}
		err = one.Struct(this.EntityData)
		this.SetEntityDBID(entityData.Id)
		return nil
	}
	return gerror.NewCodef(gcode.New(error2.PHOENIX_ERROR_ENTITY_PROPERTY_INVALID, "", nil),
		"avatar module model data is invalid")
}

func (this *AccountModel) Create() error {
	if entityData, ok := this.EntityData.(*AccountModelData); ok {
		entityData.CreateTime = utils.GetNowTimeFormat()
		entityData.LoginTime = utils.GetNowTimeFormat()
		err := this.CreateDataToDB()
		entityData.Id = this.EntityDBID
		if err != nil {
			return err
		}
		logger.PhoenixLoggerIns().Debug("create account base success")
		return nil
	}
	return gerror.NewCodef(gcode.New(error2.PHOENIX_ERROR_ENTITY_PROPERTY_INVALID, "", nil),
		"avatar module model create fail, data is invalid")
}

func NewAccountModel(name string) interfaces.IEntityModel {
	m := &AccountModel{
		EntityModel: NewEntityModel(name),
	}
	m.SetEntityData(&AccountModelData{
		Id:          0,
		AccountName: "",
		AvatarId:    0,
		LoginTime:   "",
		CreateTime:  "",
	})
	m.SetTemplate(m.EntityData)
	return m
}
