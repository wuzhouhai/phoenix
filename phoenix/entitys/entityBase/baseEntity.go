package entityBase

import (
	"context"
	"phoenix/phoenix/app"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/entityManager"
	"phoenix/phoenix/managers/timerManager"
	"phoenix/phoenix/register"
	"time"
)

type EntityBase struct {
	EntityTypeId  dataType.PhoenixTypeEntityType // 实体类型ID
	EntityName    string                         // 实体名称
	Id            dataType.PhoenixTypeEntityId   // 实体ID
	DataBaseID    dataType.PhoenixTypeDataBaseId // 数据库ID
	EntityModel   interfaces.IEntityModel
	EntityModules map[string]interfaces.IEntityModule // 实体模块
	SyncTimerID   dataType.PhoenixTypeTimerID
	Service       interfaces.IService // 属于那个服务
}

func (this *EntityBase) Init() {
	this.Id = entityManager.PhoenixManagerEntity().GetEntityIncrement()
	this.InitModule()
	// 启动存储定时器
	this.SyncTimerID = timerManager.PhoenixManagerTimer().RegisterTimer(context.Background(),
		time.Duration(app.PhoenixIns().GetSyncInterval())*time.Second, this.OnTimerSyncToDB)

}

func (this *EntityBase) InitModule() {
	entityModulesRegister := register.GetEntityModules(this.EntityName)
	for name, _ := range entityModulesRegister {
		entityModule := register.GetEntityModule(this.EntityName, name, this.DataBaseID)
		this.EntityModules[name] = entityModule
	}
}

func (this *EntityBase) OnTimerSyncToDB(ctx context.Context) {
	logger.PhoenixLoggerIns().Debugf("entity %d sync to db", this.Id)
	this.SyncToDB()
}

func (this *EntityBase) SaveToDB() {
	if this.EntityModel != nil {
		_ = this.EntityModel.SaveEntityDataToDB()
	}
	// 存储模块数据
	for _, module := range this.EntityModules {
		_ = module.SavaData()
	}
}

func (this *EntityBase) SyncToDB() {
	if this.EntityModel != nil {
		_ = this.EntityModel.SyncEntityDataToDB()
	}
	// 存储模块数据
	for _, module := range this.EntityModules {
		_ = module.SyncToDB()
	}
}

func (this *EntityBase) GetModules() map[string]interfaces.IEntityModule {
	return this.EntityModules
}

func (this *EntityBase) GetModule(moduleName string) interfaces.IEntityModule {
	if this.EntityModules == nil || this.EntityModules[moduleName] == nil {
		return nil
	}
	return this.EntityModules[moduleName]
}

func (this *EntityBase) BindEntityModel(e interfaces.IEntityModel) {
	this.EntityModel = e
}

func (this *EntityBase) GetEntityModel() interfaces.IEntityModel {
	return this.EntityModel
}

func (this *EntityBase) GetModuleModel(moduleName string) interfaces.IEntityModuleModel {
	if this.EntityModules == nil || this.EntityModules[moduleName] == nil {
		return nil
	}
	return this.EntityModules[moduleName].GetModuleModel()
}

func (this *EntityBase) GetName() string {
	return this.EntityName
}

func (this *EntityBase) GetId() dataType.PhoenixTypeEntityId {
	return this.Id
}

func (this *EntityBase) FrameLogic() {

}

func (this *EntityBase) OnEnterWorld() {
	logger.PhoenixLoggerIns().Infof("entity %d on enter world", this.Id)
}

func (this *EntityBase) OnLeaveWorld() {
	logger.PhoenixLoggerIns().Infof("entity %d on leave world", this.Id)
}

func (this *EntityBase) Release() {

}

func (this *EntityBase) ResetObject() {
	this.DataBaseID = 0
	if this.EntityModel != nil {
		this.EntityModel.Reset()
	}
	for _, module := range this.EntityModules {
		module.Reset()
	}
	if this.SyncTimerID > 0 {
		_ = timerManager.PhoenixManagerTimer().CloseTimer(this.SyncTimerID)
	}
	this.SyncTimerID = 0
	this.Service = nil
}

func (this *EntityBase) SetService(service interfaces.IService) {
	this.Service = service
}

func NewEntityBase(entityName string, entityTypeId dataType.PhoenixTypeEntityType) EntityBase {
	return EntityBase{
		EntityTypeId:  entityTypeId,
		EntityName:    entityName,
		Id:            0,
		DataBaseID:    0,
		EntityModel:   register.GetModel(entityName),
		EntityModules: make(map[string]interfaces.IEntityModule),
		SyncTimerID:   0,
		Service:       nil,
	}
}
