// Package entityData -----------------------------
// @file      : entityData.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/4/12 17:22
// @desc      :
// -------------------------------------------
package entityData

import (
	"github.com/gogf/gf/v2/container/gmap"
	"phoenix/phoenix/utils"
)

type EntityData struct {
	updateTime  int64
	saveTime    int64
	ObjectDatas *gmap.StrAnyMap
}

func NewEntityData() *EntityData {
	eData := &EntityData{
		ObjectDatas: gmap.NewStrAnyMap(true),
	}
	return eData
}

func (this *EntityData) UpdateDate(key string, value interface{}) {
	this.updateTime = utils.GetNowTimestampSecond()
}
