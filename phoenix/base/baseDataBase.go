// Package base -----------------------------
// @file      : baseDataBase.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/11 17:59
// @desc      :
// -------------------------------------------
package base

import (
	"context"
	"database/sql"
	"github.com/gogf/gf/v2/container/garray"
	"github.com/gogf/gf/v2/container/gmap"
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
	"phoenix/phoenix/common/config"
	entityHelper2 "phoenix/phoenix/common/database/entityHelper"
)

type BaseDataBase struct {
	ctx               context.Context
	Options           config.DatabaseOption
	EntityData        *gmap.StrAnyMap
	ModuleShareFields *gmap.StrAnyMap
	EntityModule      *gmap.StrAnyMap
}

func NewBaseDateBase(ctx context.Context, option config.DatabaseOption) BaseDataBase {
	return BaseDataBase{
		ctx:               ctx,
		Options:           option,
		EntityData:        gmap.NewStrAnyMap(),
		ModuleShareFields: gmap.NewStrAnyMap(),
		EntityModule:      gmap.NewStrAnyMap(),
	}
}

func (this *BaseDataBase) SyncTable() error {
	//TODO implement me
	return nil
}

func (this *BaseDataBase) Init() {
	gdb.AddConfigNode(this.Options.GroupName, this.Options.ConfigNode)
}

func (this *BaseDataBase) Name() string {
	return this.Options.Name
}

func (this *BaseDataBase) GroupName() string {
	return this.Options.GroupName
}

func (this *BaseDataBase) GetDb() gdb.DB {
	return g.DB(this.Options.GroupName)
}

func (this *BaseDataBase) GetPrefix() string {
	return this.Options.Prefix
}

func (this *BaseDataBase) SaveDataWithMap(data gdb.Map, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	return this.GetDb().Model(tableNameOrStruct...).Data(data).Save()
}

func (this *BaseDataBase) SaveDataWithStruct(data interface{}, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	return this.GetDb().Model(tableNameOrStruct...).Data(data).Save()
}

func (this *BaseDataBase) SaveMultiDataWithListMap(data gdb.List, batch int, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	if batch > 0 {
		return this.GetDb().Model(tableNameOrStruct...).Data(data).Batch(batch).Save()
	}
	return this.GetDb().Model(tableNameOrStruct...).Data(data).Save()
}

func (this *BaseDataBase) InsertDataWithMap(data gdb.Map, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	return this.GetDb().Model(tableNameOrStruct...).Data(data).Insert()
}

func (this *BaseDataBase) InsertDataWithStruct(data interface{}, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	return this.GetDb().Model(tableNameOrStruct...).Data(data).Insert()
}

func (this *BaseDataBase) InsertDataWithListMap(data gdb.List, batch int, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	if batch > 0 {
		return this.GetDb().Model(tableNameOrStruct...).Data(data).Batch(batch).Insert()
	}
	return this.GetDb().Model(tableNameOrStruct...).Data(data).Insert()
}

func (this *BaseDataBase) InsertDataWithMapAndGetId(data gdb.Map, tableNameOrStruct ...interface{}) (lastInsertId int64, err error) {
	return this.GetDb().Model(tableNameOrStruct...).Data(data).InsertAndGetId()
}

func (this *BaseDataBase) InsertDataWithStructAndGetId(data interface{}, tableNameOrStruct ...interface{}) (lastInsertId int64, err error) {
	return this.GetDb().Model(tableNameOrStruct...).Data(data).InsertAndGetId()
}

func (this *BaseDataBase) ReplaceDataWithMap(data gdb.Map, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	return this.GetDb().Model(tableNameOrStruct...).Data(data).Replace()
}

func (this *BaseDataBase) ReplaceDataWithStruct(data interface{}, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	return this.GetDb().Model(tableNameOrStruct...).Data(data).Replace()
}

func (this *BaseDataBase) ReplaceDataWithListMap(data gdb.List, batch int, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	if batch > 0 {
		return this.GetDb().Model(tableNameOrStruct...).Data(data).Batch(batch).Replace()
	}
	return this.GetDb().Model(tableNameOrStruct...).Data(data).Replace()
}

func (this *BaseDataBase) UpdateEntityData(entityName string, entityData *entityHelper2.DBEntity) {
	this.EntityData.Set(entityName, entityData)
}

func (this *BaseDataBase) GetEntityData(entityName string) *entityHelper2.DBEntity {
	return this.EntityData.Get(entityName).(*entityHelper2.DBEntity)
}

func (this *BaseDataBase) CheckEntityIsExist(entityName string) bool {
	return this.EntityData.Contains(entityName)
}

func (this *BaseDataBase) GetEntityDatas() *gmap.StrAnyMap {
	return this.EntityData
}

func (this *BaseDataBase) AppendModuleShareField(shareField *entityHelper2.DBEntityField) {
	this.ModuleShareFields.Set(shareField.FieldName, shareField)
}

func (this *BaseDataBase) GetModuleShareFields() []interface{} {
	return this.ModuleShareFields.Values()
}

func (this *BaseDataBase) CheckModuleShareField(fieldName string) bool {
	return this.ModuleShareFields.Contains(fieldName)
}

func (this *BaseDataBase) AppendModuleName(entityName string, moduleName string) {
	if !this.EntityModule.Contains(entityName) {
		this.EntityModule.Set(entityName, garray.New())
	}
	this.EntityModule.Get(entityName).(*garray.Array).Append(moduleName)
}

func (this *BaseDataBase) CheckModuleExist(entityName string, moduleName string) bool {
	if !this.EntityModule.Contains(entityName) {
		return false
	}
	return this.EntityModule.Get(entityName).(*garray.Array).Contains(moduleName)
}
