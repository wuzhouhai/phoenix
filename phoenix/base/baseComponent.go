// Package base Package components -----------------------------
// @file      : baseComponent.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2023/12/21 18:13
// @desc      :
// -------------------------------------------
package base

import (
	"context"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/utils"
)

type BaseComponent struct {
	Ctx     context.Context
	Options config.ComponentOptions
	Id      string
	Service interfaces.IService
}

func NewBaseComponent(r interfaces.IService, options config.ComponentOptions, ctx context.Context) BaseComponent {
	return BaseComponent{
		Ctx:     ctx,
		Options: options,
		Id:      utils.GenerateComponentId(),
		Service: r,
	}
}

func (this *BaseComponent) GetId() string {
	return this.Id
}

func (this *BaseComponent) GetName() string {
	return this.Options.Name
}

func (this *BaseComponent) SendMsg(receiver interface{}, msg interface{}) error {
	return nil
}

func (this *BaseComponent) ReceiveMsg(msg interface{}) error {
	return nil
}

func (this *BaseComponent) DiscoverService(serviceType string, serviceId string, serviceSubscribe string) {
	panic("implement me")
}

func (this *BaseComponent) OfflineService(serviceType string, serviceId string) {
	panic("implement me")
}

func (this *BaseComponent) ServiceOnHello(serviceType string, serviceId string) {
	panic("implement me")
}

func (this *BaseComponent) SelectGameService(conId dataType.PhoenixTypeConnectId) string {
	panic("implement me")
}

func (this *BaseComponent) SendNatsPacketWithCheck(serviceType string, serviceId string, p interfaces.IPacket) error {
	panic("implement me")
}
