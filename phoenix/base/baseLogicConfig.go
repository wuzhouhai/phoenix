// Package base -----------------------------
// @file      : baseLogicConfig.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/5 15:30
// @desc      :
// -------------------------------------------
package base

import (
	"context"
	"errors"
	"github.com/gogf/gf/v2/container/gmap"
	"github.com/gogf/gf/v2/util/gconv"
	"phoenix/phoenix/managers/resManager"
	"phoenix/phoenix/utils"
)

type BaseLogicConfig struct {
	ctx        context.Context
	configName string
	configFile string
	configData *gmap.IntAnyMap // 放配置表中的数据 主键Key => value
	extendData *gmap.StrAnyMap // 放一些分析之后的数据(自定义) 扩展
}

func NewBaseLogicConfig(configName string, configFile string, ctx context.Context) BaseLogicConfig {
	return BaseLogicConfig{
		ctx:        ctx,
		configName: configName,
		configFile: configFile,
		configData: gmap.NewIntAnyMap(),
		extendData: gmap.NewStrAnyMap(),
	}
}

func (this *BaseLogicConfig) GetName() string {
	return this.configName
}

func (this *BaseLogicConfig) GetFileName() string {
	return this.configFile
}

func (this *BaseLogicConfig) LoadConfigFile() ([]byte, error) {
	if this.configFile == "" {
		return nil, errors.New("config file is empty")
	}
	filePath := resManager.PhoenixManagerRes().FindResPath(this.configFile, true)
	return utils.ReadFile(filePath)
}

func (this *BaseLogicConfig) GetConfigs() *gmap.IntAnyMap {
	return this.configData
}

func (this *BaseLogicConfig) SetConfigItem(itemKey int32, itemValue interface{}) {
	this.configData.Set(int(itemKey), itemValue)
}

func (this *BaseLogicConfig) GetConfigItem(itemKey int32) interface{} {
	return this.configData.Get(int(itemKey))
}

func (this *BaseLogicConfig) SetConfigExtendData(itemKey int32, itemValue interface{}) {
	extendKey := "extend_" + gconv.String(itemKey)
	this.SetExtendData(extendKey, itemValue)
}

func (this *BaseLogicConfig) GetConfigExtendData(itemKey int32) interface{} {
	extendKey := "extend_" + gconv.String(itemKey)
	return this.GetExtendData(extendKey)
}

func (this *BaseLogicConfig) CheckConfigExtendData(itemKey int32) bool {
	extendKey := "extend_" + gconv.String(itemKey)
	return this.GetExtendData(extendKey) != nil
}

func (this *BaseLogicConfig) SetExtendData(extendKey string, itemValue interface{}) {
	this.extendData.Set(extendKey, itemValue)
}

func (this *BaseLogicConfig) GetExtendData(extendKey string) interface{} {
	return this.extendData.Get(extendKey)
}
