// Package base Package routines -----------------------------
// @file      : baseRoutine.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2023/12/21 19:12
// @desc      :
// -------------------------------------------
package base

import (
	"context"
	"fmt"
	"github.com/gogf/gf/v2/container/gmap"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/defines/opcode"
	"phoenix/phoenix/defines/services"
	bundle2 "phoenix/phoenix/entitys/instance/bundle"
	"phoenix/phoenix/entitys/instance/message"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/objectsManager"
	"phoenix/phoenix/network/protocol/nrpc/golang/natsRpc"
	"phoenix/phoenix/network/protocol/nrpc/golang/service"
	"phoenix/phoenix/network/reader"
	"phoenix/phoenix/register"
	"phoenix/phoenix/utils"
)

type BaseService struct {
	Id            string
	Ctx           context.Context
	Options       config.ServiceOptions
	Components    map[string]interfaces.IComponent
	NatsComponent interfaces.INatsComponent
	Managers      *gmap.IntAnyMap
	PacketReader  *reader.PacketReader
	ChStop        chan bool
	LuaHelper     interfaces.ILua
}

func NewBaseService(options config.ServiceOptions, ctx context.Context) BaseService {
	return BaseService{
		Id:            utils.GenerateRoutineId(),
		Ctx:           ctx,
		Options:       options,
		Components:    make(map[string]interfaces.IComponent),
		ChStop:        make(chan bool),
		NatsComponent: nil,
		Managers:      gmap.NewIntAnyMap(true),
		PacketReader:  reader.NewPacketReader(),
		LuaHelper:     nil,
	}
}

func (this *BaseService) InitManager() {

}

func (this *BaseService) GetId() string {
	return this.Id
}

func (this *BaseService) GetName() string {
	return this.Options.Name
}

func (this *BaseService) GetType() string {
	return this.Options.Type
}

func (this *BaseService) CreateComponents() {
	for _, v := range this.Options.Components {
		componentCreateFunc := register.GetComponentsCreateFunc(v.Name)
		if componentCreateFunc == nil {
			logger.PhoenixLoggerIns().Error(fmt.Sprintf("component %s not found", v.Name))
			continue
		}
		component, err := componentCreateFunc(this, v, this.Ctx)
		if err != nil {
			logger.PhoenixLoggerIns().Error(fmt.Sprintf("component %s create fail %s", v.Name, err.Error()))
		} else {
			this.Components[component.GetId()] = component
		}
	}
}

func (this *BaseService) OnX2Gate2XRpcMessage(d interface{}) error {
	panic("implement me")
}

func (this *BaseService) Start() error {
	//TODO implement me
	logger.PhoenixLoggerIns().Debugf("%s ,%s Start", this.Id, this.GetName())
	for _, v := range this.Components {
		err := v.Start()
		if err != nil {
			return err
		}
	}
	return nil
}

func (this *BaseService) SetLua(l interfaces.ILua) {
	this.LuaHelper = l
}

func (this *BaseService) Stop() {
	//TODO implement me
	for _, v := range this.Components {
		v.Stop()
	}
	if this.LuaHelper != nil {
		this.LuaHelper.Close()
	}
	this.ChStop <- true
}

func (this *BaseService) Init() error {
	logger.PhoenixLoggerIns().Debug(fmt.Sprintf("Service %s Init", this.GetName()))
	for _, v := range this.Components {
		err := v.Init()
		if err != nil {
			return err
		}
	}
	return nil
}

func (this *BaseService) GetNatsComponent() interfaces.INatsComponent {
	return this.NatsComponent
}

func (this *BaseService) AddComponent(comId string, component interfaces.IComponent) {
	logger.PhoenixLoggerIns().Debugf("%s AddComponent %s, %s", this.Id, comId, component.GetName())
	this.Components[comId] = component
	if component.GetName() == "natsComponent" {
		this.NatsComponent = component.(interfaces.INatsComponent)
	}
}

func (this *BaseService) DiscoverService(serviceType string, serviceId string, serviceSubscribe string) {
	if this.NatsComponent != nil {
		this.NatsComponent.DiscoverService(serviceType, serviceId, serviceSubscribe)
	}
}

func (this *BaseService) OfflineService(serviceType string, serviceId string) {
	if this.NatsComponent != nil {
		this.NatsComponent.OfflineService(serviceType, serviceId)
	}
}

func (this *BaseService) SelectGameService(accountId dataType.PhoenixTypeDataBaseId) string {
	if this.NatsComponent != nil {
		serviceId, e := this.NatsComponent.SelectGameService(accountId)
		if e != nil {
			return ""
		}
		return serviceId
	}
	return ""
}

func (this *BaseService) SendNatsPacket(serviceType string, serviceId string, p interfaces.IPacket) error {
	if this.NatsComponent != nil {
		return this.NatsComponent.SendNatsPacketWithCheck(serviceType, serviceId, p)
	}
	return nil
}

func (this *BaseService) SendNatsPacketWithType(serviceType string, p interfaces.IPacket) error {
	if this.NatsComponent != nil {
		serviceId, err := this.NatsComponent.SelectServiceRandom(serviceType)
		if err != nil {
			return err
		}
		return this.SendNatsPacket(serviceType, serviceId, p)
	}
	return nil

}

func (this *BaseService) CreateX2GateRpcClientPacket(serviceId string, p interfaces.IPacket,
	connectorId dataType.PhoenixTypeConnectId) interfaces.INatsPacket {
	packet := message.NewRpcPacket(opcode.Opcode_Nats_X2Gate_ClientMessage)
	if packet != nil {
		if this.NatsComponent != nil {
			msg := packet.GetMsg().(*service.X2GateRpcClientMessage)
			msg.Channel = &natsRpc.RpcBase{
				FromSubscribe:   "",
				FromServiceType: "",
				FromServiceId:   "",
				ToSubscribe:     "",
				ConnectorId:     0,
			}
			err := this.NatsComponent.CreateRpcBaseWithServiceId(services.PHOENIX_SERVICE_GATE,
				serviceId, msg.Channel, connectorId)
			bundle := bundle2.NewBundle()
			bundle.AppendPacket(p)
			defer objectsManager.PhoenixManagerObjects().DestroyObject(bundle)
			packet.GetMsg().(*service.X2GateRpcClientMessage).Data = bundle.GetData()
			if err != nil {
				return nil
			}
		}
	}
	return packet
}

func (this *BaseService) CreateGate2XRpcClientPacket(serviceType string, serviceId string, p interfaces.IPacket,
	connectorId dataType.PhoenixTypeConnectId) interfaces.INatsPacket {
	packet := message.NewRpcPacket(opcode.Opcode_Nats_Gate2X_ClientMessage)
	if packet != nil {
		if this.NatsComponent != nil {
			msg := packet.GetMsg().(*service.Gate2XRpcClientMessage)
			msg.Channel = &natsRpc.RpcBase{
				FromSubscribe:   "",
				FromServiceType: "",
				FromServiceId:   "",
				ToSubscribe:     "",
				ConnectorId:     0,
			}
			err := this.NatsComponent.CreateRpcBaseWithServiceId(serviceType, serviceId, msg.Channel, connectorId)
			bundle := bundle2.NewBundle()
			bundle.AppendPacket(p)
			defer objectsManager.PhoenixManagerObjects().DestroyObject(bundle)
			packet.GetMsg().(*service.Gate2XRpcClientMessage).Data = bundle.GetData()
			if err != nil {
				return nil
			}
		}
	}
	return packet
}

func (this *BaseService) Process() {
	panic("implement me")
}
