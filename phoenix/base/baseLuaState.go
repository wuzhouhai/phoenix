// Package base -----------------------------
// @file      : baseLuaState.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/10/22 11:04
// @desc      :
// -------------------------------------------
package base

import (
	lua "github.com/yuin/gopher-lua"
	"phoenix/phoenix/scripts/luaModule"
)

type BaseLuaState struct {
	luaState *lua.LState
}

func NewBaseLuaState() BaseLuaState {
	return BaseLuaState{
		luaState: lua.NewState(),
	}
}

func (this *BaseLuaState) GetLuaState() *lua.LState {
	return this.luaState
}

func (this *BaseLuaState) Close() {
	this.luaState.Close()
	this.luaState = nil
}

func (this *BaseLuaState) RegisterModules() {
	this.luaState.PreloadModule("phoenixLog", luaModule.LuaModuleLoadLog)
}
