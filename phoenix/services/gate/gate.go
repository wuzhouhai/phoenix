// Package gate Package routines -----------------------------
// @file      : gate.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2023/12/20 18:59
// @desc      :
// -------------------------------------------
package gate

import (
	"context"
	"phoenix/phoenix/base"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/connectManager"
	"phoenix/phoenix/network/handles/funcHandle"
	"phoenix/phoenix/network/protocol/nrpc/golang/service"
	"phoenix/phoenix/register"
)

type Gate struct {
	base.BaseService
}

func (this *Gate) Start() error {
	err := this.BaseService.Start()
	if err != nil {
		return err
	}
	// 测试lua脚本
	//l := this.LuaHelper.GetLuaState()
	//err = l.CallByParam(lua.P{
	//	Fn:      l.GetGlobal("Test"),
	//	NRet:    1,
	//	Protect: true,
	//})
	//if err != nil {
	//	logger.PhoenixLoggerIns().Errorf("gate Init lua call Test error: %s", err.Error())
	//}
	//ret := l.Get(-1)
	//l.Pop(1)
	//res, ok := ret.(lua.LNumber)
	//if ok {
	//	logger.PhoenixLoggerIns().Infof("gate Init lua call Test success re: %d", res)
	//} else {
	//	logger.PhoenixLoggerIns().Errorf("gate Init lua call Test error")
	//}
	return nil
}

func (this *Gate) OnX2GateRpcClientMessage(d interface{}) {
	if msg, ok := d.(*service.X2GateRpcClientMessage); ok {
		e, p := this.PacketReader.Reader(msg.Data)
		if e != nil {
			logger.PhoenixLoggerIns().Errorf("gate OnX2GateRpcClientMessage read packet error: %s",
				e.Error())
			return
		}
		if msg.Channel.ConnectorId > 0 {
			i := connectManager.PhoenixManagerConnect().GetConnect(msg.Channel.ConnectorId)
			handler := register.GetSocketHandler(p.Opcode)
			if handler == nil {
				logger.PhoenixLoggerIns().Errorf("gate OnX2GateRpcClientMessage handler is nil opcode: %d",
					p.Opcode)
				return
			}
			if handler.PreSendHandleName != "" {
				funcHandle.HandleProcessNatsRpc(i, p.Msg, handler.PreSendHandleName, msg.Channel)
			}
			connectManager.PhoenixManagerConnect().AddNatsToClientPackets(msg.Channel.ConnectorId, p)
		} else {
			logger.PhoenixLoggerIns().Errorf("gate OnX2GateRpcClientMessage connectorId is zero msgCode :%s",
				p.Opcode)
		}
	}
}

func (this *Gate) Process() {
	select {
	case <-this.ChStop:
		break
	}
}

func NewGate(options config.ServiceOptions, ctx context.Context) (interfaces.IService, error) {
	gate := &Gate{
		BaseService: base.NewBaseService(options, ctx),
	}
	return gate, nil
}
