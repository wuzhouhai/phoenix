// Package login -----------------------------
// @file      : login.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/19 14:58
// @desc      :
// -------------------------------------------
package login

import (
	"context"
	"fmt"
	"phoenix/phoenix/base"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/entityName"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/objectsManager"
	"phoenix/phoenix/network/handles/funcHandle"
	"phoenix/phoenix/network/protocol/nrpc/golang/service"
	"phoenix/phoenix/register"
)

type Login struct {
	base.BaseService
}

func (this *Login) OnGate2XRpcClientMessage(d interface{}) error {
	if msg, ok := d.(*service.Gate2XRpcClientMessage); ok {
		err, p := this.PacketReader.Reader(msg.Data)
		if err != nil {
			return err
		}
		handler := register.GetSocketHandler(p.Opcode)
		if handler == nil {
			logger.PhoenixLoggerIns().Error(fmt.Sprintf("没有找到对应的消息处理函数，opcode: %d", p.Opcode))
		}
		ac, _ := objectsManager.PhoenixManagerObjects().CreateObject(entityName.Phoenix_EntityName_Account)
		acI := ac.(interfaces.IAgentEntity)
		acI.SetConnectId(msg.Channel.ConnectorId)
		acI.SetService(this)
		funcHandle.HandleProcessNatsRpc(ac, p.Msg, handler.AgentHandleName, msg.Channel)
		ac.Release()
	}
	return nil
}

func (this *Login) Process() {
	select {
	case <-this.ChStop:
		break
	}
}
func NewLogin(options config.ServiceOptions, ctx context.Context) (interfaces.IService, error) {
	s := &Login{
		BaseService: base.NewBaseService(options, ctx),
	}
	return s, nil
}
