// Package game -----------------------------
// @file      : game.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/5 16:43
// @desc      :
// -------------------------------------------
package game

import (
	"context"
	"phoenix/apps/examples/phoenix_app_dwys/protocol/golang/protos"
	"phoenix/phoenix/app"
	"phoenix/phoenix/base"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/defines/dataType"
	error2 "phoenix/phoenix/defines/error"
	"phoenix/phoenix/defines/opcode"
	"phoenix/phoenix/defines/services"
	"phoenix/phoenix/entitys/instance/message"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/objectsManager"
	"phoenix/phoenix/managers/playerManager"
	"phoenix/phoenix/network/protocol/nrpc/golang/service"
	"phoenix/phoenix/objects"
)

type Game struct {
	base.BaseService
}

func (this *Game) Init() error {
	err := this.BaseService.Init()
	if err != nil {
		return err
	}
	return nil
}

func (this *Game) OnGate2XRpcClientMessage(d interface{}) error {
	if msg, ok := d.(*service.Gate2XRpcClientMessage); ok {
		err, p := this.PacketReader.Reader(msg.Data)
		if err != nil {
			return err
		}
		playerManager.PhoenixManagerPlayer().AddGamePacket(msg.Channel.ConnectorId, msg.Channel.FromServiceId, p)
	}
	return nil
}

func (this *Game) OnLogin2GameClientLogin(d interface{}) error {
	if msg, ok := d.(*service.Login2GameClientLogin); ok {
		iPlayer := playerManager.PhoenixManagerPlayer().GetPlayer(dataType.PhoenixTypeDataBaseId(msg.AvatarId))
		if iPlayer != nil {
			// 处理重复登录问题
			allow := app.PhoenixIns().AllowReplace()
			if !allow {
				packet := message.NewPacket(opcode.Opcode_SC_LOGIN)
				if packet != nil {
					sendMsg := packet.GetMsg().(*protos.S2CLogin)
					sendMsg.Base = &protos.S2CBase{
						Code: error2.PHOENIX_ERROR_ACCOUNT_ONLINE,
						Msg:  "账号已登录",
					}
					natsPacket := this.CreateX2GateRpcClientPacket(msg.GateServiceId, packet,
						msg.Channel.ConnectorId)
					_ = this.SendNatsPacket(services.PHOENIX_SERVICE_GATE, msg.GateServiceId, natsPacket)
					defer objectsManager.PhoenixManagerObjects().DestroyObject(packet)
					defer objectsManager.PhoenixManagerObjects().DestroyObject(natsPacket)
				}
			} else {
				// 处理允许挤号
				playerManager.PhoenixManagerPlayer().BeKick(dataType.PhoenixTypeDataBaseId(msg.AvatarId),
					"账号在别处登录")
				// 绑定新的连接信息
				iPlayer.SetConnectorId(msg.Channel.ConnectorId)
				iPlayer.SetGateServiceId(msg.GateServiceId)
				iPlayer.GetAgentEntity().SetConnectId(msg.Channel.ConnectorId)
				iPlayer.GetAgentEntity().SetGateService(msg.GateServiceId)
				playerManager.PhoenixManagerPlayer().ChangeConnector(iPlayer)
				err := iPlayer.GetAgentEntity().ReAvatarLogin(dataType.PhoenixTypeDataBaseId(msg.AvatarId), msg.AvatarName,
					msg.AccountName)
				if err != nil {
					return err
				}
			}
		} else {
			iPlayer = objects.NewPlayer(dataType.PhoenixTypeDataBaseId(msg.AvatarId), this, msg.GateServiceId,
				msg.Channel.ConnectorId)
			err := iPlayer.GetAgentEntity().OnAvatarLogin(dataType.PhoenixTypeDataBaseId(msg.AvatarId), msg.AvatarName,
				msg.AccountName)
			if err != nil {
				return err
			}
			playerManager.PhoenixManagerPlayer().AddPlayer(iPlayer)
			iPlayer.StartWork()
		}
	}
	return nil
}

func (this *Game) OnGate2GameDisconnect(d interface{}) error {
	if msg, ok := d.(*service.Gate2GameDisconnect); ok {
		playerManager.PhoenixManagerPlayer().DisConnect(msg.Channel.ConnectorId, msg.Channel.FromServiceId)
	}
	return nil
}

func (this *Game) Process() {
	select {
	case <-this.ChStop:
		break
	}
}

func NewGame(options config.ServiceOptions, ctx context.Context) (interfaces.IService, error) {
	Game := &Game{
		BaseService: base.NewBaseService(options, ctx),
	}
	return Game, nil
}
