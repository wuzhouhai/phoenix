// Package web -----------------------------
// @file      : web.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/3 18:07
// @desc      :
// -------------------------------------------
package web

import (
	"context"
	"phoenix/phoenix/base"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/interfaces"
)

type Web struct {
	base.BaseService
}

func (this *Web) Process() {
	select {
	case <-this.ChStop:
		break
	}
}
func NewWeb(options config.ServiceOptions, ctx context.Context) (interfaces.IService, error) {
	routine := &Web{
		BaseService: base.NewBaseService(options, ctx),
	}
	return routine, nil
}
