// Package register -----------------------------
// @file      : entity_module_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/7/4 14:15
// @desc      : 实体模块注册
// -------------------------------------------
package register

import (
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/interfaces"
)

var modelModuleRegister = make(map[string]map[string]func(name string, entityDBID dataType.PhoenixTypeDataBaseId) interfaces.IEntityModuleModel)

func RegisterModelModule(entityName string, name string, createFunc func(name string, entityDBID dataType.PhoenixTypeDataBaseId) interfaces.IEntityModuleModel) {
	if _, ok := modelModuleRegister[entityName]; !ok {
		modelModuleRegister[entityName] = make(map[string]func(name string, entityDBID dataType.PhoenixTypeDataBaseId) interfaces.IEntityModuleModel)
	}
	modelModuleRegister[entityName][name] = createFunc
}

func GetModelModule(entityName string, name string, entityDBID dataType.PhoenixTypeDataBaseId) interfaces.IEntityModuleModel {
	if _, ok := modelModuleRegister[entityName]; !ok {
		return nil
	}
	if _, ok := modelModuleRegister[entityName][name]; !ok {
		return nil
	}
	return modelModuleRegister[entityName][name](name, entityDBID)
}
