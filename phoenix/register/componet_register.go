// Package register Package componets -----------------------------
// @file      : componet_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2023/12/21 16:41
// @desc      :
// -------------------------------------------
package register

import (
	"context"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/interfaces"
)

var components = make(map[string]func(r interfaces.IService, options config.ComponentOptions, ctx context.Context) (interfaces.IComponent, error))

func RegisterComponent(name string, f func(r interfaces.IService, options config.ComponentOptions, ctx context.Context) (interfaces.IComponent, error)) {
	components[name] = f
}

func GetComponentsCreateFunc(name string) func(r interfaces.IService, options config.ComponentOptions, ctx context.Context) (interfaces.IComponent, error) {
	return components[name]
}
