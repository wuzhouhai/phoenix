// Package registerInit -----------------------------
// @file      : codec_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/22 14:10
// @desc      :
// -------------------------------------------
package register

import (
	"phoenix/phoenix/defines/opcode"
	"reflect"
)

var codecRegister = make(map[opcode.OpCodeType]reflect.Type)

func RegisterCodec(name opcode.OpCodeType, codecType reflect.Type) {
	codecRegister[name] = codecType
}

func GetCodec(name opcode.OpCodeType) reflect.Type {
	return codecRegister[name]
}
