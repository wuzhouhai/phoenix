// Package register -----------------------------
// @file      : moduleLogic_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/8 19:06
// @desc      : 系统模块逻辑注册
// -------------------------------------------
package register

import "phoenix/phoenix/interfaces"

var moduleLogicRegister = make(map[string]map[string]func(logicName string) interfaces.IModuleLogic)

func RegisterModuleLogic(moduleName string, logicName string, f func(logicName string) interfaces.IModuleLogic) {
	if _, ok := moduleLogicRegister[moduleName]; !ok {
		moduleLogicRegister[moduleName] = make(map[string]func(logicName string) interfaces.IModuleLogic)
	}
	moduleLogicRegister[moduleName][logicName] = f
}

func GetModuleLogics(moduleName string) map[string]func(logicName string) interfaces.IModuleLogic {
	if _, ok := moduleLogicRegister[moduleName]; !ok {
		return nil
	}
	return moduleLogicRegister[moduleName]
}
