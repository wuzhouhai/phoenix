// Package register -----------------------------
// @file      : protobuf_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/16 11:40
// @desc      : protobuf注册
// -------------------------------------------
package register

import (
	"phoenix/phoenix/defines/dataType"
	"reflect"
)

var protobufRegister = make(map[dataType.PhoenixTypeOpCode]reflect.Type)

func RegisterProtobuf(opcode dataType.PhoenixTypeOpCode, t reflect.Type) {
	protobufRegister[opcode] = t
}

func GetProtobuf(opcode dataType.PhoenixTypeOpCode) reflect.Type {
	return protobufRegister[opcode]
}
