// Package register -----------------------------
// @file      : module_entity_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/7/4 19:03
// @desc      :
// -------------------------------------------
package register

import (
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/interfaces"
)

var entityModuleRegister = make(map[string]map[string]func(entityDBID dataType.PhoenixTypeDataBaseId) interfaces.IEntityModule)

func RegisterEntityModule(entityName string, name string, createFunc func(entityDBID dataType.PhoenixTypeDataBaseId) interfaces.IEntityModule) {
	if _, ok := entityModuleRegister[entityName]; !ok {
		entityModuleRegister[entityName] = make(map[string]func(entityDBID dataType.PhoenixTypeDataBaseId) interfaces.IEntityModule)
	}
	entityModuleRegister[entityName][name] = createFunc
}

func GetEntityModule(entityName string, name string, entityDBID dataType.PhoenixTypeDataBaseId) interfaces.IEntityModule {
	if _, ok := entityModuleRegister[entityName]; !ok {
		return nil
	}
	if _, ok := entityModuleRegister[entityName][name]; !ok {
		return nil
	}
	return entityModuleRegister[entityName][name](entityDBID)
}

func GetEntityModules(entityName string) map[string]func(entityDBID dataType.PhoenixTypeDataBaseId) interfaces.IEntityModule {
	if _, ok := entityModuleRegister[entityName]; !ok {
		return nil
	}
	return entityModuleRegister[entityName]
}
