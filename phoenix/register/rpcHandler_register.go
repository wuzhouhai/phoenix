// Package register -----------------------------
// @file      : rpcHandler_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/6 16:14
// @desc      :
// -------------------------------------------
package register

import (
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/defines/opcode"
	"phoenix/phoenix/network/handles/rpcHandles"
	"reflect"
)

var natsRpcHandlers = make(map[dataType.PhoenixTypeOpCode]*rpcHandles.RpcHandles)

func RegisterNatsRpcHandler(opcode dataType.PhoenixTypeOpCode, opCodecType opcode.OpCodeType, msgProtoType reflect.Type,
	agentHandleName string) {
	natsRpcHandlers[opcode] = rpcHandles.NewRpcHandles(opcode, opCodecType, msgProtoType, agentHandleName)
}

func GetNatsRpcHandler(opcode dataType.PhoenixTypeOpCode) *rpcHandles.RpcHandles {
	return natsRpcHandlers[opcode]
}
