// Package register -----------------------------
// @file      : database_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/11 19:05
// @desc      :
// -------------------------------------------
package register

import (
	"context"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/interfaces"
)

var databaseRegister = make(
	map[string]func(ctx context.Context, option config.DatabaseOption) (
		interfaces.IDataBase, error))

func RegisterDatabase(name string, createFunc func(ctx context.Context, option config.DatabaseOption) (
	interfaces.IDataBase, error)) {
	databaseRegister[name] = createFunc
}

func GetDatabaseCreateFunc(name string) func(ctx context.Context, option config.DatabaseOption) (
	interfaces.IDataBase, error) {
	return databaseRegister[name]
}
