// Package register -----------------------------
// @file      : objects_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/27 10:42
// @desc      :
// -------------------------------------------
package register

type ObjectPoolFunc struct {
	CreateFunc  func() (interface{}, error)
	DestroyFunc func(i interface{})
}

var objectsRegister = make(map[string]ObjectPoolFunc)

func RegisterObjects(name string, createFunc func() (interface{}, error), destroyFunc func(i interface{})) {
	objectsRegister[name] = ObjectPoolFunc{
		CreateFunc:  createFunc,
		DestroyFunc: destroyFunc,
	}
}

func GetObjectsRegister() map[string]ObjectPoolFunc {
	return objectsRegister
}
