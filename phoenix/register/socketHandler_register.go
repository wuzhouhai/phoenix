// Package register -----------------------------
// @file      : webSocketHandler_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/18 18:36
// @desc      :
// -------------------------------------------
package register

import (
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/defines/opcode"
	"phoenix/phoenix/network/handles/socketHandles"
	"reflect"
)

var socketHandlers = make(map[dataType.PhoenixTypeOpCode]*socketHandles.SocketHandles)

func RegisterSocketHandler(opcode dataType.PhoenixTypeOpCode, opCodecType opcode.OpCodeType, msgProtoType reflect.Type,
	agentHandleName string, preSendHandleName string) {
	socketHandlers[opcode] = socketHandles.NewWebSocketHandles(opcode, opCodecType, msgProtoType, agentHandleName,
		preSendHandleName)
}

func GetSocketHandler(opcode dataType.PhoenixTypeOpCode) *socketHandles.SocketHandles {
	return socketHandlers[opcode]
}
