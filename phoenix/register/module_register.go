// Package register -----------------------------
// @file      : module_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/8 18:37
// @desc      : 系统模块注册
// -------------------------------------------
package register

import (
	"context"
	"phoenix/phoenix/interfaces"
)

var modules = make(map[string]func(ctx context.Context, moduleName string) (interfaces.IModule, error))

func RegisterModule(moduleName string, f func(ctx context.Context, moduleName string) (interfaces.IModule, error)) {
	modules[moduleName] = f
}

func GetRegisterModules() map[string]func(ctx context.Context, moduleName string) (interfaces.IModule, error) {
	return modules
}
