// Package register -----------------------------
// @file      : model_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/4/12 17:49
// @desc      : 实体模块数据类型注册
// -------------------------------------------
package register

import (
	"phoenix/phoenix/interfaces"
)

var modelRegister = make(map[string]func(name string) interfaces.IEntityModel)

func RegisterModel(name string, createFunc func(name string) interfaces.IEntityModel) {
	modelRegister[name] = createFunc
}

func GetModel(name string) interfaces.IEntityModel {
	if _, ok := modelRegister[name]; !ok {
		return nil
	}
	return modelRegister[name](name)
}
