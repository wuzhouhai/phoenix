// Package register -----------------------------
// @file      : logicConfig_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/5 14:16
// @desc      :
// -------------------------------------------
package register

import (
	"context"
	"phoenix/phoenix/interfaces"
)

var logicConfigs = make(map[string]func(configName string, configFile string, ctx context.Context) (interfaces.ILogicConfig, error))

func RegisterLogicConfig(name string, f func(configName string, configFile string, ctx context.Context) (interfaces.ILogicConfig, error)) {
	logicConfigs[name] = f
}

func GetLogicConfigCreateFunc(name string) func(configName string, configFile string, ctx context.Context) (interfaces.ILogicConfig, error) {
	return logicConfigs[name]
}

func GetLogicConfigs() map[string]func(configName string, configFile string, ctx context.Context) (interfaces.ILogicConfig, error) {
	return logicConfigs
}
