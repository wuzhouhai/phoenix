// Package register Package routines -----------------------------
// @file      : routines_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2023/12/21 15:24
// @desc      :
// -------------------------------------------
package register

import (
	"context"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/interfaces"
)

var routines = make(map[string]func(options config.ServiceOptions, ctx context.Context) (interfaces.IService, error))

func Register(name string, enter func(options config.ServiceOptions, ctx context.Context) (interfaces.IService, error)) {
	routines[name] = enter
}

func GetRoutineCreateFunction(name string) func(options config.ServiceOptions, ctx context.Context) (interfaces.IService, error) {
	return routines[name]
}
