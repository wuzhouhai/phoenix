// Package register -----------------------------
// @file      : webHandler_register.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/3 11:04
// @desc      :
// -------------------------------------------
package register

import "github.com/gogf/gf/v2/net/ghttp"

// 全局前置中间件
var globalPreMiddlewares = make([]ghttp.HandlerFunc, 0)

// 全局后置中间件
var globalPostMiddlewares = make([]ghttp.HandlerFunc, 0)

// 前置中间件,用组为单位 组名为key 方法数组为value
var preMiddlewares = make(map[string][]ghttp.HandlerFunc)

// 后置中间件,用组为单位 组名为key 方法数组为value
var postMiddlewares = make(map[string][]ghttp.HandlerFunc)

// 路由组 组名为key 创建对象方法为value
var routerGroups = make(map[string]func() interface{})

// 路由组序列
var routerGroupsOrder = make([]string, 0)

func RegisterLocalPreMiddleware(f ...ghttp.HandlerFunc) {
	globalPreMiddlewares = append(globalPreMiddlewares, f...)
}

func RegisterLocalPostMiddleware(f ...ghttp.HandlerFunc) {
	globalPostMiddlewares = append(globalPostMiddlewares, f...)
}

func RegisterPreMiddleware(group string, f ghttp.HandlerFunc) {
	preMiddlewares[group] = append(preMiddlewares[group], f)
}

func RegisterPostMiddleware(group string, f ghttp.HandlerFunc) {
	postMiddlewares[group] = append(postMiddlewares[group], f)
}

func RegisterRouterGroup(group string, f func() interface{}) {
	if _, ok := routerGroups[group]; ok {
		panic("group " + group + " has been registered")
	}
	routerGroupsOrder = append(routerGroupsOrder, group)
	routerGroups[group] = f
}

func GetLocalPreMiddlewares() []ghttp.HandlerFunc {
	return globalPreMiddlewares
}

func GetLocalPostMiddlewares() []ghttp.HandlerFunc {
	return globalPostMiddlewares
}

func GetPreMiddlewares(group string) []ghttp.HandlerFunc {
	return preMiddlewares[group]
}

func GetPostMiddlewares(group string) []ghttp.HandlerFunc {
	return postMiddlewares[group]
}

func GetRouters() []string {
	return routerGroupsOrder
}

func GetRouterGroupCreateFunc(group string) func() interface{} {
	return routerGroups[group]
}
