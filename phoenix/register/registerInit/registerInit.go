// Package registerInit -----------------------------
// @file      : registerInit.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2023/12/22 18:32
// @desc      :
// -------------------------------------------
package registerInit

import (
	"phoenix/apps/examples/phoenix_app_dwys/protocol/golang/protos"
	"phoenix/phoenix/common/database"
	"phoenix/phoenix/componets/proxyComponent"
	"phoenix/phoenix/componets/webComponent"
	"phoenix/phoenix/componets/websocketProxyComponent"
	"phoenix/phoenix/defines/component"
	"phoenix/phoenix/defines/entityName"
	"phoenix/phoenix/defines/opcode"
	"phoenix/phoenix/defines/services"
	"phoenix/phoenix/entitys/entityModel"
	"phoenix/phoenix/entitys/instance"
	"phoenix/phoenix/entitys/instance/bundle"
	"phoenix/phoenix/entitys/instance/message"
	"phoenix/phoenix/network/codec"
	"phoenix/phoenix/network/protocol/nrpc/golang/service"
	"phoenix/phoenix/network/web/middlewares"
	register2 "phoenix/phoenix/register"
	"phoenix/phoenix/services/game"
	"phoenix/phoenix/services/gate"
	"phoenix/phoenix/services/login"
	"phoenix/phoenix/services/web"
	"reflect"
)

func init() {
	initComponent()
	initRoutine()
	initProMiddleware()
	initDatabase()
	initCodec()
	initObjects()
	initModel()
	registerC2S()
	registerS2C()
	registerNatsRpc()
}

func initComponent() {
	// 注册组件,注册过的组件才可以在配置中使用
	register2.RegisterComponent(component.COMPONENT_NETWORK, proxyComponent.NewProxyComponent)
	register2.RegisterComponent(component.COMPONENT_WEB, webComponent.NewWebComponent)
	register2.RegisterComponent(component.COMPONENT_WEB_SOCKET, websocketProxyComponent.NewWebSocketProxyComponent)
}

func initRoutine() {
	// 注册服务
	register2.Register(services.PHOENIX_SERVICE_GATE, gate.NewGate)
	register2.Register(services.PHOENIX_SERVICE_WEB, web.NewWeb)
	register2.Register(services.PHOENIX_SERVICE_GAME, game.NewGame)
	register2.Register(services.PHOENIX_SERVICE_LOGIN, login.NewLogin)
}

func initProMiddleware() {
	// 注册web中间件前置
	register2.RegisterLocalPreMiddleware(middlewares.MiddlewareCORS)
	// 注册web中间件后置
	register2.RegisterLocalPostMiddleware(middlewares.MiddlewareLogger)
}

func initDatabase() {
	// 注册数据库
	register2.RegisterDatabase("mysql", database.NewMysqlDatabase)
}

func initCodec() {
	// 注册协议解析
	register2.RegisterCodec(opcode.OpCodeType_Protobuf, reflect.TypeOf(codec.ProtobufCodec{}))
}

func initObjects() {
	// 注册对象池
	register2.RegisterObjects(entityName.Phoenix_EntityName_Account, instance.AccountObjectCreate, instance.AccountObjectDestroy)
	register2.RegisterObjects(entityName.Phoenix_EntityName_Packet, message.PacketObjectCreate, message.PacketObjectDestroy)
	register2.RegisterObjects(entityName.Phoenix_EntityName_NatsRpcPacket, message.NatsRpcPacketObjectCreate, message.NatsRpcPacketObjectDestroy)
	register2.RegisterObjects(entityName.Phoenix_EntityName_Bundle, bundle.BundleObjectCreate, bundle.BundleObjectDestroy)
}

func initModel() {
	// 注册实体数据模型
	register2.RegisterModel(entityName.Phoenix_EntityName_Account, entityModel.NewAccountModel)
}

func registerC2S() {
	// 客户端到服务器消息
	register2.RegisterSocketHandler(opcode.Opcode_CS_LOGIN, opcode.OpCodeType_Protobuf,
		reflect.TypeOf(protos.C2SLogin{}), "OnLogin", "")
}

func registerS2C() {
	// 服务器到客户端消息
	register2.RegisterSocketHandler(opcode.Opcode_SC_LOGIN, opcode.OpCodeType_Protobuf,
		reflect.TypeOf(protos.S2CLogin{}), "", "OnPreS2CLogin")
	register2.RegisterSocketHandler(opcode.Opcode_SC_KICK, opcode.OpCodeType_Protobuf,
		reflect.TypeOf(protos.S2CKick{}), "", "OnPreS2CKick")
}

func registerNatsRpc() {
	register2.RegisterNatsRpcHandler(opcode.Opcode_Nats_Gate2X_ClientMessage, opcode.OpCodeType_Protobuf,
		reflect.TypeOf(service.Gate2XRpcClientMessage{}), "OnGate2XRpcClientMessage")
	register2.RegisterNatsRpcHandler(opcode.Opcode_Nats_X2Gate_ClientMessage, opcode.OpCodeType_Protobuf,
		reflect.TypeOf(service.X2GateRpcClientMessage{}), "OnX2GateRpcClientMessage")
	register2.RegisterNatsRpcHandler(opcode.Opcode_Nats_Login2Game_ClientLogin, opcode.OpCodeType_Protobuf,
		reflect.TypeOf(service.Login2GameClientLogin{}), "OnLogin2GameClientLogin")
	register2.RegisterNatsRpcHandler(opcode.Opcode_Nats_Gate2Game_Disconnect, opcode.OpCodeType_Protobuf,
		reflect.TypeOf(service.Gate2GameDisconnect{}), "OnGate2GameDisconnect")
}
