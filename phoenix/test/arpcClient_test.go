package test

import (
	"fmt"
	"github.com/gogf/gf/v2/container/gmap"
	"github.com/lesismal/arpc"
	"github.com/lesismal/arpc/extension/middleware/coder/msgpack"
	"log"
	"net"
	"testing"
	"time"
)

func Test_aprcClient(t *testing.T) {
	client, err := arpc.NewClient(func() (net.Conn, error) {
		return net.DialTimeout("tcp", "192.168.1.162:9697", time.Second*3)
	})
	if err != nil {
		panic(err)
	}
	defer client.Stop()
	client.Handler.Handle("rpc_c_repose", func(ctx *arpc.Context) {
		rsp := ""
		ctx.Bind(&rsp)
		log.Printf("Call rpc_c_repose: \"%v\"", rsp)
	})
	req := "hello"
	rsp := ""
	err = client.Call("rpc_hello", &req, &rsp, time.Second*5)
	if err != nil {
		log.Fatalf("Call failed: %v", err)
	} else {
		log.Printf("Call Response: \"%v\"", rsp)
	}
}

func Test_aprcClient_2(t *testing.T) {
	client, err := arpc.NewClient(func() (net.Conn, error) {
		return net.DialTimeout("tcp", "192.168.1.162:9998", time.Second*3)
	})
	if err != nil {
		panic(err)
	}
	defer client.Stop()

	client.Handler.UseCoder(msgpack.New())

	for i := 0; i < 5; i++ {
		req := fmt.Sprintf("hello %v", i)
		rsp := ""
		midlewareValues := map[interface{}]interface{}{}
		k, v := fmt.Sprintf("key-%v", i), fmt.Sprintf("value-%v", i)
		midlewareValues[k] = v
		err = client.Call("echo", &req, &rsp, time.Second*5, midlewareValues)
		if err != nil {
			log.Fatalf("Call /echo failed: %v", err)
		} else {
			log.Printf("Call /echo Response: \"%v\"", rsp)
		}
	}
}

func Test_Json(t *testing.T) {
	//fileData, err := utils.ReadFile("D:\\phoenix\\bin\\res\\logicConfig\\t_ChooseCard.json")
	//if err == nil {
	//	//var re map[string]interface{}
	//	//err := json.Unmarshal(fileData, &re)
	//	//if err != nil {
	//	//	return
	//	//}
	//	if j, err := gjson.DecodeToJson(fileData); err != nil {
	//		panic(err)
	//	} else {
	//		var configs []logicConfig.ChooseCard
	//		if err := j.Scan(&configs); err != nil {
	//			panic(err)
	//		}
	//		//fmt.Printf(`%+v`, users)
	//		fmt.Println("OK")
	//	}
	//}
	tMap := gmap.StrAnyMap{}
	tMap.Set("LotteryPaper", 100)
	eJson := tMap.String()
	fmt.Println(eJson)
}
