// Package test -----------------------------
// @file      : encrypt_test.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/4/11 17:15
// @desc      :
// -------------------------------------------
package test

import (
	"phoenix/phoenix/utils"
	"testing"
)

func Test_Encrypt(t *testing.T) {
	key := "pvldUkpRKNTjtE8SKifZwYTN5LzPZ4Pl" // 必须是256位（32字节）
	//encodedCiphertext := "vb+z6uwV93+XCLKRfXGrMkNBq7nDztdsfubh1uJvM9roLCXdAd3Spxs0dT87rCvUgkSSUn5f"
	//iv := "adde1543376f77c1fb2b9ee0500a9478"
	encodedCiphertext := "T1QzDOFf3UHe3R/1pLebkEgwjyeZe4RPQoqzl/0w9qoxxhK9x8azmtiMsnmvIGRzQK2IbJfu"
	iv := "c14c7cc02866ef1912c3c88fd7d6b769"
	te, e := utils.AESDecryptFromString(encodedCiphertext, key, iv)
	if e != nil {
		t.Error(e)
	}
	t.Log(te)
}

func Test_Encrypt2(t *testing.T) {
	key := "pvldUkpRKNTjtE8SKifZwYTN5LzPZ4Pl" // 必须是256位（32字节）
	te, iv, e := utils.AESEncryptToString("{\"accountName\":\"2024002\",\"avatarId\":6,\"avatarName\":\"\"}", key)
	if e != nil {
		t.Error(e)
	}
	t.Log(te, iv)
}
