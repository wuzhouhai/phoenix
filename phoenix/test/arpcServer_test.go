package test

import (
	"github.com/lesismal/arpc"
	"github.com/lesismal/arpc/extension/middleware/coder/msgpack"
	"github.com/lesismal/arpc/log"
	"testing"
)

func Test_arpcServer(t *testing.T) {
	server := arpc.NewServer()
	server.Handler.Handle("/echo", func(context *arpc.Context) {
		str := ""
		if err := context.Bind(&str); err == nil {
			context.Write(str)
		}
	})
	server.Run("0.0.0.0:9998")
}

func Test_arpcServer_2(t *testing.T) {
	svr := arpc.NewServer()

	svr.Handler.UseCoder(msgpack.New())

	// register router
	svr.Handler.Handle("echo", func(ctx *arpc.Context) {
		ctx.Write(ctx.Body())
		log.Info("echo, %v", ctx.Values())
	})

	svr.Run("0.0.0.0:9998")
}
