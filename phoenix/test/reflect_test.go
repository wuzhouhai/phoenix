package test

import (
	"crypto/sha1"
	"fmt"
	"reflect"
	"sort"
	"strings"
	"testing"
)

type IAuthor interface {
	CallName()
}

type Author struct {
	Name string
	Age  int
	book Book
}

type AuthorA struct {
	Name string
	Age  int
	book Book
}

type Book struct {
	Name  string
	Price float64
}

func (this *Author) CallName() {

}

func (this *Author) Test() {

}

func (this *AuthorA) CallName() {
	fmt.Println("call name")
}

func (this *AuthorA) Test2() {
	fmt.Println("call Test2")
}

func Test_BytesJump(t *testing.T) {
	// 收到GET请求
	sortedString := make([]string, 4)
	sortedString = append(sortedString, "#40y*m^1!fso3ffi")
	sortedString = append(sortedString, "1705118078")
	sortedString = append(sortedString, "984")
	sortedString = append(sortedString, "")
	Signature := "a6a09534a92206b9457084593a72a0f19ef015e1"
	// 需要对这些参数按字符串自然大小进行排序
	sort.Strings(sortedString)
	// 使用SHA1算法
	h := sha1.New()
	h.Write([]byte(strings.Join(sortedString, "")))

	bs := h.Sum(nil)
	_signature := fmt.Sprintf("%x", bs)

	// signature 一致表示请求来源于 字节小程序服务端
	if _signature == Signature {

	}
}

func Test_Reflect(t *testing.T) {
	author := "draven"
	fmt.Println("TypeOf author:", reflect.TypeOf(author))
	fmt.Println("ValueOf author:", reflect.ValueOf(author))
}

func Test_Reflect_Struct(t *testing.T) {
	au := Author{
		Name: "draven",
		Age:  18,
		book: Book{
			Name:  "book1",
			Price: 100.0,
		},
	}
	au2 := AuthorA{
		Name: "draven",
		Age:  18,
		book: Book{
			Name:  "book1",
			Price: 100.0,
		},
	}
	var iAu IAuthor
	var iAu2 IAuthor
	iAu = &au
	iAu2 = &au2
	fmt.Println("TypeOf author:", reflect.TypeOf(au))
	fmt.Println("ValueOf author:", reflect.ValueOf(au))
	for i := 0; i < reflect.TypeOf(au).NumField(); i++ {
		field := reflect.TypeOf(au).Field(i)
		fmt.Println("Field:", field.Name, field.Type)
	}
	// 打印方法
	for i := 0; i < reflect.TypeOf(au).NumMethod(); i++ {
		method := reflect.TypeOf(au).Method(i)
		fmt.Println("Method:", method.Name, method.Type)
	}

	// 打印Author指针方法
	for i := 0; i < reflect.TypeOf(&au).NumMethod(); i++ {
		method := reflect.TypeOf(&au).Method(i)
		fmt.Println("Method:", method.Name, method.Type)
	}

	for i := 0; i < reflect.TypeOf(iAu).NumMethod(); i++ {
		method := reflect.TypeOf(iAu).Method(i)
		fmt.Println("iAu Method:", method.Name, method.Type)
	}

	for i := 0; i < reflect.TypeOf(iAu2).NumMethod(); i++ {
		method := reflect.TypeOf(iAu2).Method(i)
		fmt.Println("iAu2 Method:", method.Name, method.Type)
	}
	var iAu3 IAuthor
	iAu3 = iAu2

	for i := 0; i < reflect.TypeOf(iAu3).NumMethod(); i++ {
		method := reflect.TypeOf(iAu3).Method(i)
		fmt.Println("iAu3 Method:", method.Name, method.Type)
	}
	reflect.ValueOf(iAu3).MethodByName("Test2").Call([]reflect.Value{})
}
