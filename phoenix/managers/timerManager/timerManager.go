// Package timerManager -----------------------------
// @file      : timerManager.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/24 16:07
// @desc      :
// -------------------------------------------
package timerManager

import (
	"context"
	"errors"
	"fmt"
	"github.com/gogf/gf/v2/os/gtimer"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	"sync"
	"sync/atomic"
	"time"
)

type TimerManager struct {
	timer            *gtimer.Timer
	timerIncrementId dataType.PhoenixTypeTimerID
	timers           map[dataType.PhoenixTypeTimerID]*gtimer.Entry
	isRunning        bool
}

func (this *TimerManager) Start() {
	this.timer.Start()
}

func (this *TimerManager) Stop() {
	this.timer.Stop()
}

func (this *TimerManager) Close() {
	this.timer.Close()
}

func (this *TimerManager) RegisterTimer(ctx context.Context, interval time.Duration, job gtimer.JobFunc) dataType.PhoenixTypeTimerID {
	atomic.AddUint64(&this.timerIncrementId, 1)
	e := this.timer.Add(ctx, interval, job)
	this.timers[this.timerIncrementId] = e
	return this.timerIncrementId
}

func (this *TimerManager) RegisterOnceTimer(ctx context.Context, interval time.Duration, job gtimer.JobFunc) dataType.PhoenixTypeTimerID {
	atomic.AddUint64(&this.timerIncrementId, 1)
	e := this.timer.AddOnce(ctx, interval, job)
	this.timers[this.timerIncrementId] = e
	return this.timerIncrementId
}

func (this *TimerManager) RegisterSingletonTimer(ctx context.Context, interval time.Duration, job gtimer.JobFunc) dataType.PhoenixTypeTimerID {
	atomic.AddUint64(&this.timerIncrementId, 1)
	e := this.timer.AddSingleton(ctx, interval, job)
	this.timers[this.timerIncrementId] = e
	return this.timerIncrementId
}

func (this *TimerManager) RegisterTimesTimer(ctx context.Context, interval time.Duration, times int, job gtimer.JobFunc) dataType.PhoenixTypeTimerID {
	atomic.AddUint64(&this.timerIncrementId, 1)
	e := this.timer.AddTimes(ctx, interval, times, job)
	this.timers[this.timerIncrementId] = e
	return this.timerIncrementId
}

func (this *TimerManager) StartTimer(tid dataType.PhoenixTypeTimerID) error {
	if this.timers[tid] != nil {
		this.timers[tid].Start()
	} else {
		return errors.New(fmt.Sprintf("timerManager: timer %d not exist", tid))
	}
	return nil
}

func (this *TimerManager) CloseTimer(tid dataType.PhoenixTypeTimerID) error {
	if this.timers[tid] != nil {
		logger.PhoenixLoggerIns().Debugf(fmt.Sprintf("timerManager: timer %d close", tid))
		this.timers[tid].Close()
		delete(this.timers, tid)
	} else {
		return errors.New(fmt.Sprintf("timerManager: timer %d not exist", tid))
	}
	return nil
}

func (this *TimerManager) RunTimer(tid dataType.PhoenixTypeTimerID) error {
	if this.timers[tid] != nil {
		this.timers[tid].Run()
	} else {
		return errors.New(fmt.Sprintf("timerManager: timer %d not exist", tid))
	}
	return nil
}

func (this *TimerManager) StopTimer(tid dataType.PhoenixTypeTimerID) error {
	if this.timers[tid] != nil {
		this.timers[tid].Stop()
	} else {
		return errors.New(fmt.Sprintf("timerManager: timer %d not exist", tid))
	}
	return nil
}

func (this *TimerManager) ResetTimer(tid dataType.PhoenixTypeTimerID) error {
	if this.timers[tid] != nil {
		this.timers[tid].Reset()
	} else {
		return errors.New(fmt.Sprintf("timerManager: timer %d not exist", tid))
	}
	return nil
}

func (this *TimerManager) SetSingleton(tid dataType.PhoenixTypeTimerID, enable bool) {
	if this.timers[tid] != nil {
		this.timers[tid].SetSingleton(enable)
	}
}

func (this *TimerManager) IsSingleton(tid dataType.PhoenixTypeTimerID) bool {
	if this.timers[tid] != nil {
		return this.timers[tid].IsSingleton()
	}
	return false
}

var (
	onceTimerManager sync.Once
	timerManagerIns  *TimerManager
)

func PhoenixManagerTimer() *TimerManager {
	onceTimerManager.Do(func() {
		timerManagerIns = &TimerManager{
			timer: gtimer.New(gtimer.TimerOptions{
				Interval: time.Millisecond * 10,
				Quick:    false,
			}),
			timerIncrementId: dataType.PhoenixTypeTimerID(0),
			timers:           make(map[dataType.PhoenixTypeTimerID]*gtimer.Entry),
			isRunning:        false,
		}
	})
	return timerManagerIns
}
