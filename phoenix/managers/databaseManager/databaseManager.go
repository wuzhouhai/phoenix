// Package databaseManager -----------------------------
// @file      : databaseManager.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/5 18:15
// @desc      :
// -------------------------------------------
package databaseManager

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/gogf/gf/v2/database/gdb"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/register"
	"sync"
)

type DatabaseManager struct {
	databases map[string]interfaces.IDataBase
}

func (this *DatabaseManager) InitDatabases(ctx context.Context, options config.PhoenixOptions) {
	hasDefaultDatabase := false
	for _, databaseOption := range options.DatabaseOptions {
		databaseCreateFunc := register.GetDatabaseCreateFunc(databaseOption.Type)
		if databaseCreateFunc == nil {
			err := errors.New("database type not found")
			panic(err)
		}
		database, err := databaseCreateFunc(ctx, databaseOption)
		if err != nil {
			panic(err)
		}
		database.Init()
		this.databases[databaseOption.GroupName] = database
		if databaseOption.GroupName == "default" {
			hasDefaultDatabase = true
		}
	}
	if !hasDefaultDatabase {
		panic(errors.New("default database not found"))
	}
}

func (this *DatabaseManager) DataBase(groupName string) (interfaces.IDataBase, error) {
	if database, ok := this.databases[groupName]; ok {
		return database, nil
	}
	logger.PhoenixLoggerIns().Error(fmt.Sprintf("database %s not found", groupName))
	return nil, errors.New("database not found")
}

func (this *DatabaseManager) DefaultDataBase() (interfaces.IDataBase, error) {
	return this.DataBase("default")
}

func (this *DatabaseManager) SaveDataWithMap(groupName string, data gdb.Map, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	database, err := this.DataBase(groupName)
	if err != nil {
		return nil, err
	}
	return database.SaveDataWithMap(data, tableNameOrStruct...)
}

func (this *DatabaseManager) SaveDataWithStruct(groupName string, data interface{}, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	database, err := this.DataBase(groupName)
	if err != nil {
		return nil, err
	}
	return database.SaveDataWithStruct(data, tableNameOrStruct...)
}

func (this *DatabaseManager) SaveMultiDataWithListMap(groupName string, data gdb.List, batch int, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	database, err := this.DataBase(groupName)
	if err != nil {
		return nil, err
	}
	return database.SaveMultiDataWithListMap(data, batch, tableNameOrStruct...)
}

func (this *DatabaseManager) InsertDataWithMap(groupName string, data gdb.Map, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	database, err := this.DataBase(groupName)
	if err != nil {
		return nil, err
	}
	return database.InsertDataWithMap(data, tableNameOrStruct...)
}

func (this *DatabaseManager) InsertDataWithStruct(groupName string, data interface{}, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	database, err := this.DataBase(groupName)
	if err != nil {
		return nil, err
	}
	return database.InsertDataWithStruct(data, tableNameOrStruct...)
}

func (this *DatabaseManager) InsertDataWithListMap(groupName string, data gdb.List, batch int, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	database, err := this.DataBase(groupName)
	if err != nil {
		return nil, err
	}
	return database.InsertDataWithListMap(data, batch, tableNameOrStruct...)
}

func (this *DatabaseManager) InsertDataWithMapAndGetId(groupName string, data gdb.Map, tableNameOrStruct ...interface{}) (lastInsertId int64, err error) {
	database, err := this.DataBase(groupName)
	if err != nil {
		return -1, err
	}
	return database.InsertDataWithMapAndGetId(data, tableNameOrStruct...)
}

func (this *DatabaseManager) InsertDataWithStructAndGetId(groupName string, data interface{}, tableNameOrStruct ...interface{}) (lastInsertId int64, err error) {
	database, err := this.DataBase(groupName)
	if err != nil {
		return -1, err
	}
	return database.InsertDataWithStructAndGetId(data, tableNameOrStruct...)
}

func (this *DatabaseManager) ReplaceDataWithMap(groupName string, data gdb.Map, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	database, err := this.DataBase(groupName)
	if err != nil {
		return nil, err
	}
	return database.ReplaceDataWithMap(data, tableNameOrStruct...)
}

func (this *DatabaseManager) ReplaceDataWithStruct(groupName string, data interface{}, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	database, err := this.DataBase(groupName)
	if err != nil {
		return nil, err
	}
	return database.ReplaceDataWithStruct(data, tableNameOrStruct...)
}

func (this *DatabaseManager) ReplaceDataWithListMap(groupName string, data gdb.List, batch int, tableNameOrStruct ...interface{}) (result sql.Result, err error) {
	database, err := this.DataBase(groupName)
	if err != nil {
		return nil, err
	}
	return database.ReplaceDataWithListMap(data, batch, tableNameOrStruct...)
}

func NewDataBaseManager() *DatabaseManager {
	return &DatabaseManager{
		databases: make(map[string]interfaces.IDataBase),
	}
}

var databaseManagerInstance *DatabaseManager
var once sync.Once

func PhoenixManagerDatabase() *DatabaseManager {
	once.Do(func() {
		databaseManagerInstance = &DatabaseManager{
			databases: make(map[string]interfaces.IDataBase),
		}
	})
	return databaseManagerInstance
}
