package entityManager

import (
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/interfaces"
	"sync"
	"sync/atomic"
)

type EntityManager struct {
	entities        sync.Map
	entityIncrement dataType.PhoenixTypeEntityId
}

// GetEntityIncrement 获取实体自增长ID
func (this *EntityManager) GetEntityIncrement() dataType.PhoenixTypeEntityId {
	return atomic.AddUint64(&this.entityIncrement, 1)
}

// AddEntity 增加实体
func (this *EntityManager) AddEntity(entity interfaces.IEntity) {
	this.entities.Store(entity.GetId(), entity)
}

// RemoveEntity 删除实体
func (this *EntityManager) RemoveEntity(id dataType.PhoenixTypeEntityId) {
	this.entities.Delete(id)
}

var entityManagerInstance *EntityManager
var syncOnce sync.Once

func PhoenixManagerEntity() *EntityManager {
	syncOnce.Do(func() {
		entityManagerInstance = &EntityManager{}
	})
	return entityManagerInstance
}
