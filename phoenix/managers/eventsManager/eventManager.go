// Package eventsManager -----------------------------
// @file      : eventManager.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/26 17:51
// @desc      :
// -------------------------------------------
package eventsManager

import (
	"phoenix/phoenix/common/eventBus"
	"sync"
)

type EventsManager struct {
	eventBusIns eventBus.Bus
}

func (this *EventsManager) Subscribe(topic string, fn interface{}) error {
	return this.eventBusIns.Subscribe(topic, fn)
}

func (this *EventsManager) SubscribeAsync(topic string, fn interface{}, transactional bool) error {
	return this.eventBusIns.SubscribeAsync(topic, fn, transactional)
}

func (this *EventsManager) SubscribeOnce(topic string, fn interface{}) error {
	return this.eventBusIns.SubscribeOnce(topic, fn)
}

func (this *EventsManager) SubscribeOnceAsync(topic string, fn interface{}) error {
	return this.eventBusIns.SubscribeOnceAsync(topic, fn)
}

func (this *EventsManager) Unsubscribe(topic string, handler interface{}) error {
	return this.eventBusIns.Unsubscribe(topic, handler)
}

func (this *EventsManager) Publish(topic string, args ...interface{}) {
	this.eventBusIns.Publish(topic, args...)
}

var instance *EventsManager
var once sync.Once

func PhoenixManagerEvent() *EventsManager {
	once.Do(func() {
		instance = &EventsManager{
			eventBusIns: eventBus.NewBus(),
		}
	})
	return instance
}
