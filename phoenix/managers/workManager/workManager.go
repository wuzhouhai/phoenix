// Package workManager -----------------------------
// @file      : workManager.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/19 14:36
// @desc      :
// -------------------------------------------
package workManager

import (
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/tasks/works"
	"sync"
)

type WorkManager struct {
	antsWorker *works.AntsWorker
}

func (this *WorkManager) AddTask(task interfaces.ITask) error {
	logger.PhoenixLoggerIns().Debugf("workManager AddTask %d", task.GetConId())
	return this.antsWorker.AddTask(task)
}

var once sync.Once
var workManager *WorkManager

func PhoenixManagerWork() *WorkManager {
	once.Do(func() {
		workManager = &WorkManager{
			antsWorker: works.NewAntsWorker(),
		}
	})
	return workManager
}
