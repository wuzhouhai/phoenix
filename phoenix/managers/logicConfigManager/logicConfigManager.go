// Package logicConfigManager -----------------------------
// @file      : logicConfigManager.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/5 11:57
// @desc      :
// -------------------------------------------
package logicConfigManager

import (
	"context"
	"github.com/gogf/gf/v2/container/gmap"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/register"
	"sync"
)

type LogicConfigManager struct {
	logicConfigs gmap.StrAnyMap
}

func (this *LogicConfigManager) Init() {
	allLogicConfigs := register.GetLogicConfigs()
	for configName, createFunc := range allLogicConfigs {
		configObj, _ := createFunc(configName, configName+".json", context.TODO())
		configObj.InitLogicConfig()
		this.logicConfigs.Set(configName, configObj)
	}
}

func (this *LogicConfigManager) GetConfig(configName string) interfaces.ILogicConfig {
	if !this.logicConfigs.Contains(configName) {
		return nil
	}
	return this.logicConfigs.Get(configName).(interfaces.ILogicConfig)
}

var instance *LogicConfigManager
var once sync.Once

func PhoenixManagerLogicConfig() *LogicConfigManager {
	once.Do(func() {
		instance = &LogicConfigManager{}
	})
	return instance
}
