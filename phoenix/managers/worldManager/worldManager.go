// Package worldManager -----------------------------
// @file      : worldManager.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/22 16:20
// @desc      :
// -------------------------------------------
package worldManager

import (
	"context"
	"github.com/gogf/gf/v2/container/gmap"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/interfaces"
	"sync"
)

type WorldManager struct {
	entities       map[dataType.PhoenixTypeEntityId]interfaces.IEntity
	onlineAccounts *gmap.IntAnyMap
}

func (this *WorldManager) CheckIsOnline(dbid dataType.PhoenixTypeDataBaseId) bool {
	if this.onlineAccounts.Contains(dbid) {
		return true
	}
	return false
}

func (this *WorldManager) GetOnlineAgentEntity(dbid dataType.PhoenixTypeDataBaseId) interfaces.IAgentEntity {
	if this.onlineAccounts.Contains(dbid) {
		return this.onlineAccounts.Get(dbid).(interfaces.IAgentEntity)
	}
	return nil
}

func (this *WorldManager) Online(dbid dataType.PhoenixTypeDataBaseId, e interfaces.IAgentEntity) bool {
	if this.CheckIsOnline(dbid) {
		logger.PhoenixLoggerIns().Errorf("account dbid:%d is online", dbid)
		return false
	}
	this.onlineAccounts.Set(dbid, e)
	return true
}

func (this *WorldManager) Offline(dbid dataType.PhoenixTypeDataBaseId) bool {
	if this.CheckIsOnline(dbid) {
		this.onlineAccounts.Remove(dbid)
		return true
	}
	return false
}

func (this *WorldManager) EnterWorld(e interfaces.IEntity) {
	if this.entities[e.GetId()] == nil {
		this.entities[e.GetId()] = e
	}
}

func (this *WorldManager) LeaveWorld(eid dataType.PhoenixTypeEntityId) {
	delete(this.entities, eid)
}

func (this *WorldManager) OnTimerFunc(ctx context.Context) {
	for _, entity := range this.entities {
		entity.FrameLogic()
	}
}

var onceWorldManager sync.Once
var worldManager *WorldManager

func PhoenixManagerWorld() *WorldManager {
	onceWorldManager.Do(func() {
		worldManager = &WorldManager{
			entities:       make(map[dataType.PhoenixTypeEntityId]interfaces.IEntity),
			onlineAccounts: gmap.NewIntAnyMap(true),
		}
	})
	return worldManager
}
