// Package playerManager -----------------------------
// @file      : PlayerManager.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/25 11:41
// @desc      :
// -------------------------------------------
package playerManager

import (
	"github.com/gogf/gf/v2/container/gmap"
	"phoenix/phoenix/common/fixCache"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/interfaces"
	"sync"
)

type PlayerManager struct {
	players             *gmap.StrAnyMap
	playerConnectorInfo *gmap.StrIntMap // connectorID -> avatarId
}

var oncePlayerManager sync.Once
var playerManager *PlayerManager

func (this *PlayerManager) AddPlayer(player interfaces.IPlayer) {
	if player == nil {
		return
	}
	mKey := fixCache.GetPlayerManagerKey(player.GetId())
	if this.players.Contains(mKey) {
		logger.PhoenixLoggerIns().Warnf("PlayerManager.AddPlayer: player already exist, payerId:%d",
			player.GetId())
		return
	}
	conKey := fixCache.GetPayerManagerConnectorKey(player.GetConnectorId(), player.GetGateServiceID())
	this.players.Set(mKey, player)
	this.playerConnectorInfo.Set(conKey, player.GetId())
}

func (this *PlayerManager) ChangeConnector(player interfaces.IPlayer) {
	if player == nil {
		return
	}
	mKey := fixCache.GetPlayerManagerKey(player.GetId())
	if !this.players.Contains(mKey) {
		return
	}
	conKey := fixCache.GetPayerManagerConnectorKey(player.GetConnectorId(), player.GetGateServiceID())
	this.playerConnectorInfo.Set(conKey, player.GetId())
}

func (this *PlayerManager) GetPlayer(id dataType.PhoenixTypeDataBaseId) interfaces.IPlayer {
	mKey := fixCache.GetPlayerManagerKey(id)
	if !this.players.Contains(mKey) {
		return nil
	}
	return this.players.Get(mKey).(interfaces.IPlayer)
}

func (this *PlayerManager) RemovePlayer(id dataType.PhoenixTypeDataBaseId) {
	mKey := fixCache.GetPlayerManagerKey(id)
	if !this.players.Contains(mKey) {
		return
	}
	this.players.Remove(mKey)
}

func (this *PlayerManager) AddGamePacket(conId dataType.PhoenixTypeConnectId, gateServiceId string, p interface{}) {
	conKey := fixCache.GetPayerManagerConnectorKey(conId, gateServiceId)
	if this.playerConnectorInfo.Contains(conKey) {
		avatarId := this.playerConnectorInfo.Get(conKey)
		mKey := fixCache.GetPlayerManagerKey(avatarId)
		if this.players.Contains(mKey) {
			iPlayer := this.players.Get(mKey).(interfaces.IPlayer)
			iPlayer.AddGamePacket(p)
		}
	}
}

func (this *PlayerManager) DisConnect(conId dataType.PhoenixTypeConnectId, gateServiceId string) {
	conKey := fixCache.GetPayerManagerConnectorKey(conId, gateServiceId)
	if this.playerConnectorInfo.Contains(conKey) {
		avatarId := this.playerConnectorInfo.Get(conKey)
		mKey := fixCache.GetPlayerManagerKey(avatarId)
		if this.players.Contains(mKey) {
			iPlayer := this.players.Get(mKey).(interfaces.IPlayer)
			iPlayer.DisConnect()
			this.players.Remove(mKey)
		}
		this.playerConnectorInfo.Remove(conKey)
	}
}

func (this *PlayerManager) BeKick(avatarId dataType.PhoenixTypeDataBaseId, msg string) {
	mKey := fixCache.GetPlayerManagerKey(avatarId)
	if this.players.Contains(mKey) {
		iPlayer := this.players.Get(mKey).(interfaces.IPlayer)
		conKey := fixCache.GetPayerManagerConnectorKey(iPlayer.GetConnectorId(), iPlayer.GetGateServiceID())
		iPlayer.Kick(msg)
		this.playerConnectorInfo.Remove(conKey)
	}
}

func PhoenixManagerPlayer() *PlayerManager {
	oncePlayerManager.Do(func() {
		playerManager = &PlayerManager{
			players:             gmap.NewStrAnyMap(true),
			playerConnectorInfo: gmap.NewStrIntMap(true),
		}
	})
	return playerManager
}
