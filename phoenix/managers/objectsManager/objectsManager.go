// Package objectsManager -----------------------------
// @file      : objectsManager.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/26 16:15
// @desc      :
// -------------------------------------------
package objectsManager

import (
	"github.com/gogf/gf/v2/container/gpool"
	"github.com/gogf/gf/v2/errors/gerror"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/register"
	"sync"
	"time"
)

type ObjectsManager struct {
	objectPools map[string]*gpool.Pool
}

func (this *ObjectsManager) init() {
	objects := register.GetObjectsRegister()
	for name, object := range objects {
		this.objectPools[name] = gpool.New(10*time.Second, object.CreateFunc, object.DestroyFunc)
	}
}

func (this *ObjectsManager) CreateObject(name string) (interfaces.IEntity, error) {
	if _, ok := this.objectPools[name]; !ok {
		return nil, gerror.New("对象池不存在")
	}
	i, err := this.objectPools[name].Get()
	if err != nil {
		return nil, err
	}
	e := i.(interfaces.IEntity)
	e.Init()
	return e, nil
}

func (this *ObjectsManager) DestroyObject(i interfaces.IEntity) {
	eName := i.GetName()
	i.(interfaces.IEntity).ResetObject()
	err := this.objectPools[eName].Put(i)
	if err != nil {
		return
	}
}

func (this *ObjectsManager) LogInfo() {
	logger.PhoenixLoggerIns().Info("================缓存对象日志===================")
	for name, pool := range this.objectPools {
		logger.PhoenixLoggerIns().Infof("对象池名称：%s, 对象池大小：%d", name, pool.Size())
	}
	logger.PhoenixLoggerIns().Info("================缓存对象日志===================")
}

var once sync.Once
var instance *ObjectsManager

func PhoenixManagerObjects() *ObjectsManager {
	once.Do(func() {
		instance = &ObjectsManager{
			objectPools: make(map[string]*gpool.Pool),
		}
		instance.init()
	})
	return instance
}
