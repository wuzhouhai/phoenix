// Package luaManager -----------------------------
// @file      : luaManager.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/10/22 14:25
// @desc      :
// -------------------------------------------
package luaManager

import (
	"github.com/gogf/gf/v2/container/garray"
	"github.com/gogf/gf/v2/container/gmap"
	lua "github.com/yuin/gopher-lua"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/managers/resManager"
	"phoenix/phoenix/utils"
	"sync"
)

type LuaManager struct {
	compileLuaMap *gmap.StrAnyMap // 按照服务所属编译好的lua文件字节码
}

var instance *LuaManager
var once sync.Once

func PhoenixManagerLua() *LuaManager {
	once.Do(func() {
		instance = &LuaManager{
			compileLuaMap: gmap.NewStrAnyMap(true),
		}
	})
	return instance
}

func (this *LuaManager) Init() {

}

func (this *LuaManager) CompileLuaFiles(serviceName string) {
	if this.compileLuaMap.Contains(serviceName) {
		return
	}
	luaCompileList := garray.NewArray()
	luaResPath := resManager.PhoenixManagerRes().FindPath(serviceName + "Script")
	if luaResPath == "" {
		return
	}
	luaFiles, err := utils.FindFilesByType(luaResPath, ".lua", true)
	if err != nil {
		logger.PhoenixLoggerIns().Errorf("find lua files error: %s", err.Error())
		return
	}
	for _, luaFile := range luaFiles {
		luaFileBytes, err := utils.CompileLua(luaFile)
		if err != nil {
			logger.PhoenixLoggerIns().Errorf("read lua file error: %s", err.Error())
			continue
		}
		luaCompileList.Append(luaFileBytes)
	}
	this.compileLuaMap.Set(serviceName, luaCompileList)
}

func (this *LuaManager) DoCompiledFiles(serviceName string, l *lua.LState) {
	if !this.compileLuaMap.Contains(serviceName) {
		return
	}
	luaCompileList := this.compileLuaMap.Get(serviceName).(*garray.Array)
	luaCompileList.Iterator(func(key int, value interface{}) bool {
		err := utils.DoCompiledFile(l, value.(*lua.FunctionProto))
		if err != nil {
			logger.PhoenixLoggerIns().Errorf("do compiled file error: %s", err.Error())
		}
		return true
	})
}
