// Package resManager -----------------------------
// @file      : resManager.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2023/12/18 17:17
// @desc      : 资源管理器
// -------------------------------------------
package resManager

import (
	"github.com/gogf/gf/v2/container/gset"
	"os"
	"phoenix/phoenix/defines/env"
	"phoenix/phoenix/defines/paths"
	"phoenix/phoenix/utils"
	"runtime"
	"sync"
)

type ResManager struct {
	// 资源文件夹数组
	resPath *gset.StrSet
	// 查询过的资源路径缓存
	resCache map[string]string
	// 查询过的文件夹资源路径缓存
	resPathCache map[string]string
}

func (this *ResManager) Init() {
	phoenixRoot := os.Getenv(env.ENV_PHOENIX_ROOT)
	if phoenixRoot != "" {
		this.AddResPath(phoenixRoot + paths.Res_Path)
	}
	phoenixResPath := os.Getenv(env.ENV_PHOENIX_RES_PATH)
	if phoenixResPath != "" {
		var resPaths []string
		if runtime.GOOS == "windows" {
			resPaths = utils.SplitString(phoenixResPath, ";")
		} else {
			resPaths = utils.SplitString(phoenixResPath, ":")
		}
		this.AddResPaths(resPaths)
	}
}

func (this *ResManager) AddResPaths(paths []string) {
	if len(paths) == 0 {
		return
	}
	this.resPath.Add(paths...)
}

func (this *ResManager) AddResPath(path string) {
	this.resPath.Add(path)
}

func (this *ResManager) CheckResPath(path string) bool {
	return this.resPath.Contains(path)
}

func (this *ResManager) FindPath(pathName string) string {
	// 先从缓存中查询
	var resPathName string
	if path, ok := this.resPathCache[pathName]; ok {
		resPathName = path
	} else {
		this.resPath.Iterator(func(v string) bool {
			if err, ok := this.FindPathInFolder(v, pathName, &resPathName); ok {
				if err == nil {
					this.resPathCache[pathName] = resPathName
				}
			}
			return true
		})
	}
	if resPathName == "" {
		return ""
	}
	return resPathName
}

func (this *ResManager) FindResPath(fileName string, isFullPath bool) string {
	// 先从缓存中查询
	var filePath string
	if path, ok := this.resCache[fileName]; ok {
		filePath = path
	} else {
		this.resPath.Iterator(func(v string) bool {
			if err, ok := this.FindFileInFolder(v, fileName, &filePath); ok {
				if err == nil {
					this.resCache[fileName] = filePath
				}
			}
			return true
		})
	}
	if filePath == "" {
		return ""
	}
	if isFullPath {
		filePath = filePath + fileName
	}
	return filePath
}

func (this *ResManager) FindFileInFolder(folderPath string, fileName string, filePath *string) (error, bool) {
	readerInfos, err := os.ReadDir(folderPath)
	if err != nil {
		return err, false
	}
	for _, info := range readerInfos {
		if info.IsDir() {
			_, re := this.FindFileInFolder(folderPath+"/"+info.Name(), fileName, filePath)
			if re {
				return nil, true
			}
		}
		if info.Name() == fileName {
			*filePath = folderPath + "/"
			return nil, true
		}
	}
	return nil, false
}

func (this *ResManager) FindPathInFolder(folderPath string, pathName string, path *string) (error, bool) {
	readerInfos, err := os.ReadDir(folderPath)
	if err != nil {
		return err, false
	}
	for _, info := range readerInfos {
		if info.IsDir() {
			if info.Name() == pathName {
				*path = folderPath + "/" + pathName + "/"
				return nil, true
			}
			_, re := this.FindPathInFolder(folderPath+"/"+info.Name(), pathName, path)
			if re {
				return nil, true
			}
		}
	}
	return nil, false
}

var resManagerIns *ResManager
var onceResManager sync.Once

func PhoenixManagerRes() *ResManager {
	onceResManager.Do(func() {
		resManagerIns = &ResManager{
			resPath:      gset.NewStrSet(),
			resCache:     make(map[string]string),
			resPathCache: make(map[string]string),
		}
		resManagerIns.Init()
	})
	return resManagerIns
}
