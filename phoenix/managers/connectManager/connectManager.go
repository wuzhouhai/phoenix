package connectManager

import (
	"github.com/gogf/gf/v2/container/gmap"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/entitys/instance/message"
	"phoenix/phoenix/interfaces"
	"sync"
	"sync/atomic"
)

type ConnectManager struct {
	connectors     *gmap.IntAnyMap
	connectNumbers uint64
	stopChan       chan bool
}

func NewConnectManager() *ConnectManager {
	return &ConnectManager{
		connectors: gmap.NewIntAnyMap(true),
	}
}

func (this *ConnectManager) PushConnect(connector interfaces.IConnector) {
	logger.PhoenixLoggerIns().Debugf("connectManager.PushConnect id:%v", connector.GetId())
	connector.StartWork()
	this.connectors.Set(int(connector.GetId()), connector)
	atomic.AddUint64(&this.connectNumbers, 1)
}

func (this *ConnectManager) DeleteConnect(id dataType.PhoenixTypeConnectId) {
	logger.PhoenixLoggerIns().Debugf("connectManager.DeleteConnect id:%v", id)
	// 断开连接清理
	if this.connectors.Contains(int(id)) {
		con := this.connectors.Get(int(id))
		if con != nil {
			con.(interfaces.IConnector).DisConnect()
			atomic.AddUint64(&this.connectNumbers, ^uint64(0))
		} else {
			logger.PhoenixLoggerIns().Debugf("connectManager.DeleteConnect id:%v not found", id)
		}
	}
}

func (this *ConnectManager) GetConnect(id dataType.PhoenixTypeConnectId) interfaces.IConnector {
	if this.connectors.Contains(int(id)) {
		con := this.connectors.Get(int(id))
		if con != nil {
			return con.(interfaces.IConnector)
		}
	}
	return nil
}

func (this *ConnectManager) CloseConnect(id dataType.PhoenixTypeConnectId) {
	if this.connectors.Contains(int(id)) {
		con := this.connectors.Get(int(id))
		if con != nil {
			con.(interfaces.IConnector).CloseConn()
		}
	}
}

func (this *ConnectManager) GetConnectCounts() uint64 {
	return atomic.LoadUint64(&this.connectNumbers)
}

func (this *ConnectManager) OnAppClose() {
	this.connectors.Iterator(func(key int, value interface{}) bool {
		con := value.(interfaces.IConnector)
		con.CloseConn()
		return true
	})
}

func (this *ConnectManager) AddPackets(id dataType.PhoenixTypeConnectId, p *message.Packet) {
	if p == nil {
		return
	}
	if this.connectors.Contains(int(id)) {
		con := this.connectors.Get(int(id))
		if con != nil {
			con.(interfaces.IConnector).AddPacket(p)
		}
	}
}

func (this *ConnectManager) AddNatsToClientPackets(id dataType.PhoenixTypeConnectId, p *message.Packet) {
	if p == nil {
		return
	}
	if this.connectors.Contains(int(id)) {
		con := this.connectors.Get(int(id))
		if con != nil {
			con.(interfaces.IConnector).AddNatsToClientPacket(p)
		}
	}
}

var connectManager *ConnectManager
var onceConnectManager sync.Once

func PhoenixManagerConnect() *ConnectManager {
	onceConnectManager.Do(func() {
		connectManager = NewConnectManager()
	})
	return connectManager
}
