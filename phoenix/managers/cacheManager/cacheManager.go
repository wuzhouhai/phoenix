// Package cacheManager -----------------------------
// @file      : cacheManager.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/19 11:32
// @desc      :
// -------------------------------------------
package cacheManager

import (
	"context"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/common/database/redis"
	"sync"
)

type CacheManager struct {
	redisHelper *redis.RedisHelper
}

func (this *CacheManager) Init(ctx context.Context, options config.PhoenixOptions) {
	this.redisHelper = redis.NewRedisHelper(&options.RedisOptions)
}

func (this *CacheManager) Put(key string, value interface{}, expire int) error {
	return this.redisHelper.Put(key, value, expire)
}
func (this *CacheManager) Get(key string) (string, error) {
	return this.redisHelper.Get(key)
}

func (this *CacheManager) Del(key string) error {
	return this.redisHelper.Del(key)
}
func (this *CacheManager) Exists(key string) bool {
	return this.redisHelper.Exists(key)
}

func NewCacheManager() *CacheManager {
	return &CacheManager{
		redisHelper: nil,
	}
}

var cacheManager *CacheManager
var onceCacheManager sync.Once

func PhoenixManagerCache() *CacheManager {
	onceCacheManager.Do(func() {
		cacheManager = NewCacheManager()
	})
	return cacheManager
}
