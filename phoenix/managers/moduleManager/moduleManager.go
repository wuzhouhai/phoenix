// Package moduleManager -----------------------------
// @file      : moduleManager.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/8 11:31
// @desc      :
// -------------------------------------------
package moduleManager

import (
	"errors"
	"github.com/gogf/gf/v2/container/gmap"
	"github.com/gogf/gf/v2/util/gconv"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/interfaces"
	"reflect"
	"sync"
)

type ModuleManager struct {
	moduleOperate interfaces.IModuleOperate
	modules       map[dataType.PhoenixTypeModuleId]interfaces.IModule
}

func (this *ModuleManager) CheckModuleOperate() bool {
	return this.moduleOperate != nil
}

func (this *ModuleManager) SetModuleOperate(oper interfaces.IModuleOperate) {
	this.moduleOperate = oper
}

func (this *ModuleManager) UpdateModuleData(userId dataType.PhoenixTypeUserId, moduleId dataType.PhoenixTypeModuleId, data interface{}) (interface{}, error) {
	if this.moduleOperate == nil {
		logger.PhoenixLoggerIns().Error("module operate is nil")
		return nil, errors.New("module operate is nil")
	}
	return this.moduleOperate.UpdateModuleData(userId, moduleId, data)
}

func (this *ModuleManager) GetModuleData(userId dataType.PhoenixTypeUserId, moduleId dataType.PhoenixTypeModuleId) (map[string]interface{}, error) {
	if this.moduleOperate == nil {
		logger.PhoenixLoggerIns().Error("module operate is nil")
		return nil, errors.New("module operate is nil")
	}
	return this.moduleOperate.GetModuleData(userId, moduleId)
}

func (this *ModuleManager) IncreaseModuleData(userId dataType.PhoenixTypeUserId, moduleId dataType.PhoenixTypeModuleId, dataKey string, changValue int64) (int64, error) {
	mData, err := this.GetModuleData(userId, moduleId)
	if err != nil {
		return 0, err
	}
	if mData != nil {
		dataValue, ok := mData[dataKey]
		if ok {
			if reflect.TypeOf(dataValue).Kind() == reflect.String {
				logger.PhoenixLoggerIns().Errorf("数据类型错误 模块%d, 存储datakey<%s>, 值:%s, 不是数值类型",
					moduleId, dataKey, dataValue)
				return 0, errors.New("module data type error, not number")
			}
			mData[dataKey] = gconv.Int64(dataValue) + changValue
		} else {
			mData[dataKey] = changValue
		}
	}
	_, err = this.UpdateModuleData(userId, moduleId, mData)
	if err != nil {
		return 0, err
	}
	return mData[dataKey].(int64), nil
}

func (this *ModuleManager) ChangeModuleData(userId dataType.PhoenixTypeUserId, moduleId dataType.PhoenixTypeModuleId,
	dataKey string, changValue interface{}) (interface{}, error) {
	mData, err := this.GetModuleData(userId, moduleId)
	if err != nil {
		return 0, err
	}
	if mData == nil {
		return nil, nil
	}
	mData[dataKey] = changValue
	_, err = this.UpdateModuleData(userId, moduleId, mData)
	if err != nil {
		return nil, err
	}
	return mData[dataKey], nil
}

func (this *ModuleManager) RegisterModule(module interfaces.IModule) {
	_, ok := this.modules[module.GetModuleId()]
	if ok {
		logger.PhoenixLoggerIns().Errorf("模块注册失败 模块id:%d, 模块名:%s, 已存在",
			module.GetModuleId(), module.GetModuleName())
		return
	}
	this.modules[module.GetModuleId()] = module
}

func (this *ModuleManager) RegisterModuleLogic(moduleId dataType.PhoenixTypeModuleId, logicName string, logic interfaces.IModuleLogic) error {
	moduleIns, ok := this.modules[moduleId]
	if !ok {
		logger.PhoenixLoggerIns().Errorf("模块逻辑执行失败 模块id:%d, 模块名:%s, 逻辑名:%s, 未注册",
			moduleId, moduleIns.GetModuleName(), logicName)
		return errors.New("module logic not register")
	}
	return moduleIns.RegisterModuleLogic(logicName, logic)
}

func (this *ModuleManager) DoModuleLogic(moduleId dataType.PhoenixTypeModuleId, logicName string, userId dataType.PhoenixTypeUserId,
	params *gmap.StrAnyMap) (*gmap.StrAnyMap, error) {
	moduleIns, ok := this.modules[moduleId]
	if !ok {
		logger.PhoenixLoggerIns().Errorf("模块逻辑执行失败 模块id:%d, 模块名:%s, 逻辑名:%s, 未注册",
			moduleId, moduleIns.GetModuleName(), logicName)
		return nil, errors.New("module logic not register")
	}
	mData, _ := this.GetModuleData(userId, moduleId)
	if mData == nil {
		mData = moduleIns.NewModuleData()
	}
	re, err := moduleIns.DoLogic(logicName, userId, mData, params)
	_, e := this.UpdateModuleData(userId, moduleId, mData)
	if e != nil {
		logger.PhoenixLoggerIns().Errorf("模块逻辑执行失败 模块id:%d, 模块名:%s, 逻辑名:%s, 更新数据失败",
			moduleId, moduleIns.GetModuleName(), logicName, e)
		return nil, e
	}
	return re, err
}

var once sync.Once
var instance *ModuleManager

func PhoenixManagerModule() *ModuleManager {
	once.Do(func() {
		instance = &ModuleManager{
			modules: make(map[dataType.PhoenixTypeModuleId]interfaces.IModule),
		}
	})
	return instance
}
