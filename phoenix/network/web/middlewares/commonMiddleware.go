// Package middlewares -----------------------------
// @file      : commonMiddleware.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/3 16:11
// @desc      :
// -------------------------------------------
package middlewares

import (
	"fmt"
	"github.com/gogf/gf/v2/net/ghttp"
	"phoenix/phoenix/common/logger"
)

func MiddlewareCORS(r *ghttp.Request) {
	r.Response.CORSDefault()
	r.Middleware.Next()
}

func MiddlewareLogger(r *ghttp.Request) {
	r.Middleware.Next()
	errStr := ""
	if err := r.GetError(); err != nil {
		errStr = err.Error()
	}
	if r.Response.Status == 200 {
		logger.PhoenixLoggerIns().Debug(fmt.Sprintf("%s %s %s %s", r.Response.Status, r.URL.Path, errStr,
			r.Response.BufferString()))
	} else {
		logger.PhoenixLoggerIns().Error(fmt.Sprintf("%s %s %s %s", r.Response.Status, r.URL.Path, errStr,
			r.Response.BufferString()))
	}
}
