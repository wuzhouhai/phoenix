// Package reader -----------------------------
// @file      : PacketReader.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/17 16:54
// @desc      :
// -------------------------------------------
package reader

import (
	"errors"
	"phoenix/phoenix/defines/normal"
	"phoenix/phoenix/entitys/instance/message"
	"phoenix/phoenix/entitys/instance/message/header"
	"phoenix/phoenix/network/codec"
)

type PacketReader struct {
	HeaderCodec *codec.HeaderCodec
	Header      *header.Header
	ReadSize    int
}

func NewPacketReader() *PacketReader {
	return &PacketReader{
		HeaderCodec: &codec.HeaderCodec{},
		Header:      header.NewHeader(),
	}
}

func (this *PacketReader) GetHeader() *header.Header {
	return this.Header
}

func (this *PacketReader) Reader(datas []byte) (error, *message.Packet) {
	if this.Header.Length == -1 && len(datas) >= normal.HeaderSize {
		headDatas := datas[:normal.HeaderSize]
		err := this.HeaderCodec.Decode(headDatas, this.Header)
		if err != nil {
			return err, nil
		}
		this.ReadSize += normal.HeaderSize
	}
	if this.Header.Length >= 0 {
		if this.Header.Length > normal.MaxBodySize {
			return errors.New("packet size too large"), nil
		}
		if len(datas)-normal.HeaderSize >= int(this.Header.Length) {
			bodyDatas := datas[normal.HeaderSize : normal.HeaderSize+int(this.Header.Length)]
			opcode := this.Header.Opcode
			packet := message.NewPacket(opcode)
			err := packet.DecodeMsg(bodyDatas)
			packet.Size = this.Header.Length
			this.ReadSize += int(this.Header.Length)
			if err == nil {
				this.Header.Reset()
				return nil, packet
			} else {
				return err, nil
			}
		} else {
			return nil, nil
		}
	}
	return errors.New("reader packet failed"), nil
}

func (this *PacketReader) ReaderNatsRpc(datas []byte) (error, *message.NatsRpcPacket) {
	if this.Header.Length == -1 && len(datas) >= normal.HeaderSize {
		headDatas := datas[:normal.HeaderSize]
		err := this.HeaderCodec.Decode(headDatas, this.Header)
		if err != nil {
			return err, nil
		}
		this.ReadSize += normal.HeaderSize
	}
	if this.Header.Length >= 0 {
		if this.Header.Length > normal.MaxBodySize {
			return errors.New("packet size too large"), nil
		}
		if len(datas)-normal.HeaderSize >= int(this.Header.Length) {
			bodyDatas := datas[normal.HeaderSize : normal.HeaderSize+int(this.Header.Length)]
			opcode := this.Header.Opcode
			packet := message.NewRpcPacket(opcode)
			err := packet.DecodeMsg(bodyDatas)
			packet.Size = this.Header.Length
			this.ReadSize += int(this.Header.Length)
			if err == nil {
				this.Header.Reset()
				return nil, packet
			} else {
				return err, nil
			}
		} else {
			return nil, nil
		}
	}
	return errors.New("reader packet failed"), nil
}

func (this *PacketReader) Reset() {
	this.ReadSize = 0
}
