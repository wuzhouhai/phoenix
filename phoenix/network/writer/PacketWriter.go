// Package writer -----------------------------
// @file      : PacketWriter.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/22 11:40
// @desc      :
// -------------------------------------------
package writer

import (
	"bytes"
	"errors"
	"fmt"
	"phoenix/phoenix/entitys/instance/message/header"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/network/codec"
	"phoenix/phoenix/register"
)

type PacketWriter struct {
	HeaderCodec *codec.HeaderCodec
	Header      *header.Header
}

func NewPacketWriter() *PacketWriter {
	return &PacketWriter{
		HeaderCodec: &codec.HeaderCodec{},
		Header:      header.NewHeader(),
	}
}

func (this *PacketWriter) Writer(dataBuffer *bytes.Buffer, packet interfaces.IPacket) error {
	err, msgData := packet.EncodeMsg()
	if err != nil {
		return err
	}
	handler := register.GetSocketHandler(packet.GetOpcode())
	if handler == nil {
		return errors.New(fmt.Sprintf("opcode handler %d not register", packet.GetOpcode()))
	}
	this.Header.Length = int32(len(msgData))
	this.Header.VerifyID = 0
	this.Header.Opcode = packet.GetOpcode()
	this.Header.OpcodeType = handler.OpCodecType
	hErr, hDatas := this.HeaderCodec.Encode(this.Header)
	if hErr != nil {
		return hErr
	}
	dataBuffer.Write(hDatas)
	dataBuffer.Write(msgData)
	return nil
}

func (this *PacketWriter) WriterNatsRpc(dataBuffer *bytes.Buffer, packet interfaces.IPacket) error {
	err, msgData := packet.EncodeMsg()
	if err != nil {
		return err
	}
	handler := register.GetNatsRpcHandler(packet.GetOpcode())
	if handler == nil {
		return errors.New(fmt.Sprintf("opcode handler %d not register", packet.GetOpcode()))
	}
	this.Header.Length = int32(len(msgData))
	this.Header.VerifyID = 0
	this.Header.Opcode = packet.GetOpcode()
	this.Header.OpcodeType = handler.OpCodecType
	hErr, hDatas := this.HeaderCodec.Encode(this.Header)
	if hErr != nil {
		return hErr
	}
	dataBuffer.Write(hDatas)
	dataBuffer.Write(msgData)
	return nil
}
