package network

import "strconv"

type EndPoint struct {
	Address string
	Port    int
}

func NewEndPoint(address string, port int) *EndPoint {
	return &EndPoint{
		Address: address,
		Port:    port,
	}
}

func (ep *EndPoint) String() string {
	return ep.Address + ":" + strconv.Itoa(ep.Port)
}

func (ep *EndPoint) GetAddress() string {
	return ep.Address
}

func (ep *EndPoint) GetPortString() string {
	return strconv.Itoa(ep.Port)
}

func (ep *EndPoint) GetPort() int {
	return ep.Port
}

func (ep *EndPoint) TcpFormat() string {
	return "tcp://" + ep.Address + ":" + strconv.Itoa(ep.Port)
}
