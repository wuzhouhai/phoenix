// Package rpc -----------------------------
// @file      : natsRpc.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/6 16:03
// @desc      :
// -------------------------------------------
package rpc

import (
	"bytes"
	"fmt"
	"github.com/gogf/gf/v2/container/garray"
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/componets/natsComponent/codecNats"
	error2 "phoenix/phoenix/defines/error"
	"phoenix/phoenix/entitys/instance/message"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/objectsManager"
	"phoenix/phoenix/network/handles/funcHandle"
	"phoenix/phoenix/network/writer"
	"phoenix/phoenix/register"
)

type NatsRpc struct {
	rpcHandler   interface{}                 // rpc处理对象
	Packets      chan *message.NatsRpcPacket // 消息处理数组
	codec        *codecNats.CodecNats
	PacketWriter *writer.PacketWriter
	Released     chan bool
}

func NewNatsRpc(rpc interface{}) *NatsRpc {
	return &NatsRpc{
		rpcHandler:   rpc,
		Packets:      make(chan *message.NatsRpcPacket, 1024),
		codec:        codecNats.NewCodecNats(),
		PacketWriter: writer.NewPacketWriter(),
		Released:     make(chan bool),
	}
}

func (this *NatsRpc) AddPacket(p interface{}) {
	if p, ok := p.(*message.NatsRpcPacket); ok {
		logger.PhoenixLoggerIns().Debug(fmt.Sprintf("AddPacket 收到消息 RPC, opcode: %d", p.Opcode))
		this.Packets <- p
	}
}

func (this *NatsRpc) EncodeNatsRpcPacket(p interfaces.IPacket) (error, []byte) {
	if p, ok := (p).(*message.NatsRpcPacket); ok {
		datas := bytes.Buffer{}
		err := this.PacketWriter.WriterNatsRpc(&datas, p)
		if err != nil {
			return err, nil
		}
		return nil, datas.Bytes()
	}
	return gerror.NewCodef(gcode.New(error2.PHOENIX_ERROR_DATA_TYPE_ERROR, "", nil), ""), nil
}

func (this *NatsRpc) ReadBufferBytes(datas []byte) {
	this.codec.ReadBufferBytes(datas)
}

func (this *NatsRpc) Decode() (*garray.Array, error) {
	return this.codec.Decode()
}

func (this *NatsRpc) StartWork() {
	go this.DoMsg()
}

func (this *NatsRpc) Stop() {
	this.Released <- true
}

func (this *NatsRpc) DoMsg() {
	for {
		select {
		case <-this.Released:
			return
		case p := <-this.Packets:
			logger.PhoenixLoggerIns().Debug(fmt.Sprintf("收到RPC消息, opcode: %d", p.Opcode))
			h := register.GetNatsRpcHandler(p.Opcode)
			if h == nil {
				logger.PhoenixLoggerIns().Error(fmt.Sprintf("没有找到对应的RPC消息处理函数，opcode: %d", p.Opcode))
				continue
			}
			if this.rpcHandler == nil {
				logger.PhoenixLoggerIns().Error(fmt.Sprintf("rpc没有找到对应的实体，opcode: %d", p.Opcode))
			}

			funcHandle.HandleProcess(this.rpcHandler, p.Msg, h.AgentHandleName)

			objectsManager.PhoenixManagerObjects().DestroyObject(p)
		}
	}
}
