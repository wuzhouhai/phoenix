// Package codec -----------------------------
// @file      : protubufCodec.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/15 18:03
// @desc      :
// -------------------------------------------
package codec

import (
	"google.golang.org/protobuf/proto"
)

type ProtobufCodec struct {
}

func (this *ProtobufCodec) Encode(obj interface{}) ([]byte, error) {
	data, err := proto.Marshal(obj.(proto.Message))
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (this *ProtobufCodec) Decode(data []byte, obj interface{}) error {
	err := proto.Unmarshal(data, obj.(proto.Message))
	return err
}
