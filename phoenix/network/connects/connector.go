package connects

import (
	"fmt"
	"github.com/gobwas/ws"
	"github.com/panjf2000/gnet/v2"
	"phoenix/apps/examples/phoenix_app_dwys/protocol/golang/protos"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/defines/normal"
	"phoenix/phoenix/defines/opcode"
	"phoenix/phoenix/defines/services"
	"phoenix/phoenix/entitys/instance/bundle"
	"phoenix/phoenix/entitys/instance/message"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/objectsManager"
	"phoenix/phoenix/network/protocol/nrpc/golang/natsRpc"
	"phoenix/phoenix/network/protocol/nrpc/golang/service"
)

type Connector struct {
	Id                  dataType.PhoenixTypeConnectId // 链接ID
	Con                 gnet.Conn                     // 网络连接对象
	ConType             string                        // 网络连接类型
	WebSocketOpCode     ws.OpCode                     // WebSocket协议类型
	Packets             chan *message.Packet          // 客户端消息处理数组
	NatsToClientPackets chan *message.Packet          // 推送到客户端的消息队列
	Released            chan bool
	GameServiceId       string              // 对应游戏服务ID
	Service             interfaces.IService // 属于那个服务
}

func NewConnector(id dataType.PhoenixTypeConnectId, con gnet.Conn, s interfaces.IService) *Connector {
	conn := &Connector{
		Id:                  id,
		Con:                 con,
		ConType:             normal.ConnectTypeSocket,
		Packets:             make(chan *message.Packet, 128),
		NatsToClientPackets: make(chan *message.Packet, 64),
		Released:            make(chan bool),
		GameServiceId:       "",
		Service:             s,
	}
	conn.Init()
	return conn
}

func (this *Connector) StartWork() {
	go this.DoMsg()
}

func (this *Connector) Init() {
	logger.PhoenixLoggerIns().Debugf("Connector 链接初始化, 连接ID: %d", this.Id)
	//ac, _ := objectsManager.PhoenixManagerObjects().CreateObject(entityName.Phoenix_EntityName_Account)
	//acI := ac.(interfaces.IAgentEntity)
	//this.SetAgentEntity(acI)
}

func (this *Connector) SetGameServiceId(gServiceId string) {
	this.GameServiceId = gServiceId
}

func (this *Connector) GetGameServiceId() string {
	return this.GameServiceId
}

func (this *Connector) GetId() dataType.PhoenixTypeConnectId {
	return this.Id
}

func (this *Connector) GetConn() gnet.Conn {
	return this.Con
}

func (this *Connector) CloseConn() {
	if this.Con != nil {
		this.Con.Close()
	}
}

func (this *Connector) Kick() {
	this.CloseConn()
}

func (this *Connector) SetConType(conType string) {
	this.ConType = conType
}

func (this *Connector) GetConType() string {
	return this.ConType
}

func (this *Connector) DisConnect() {
	logger.PhoenixLoggerIns().Debug(fmt.Sprintf("%d 连接断开", this.Id))
	this.Released <- true
	// 构造发送到Game断开连接消息
	if this.GameServiceId != "" {
		packet := message.NewRpcPacket(opcode.Opcode_Nats_Gate2Game_Disconnect)
		if packet != nil {
			defer objectsManager.PhoenixManagerObjects().DestroyObject(packet)
			natsComponent := this.Service.GetNatsComponent()
			if natsComponent != nil {
				loginMsg := packet.GetMsg().(*service.Gate2GameDisconnect)
				loginMsg.Channel = &natsRpc.RpcBase{
					FromSubscribe:   "",
					FromServiceType: "",
					FromServiceId:   "",
					ToSubscribe:     "",
					ConnectorId:     0,
				}
				err := natsComponent.CreateRpcBaseWithServiceId(services.PHOENIX_SERVICE_GAME, this.GameServiceId,
					loginMsg.Channel, this.Id)
				if err != nil {
					logger.PhoenixLoggerIns().Error(fmt.Sprintf("CreateRpcBaseWithServiceId err: %s", err))
					return
				}
			}
			err := this.Service.SendNatsPacket(services.PHOENIX_SERVICE_GAME, this.GameServiceId, packet)
			if err != nil {
				return
			}
		}
	}
}

func (this *Connector) AddPacket(p interface{}) {
	if p, ok := p.(*message.Packet); ok {
		logger.PhoenixLoggerIns().Debug(fmt.Sprintf("AddPacket 收到消息, 连接ID: %d, opcode: %d", this.Id, p.Opcode))
		this.Packets <- p
	}
}

func (this *Connector) AddNatsToClientPacket(p interface{}) {
	if p, ok := p.(*message.Packet); ok {
		logger.PhoenixLoggerIns().Debug(fmt.Sprintf("AddNatsToClientPacket 收到消息, 连接ID: %d, opcode: %d", this.Id, p.Opcode))
		this.NatsToClientPackets <- p
	}
}

func (this *Connector) SetWebSocketOpcode(opcode ws.OpCode) {
	this.WebSocketOpCode = opcode
}

func (this *Connector) GetWebSocketOpcode() ws.OpCode {
	return this.WebSocketOpCode
}

func (this *Connector) DoMsg() {
	for {
		select {
		case <-this.Released:
			return
		case p := <-this.Packets:
			logger.PhoenixLoggerIns().Debug(fmt.Sprintf("收到消息, 连接ID: %d, opcode: %d", this.Id, p.Opcode))
			if p.Opcode > opcode.Opcode_CS_DEFINE_LOGIN_END {
				if this.GameServiceId != "" {
					natsPacket := this.Service.CreateGate2XRpcClientPacket(services.PHOENIX_SERVICE_GAME, this.GameServiceId,
						p, this.Id)
					e := this.Service.GetNatsComponent().SendNatsPacketWithCheck(services.PHOENIX_SERVICE_GAME, this.GameServiceId,
						natsPacket)
					if e != nil {
						logger.PhoenixLoggerIns().Error(fmt.Sprintf("SendNatsPacketWithCheck err: %s", e.Error()))
						// 断开连接
						this.Kick()
						return
					}
				} else {
					logger.PhoenixLoggerIns().Errorf("未找到对应游戏服务, connectId:%d, opcode:%d", this.Id,
						p.Opcode)
					// 断开连接
					this.Kick()
				}
			} else {
				// 转发到登录服务器
				serviceId, e := this.Service.GetNatsComponent().SelectServiceRandom(services.PHOENIX_SERVICE_LOGIN)
				if e != nil {
					logger.PhoenixLoggerIns().Error(fmt.Sprintf("SelectServiceRandom err: %s", e))
					return
				}
				natsPacket := this.Service.CreateGate2XRpcClientPacket(services.PHOENIX_SERVICE_LOGIN, serviceId,
					p, this.Id)
				e = this.Service.GetNatsComponent().SendNatsPacketWithCheck(services.PHOENIX_SERVICE_LOGIN, serviceId,
					natsPacket)
				if e != nil {
					logger.PhoenixLoggerIns().Error(fmt.Sprintf("SendNatsPacketWithCheck err: %s", e))
					return
				}
			}
			objectsManager.PhoenixManagerObjects().DestroyObject(p)
		case p := <-this.NatsToClientPackets:
			e := this.SendPacket(p)
			if e != nil {
				logger.PhoenixLoggerIns().Error(fmt.Sprintf("NatsToCLient SendPacket err: %s", e.Error()))
			}
		}
	}
}

func (this *Connector) SendPacket(p *message.Packet) error {
	sendBundle := bundle.NewBundle()
	sendBundle.AddendReceiver(this.Id)
	sendBundle.AppendPacket(p)
	defer objectsManager.PhoenixManagerObjects().DestroyObject(sendBundle)
	defer objectsManager.PhoenixManagerObjects().DestroyObject(p)
	return sendBundle.SendData()
}

func (this *Connector) OnPreS2CLogin(d interface{}, natsRpcBase *natsRpc.RpcBase) error {
	logger.PhoenixLoggerIns().Debug("Connector OnPreS2CLogin")
	if msg, ok := d.(*protos.S2CLogin); ok {
		if msg.Base.Code != 0 {
			this.Kick()
		} else {
			this.GameServiceId = natsRpcBase.FromServiceId
		}
	}
	return nil
}

func (this *Connector) OnPreS2CKick(d interface{}, natsRpcBase *natsRpc.RpcBase) error {
	this.Kick()
	return nil
}
