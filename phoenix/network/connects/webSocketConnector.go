// Package connects -----------------------------
// @file      : webSocketConnector.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/22 10:57
// @desc      :
// -------------------------------------------
package connects

import (
	ws2 "github.com/gobwas/ws"
	"github.com/panjf2000/gnet/v2"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/defines/normal"
	"phoenix/phoenix/interfaces"
)

type WebSocketConnector struct {
	Connector
}

func NewWebSocketConnector(id dataType.PhoenixTypeConnectId, con gnet.Conn, s interfaces.IService) *WebSocketConnector {
	ws := &WebSocketConnector{
		Connector: *NewConnector(id, con, s),
	}
	ws.SetConType(normal.ConnectTypeWebSocket)
	ws.SetWebSocketOpcode(ws2.OpBinary)
	return ws
}
