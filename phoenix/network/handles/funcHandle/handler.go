// Package funcHandle -----------------------------
// @file      : login.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/4/16 18:05
// @desc      :
// -------------------------------------------
package funcHandle

import (
	"fmt"
	"reflect"
)

func HandleProcess(i interface{}, d interface{}, agentHandleName string) {
	// 使用反射获取类型信息
	value := reflect.ValueOf(i)
	// 查找方法
	method := value.MethodByName(agentHandleName)
	if !method.IsValid() {
		fmt.Printf("Method %s not found\n", agentHandleName)
		return
	}
	// 准备调用方法的参数
	args := []reflect.Value{reflect.ValueOf(d)}
	// 调用方法
	method.Call(args)
}

func HandleProcessNatsRpc(i interface{}, d interface{}, agentHandleName string, natsRpcBase interface{}) {
	// 使用反射获取类型信息
	value := reflect.ValueOf(i)
	// 查找方法
	method := value.MethodByName(agentHandleName)
	if !method.IsValid() {
		fmt.Printf("HandleProcessNatsRpc Method %s not found\n", agentHandleName)
		return
	}
	// 准备调用方法的参数
	args := []reflect.Value{reflect.ValueOf(d), reflect.ValueOf(natsRpcBase)}
	// 调用方法
	method.Call(args)
}
