// Package rpcHandles -----------------------------
// @file      : rpcHandles.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/6 16:15
// @desc      :
// -------------------------------------------
package rpcHandles

import (
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/defines/opcode"
	"reflect"
)

type RpcHandles struct {
	Opcode          dataType.PhoenixTypeOpCode // 消息ID
	OpCodecType     opcode.OpCodeType
	MsgProtoType    reflect.Type // protobuf类型反射
	AgentHandleName string       // 调用agent实体函数名
}

func NewRpcHandles(opcode dataType.PhoenixTypeOpCode, opCodecType opcode.OpCodeType, msgProtoType reflect.Type,
	agentHandleName string) *RpcHandles {
	return &RpcHandles{
		Opcode:          opcode,
		OpCodecType:     opCodecType,
		MsgProtoType:    msgProtoType,
		AgentHandleName: agentHandleName,
	}
}
