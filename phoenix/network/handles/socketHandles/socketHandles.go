// Package socketHandles Package webSocketHandles -----------------------------
// @file      : webSocketHandles.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/18 18:30
// @desc      :
// -------------------------------------------
package socketHandles

import (
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/defines/opcode"
	"reflect"
)

type SocketHandles struct {
	Opcode            dataType.PhoenixTypeOpCode // 消息ID
	OpCodecType       opcode.OpCodeType
	MsgProtoType      reflect.Type // protobuf类型反射
	PreSendHandleName string       // 发送之前的处理函数名,针对connector
	AgentHandleName   string       // 调用agent实体函数名
}

func NewWebSocketHandles(opcode dataType.PhoenixTypeOpCode, opCodecType opcode.OpCodeType, msgProtoType reflect.Type,
	agentHandleName string, preSendHandleName string) *SocketHandles {
	return &SocketHandles{
		Opcode:            opcode,
		OpCodecType:       opCodecType,
		MsgProtoType:      msgProtoType,
		PreSendHandleName: preSendHandleName,
		AgentHandleName:   agentHandleName,
	}
}
