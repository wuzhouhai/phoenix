// Package messageData -----------------------------
// @file      : avatarLogin.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/4/12 16:19
// @desc      :
// -------------------------------------------
package webToServer

import (
	"encoding/json"
	"phoenix/phoenix/defines/dataType"
)

type LoginData struct {
	AccountName string                         `json:"accountName"`
	AvatarId    dataType.PhoenixTypeDataBaseId `json:"avatarId"`
	AvatarName  string                         `json:"avatarName"`
	LoginTime   int64                          `json:"loginTime"`
}

func (this *LoginData) DecodeFromJsonString(content string) error {
	err := json.Unmarshal([]byte(content), this)
	if err != nil {
		return err
	}
	return nil
}
