// Package interfaces -----------------------------
// @file      : interface_entity_model.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/7/1 15:52
// @desc      :
// -------------------------------------------
package interfaces

import (
	"github.com/gogf/gf/v2/container/gmap"
	"phoenix/phoenix/defines/dataType"
)

type IEntityModelData interface {
	Reset()
}

type IBaseModel interface {
	IsPersistent() bool
	Reset()
	SetEntityDBID(entityDBID dataType.PhoenixTypeDataBaseId)
	GetEntityDBID() dataType.PhoenixTypeDataBaseId
}

type IEntityModel interface {
	IBaseModel
	Create() error
	LoadEntityDataFromDB(records interface{}) error
	UpdateEntityData(isSyncToDB bool) error
	SetEntityData(d IEntityModelData)
	GetEntityData() IEntityModelData
	SyncEntityDataToDB() error
	SaveEntityDataToDB() error
}

type IEntityModuleModel interface {
	IBaseModel
	IsSingleData() bool
	SetModuleSingleDataDBID(dbid dataType.PhoenixTypeDataBaseId)
	SetUpdateSingleData()
	SetModelData(dbId dataType.PhoenixTypeDataBaseId, data interface{})
	GetModelData(dbId dataType.PhoenixTypeDataBaseId) interface{}
	GetModelDatas() *gmap.IntAnyMap
	SetModelDataUpdate(dbId dataType.PhoenixTypeDataBaseId)
	SyncModuleToDB() error
	UpdateDataModule(dbId dataType.PhoenixTypeDataBaseId, isSyncToDB bool) error
	SaveModuleToDB() error
	LoadModule() error
}
