// Package interfaces -----------------------------
// @file      : interface_codec.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/22 14:33
// @desc      :
// -------------------------------------------
package interfaces

type ICodec interface {
	Decode(data []byte, obj interface{}) error
	Encode(obj interface{}) ([]byte, error)
}
