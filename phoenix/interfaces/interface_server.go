// Package interfaces -----------------------------
// @file      : interface_server.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/8 11:55
// @desc      :
// -------------------------------------------
package interfaces

type IServer interface {
	OnInit()
	OnAfterInit()
	OnStart()
}
