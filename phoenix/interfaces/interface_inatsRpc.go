// Package interfaces -----------------------------
// @file      : interface_inatsRpc.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/9 17:32
// @desc      :
// -------------------------------------------
package interfaces

import (
	"github.com/gogf/gf/v2/container/garray"
)

type INatsRpc interface {
	AddPacket(p interface{})
	StartWork()
	ReadBufferBytes(datas []byte)
	EncodeNatsRpcPacket(p IPacket) (error, []byte)
	Decode() (*garray.Array, error)
}
