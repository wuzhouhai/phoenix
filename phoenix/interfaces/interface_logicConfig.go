// Package interfaces -----------------------------
// @file      : interface_logicConfig.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/5 14:11
// @desc      :
// -------------------------------------------
package interfaces

import "github.com/gogf/gf/v2/container/gmap"

type ILogicConfig interface {
	InitLogicConfig()
	GetConfigs() *gmap.IntAnyMap
	GetConfigItem(itemKey int32) interface{}
	GetConfigExtendData(itemKey int32) interface{}
	CheckConfigExtendData(itemKey int32) bool
	GetExtendData(extendKey string) interface{}
}
