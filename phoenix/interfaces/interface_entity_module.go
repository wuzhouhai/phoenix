// Package interfaces -----------------------------
// @file      : interface_entity_module.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/7/4 18:53
// @desc      :
// -------------------------------------------
package interfaces

import (
	"phoenix/phoenix/defines/dataType"
)

type IEntityModule interface {
	Init()
	GetEntityModuleName() string
	SetEntityDBId(entityID dataType.PhoenixTypeDataBaseId)
	GetEntityDBId() dataType.PhoenixTypeDataBaseId
	LoadData() error
	SavaData() error
	SyncToDB() error
	GetModuleModel() IEntityModuleModel
	Reset()
}
