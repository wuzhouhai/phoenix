// Package interfaces -----------------------------
// @file      : interface_moduleLogic.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/8 16:15
// @desc      :
// -------------------------------------------
package interfaces

import (
	"github.com/gogf/gf/v2/container/gmap"
	"phoenix/phoenix/defines/dataType"
)

type IModuleLogic interface {
	DoLogic(userId dataType.PhoenixTypeUserId, moduleData dataType.PhoenixTypeModuleData, params *gmap.StrAnyMap) (*gmap.StrAnyMap, error)
}
