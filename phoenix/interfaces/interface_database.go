// Package interfaces -----------------------------
// @file      : interface_database.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/6 17:11
// @desc      :
// -------------------------------------------
package interfaces

import (
	"database/sql"
	"github.com/gogf/gf/v2/database/gdb"
	"phoenix/phoenix/common/database/entityHelper"
)

type IDataBase interface {
	// Init 初始化
	Init()
	// Name 获取数据库名称
	Name() string
	// GroupName 获取数据库组名
	GroupName() string
	GetPrefix() string
	// 同步数据库表结构
	SyncTable() error
	// GetDb 获取数据库操作对象
	GetDb() gdb.DB
	// SaveDataWithMap 保存数据到数据库 tableName 表名 data 数据 类型 Map
	SaveDataWithMap(data gdb.Map, tableNameOrStruct ...interface{}) (result sql.Result, err error)
	SaveDataWithStruct(data interface{}, tableNameOrStruct ...interface{}) (result sql.Result, err error)
	SaveMultiDataWithListMap(data gdb.List, batch int, tableNameOrStruct ...interface{}) (result sql.Result, err error)
	InsertDataWithMap(data gdb.Map, tableNameOrStruct ...interface{}) (result sql.Result, err error)
	InsertDataWithStruct(data interface{}, tableNameOrStruct ...interface{}) (result sql.Result, err error)
	InsertDataWithListMap(data gdb.List, batch int, tableNameOrStruct ...interface{}) (result sql.Result, err error)
	InsertDataWithMapAndGetId(data gdb.Map, tableNameOrStruct ...interface{}) (lastInsertId int64, err error)
	InsertDataWithStructAndGetId(data interface{}, tableNameOrStruct ...interface{}) (lastInsertId int64, err error)
	ReplaceDataWithMap(data gdb.Map, tableNameOrStruct ...interface{}) (result sql.Result, err error)
	ReplaceDataWithStruct(data interface{}, tableNameOrStruct ...interface{}) (result sql.Result, err error)
	ReplaceDataWithListMap(data gdb.List, batch int, tableNameOrStruct ...interface{}) (result sql.Result, err error)
	UpdateEntityData(entityName string, entityData *entityHelper.DBEntity)
}
