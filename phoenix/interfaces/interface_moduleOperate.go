// Package interfaces -----------------------------
// @file      : interface_moduleOperate.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/8 11:32
// @desc      :
// -------------------------------------------
package interfaces

import "phoenix/phoenix/defines/dataType"

type IModuleOperate interface {
	UpdateModuleData(userId dataType.PhoenixTypeUserId, moduleId dataType.PhoenixTypeModuleId, data interface{}) (interface{}, error)
	GetModuleData(userId dataType.PhoenixTypeUserId, moduleId dataType.PhoenixTypeModuleId) (map[string]interface{}, error)
}
