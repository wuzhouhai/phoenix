// Package interfaces -----------------------------
// @file      : interface_lua.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/10/22 11:24
// @desc      :
// -------------------------------------------
package interfaces

import lua "github.com/yuin/gopher-lua"

type ILua interface {
	GetLuaState() *lua.LState
	RegisterModules()
	Close()
}

type IEntityLua interface {
	ILua
}
