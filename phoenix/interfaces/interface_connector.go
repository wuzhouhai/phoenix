package interfaces

import (
	"github.com/gobwas/ws"
	"github.com/panjf2000/gnet/v2"
	"phoenix/phoenix/defines/dataType"
)

type IConnector interface {
	Init()
	StartWork()
	GetId() dataType.PhoenixTypeConnectId
	GetConn() gnet.Conn
	SetConType(conType string)
	GetConType() string
	SetWebSocketOpcode(opcode ws.OpCode)
	GetWebSocketOpcode() ws.OpCode
	DisConnect()
	CloseConn()
	Kick()
	AddPacket(p interface{})
	AddNatsToClientPacket(p interface{})
	SetGameServiceId(gServiceId string)
	GetGameServiceId() string
}
