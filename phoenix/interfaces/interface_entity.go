package interfaces

import (
	"phoenix/phoenix/defines/dataType"
)

type IEntity interface {
	Init()
	BindEntityModel(e IEntityModel)
	GetModule(moduleName string) IEntityModule
	GetEntityModel() IEntityModel
	GetId() dataType.PhoenixTypeEntityId
	GetName() string
	FrameLogic()
	OnEnterWorld()
	OnLeaveWorld()
	ResetObject() // 实体被放回对象池,重置
	Release()
	SaveToDB()
	SetService(service IService)
}

type IAgentEntity interface {
	IEntity
	OnAvatarLogin(avatarId dataType.PhoenixTypeDataBaseId, avatarName string, accountName string) error
	ReAvatarLogin(avatarId dataType.PhoenixTypeDataBaseId, avatarName string, accountName string) error
	SetConnectId(connId dataType.PhoenixTypeConnectId)
	GetConnectId() dataType.PhoenixTypeConnectId
	SetAccountDBID(dbid dataType.PhoenixTypeDataBaseId)
	GetAccountDBID() dataType.PhoenixTypeDataBaseId
	DisConnect()
	BeKick(msg string)
	SetGateService(gateId string)
	GetGateService() string
}
