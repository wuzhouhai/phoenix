// Package interfaces -----------------------------
// @file      : interface_phoenix.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/4/16 15:15
// @desc      :
// -------------------------------------------
package interfaces

import "phoenix/phoenix/common/config"

type IPhoenix interface {
	GetServerKey() string
	GetAgentEntityName() string
	GetSyncInterval() int
	AllowReplace() bool
	GetOptions() *config.PhoenixOptions
	GetServerCachePrefix() string
}
