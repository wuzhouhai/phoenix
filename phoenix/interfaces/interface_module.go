// Package interfaces -----------------------------
// @file      : interface_module.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/8 17:09
// @desc      :
// -------------------------------------------
package interfaces

import (
	"github.com/gogf/gf/v2/container/gmap"
	"phoenix/phoenix/defines/dataType"
)

type IModule interface {
	GetModuleName() string
	GetModuleId() dataType.PhoenixTypeModuleId
	RegisterModuleLogic(logicName string, logic IModuleLogic) error
	DoLogic(logicName string, userId dataType.PhoenixTypeUserId, moduleData dataType.PhoenixTypeModuleData, params *gmap.StrAnyMap) (*gmap.StrAnyMap, error)
	NewModuleData() dataType.PhoenixTypeModuleData
	Init()
}
