package interfaces

import "phoenix/phoenix/defines/dataType"

type ITask interface {
	GetConId() dataType.PhoenixTypeConnectId
	DoTask()
}
