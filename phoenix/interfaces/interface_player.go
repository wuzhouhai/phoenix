// Package interfaces -----------------------------
// @file      : interface_player.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/25 10:16
// @desc      :
// -------------------------------------------
package interfaces

import "phoenix/phoenix/defines/dataType"

type IPlayer interface {
	GetId() dataType.PhoenixTypeDataBaseId
	GetConnectorId() dataType.PhoenixTypeConnectId
	SetConnectorId(conId dataType.PhoenixTypeConnectId)
	SetGateServiceId(gateServiceId string)
	GetGateServiceID() string
	SetAgentEntity(agentEntity IAgentEntity)
	GetAgentEntity() IAgentEntity
	DisConnect()
	AddGamePacket(p interface{})
	OnFrame()
	StartWork()
	Kick(msg string)
}
