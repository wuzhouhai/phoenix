// Package interfaces -----------------------------
// @file      : interface_packet.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/19 16:36
// @desc      :
// -------------------------------------------
package interfaces

import "phoenix/phoenix/defines/dataType"

type IPacket interface {
	IEntity
	GetOpcode() dataType.PhoenixTypeOpCode
	EncodeMsg() (error, []byte)
	DecodeMsg(msgData []byte) error
}

type INatsPacket interface {
	IPacket
}
