package interfaces

import "phoenix/phoenix/defines/dataType"

type IComponent interface {
	Init() error
	Start() error
	Stop()
	GetId() string
	GetName() string
	SendMsg(receiver interface{}, msg interface{}) error
	ReceiveMsg(msg interface{}) error
}

type INatsComponent interface {
	IComponent
	DiscoverService(serviceType string, serviceId string, serviceSubscribe string)
	OfflineService(serviceType string, serviceId string)
	ServiceOnHello(serviceType string, serviceId string)
	SelectGameService(accountId dataType.PhoenixTypeDataBaseId) (string, error)
	SelectServiceRandom(serviceType string) (string, error)
	SelectService(serviceType string, indexId int) (string, error)
	SendNatsPacketWithCheck(serviceType string, serviceId string, p IPacket) error
	CreateRpcBaseWithServiceId(serviceType string, serviceId string, channel interface{}, connectorId dataType.PhoenixTypeConnectId) error
}
