// Package interfaces -----------------------------
// @file      : interface_routine.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2023/12/21 15:33
// @desc      :
// -------------------------------------------
package interfaces

import (
	"phoenix/phoenix/defines/dataType"
)

type IService interface {
	CreateComponents()
	Init() error
	Start() error
	Process()
	Stop()
	GetId() string
	GetName() string
	GetType() string
	AddComponent(comId string, component IComponent)
	GetNatsComponent() INatsComponent
	DiscoverService(serviceType string, serviceId string, serviceSubscribe string)
	OfflineService(serviceType string, serviceId string)
	SelectGameService(accountId dataType.PhoenixTypeDataBaseId) string
	SendNatsPacket(serviceType string, serviceId string, p IPacket) error
	SendNatsPacketWithType(serviceType string, p IPacket) error
	CreateX2GateRpcClientPacket(serviceId string, p IPacket, connectorId dataType.PhoenixTypeConnectId) INatsPacket
	CreateGate2XRpcClientPacket(serviceType string, serviceId string, p IPacket,
		connectorId dataType.PhoenixTypeConnectId) INatsPacket
	SetLua(l ILua)
}
