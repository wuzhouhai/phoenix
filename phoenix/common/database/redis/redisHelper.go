// Package redis -----------------------------
// @file      : redisHelper.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/19 10:52
// @desc      :
// -------------------------------------------
package redis

import (
	"context"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/redis/go-redis/v9"
	"phoenix/phoenix/common/config"
	"time"
)

type RedisHelper struct {
	ctx         context.Context
	options     *config.RedisOptions
	RedisClient *redis.Client
}

func (this *RedisHelper) Init() {
	this.RedisClient = redis.NewClient(&redis.Options{
		Addr:     this.options.Host + ":" + gconv.String(this.options.Port),
		Password: this.options.Pass,
		DB:       this.options.DB,
	})
}

func (this *RedisHelper) Put(key string, value interface{}, expire int) error {
	return this.RedisClient.Set(this.ctx, key, value, time.Duration(expire)*time.Second).Err()
}
func (this *RedisHelper) Get(key string) (string, error) {
	return this.RedisClient.Get(this.ctx, key).Result()
}

func (this *RedisHelper) Del(key string) error {
	return this.RedisClient.Del(this.ctx, key).Err()
}
func (this *RedisHelper) Exists(key string) bool {
	return this.RedisClient.Exists(this.ctx, key).Val() > 0
}

func NewRedisHelper(op *config.RedisOptions) *RedisHelper {
	redisHelper := &RedisHelper{
		ctx:         context.Background(),
		options:     op,
		RedisClient: nil,
	}
	redisHelper.Init()
	return redisHelper
}
