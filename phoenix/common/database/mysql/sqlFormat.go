// Package mysql -----------------------------
// @file      : sqlFormat.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/4/2 15:43
// @desc      :
// -------------------------------------------
package mysql

import (
	"github.com/gogf/gf/v2/container/garray"
	"github.com/gogf/gf/v2/text/gstr"
	"phoenix/phoenix/common/database/entityHelper"
	"phoenix/phoenix/common/logger"
	"strconv"
	"strings"
)

const DEFAULTE_CHAR_LENGTH = 255
const SQLTABLE = " "
const SQLFEILD_START = "("
const SQLFEILD_END = ")"

func StartCreateTable(tableName string, sqlBuilder *strings.Builder) {
	sqlBuilder.WriteString("CREATE TABLE IF NOT EXISTS `")
	sqlBuilder.WriteString(tableName)
	sqlBuilder.WriteString("`")
	sqlBuilder.WriteString(SQLTABLE)
}

func FillField(dbFiled *entityHelper.DBEntityField, sqlBuilder *strings.Builder) {
	// 处理列名和类型
	sqlBuilder.WriteString("`")
	sqlBuilder.WriteString(dbFiled.FieldName)
	sqlBuilder.WriteString("`")
	sqlBuilder.WriteString(SQLTABLE)
	sqlBuilder.WriteString(dbFiled.FieldType)
	// 处理长度
	isChar := gstr.Compare(dbFiled.FieldType, "char") == 0 || gstr.Compare(dbFiled.FieldType, "varchar") == 0 ||
		gstr.Compare(dbFiled.FieldType, "CHAR") == 0 || gstr.Compare(dbFiled.FieldType, "VARCHAR") == 0
	if isChar {
		charLength := DEFAULTE_CHAR_LENGTH
		if dbFiled.FieldLength != 0 {
			charLength = dbFiled.FieldLength
		}
		sqlBuilder.WriteString("(")
		sqlBuilder.WriteString(strconv.Itoa(charLength))
		sqlBuilder.WriteString(")")
		sqlBuilder.WriteString(SQLTABLE)
	}
	// 处理无符号,自增长等内容,注意,这里暂时没有全类型检测是否能设置这个参数
	sqlBuilder.WriteString(SQLTABLE)
	if dbFiled.FieldIsPrimaryKey == 1 {
		sqlBuilder.WriteString("PRIMARY KEY")
		sqlBuilder.WriteString(SQLTABLE)
	}
	if dbFiled.FieldUnsigned == 1 && !isChar && dbFiled.FieldIsPrimaryKey != 1 {
		sqlBuilder.WriteString("UNSIGNED")
		sqlBuilder.WriteString(SQLTABLE)
	}
	if dbFiled.FieldAutoIncrement == 1 && !isChar {
		sqlBuilder.WriteString("AUTO_INCREMENT")
		sqlBuilder.WriteString(SQLTABLE)
	}
	if dbFiled.FieldNotNull == 1 {
		sqlBuilder.WriteString("NOT NULL")
		sqlBuilder.WriteString(SQLTABLE)
	}

	// 插入默认值
	if dbFiled.FieldSetDefault == 1 {
		if isChar {
			sqlBuilder.WriteString("DEFAULT '")
			sqlBuilder.WriteString(dbFiled.FieldDefault)
			sqlBuilder.WriteString("'")
			sqlBuilder.WriteString(SQLTABLE)
		} else {
			sqlBuilder.WriteString("DEFAULT ")
			sqlBuilder.WriteString(dbFiled.FieldDefault)
			sqlBuilder.WriteString(SQLTABLE)
		}
	}

	// 插入描述
	if len(dbFiled.FieldDec) > 0 {
		sqlBuilder.WriteString("COMMENT '")
		sqlBuilder.WriteString(dbFiled.FieldDec)
		sqlBuilder.WriteString("'")
		sqlBuilder.WriteString(SQLTABLE)
	}
}

func FillIndexKeys(indexKeys *garray.Array, sqlBuilder *strings.Builder) {
	if indexKeys.IsEmpty() {
		return
	}
	indexKeys.Iterator(func(k int, v interface{}) bool {
		dbField := v.(entityHelper.DBEntityField)
		if dbField.FieldIndex == 1 {
			if dbField.FieldIndexType != "" {
				if gstr.Compare(dbField.FieldIndexType, "UNIQUE") != 0 &&
					gstr.Compare(dbField.FieldIndexType, "FULLTEXT") != 0 &&
					gstr.Compare(dbField.FieldIndexType, "INDEX") != 0 {
					logger.PhoenixLoggerIns().Errorf("索引类型错误,索引字段名:%s, 索引类型为:%s",
						dbField.FieldName, dbField.FieldIndexType)
					return true
				}
			}
			sqlBuilder.WriteString(",")
			sqlBuilder.WriteString(SQLTABLE)
			FillIndex(dbField.FieldName, dbField.FieldIndexType, sqlBuilder)
		}
		return true
	})
}

func FillIndex(indexKey string, indexType string, sqlBuilder *strings.Builder) {
	if indexType == "" || gstr.Compare(indexType, "INDEX") == 0 {
		sqlBuilder.WriteString("INDEX")
		sqlBuilder.WriteString(SQLTABLE)
	} else {
		sqlBuilder.WriteString(indexType)
		sqlBuilder.WriteString(SQLTABLE)
		sqlBuilder.WriteString("INDEX")
		sqlBuilder.WriteString(SQLTABLE)
	}
	sqlBuilder.WriteString("idx_")
	sqlBuilder.WriteString(indexKey)
	sqlBuilder.WriteString("(")
	sqlBuilder.WriteString(indexKey)
	sqlBuilder.WriteString(")")
}

func EndCreateTable(sqlBuilder *strings.Builder) {
	sqlBuilder.WriteString("ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;")
}

func StartAlterTable(tableName string, sqlBuilder *strings.Builder) {
	sqlBuilder.WriteString("ALTER TABLE `")
	sqlBuilder.WriteString(tableName)
	sqlBuilder.WriteString("`")
	sqlBuilder.WriteString(SQLTABLE)
}

func AlterTableField(dbFiled entityHelper.DBEntityField, sqlBuilder *strings.Builder) {
	sqlBuilder.WriteString("ADD ")
	sqlBuilder.WriteString(dbFiled.FieldName)
	sqlBuilder.WriteString(SQLTABLE)
	sqlBuilder.WriteString(dbFiled.FieldType)

	// 处理长度
	isChar := gstr.Compare(dbFiled.FieldType, "char") == 0 || gstr.Compare(dbFiled.FieldType, "varchar") == 0 ||
		gstr.Compare(dbFiled.FieldType, "CHAR") == 0 || gstr.Compare(dbFiled.FieldType, "VARCHAR") == 0
	if isChar {
		charLength := DEFAULTE_CHAR_LENGTH
		if dbFiled.FieldLength != 0 {
			charLength = dbFiled.FieldLength
		}
		sqlBuilder.WriteString("(")
		sqlBuilder.WriteString(strconv.Itoa(charLength))
		sqlBuilder.WriteString(")")
		sqlBuilder.WriteString(SQLTABLE)
	}
	// 处理无符号,自增长等内容,注意,这里暂时没有全类型检测是否能设置这个参数
	sqlBuilder.WriteString(SQLTABLE)
	if dbFiled.FieldUnsigned == 1 && !isChar {
		sqlBuilder.WriteString("UNSIGNED")
		sqlBuilder.WriteString(SQLTABLE)
	}
	if dbFiled.FieldAutoIncrement == 1 && !isChar {
		sqlBuilder.WriteString("AUTO_INCREMENT")
		sqlBuilder.WriteString(SQLTABLE)
	}
	if dbFiled.FieldNotNull == 1 {
		sqlBuilder.WriteString("NOT NULL")
		sqlBuilder.WriteString(SQLTABLE)
	}

	// 插入默认值
	if dbFiled.FieldSetDefault == 1 {
		if isChar {
			sqlBuilder.WriteString("DEFAULT '")
			sqlBuilder.WriteString(dbFiled.FieldDefault)
			sqlBuilder.WriteString("'")
			sqlBuilder.WriteString(SQLTABLE)
		} else {
			sqlBuilder.WriteString("DEFAULT ")
			sqlBuilder.WriteString(dbFiled.FieldDefault)
			sqlBuilder.WriteString(SQLTABLE)
		}
	}

	// 插入描述
	if len(dbFiled.FieldDec) > 0 {
		sqlBuilder.WriteString("COMMENT '")
		sqlBuilder.WriteString(dbFiled.FieldDec)
		sqlBuilder.WriteString("'")
		sqlBuilder.WriteString(SQLTABLE)
	}
}

func EndAlterTable(sqlBuilder *strings.Builder) {
	sqlBuilder.WriteString(";")
}
