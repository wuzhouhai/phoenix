// Package database -----------------------------
// @file      : mysqlDataBase.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/6 17:10
// @desc      :
// -------------------------------------------
package database

import (
	"context"
	"github.com/gogf/gf/v2/container/garray"
	"github.com/gogf/gf/v2/container/gmap"
	"github.com/spf13/viper"
	"phoenix/phoenix/base"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/common/database/entityHelper"
	"phoenix/phoenix/common/database/mysql"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/resManager"
	"phoenix/phoenix/utils"
	"strings"
)

type MysqlDatabase struct {
	base.BaseDataBase
}

func (this *MysqlDatabase) Init() {
	this.BaseDataBase.Init()
	if this.Options.LoadEntity {
		err := this.SyncTable()
		if err != nil {
			logger.PhoenixLoggerIns().Errorf("mysql database init error: %s", err.Error())
		}
	}
}

func (this *MysqlDatabase) CheckTableIsExist(tableName string) (bool, error) {
	sqlStrBuilder := strings.Builder{}
	sqlStrBuilder.WriteString("SHOW TABLES LIKE '" + tableName + "';")
	re, err := this.GetDb().Query(context.TODO(), sqlStrBuilder.String())
	if err != nil {
		return false, err
	}
	if re.Len() > 0 {
		return true, nil
	}
	return false, nil
}

func (this *MysqlDatabase) QueryDifferenceField(tableName string, configFields []entityHelper.DBEntityField) *gmap.StrAnyMap {
	sqlStrBuilder := strings.Builder{}
	fieldMap := gmap.StrAnyMap{}
	for _, configField := range configFields {
		fieldMap.Set(configField.FieldName, configField)
	}
	sqlStrBuilder.WriteString("SHOW COLUMNS FROM " + tableName)
	reColumns, err := this.GetDb().Query(context.TODO(), sqlStrBuilder.String())
	if err != nil {
		panic(err)
	}
	allColumns := reColumns.List()
	for _, column := range allColumns {
		if fieldMap.Contains(column["Field"].(string)) {
			fieldMap.Remove(column["Field"].(string))
		}
	}
	return &fieldMap
}

func (this *MysqlDatabase) SyncTable() error {
	if this.Options.LoadEntity {
		entityPath := resManager.PhoenixManagerRes().FindPath("entities_phoenix_" + this.Options.GroupName)
		err := this.SyncTableFormConfig(entityPath)
		if err != nil {
			return err
		}
		entityPath = resManager.PhoenixManagerRes().FindPath("entities_" + this.Options.GroupName)
		err = this.SyncTableFormConfig(entityPath)
		if err != nil {
			return err
		}
		// 加载模块共享字段信息
		moduleSharePath := resManager.PhoenixManagerRes().FindPath("entities_phoenix_module_share")
		err = this.AnalysisModuleShareField(moduleSharePath)
		if err != nil {
			return err
		}
		// 创建模块表
		this.EntityData.Iterator(func(k string, v interface{}) bool {
			modulePathName := utils.ToLower("module_" + k)
			entityModulePath := resManager.PhoenixManagerRes().FindPath(modulePathName)
			err = this.SyncModuleTableFormConfig(k, entityModulePath)
			if err != nil {
				logger.PhoenixLoggerIns().Errorf("sync module table error: %s", err.Error())
				return false
			}
			return true
		})
	}
	return nil
}

func (this *MysqlDatabase) SyncTableFormConfig(entityPath string) error {
	entityConfigFlies, err := utils.FindFilesByType(entityPath, "yaml", false)
	if err != nil {
		return err
	}
	// 遍历配置创建表
	yamlConfig := viper.GetViper()
	yamlConfig.AddConfigPath(entityPath)
	yamlConfig.SetConfigType("yaml")
	for _, entityConfigFile := range entityConfigFlies {
		yamlConfig.SetConfigName(entityConfigFile)
		if err := yamlConfig.ReadInConfig(); err != nil {
			panic(err)
		}
		dbEntity := entityHelper.NewDBEntity()
		if err := yamlConfig.Unmarshal(dbEntity); err != nil {
			panic(err)
		}
		if this.CheckEntityIsExist(dbEntity.EntityName) {
			logger.PhoenixLoggerIns().Errorf("entity %s is exist", dbEntity.EntityName)
			continue
		}
		this.UpdateEntityData(dbEntity.EntityName, dbEntity)
		if err := this.SyncTableByEntity(dbEntity); err != nil {
			return err
		}
	}
	return nil
}

func (this *MysqlDatabase) SyncModuleTableFormConfig(entityName string, configPath string) error {
	entityConfigFlies, err := utils.FindFilesByType(configPath, "yaml", false)
	if err != nil {
		return err
	}
	if !this.CheckEntityIsExist(entityName) {
		logger.PhoenixLoggerIns().Errorf("entity %s is not exist,module init failed", entityName)
		return nil
	}
	// 遍历配置创建表
	yamlConfig := viper.GetViper()
	yamlConfig.AddConfigPath(configPath)
	yamlConfig.SetConfigType("yaml")
	for _, entityConfigFile := range entityConfigFlies {
		yamlConfig.SetConfigName(entityConfigFile)
		if err := yamlConfig.ReadInConfig(); err != nil {
			panic(err)
		}
		dbEntity := entityHelper.NewDBEntity()
		if err := yamlConfig.Unmarshal(dbEntity); err != nil {
			panic(err)
		}
		if this.CheckModuleExist(entityName, dbEntity.EntityName) {
			logger.PhoenixLoggerIns().Errorf("entity %s module %s is exist", entityName, dbEntity.EntityName)
			continue
		}
		this.AppendModuleName(entityName, dbEntity.EntityName)
		dbEntity.TableName = "module_" + dbEntity.TableName
		// 将共享字段添加到字段列表中
		for _, moduleShareField := range this.GetModuleShareFields() {
			dbEntity.Fields = append(dbEntity.Fields, *moduleShareField.(*entityHelper.DBEntityField))
		}
		if err := this.SyncTableByEntity(dbEntity); err != nil {
			return err
		}
	}
	return nil
}

func (this *MysqlDatabase) SyncTableByEntity(entity *entityHelper.DBEntity) error {
	sqlStrBuilder := &strings.Builder{}
	tableName := entity.EntityName
	if entity.TableName != "" {
		tableName = entity.TableName
	}
	if this.GetPrefix() != "" {
		tableName = this.GetPrefix() + tableName
	}
	if exist, err := this.CheckTableIsExist(tableName); err != nil {
		return err
	} else if exist {
		differenceFields := this.QueryDifferenceField(tableName, entity.Fields)
		if !differenceFields.IsEmpty() {
			mysql.StartAlterTable(tableName, sqlStrBuilder)
			allCount := differenceFields.Size()
			fillCount := 0
			differenceFields.Iterator(func(key string, value interface{}) bool {
				dbField := value.(entityHelper.DBEntityField)
				mysql.AlterTableField(dbField, sqlStrBuilder)
				fillCount += 1
				if fillCount == allCount {
					mysql.EndAlterTable(sqlStrBuilder)
				} else {
					sqlStrBuilder.WriteString(",")
				}
				return true
			})
			_, sqlErr := this.GetDb().Exec(context.TODO(), sqlStrBuilder.String())
			if sqlErr != nil {
				return sqlErr
			}
		}
		return nil
	}
	mysql.StartCreateTable(tableName, sqlStrBuilder)
	sqlStrBuilder.WriteString(mysql.SQLFEILD_START)
	indexKeys := garray.New()
	for i, dbEntityField := range entity.Fields {
		mysql.FillField(&dbEntityField, sqlStrBuilder)
		if dbEntityField.FieldIndex == 1 {
			indexKeys.Append(dbEntityField)
		}
		if i != len(entity.Fields)-1 {
			sqlStrBuilder.WriteString(",")
		}
	}
	mysql.FillIndexKeys(indexKeys, sqlStrBuilder)
	sqlStrBuilder.WriteString(mysql.SQLFEILD_END)
	sqlStrBuilder.WriteString(mysql.SQLTABLE)
	mysql.EndCreateTable(sqlStrBuilder)

	_, err := this.GetDb().Exec(context.TODO(), sqlStrBuilder.String())
	if err != nil {
		return err
	}

	return nil
}

func (this *MysqlDatabase) AnalysisModuleShareField(moduleSharePath string) error {
	moduleShareConfigFlies, err := utils.FindFilesByType(moduleSharePath, "yaml", false)
	if err != nil {
		return err
	}
	// 遍历配置创建表
	yamlConfig := viper.GetViper()
	yamlConfig.AddConfigPath(moduleSharePath)
	yamlConfig.SetConfigType("yaml")
	for _, configFile := range moduleShareConfigFlies {
		yamlConfig.SetConfigName(configFile)
		if err := yamlConfig.ReadInConfig(); err != nil {
			panic(err)
		}
		dbFields := entityHelper.NewDBEntityModuleFields()
		if err := yamlConfig.Unmarshal(dbFields); err != nil {
			panic(err)
		}
		// 遍历Fields增加到字典中
		for _, dbField := range dbFields.Fields {
			if this.CheckModuleShareField(dbField.FieldName) {
				logger.PhoenixLoggerIns().Errorf("module share field %s is exist", dbField.FieldName)
				continue
			}
			this.AppendModuleShareField(&dbField)
		}
	}
	return nil
}

func NewMysqlDatabase(ctx context.Context, option config.DatabaseOption) (interfaces.IDataBase, error) {
	return &MysqlDatabase{
		BaseDataBase: base.NewBaseDateBase(ctx, option),
	}, nil
}
