// Package entityHelper -----------------------------
// @file      : dbEntityField.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/4/2 13:54
// @desc      :
// -------------------------------------------
package entityHelper

type DBEntityField struct {
	FieldName          string // 字段名称
	FieldType          string // 字段类型
	FieldLength        int    // 字段长度
	FieldNotNull       int    // 是否非空
	FieldUnsigned      int    // 是否无符号
	FieldIsPrimaryKey  int    // 是否主键
	FieldAutoIncrement int    // 是否自增
	FieldSetDefault    int    // 是否设置默认值
	FieldDefault       string // 默认值
	FieldDec           string // 字段描述
	FieldIndex         int    // 是否索引
	FieldIndexType     string // 索引类型
}
