// Package entityHelper -----------------------------
// @file      : dbEntity.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/4/2 13:51
// @desc      :
// -------------------------------------------
package entityHelper

type DBEntity struct {
	EntityName string          // 实体名称
	TableName  string          // 表名称,可以没有
	Fields     []DBEntityField // 字段信息 DBEntityField
}

func NewDBEntity() *DBEntity {
	return &DBEntity{}
}

type DBEntityModuleFields struct {
	Fields []DBEntityField
}

func NewDBEntityModuleFields() *DBEntityModuleFields {
	return &DBEntityModuleFields{}
}
