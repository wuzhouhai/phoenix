package logger

import (
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"os"
	"phoenix/phoenix/common/config"
	"sync"
)

type PhoenixLogger struct {
	logger *zap.Logger
}

func NewPhoenixLogger(op *config.PhoenixOptions) *PhoenixLogger {
	phoenixLogger := new(PhoenixLogger)
	atomicLevel := zap.NewAtomicLevel()
	switch op.LogOptions.LogLevel {
	case "Debug":
		atomicLevel.SetLevel(zap.DebugLevel)
	case "Info":
		atomicLevel.SetLevel(zap.InfoLevel)
	case "Warn":
		atomicLevel.SetLevel(zap.WarnLevel)
	case "Error":
		atomicLevel.SetLevel(zap.ErrorLevel)
	case "Fatal":
		atomicLevel.SetLevel(zap.FatalLevel)
	default:
		atomicLevel.SetLevel(zap.InfoLevel)
	}
	encoderConfig := zapcore.EncoderConfig{
		TimeKey:        "time",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "line",
		MessageKey:     "msg",
		FunctionKey:    "func",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.CapitalColorLevelEncoder,
		EncodeTime:     zapcore.TimeEncoderOfLayout("2006-01-02 15:04:05"),
		EncodeDuration: zapcore.SecondsDurationEncoder,
		//EncodeCaller:   zapcore.FullCallerEncoder,
		EncodeName: zapcore.FullNameEncoder,
	}
	writer := &lumberjack.Logger{
		Filename:   fmt.Sprintf("%s/phoenix_log.log", op.LogOptions.LogPath),
		MaxSize:    100,
		MaxAge:     30,
		MaxBackups: 10,
		LocalTime:  true,
		Compress:   false,
	}
	zapCore := zapcore.NewCore(
		zapcore.NewConsoleEncoder(encoderConfig),
		zapcore.NewMultiWriteSyncer(zapcore.AddSync(writer), zapcore.AddSync(os.Stdout)),
		atomicLevel,
	)
	//phoenixLogger.logger = zap.New(zapCore, zap.AddCaller())
	phoenixLogger.logger = zap.New(zapCore)
	logger = phoenixLogger
	return phoenixLogger
}

func (lg *PhoenixLogger) Debug(msg string, fields ...zapcore.Field) {
	lg.logger.Debug(msg, fields...)
}

func (lg *PhoenixLogger) Info(msg string, fields ...zapcore.Field) {
	lg.logger.Info(msg, fields...)
}

func (lg *PhoenixLogger) Warn(msg string, fields ...zapcore.Field) {
	lg.logger.Warn(msg, fields...)
}

func (lg *PhoenixLogger) Error(msg string, fields ...zapcore.Field) {
	lg.logger.Error(msg, fields...)
}

func (lg *PhoenixLogger) Fatal(msg string, fields ...zapcore.Field) {
	lg.logger.Fatal(msg, fields...)
}

func (lg *PhoenixLogger) Debugf(template string, args ...interface{}) {
	lg.logger.Sugar().Debugf(template, args...)
}

func (lg *PhoenixLogger) Infof(template string, args ...interface{}) {
	lg.logger.Sugar().Infof(template, args...)
}

func (lg *PhoenixLogger) Warnf(template string, args ...interface{}) {
	lg.logger.Sugar().Warnf(template, args...)
}

func (lg *PhoenixLogger) Errorf(template string, args ...interface{}) {
	lg.logger.Sugar().Errorf(template, args...)
}

func (lg *PhoenixLogger) Fatalf(template string, args ...interface{}) {
	lg.logger.Sugar().Fatalf(template, args...)
}

// 实现一个单例
var logger *PhoenixLogger
var once sync.Once

func PhoenixLoggerIns() *PhoenixLogger {
	return logger
}
