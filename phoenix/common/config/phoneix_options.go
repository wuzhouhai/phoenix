package config

import "github.com/gogf/gf/v2/database/gdb"

type LoggerOptions struct {
	LogLevel    string // 日志级别
	LogPath     string // 日志路径
	LogFileName string // 日志文件名
}

type DatabaseOption struct {
	gdb.ConfigNode
	GroupName  string
	LoadEntity bool
}

type ProxyOptions struct {
	Host string // 网络地址
	Port int    // 端口
}

type Options struct {
	// 对外网络接口配置 start
	Proxy     ProxyOptions // 端口信息
	Multicore bool         // 是否开启多核
	// 对外网络接口配置 end
	Web WebOptions // web配置
}

type ComponentOptions struct {
	Name    string  // 组件名称
	Options Options // 组件配置
}

type RedisOptions struct {
	Host string // 网络地址
	Port int    // 端口
	Pass string // 密码
	DB   int    // 数据库
}

type NatsOptions struct {
	Host       string // 网络地址
	Port       int    // 端口
	EnableAuth bool   // 是否开启认证
	User       string // 用户名
	Pass       string // 密码
}

type EtcdOptions struct {
	Host       string // 网络地址
	Port       int    // 端口
	EnableAuth bool   // 是否开启认证
	User       string // 用户名
	Pass       string // 密码
}

type ServiceOptions struct {
	Name       string             // 协程名称
	Type       string             // 协程类型
	Components []ComponentOptions // 组件配置
}

type PhoenixOptions struct {
	ServerKey        string        // 服务器秘钥
	ServerName       string        // 服务器名称
	ServerID         int           // 服务器ID
	LogOptions       LoggerOptions // 日志配置
	AgentEntityName  string        // 代理实体名称
	SyncInterval     int           // 同步间隔
	AllowReplace     bool          // 是否允许挤号
	PerformancePrint bool          // 是否开启性能日志
	PerformancePort  int           // 性能日志端口
	RedisOptions
	NatsOptions
	EtcdOptions
	DatabaseOptions []DatabaseOption   // 数据库配置
	Components      []ComponentOptions // 组件配置
	Services        []ServiceOptions   // 协程配置
}

type WebOptions struct {
	GServerOptions []ghttpServerOption
}

type WebSocketOptions struct {
	Host      string // 网络地址
	Port      int    // 端口
	Multicore bool   // 是否开启多核
}

type ghttpServerOption struct {
	ConfigKey   string
	ConfigValue interface{}
}

func NewPhoenixOptions() *PhoenixOptions {
	return &PhoenixOptions{}
}

func NewPhoenixComponentOptions(name string) *ComponentOptions {
	return &ComponentOptions{
		Name: name,
	}
}
