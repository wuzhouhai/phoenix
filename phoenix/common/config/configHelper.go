package config

import (
	"github.com/spf13/viper"
	"phoenix/phoenix/defines/files"
	"phoenix/phoenix/managers/resManager"
)

func LoadPhoenixConfig() *PhoenixOptions {
	configPath := resManager.PhoenixManagerRes().FindResPath(files.FILE_CONFIG_FILE, false)
	if configPath == "" {
		panic("config file not found")
	}
	yamlConfig := viper.GetViper()
	yamlConfig.AddConfigPath(configPath)
	yamlConfig.SetConfigName(files.FILE_CONFIG_FILE)
	yamlConfig.SetConfigType("yaml")
	if err := yamlConfig.ReadInConfig(); err != nil {
		panic(err)
	}
	po := NewPhoenixOptions()
	if err := yamlConfig.Unmarshal(po); err != nil {
		panic(err)
	}
	return po
}

func LoadUserPhoenixConfig() *PhoenixOptions {
	configPath := resManager.PhoenixManagerRes().FindResPath(files.FILE_USER_CONFIG_FILE, false)
	if configPath == "" {
		panic("config file not found")
	}
	yamlConfig := viper.GetViper()
	yamlConfig.AddConfigPath(configPath)
	yamlConfig.SetConfigName(files.FILE_USER_CONFIG_FILE)
	yamlConfig.SetConfigType("yaml")
	if err := yamlConfig.ReadInConfig(); err != nil {
		panic(err)
	}
	po := NewPhoenixOptions()
	if err := yamlConfig.Unmarshal(po); err != nil {
		panic(err)
	}
	return po
}

func MergePhoenixConfig(defaultConfig *PhoenixOptions, userConfig *PhoenixOptions) {
	if userConfig.ServerKey != "" {
		defaultConfig.ServerKey = userConfig.ServerKey
	}
	if userConfig.ServerName != "" {
		defaultConfig.ServerName = userConfig.ServerName
	}
	if userConfig.ServerID != 0 {
		defaultConfig.ServerID = userConfig.ServerID
	}
	if userConfig.LogOptions.LogLevel != "" {
		defaultConfig.LogOptions.LogLevel = userConfig.LogOptions.LogLevel
	}
	if userConfig.LogOptions.LogPath != "" {
		defaultConfig.LogOptions.LogPath = userConfig.LogOptions.LogPath
	}
	if userConfig.LogOptions.LogFileName != "" {
		defaultConfig.LogOptions.LogFileName = userConfig.LogOptions.LogFileName
	}
	if userConfig.AgentEntityName != "" {
		defaultConfig.AgentEntityName = userConfig.AgentEntityName
	}
	if userConfig.SyncInterval != 0 {
		defaultConfig.SyncInterval = userConfig.SyncInterval
	}
	if userConfig.AllowReplace != defaultConfig.AllowReplace {
		defaultConfig.AllowReplace = userConfig.AllowReplace
	}
	if userConfig.PerformancePrint != defaultConfig.PerformancePrint {
		defaultConfig.PerformancePrint = userConfig.PerformancePrint
	}
	if userConfig.PerformancePort != 0 {
		defaultConfig.PerformancePort = userConfig.PerformancePort
	}

	if userConfig.DatabaseOptions != nil {
		if defaultConfig.DatabaseOptions == nil {
			defaultConfig.DatabaseOptions = userConfig.DatabaseOptions
		} else {
			for _, databaseOption := range userConfig.DatabaseOptions {
				for i, defaultDatabaseOption := range defaultConfig.DatabaseOptions {
					if defaultDatabaseOption.GroupName == databaseOption.GroupName {
						defaultConfig.DatabaseOptions[i] = databaseOption
					}
				}
			}
		}
	}

	if userConfig.RedisOptions.Host != "" {
		defaultConfig.RedisOptions.Host = userConfig.RedisOptions.Host
	}
	if userConfig.RedisOptions.Port != 0 {
		defaultConfig.RedisOptions.Port = userConfig.RedisOptions.Port
	}
	if userConfig.RedisOptions.Pass != "" {
		defaultConfig.RedisOptions.Pass = userConfig.RedisOptions.Pass
	}
	if userConfig.RedisOptions.DB != 0 {
		defaultConfig.RedisOptions.DB = userConfig.RedisOptions.DB
	}

	if userConfig.NatsOptions.Host != "" {
		defaultConfig.NatsOptions.Host = userConfig.NatsOptions.Host
	}
	if userConfig.NatsOptions.Port != 0 {
		defaultConfig.NatsOptions.Port = userConfig.NatsOptions.Port
	}
	if userConfig.NatsOptions.EnableAuth != defaultConfig.NatsOptions.EnableAuth {
		defaultConfig.NatsOptions.EnableAuth = userConfig.NatsOptions.EnableAuth
	}
	if userConfig.NatsOptions.User != "" {
		defaultConfig.NatsOptions.User = userConfig.NatsOptions.User
	}
	if userConfig.NatsOptions.Pass != "" {
		defaultConfig.NatsOptions.Pass = userConfig.NatsOptions.Pass
	}

	if userConfig.EtcdOptions.Host != "" {
		defaultConfig.EtcdOptions.Host = userConfig.EtcdOptions.Host
	}
	if userConfig.EtcdOptions.Port != 0 {
		defaultConfig.EtcdOptions.Port = userConfig.EtcdOptions.Port
	}
	if userConfig.EtcdOptions.EnableAuth != defaultConfig.EtcdOptions.EnableAuth {
		defaultConfig.EtcdOptions.EnableAuth = userConfig.EtcdOptions.EnableAuth
	}
	if userConfig.EtcdOptions.User != "" {
		defaultConfig.EtcdOptions.User = userConfig.EtcdOptions.User
	}
	if userConfig.EtcdOptions.Pass != "" {
		defaultConfig.EtcdOptions.Pass = userConfig.EtcdOptions.Pass
	}

	if userConfig.Components != nil {
		if defaultConfig.Components == nil {
			defaultConfig.Components = userConfig.Components
		} else {
			for _, component := range userConfig.Components {
				for ii, defaultComponent := range defaultConfig.Components {
					if defaultComponent.Name == component.Name {
						MergePhoenixComponentsConfig(&defaultConfig.Components[ii], &component)
					}
				}
			}
		}
	}
	if userConfig.Services != nil {
		if defaultConfig.Services == nil {
			defaultConfig.Services = userConfig.Services
		} else {
			for _, routine := range userConfig.Services {
				find := false
				for i, defaultRoutine := range defaultConfig.Services {
					if defaultRoutine.Name == routine.Name {
						if routine.Type != "" {
							defaultConfig.Services[i].Type = routine.Type
						}
						for _, component := range routine.Components {
							for ii, defaultComponent := range defaultRoutine.Components {
								if defaultComponent.Name == component.Name {
									MergePhoenixComponentsConfig(&defaultRoutine.Components[ii], &component)
								}
							}
						}
						find = true
					}
				}
				if !find {
					defaultConfig.Services = append(defaultConfig.Services, routine)
				}
			}
		}
	}
}

func MergePhoenixComponentsConfig(defaultComponent *ComponentOptions, component *ComponentOptions) {
	if component.Options.Proxy.Port != 0 {
		defaultComponent.Options.Proxy.Port = component.Options.Proxy.Port
	}
	if component.Options.Proxy.Host != "" {
		defaultComponent.Options.Proxy.Host = component.Options.Proxy.Host
	}
	defaultComponent.Options.Multicore = component.Options.Multicore

	for _, webConfig := range component.Options.Web.GServerOptions {
		isExist := false
		for _, defaultWebConfig := range defaultComponent.Options.Web.GServerOptions {
			if defaultWebConfig.ConfigKey == webConfig.ConfigKey {
				defaultWebConfig.ConfigValue = webConfig.ConfigValue
				isExist = true
			}
		}
		if !isExist {
			defaultComponent.Options.Web.GServerOptions = append(defaultComponent.Options.Web.GServerOptions, webConfig)
		}
	}
}
