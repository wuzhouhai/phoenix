// Package signal -----------------------------
// @file      : signalHandler.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/25 17:00
// @desc      :
// -------------------------------------------
package signal

import (
	"os"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/eventType"
	"phoenix/phoenix/managers/eventsManager"
)

func SignalHandlerForMQ(sig os.Signal) {
	logger.PhoenixLoggerIns().Infof("Phoenix Server Will Stop!!!")
	eventsManager.PhoenixManagerEvent().Publish(eventType.PhoenixEvent_QuitApp)
}
