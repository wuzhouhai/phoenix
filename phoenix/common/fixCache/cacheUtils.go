// Package utils -----------------------------
// @file      : cacheUtils.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/23 11:14
// @desc      :
// -------------------------------------------
package fixCache

import (
	"github.com/gogf/gf/v2/util/gconv"
	"phoenix/phoenix/app"
	"phoenix/phoenix/defines/dataType"
)

func GetLoginStatusCacheKey(accountName string) string {
	return app.PhoenixIns().GetServerCachePrefix() + "login_" + accountName
}

func GetAvatarOnlineCacheKey(accountId int32) string {
	return app.PhoenixIns().GetServerCachePrefix() + "online_" + gconv.String(accountId)
}

func GetAvatarGateCacheKey(accountId int32) string {
	return app.PhoenixIns().GetServerCachePrefix() + "gate_" + gconv.String(accountId)
}

func GetPlayerManagerKey(avatarId dataType.PhoenixTypeDataBaseId) string {
	return app.PhoenixIns().GetServerCachePrefix() + "player_" + gconv.String(avatarId)
}

func GetPayerManagerConnectorKey(conId dataType.PhoenixTypeConnectId, gateServiceId string) string {
	return app.PhoenixIns().GetServerCachePrefix() + "connector_" + gateServiceId + "_" + gconv.String(conId)
}
