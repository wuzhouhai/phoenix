// Package app -----------------------------
// @file      : player.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/25 10:16
// @desc      :
// -------------------------------------------
package objects

import (
	"fmt"
	"phoenix/phoenix/app"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/entitys/instance/message"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/objectsManager"
	"phoenix/phoenix/network/handles/funcHandle"
	"phoenix/phoenix/register"
	"phoenix/phoenix/scripts"
)

type Player struct {
	Id          dataType.PhoenixTypeDataBaseId // avatarId
	ConnectorId dataType.PhoenixTypeConnectId  // 连接ID
	ServiceId   string                         // gate服务ID
	AgentEntity interfaces.IAgentEntity        // 前端代理实体
	GamePackets chan *message.Packet           // 游戏处理的消息
	Released    chan bool
	Service     interfaces.IService   // 属于那个服务
	LuaHelper   interfaces.IEntityLua // lua虚拟机
}

func NewPlayer(id dataType.PhoenixTypeDataBaseId, s interfaces.IService, gateServiceId string, conId dataType.PhoenixTypeConnectId) *Player {
	player := &Player{
		Id:          id,
		ConnectorId: conId,
		ServiceId:   gateServiceId,
		GamePackets: make(chan *message.Packet, 64),
		Released:    make(chan bool),
		Service:     s,
		LuaHelper:   scripts.NewEntityLuaState(),
	}
	player.Init()
	player.LuaHelper.RegisterModules()
	return player
}

func (this *Player) Init() {
	logger.PhoenixLoggerIns().Debugf("Player 初始化, 网关ID:%s, 连接ID: %d", this.ServiceId, this.Id)
	ac, _ := objectsManager.PhoenixManagerObjects().CreateObject(app.PhoenixIns().GetAgentEntityName())
	acI := ac.(interfaces.IAgentEntity)
	acI.SetConnectId(this.ConnectorId)
	acI.SetGateService(this.ServiceId)
	acI.SetService(this.Service)
	this.SetAgentEntity(acI)
}

func (this *Player) StartWork() {
	go this.DoMsg()
}

func (this *Player) GetId() dataType.PhoenixTypeDataBaseId {
	return this.Id
}

func (this *Player) GetConnectorId() dataType.PhoenixTypeConnectId {
	return this.ConnectorId
}

func (this *Player) SetConnectorId(conId dataType.PhoenixTypeConnectId) {
	this.ConnectorId = conId
}

func (this *Player) GetGateServiceID() string {
	return this.ServiceId
}

func (this *Player) SetGateServiceId(gateServiceId string) {
	this.ServiceId = gateServiceId
}

func (this *Player) GetAgentEntity() interfaces.IAgentEntity {
	return this.AgentEntity
}

func (this *Player) Kick(msg string) {
	if this.AgentEntity != nil {
		this.AgentEntity.BeKick(msg)
	}
}

func (this *Player) SetAgentEntity(agentEntity interfaces.IAgentEntity) {
	logger.PhoenixLoggerIns().Debugf("设置代理实体, 连接ID: %d, %s", this.Id, agentEntity)
	agentEntity.SetConnectId(this.ConnectorId)
	this.AgentEntity = agentEntity
}

func (this *Player) DisConnect() {
	logger.PhoenixLoggerIns().Debug(fmt.Sprintf("%d 连接断开", this.Id))
	if this.AgentEntity != nil {
		this.AgentEntity.DisConnect()
		this.AgentEntity.Release()
		this.AgentEntity = nil
	}
	this.Released <- true
}

func (this *Player) AddGamePacket(p interface{}) {
	if p, ok := p.(*message.Packet); ok {
		logger.PhoenixLoggerIns().Debug(fmt.Sprintf("AddGamePacket 收到消息, 连接ID: %d, opcode: %d", this.Id, p.Opcode))
		this.GamePackets <- p
	}
}
func (this *Player) OnFrame() {

}

func (this *Player) DoMsg() {
	for {
		select {
		case <-this.Released:
			return
		case p := <-this.GamePackets:
			logger.PhoenixLoggerIns().Debug(fmt.Sprintf("收到消息, 连接ID: %d, opcode: %d", this.Id, p.Opcode))
			h := register.GetSocketHandler(p.Opcode)
			if h == nil {
				logger.PhoenixLoggerIns().Error(fmt.Sprintf("没有找到对应的消息处理函数，opcode: %d", p.Opcode))
				continue
			}
			if this.AgentEntity == nil {
				logger.PhoenixLoggerIns().Error(fmt.Sprintf("没有找到对应的实体，opcode: %d", p.Opcode))
			}
			if h.AgentHandleName != "" {
				funcHandle.HandleProcess(this.AgentEntity, p.Msg, h.AgentHandleName)
			}
			objectsManager.PhoenixManagerObjects().DestroyObject(p)
			break
		}
	}
}

//func (this *Connector) SendPacket(p *message.Packet) error {
//	sendBundle := bundle.NewBundle()
//	sendBundle.AddendReceiver(this.Id)
//	sendBundle.AppendPacket(p)
//	defer objectsManager.PhoenixManagerObjects().DestroyObject(sendBundle)
//	defer objectsManager.PhoenixManagerObjects().DestroyObject(p)
//	return sendBundle.SendData()
//}
