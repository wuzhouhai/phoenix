// Package app -----------------------------
// @file      : appIns.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/4/16 15:15
// @desc      :
// -------------------------------------------
package app

import (
	"phoenix/phoenix/interfaces"
	"sync"
)

var Ins interfaces.IPhoenix
var once sync.Once

func SetPhoenixIns(app interfaces.IPhoenix) {
	once.Do(func() {
		Ins = app
	})
}

func PhoenixIns() interfaces.IPhoenix {
	return Ins
}
