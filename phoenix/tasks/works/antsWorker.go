package works

import (
	"github.com/panjf2000/ants/v2"
	"phoenix/phoenix/interfaces"
)

type AntsWorker struct {
	p *ants.PoolWithFunc
}

func NewAntsWorker() *AntsWorker {
	antsW := &AntsWorker{}
	antsW.p, _ = ants.NewPoolWithFunc(1000, WorkerFunc)
	return antsW
}

func (this *AntsWorker) Release() {
	this.p.Release()
}

func (this *AntsWorker) AddTask(task interfaces.ITask) error {
	err := this.p.Invoke(task)
	return err
}

func WorkerFunc(task interface{}) {
	workTask := task.(interfaces.ITask)
	workTask.DoTask()
}

//func SenderFunc(task interface{}) {
//	workTask := task.(interfaces.ITask)
//	workTask.DoTask()
//}
