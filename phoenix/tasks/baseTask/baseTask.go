package baseTask

import (
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/utils"
)

type BaseTask struct {
	ConId     dataType.PhoenixTypeConnectId
	Id        string
	StartTime int64
	EndTime   int64
	Status    int
	Err       error
	StoreData interface{}
}

func NewBaseTask(ConId dataType.PhoenixTypeConnectId, d interface{}) BaseTask {
	baseTask := BaseTask{
		ConId:     ConId,
		Id:        utils.GenerateTaskId(),
		StartTime: utils.GetNowTimestampSecond(),
		EndTime:   utils.GetNowTimestampSecond(),
		Status:    0,
		StoreData: d,
	}
	return baseTask
}

func (this *BaseTask) GetConId() dataType.PhoenixTypeConnectId {
	return this.ConId
}
