// Package tasks Package task -----------------------------
// @file      : LogicTask.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/18 18:17
// @desc      :
// -------------------------------------------
package tasks

import (
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/tasks/baseTask"
)

type LogicTask struct {
	baseTask.BaseTask
}

func NewLogicTask(ConId dataType.PhoenixTypeConnectId, storeData interface{}) *LogicTask {
	return &LogicTask{
		BaseTask: baseTask.NewBaseTask(ConId, storeData),
	}
}

func (this *LogicTask) DoTask() {
}
