// Package utils -----------------------------
// @file      : refectUtils.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/7/1 16:33
// @desc      : 反射操作
// -------------------------------------------
package utils

import (
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	error2 "phoenix/phoenix/defines/error"
	"reflect"
)

func SetField(obj interface{}, fieldName string, value interface{}) error {
	// 获取obj的反射对象
	objValue := reflect.ValueOf(obj)
	if objValue.Kind() == reflect.Ptr {
		// 如果是指针，则解引用
		objValue = objValue.Elem()
	}

	// 获取field的反射对象
	fieldValue := objValue.FieldByName(fieldName)
	if !fieldValue.IsValid() {
		return gerror.NewCodef(gcode.New(error2.PHOENIX_ERROR_REFECT_FIELD_INVALID, "", nil),
			"设置对象类型%s的字段%s无效", objValue.Type(), fieldName)
	}

	// 获取value的反射对象
	valueValue := reflect.ValueOf(value)
	if !valueValue.IsValid() {
		return gerror.NewCodef(gcode.New(error2.PHOENIX_ERROR_REFECT_VALUE_INVALID, "", nil),
			"设置对象类型%s的字段值%s无效", objValue.Type(), fieldName)
	}

	// 检查value的类型是否可以赋值给field
	if fieldValue.Type() != valueValue.Type() {
		return gerror.NewCodef(gcode.New(error2.PHOENIX_ERROR_REFECT_VALUE_TPYE, "", nil),
			"设置对象类型%s的字段值类型%s无效", objValue.Type(), fieldName)
	}

	// 设置新值
	fieldValue.Set(valueValue)
	return nil
}

func CreateSlice(sliceType reflect.Type) reflect.Value {
	return reflect.MakeSlice(sliceType, 0, 0)
}
