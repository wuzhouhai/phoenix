// Package utils -----------------------------
// @file      : stringUtils.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/6 10:54
// @desc      :
// -------------------------------------------
package utils

import "strings"

func SplitString(str string, sep string) []string {
	return strings.Split(str, sep)
}

// ToLower 将所有字符串转换为小写
func ToLower(str string) string {
	return strings.ToLower(str)
}
