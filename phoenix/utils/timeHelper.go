// Package utils -----------------------------
// @file      : timeHelper.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/11 17:36
// @desc      :
// -------------------------------------------
package utils

import (
	"github.com/gogf/gf/v2/os/gtime"
	"time"
)

func GetNowTimestampSecond() int64 {
	return time.Now().Unix()
}

func GetNowTimestampNanoSecond() int64 {
	return time.Now().UnixNano()
}

func GetNowTimeFormat() string {
	return time.Now().Format("2006-01-02 15:04:05")
}

func GetTimestampSecond(timeStr string) int64 {
	gt := gtime.New(timeStr)
	return gt.Timestamp()
}

func GetTimeStrFromTimestamp(t int64) string {
	if t == 0 {
		return gtime.Now().Format("Y-m-d H:i:s")
	}
	gt := gtime.New(t)
	return gt.Format("Y-m-d H:i:s")
}
