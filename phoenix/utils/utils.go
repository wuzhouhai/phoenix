package utils

import (
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/util/grand"
	"github.com/gogf/gf/v2/util/guid"
)

func GenerateUUID() string {
	return guid.S()
}

func GenerateRoutineId() string {
	return "routine_" + GenerateUUID()
}

func GenerateComponentId() string {
	return "component_" + GenerateUUID()
}

func GenerateTaskId() string {
	return "task_" + GenerateUUID()
}

// MergeMaps overwriting duplicate keys, you should handle that if there is a need
func MergeMaps(maps ...map[string]interface{}) map[string]interface{} {
	result := make(map[string]interface{})
	for _, m := range maps {
		for k, v := range m {
			result[k] = v
		}
	}
	return result
}

func MakeError(code int, msg string) error {
	return gerror.NewCodef(gcode.New(code, "", nil), msg)
}

// RandomInt 取随机数([min, max])
func RandomInt(min int, max int) int {
	return grand.N(min, max)
}
