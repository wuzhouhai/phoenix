// Package utils -----------------------------
// @file      : fileUtils.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/3/5 16:13
// @desc      :
// -------------------------------------------
package utils

import (
	"io"
	"os"
	"path/filepath"
	"strings"
)

// ReadFile 读取文件
func ReadFile(fileName string) ([]byte, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	return io.ReadAll(file)
}

func FindFilesByType(rootPath string, fileType string, isFullPath bool) ([]string, error) {
	var matchedFiles []string

	err := filepath.Walk(rootPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && strings.HasSuffix(info.Name(), fileType) {
			if isFullPath {
				matchedFiles = append(matchedFiles, path)
			} else {
				matchedFiles = append(matchedFiles, info.Name())
			}
		}
		return nil
	})

	if err != nil {
		return nil, err
	}

	return matchedFiles, nil
}
