// Package utils -----------------------------
// @file      : encryptUtils.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/4/11 11:46
// @desc      :
// -------------------------------------------
package utils

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io"
)

// Generate a random initialization vector (IV).
func generateRandomIV() ([]byte, error) {
	iv := make([]byte, 16)
	_, err := io.ReadFull(rand.Reader, iv)
	if err != nil {
		return nil, err
	}
	return iv, nil
}

// AESEncrypt Encrypt function with AES-256-CTR mode.
func AESEncrypt(plaintext []byte, key []byte) (ciphertext []byte, iv []byte, err error) {
	// Create a new AES cipher block based on our 256 bit key.
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, nil, err
	}

	// Generate a random IV.
	iv, err = generateRandomIV()
	if err != nil {
		return nil, nil, err
	}

	// Create a stream cipher for encryption.
	stream := cipher.NewCTR(block, iv)

	// Prepend the IV to the ciphertext for later use during decryption.
	ciphertext = make([]byte, len(plaintext))
	//copy(ciphertext[:aes.BlockSize], iv)

	// XOR plaintext with the stream.
	stream.XORKeyStream(ciphertext, plaintext)

	return ciphertext, iv, nil
}

// AESDecrypt Decrypt function for AES-256-CTR mode.
func AESDecrypt(ciphertext []byte, key []byte, iv []byte) ([]byte, error) {
	// Extract the IV from the beginning of the ciphertext.

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	stream := cipher.NewCTR(block, iv)

	// 如果密文是Base64编码过的，先解码
	if base64Encoded := base64.StdEncoding.EncodeToString(ciphertext); base64Encoded != "" {
		ciphertext, err = base64.StdEncoding.DecodeString(base64Encoded)
		if err != nil {
			return nil, err
		}
	}

	// 创建一个新切片存放解密后的明文
	plaintext := make([]byte, len(ciphertext))
	stream.XORKeyStream(plaintext, ciphertext)

	return plaintext, nil
}

func AESDecryptFromString(ciphertext string, key string, ivHex string) (string, error) {
	// 将hex编码的IV转换为字节切片
	ivBytes, err := hex.DecodeString(ivHex)
	if err != nil || len(ivBytes) != aes.BlockSize {
		return "", fmt.Errorf("failed to decode IV from hex: %w", err)
	}
	ciphertextBytes, err := base64.StdEncoding.DecodeString(ciphertext)
	if err != nil {
		return "", fmt.Errorf("failed to decode ciphertext from base64: %w", err)
	}
	te, e := AESDecrypt(ciphertextBytes, []byte(key), ivBytes)
	if e != nil {
		return "", e
	}
	return string(te), nil
}

func AESEncryptToString(plaintext string, key string) (string, string, error) {
	ciphertext, iv, err := AESEncrypt([]byte(plaintext), []byte(key))
	if err != nil {
		return "", "", err
	}
	// 将IV转换为hex编码的字符串
	ivHex := hex.EncodeToString(iv)
	// 将密文转换为base64编码的字符串
	//combined := append(iv, ciphertext...)
	ciphertextBase64 := base64.StdEncoding.EncodeToString(ciphertext)
	return ciphertextBase64, ivHex, nil
}
