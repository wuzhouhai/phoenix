package phoenix

import (
	"context"
	"fmt"
	_ "github.com/gogf/gf/contrib/drivers/mysql/v2"
	"github.com/gogf/gf/v2/os/gproc"
	"github.com/gogf/gf/v2/os/gtimer"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/lesismal/arpc"
	"net/http"
	_ "net/http/pprof"
	"os"
	"phoenix/phoenix/app"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/common/signal"
	"phoenix/phoenix/componets/etcdComponent"
	"phoenix/phoenix/componets/natsComponent"
	"phoenix/phoenix/defines/eventType"
	interfaces2 "phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/cacheManager"
	"phoenix/phoenix/managers/connectManager"
	"phoenix/phoenix/managers/databaseManager"
	"phoenix/phoenix/managers/eventsManager"
	"phoenix/phoenix/managers/logicConfigManager"
	"phoenix/phoenix/managers/luaManager"
	"phoenix/phoenix/managers/moduleManager"
	"phoenix/phoenix/managers/objectsManager"
	"phoenix/phoenix/managers/timerManager"
	"phoenix/phoenix/managers/worldManager"
	register2 "phoenix/phoenix/register"
	_ "phoenix/phoenix/register/registerInit"
	"phoenix/phoenix/scripts"
	"strconv"
	"sync"
	"time"
)

func init() {
	arpc.EnablePool(true)
}

type Phoenix struct {
	options    *config.PhoenixOptions
	ctx        context.Context
	die        chan bool
	components map[string]interfaces2.IComponent // 公用组件,每个routines都可以使用
	services   map[string]interfaces2.IService   // 功能协程
	Logger     *logger.PhoenixLogger
	timer      *gtimer.Timer
	iServer    interfaces2.IServer
}

func NewPhoenix() *Phoenix {
	phoenix := &Phoenix{
		ctx:        context.Background(),
		die:        make(chan bool),
		components: make(map[string]interfaces2.IComponent),
		services:   make(map[string]interfaces2.IService),
		timer: gtimer.New(gtimer.TimerOptions{
			Interval: time.Millisecond * 25,
			Quick:    false,
		}),
	}
	phoenix.options = config.LoadPhoenixConfig()
	userOptions := config.LoadUserPhoenixConfig()
	config.MergePhoenixConfig(phoenix.options, userOptions)
	app.SetPhoenixIns(phoenix)
	phoenix.Logger = logger.NewPhoenixLogger(phoenix.options)
	phoenix.CreateDatabases()
	phoenix.InitCacheManager()
	phoenix.CreateComponents()
	phoenix.CreateRoutines()
	return phoenix
}

func (this *Phoenix) InitPhoenix() error {
	this.Logger.Debug("Phoenix Init")
	for _, v := range this.components {
		err := v.Init()
		if err != nil {
			return err
		}
	}
	for _, v := range this.services {
		err := v.Init()
		if err != nil {
			logger.PhoenixLoggerIns().Errorf("%s service Init Fail err : %s", err.Error())
			return err
		}
	}
	this.iServer.OnInit()
	return nil
}

func (this *Phoenix) InitCacheManager() {
	cacheManager.PhoenixManagerCache().Init(this.ctx, *this.options)
}

// 创建数据库
func (this *Phoenix) CreateDatabases() {
	databaseManager.PhoenixManagerDatabase().InitDatabases(this.ctx, *this.options)
}

// CreateComponents 从配置中创建组件
func (this *Phoenix) CreateComponents() {
	for _, v := range this.options.Components {
		componentCreateFunc := register2.GetComponentsCreateFunc(v.Name)
		if componentCreateFunc == nil {
			this.Logger.Error(fmt.Sprintf("component %s not found", v.Name))
			continue
		}
		component, err := componentCreateFunc(nil, v, this.ctx)
		if err != nil {
			this.Logger.Error(fmt.Sprintf("component %s create fail %s", v.Name, err.Error()))
		} else {
			this.RegisterComponent(component)
		}
	}
}

func (this *Phoenix) CreateRoutines() {
	for _, v := range this.options.Services {
		routineCreateFunc := register2.GetRoutineCreateFunction(v.Type)
		if routineCreateFunc == nil {
			this.Logger.Error(fmt.Sprintf("routine %s not found", v.Name))
			continue
		}
		routine, err := routineCreateFunc(v, this.ctx)
		routine.CreateComponents()
		this.CreatNatsComponent(routine)
		this.CreateEtcdComponent(routine)
		if err != nil {
			this.Logger.Error(fmt.Sprintf("routine %s create fail %s", v.Name, err.Error()))
		} else {
			this.services[routine.GetId()] = routine
		}
	}
}

func (this *Phoenix) CreateEtcdComponent(r interfaces2.IService) {
	etcdCom, _ := etcdComponent.NewEtcdComponent(r, *config.NewPhoenixComponentOptions("etcd"),
		context.TODO())
	r.AddComponent(etcdCom.GetId(), etcdCom)
}

func (this *Phoenix) CreatNatsComponent(r interfaces2.IService) {
	natsCom, _ := natsComponent.NewNatsComponent(r, *config.NewPhoenixComponentOptions("natsComponent"),
		context.TODO())
	r.AddComponent(natsCom.GetId(), natsCom)
}

func (this *Phoenix) AfterInit() {
	// 注册所有模块
	allModules := register2.GetRegisterModules()
	for n, v := range allModules {
		module, _ := v(context.TODO(), n)
		module.Init()
		moduleManager.PhoenixManagerModule().RegisterModule(module)
	}
}

// 注册组件
func (this *Phoenix) RegisterComponent(component interfaces2.IComponent) {
	this.components[component.GetId()] = component
}

func (this *Phoenix) InitTimer() {
	this.timer.AddOnce(context.TODO(), time.Millisecond*50, this.OnTimerFunc)
	this.timer.Add(context.TODO(), time.Minute*1, this.OnTimerMinute)
}

func (this *Phoenix) OnTimerFunc(ctx context.Context) {
	frameStartTime := time.Now()
	worldManager.PhoenixManagerWorld().OnTimerFunc(ctx)
	frameEndTime := time.Now()
	//connectManager.PhoenixManagerConnect().OnFrame()
	frameTime := frameEndTime.Sub(frameStartTime)
	if frameTime.Milliseconds() > 50 {
		logger.PhoenixLoggerIns().Warnf("Game Frame Is Too Lang, Use Time %d Ms, > 50 MS",
			frameTime.Milliseconds())
		this.timer.AddOnce(context.TODO(), time.Millisecond, this.OnTimerFunc)
	} else {
		interval := 50 - frameTime.Milliseconds()
		this.timer.AddOnce(context.TODO(), time.Millisecond*time.Duration(interval), this.OnTimerFunc)
	}
}

func (this *Phoenix) OnTimerMinute(ctx context.Context) {
	objectsManager.PhoenixManagerObjects().LogInfo()
	conCounts := connectManager.PhoenixManagerConnect().GetConnectCounts()
	if conCounts > 0 {
		logger.PhoenixLoggerIns().Debugf("User Counts %d", connectManager.PhoenixManagerConnect().GetConnectCounts())
	}
}

func (this *Phoenix) Stop() {
	this.die <- true
}

func (this *Phoenix) InitSignal() {
	gproc.AddSigHandlerShutdown(signal.SignalHandlerForMQ)
	go gproc.Listen()
}

func (this *Phoenix) SubscribeEvents() {
	eventsManager.PhoenixManagerEvent().SubscribeOnce(eventType.PhoenixEvent_QuitApp, this.Stop)
}

func (this *Phoenix) ReadyForRun() {
	if !moduleManager.PhoenixManagerModule().CheckModuleOperate() {
		logger.PhoenixLoggerIns().Warnf("module operate not found, module data will can not use")
	}
}

func (this *Phoenix) ExitPhoenix() {
	this.timer.Stop()
	this.timer.Close()
	// 管理器清理
	timerManager.PhoenixManagerTimer().Stop()
	timerManager.PhoenixManagerTimer().Close()
	connectManager.PhoenixManagerConnect().OnAppClose()
	for _, v := range this.services {
		v.Stop()
	}
	for _, v := range this.components {
		v.Stop()
	}
	os.Exit(0)
}

func (this *Phoenix) InitLuaScript() {
	for _, v := range this.services {
		luaStat := scripts.NewServiceLuaState()
		v.SetLua(luaStat)
		luaManager.PhoenixManagerLua().CompileLuaFiles(v.GetType())
		luaManager.PhoenixManagerLua().DoCompiledFiles(v.GetType(), luaStat.GetLuaState())
		luaStat.RegisterModules()
	}
}

func (this *Phoenix) PhoenixStart(iser interfaces2.IServer) {
	logoString := ` 
	____  _   _  ___  _____ _   _ _____  __
	|  _ \| | | |/ _ \| ____| \ | |_ _\ \/ /
	| |_) | |_| | | | |  _| |  \| || | \  / 
	|  __/|  _  | |_| | |___| |\  || | /  \ 
	|_|   |_| |_|\___/|_____|_| \_|___/_/\_\
                                        `
	fmt.Println(logoString)
	this.iServer = iser
	err := this.InitPhoenix()
	if err != nil {
		this.ExitPhoenix()
	}
	this.AfterInit()
	this.InitTimer()
	this.InitSignal()
	this.SubscribeEvents()
	this.ctx = context.Background()
	this.InitLuaScript()
	this.ReadyForRun()
	// 初始化游戏配置
	logicConfigManager.PhoenixManagerLogicConfig().Init()
	for _, v := range this.components {
		err := v.Start()
		if err != nil {
			// 退出程序
			logger.PhoenixLoggerIns().Errorf("phoenix component %s start fail %s", v.GetName(), err.Error())
			this.ExitPhoenix()
		}
	}
	logger.PhoenixLoggerIns().Debugf("phoenix start services count:%d", len(this.services))
	for _, v := range this.services {
		logger.PhoenixLoggerIns().Debugf("phoenix service %s start", v.GetName())
		err := v.Start()
		if err != nil {
			logger.PhoenixLoggerIns().Errorf("service %s start fail %s", v.GetName(), err.Error())
			this.ExitPhoenix()
		}
		go v.Process()
	}
	this.timer.Start()
	timerManager.PhoenixManagerTimer().Start()
	if this.options.PerformancePrint {
		_ = http.ListenAndServe("0.0.0.0:"+strconv.Itoa(this.options.PerformancePort), nil)
	}
	for {
		select {
		case <-this.die:
			this.ExitPhoenix()
			return
		}
	}
}

func (this *Phoenix) GetServerKey() string {
	return this.options.ServerKey
}

func (this *Phoenix) GetAgentEntityName() string {
	return this.options.AgentEntityName
}

func (this *Phoenix) GetSyncInterval() int {
	return this.options.SyncInterval
}

func (this *Phoenix) AllowReplace() bool {
	return this.options.AllowReplace
}

func (this *Phoenix) GetOptions() *config.PhoenixOptions {
	return this.options
}

func (this *Phoenix) GetServerCachePrefix() string {
	return this.options.ServerName + "_" + gconv.String(this.options.ServerID) + "_"
}

var PhoenixIns *Phoenix
var once sync.Once

// 实现一个单例
func PhoenixApp() *Phoenix {
	once.Do(func() {
		PhoenixIns = NewPhoenix()
	})
	return PhoenixIns
}
