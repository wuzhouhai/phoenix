// Package luaModule -----------------------------
// @file      : LogModule.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/10/24 10:46
// @desc      :
// -------------------------------------------
package luaModule

import (
	lua "github.com/yuin/gopher-lua"
	"phoenix/phoenix/common/logger"
)

var exports = map[string]lua.LGFunction{
	"Debug": Log_Debug,
	"Info":  Log_Info,
	"Warn":  Log_Warn,
	"Error": Log_Error,
}

func Log_Debug(l *lua.LState) int {
	msg := l.ToString(1)
	logger.PhoenixLoggerIns().Debug(msg)
	return 0
}

func Log_Info(l *lua.LState) int {
	msg := l.ToString(1)
	logger.PhoenixLoggerIns().Info(msg)
	return 0
}

func Log_Warn(l *lua.LState) int {
	msg := l.ToString(1)
	logger.PhoenixLoggerIns().Warn(msg)
	return 0
}

func Log_Error(l *lua.LState) int {
	msg := l.ToString(1)
	logger.PhoenixLoggerIns().Error(msg)
	return 0
}

func LuaModuleLoadLog(l *lua.LState) int {
	mode := l.SetFuncs(l.NewTable(), exports)
	l.Push(mode)
	return 1
}
