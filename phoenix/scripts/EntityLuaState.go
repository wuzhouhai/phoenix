// Package scripts -----------------------------
// @file      : EntityLuaState.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/10/29 9:58
// @desc      :
// -------------------------------------------
package scripts

import "phoenix/phoenix/base"

type EntityLuaState struct {
	base.BaseLuaState
}

func NewEntityLuaState() *EntityLuaState {
	return &EntityLuaState{
		BaseLuaState: base.NewBaseLuaState(),
	}
}
