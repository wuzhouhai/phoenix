// Package scripts -----------------------------
// @file      : ServiceLuaState.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/10/23 16:41
// @desc      :
// -------------------------------------------
package scripts

import "phoenix/phoenix/base"

type ServiceLuaState struct {
	base.BaseLuaState
}

func NewServiceLuaState() *ServiceLuaState {
	return &ServiceLuaState{
		BaseLuaState: base.NewBaseLuaState(),
	}
}
