// Package wsCodec Package WsCodec -----------------------------
// @file      : WsCodec.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/15 10:55
// @desc      :
// -------------------------------------------
package wsCodec

import (
	"bytes"
	"errors"
	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
	"github.com/gogf/gf/v2/container/garray"
	"github.com/panjf2000/gnet/v2"
	"io"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/defines/normal"
	"phoenix/phoenix/network/reader"
)

type WsCodec struct {
	ConnectId    dataType.PhoenixTypeConnectId // 连接ID
	upgraded     bool                          // 链接是否升级
	Buf          bytes.Buffer                  // 从实际socket中读取到的数据缓存
	wsMsgBuf     wsMessageBuf                  // ws 消息缓存
	DataBuf      bytes.Buffer                  // 从实际socket中读取到的数据缓存
	PacketReader *reader.PacketReader          // 包读取
}

func NewWsCodec(conId dataType.PhoenixTypeConnectId) *WsCodec {
	return &WsCodec{
		ConnectId:    conId,
		PacketReader: reader.NewPacketReader(),
	}
}

type wsMessageBuf struct {
	firstHeader *ws.Header
	curHeader   *ws.Header
	cachedBuf   bytes.Buffer
}

type readWrite struct {
	io.Reader
	io.Writer
}

func (w *WsCodec) Upgrade(c gnet.Conn) (ok bool, action gnet.Action) {
	if w.upgraded {
		ok = true
		return
	}
	buf := &w.Buf
	tmpReader := bytes.NewReader(buf.Bytes())
	oldLen := tmpReader.Len()
	logger.PhoenixLoggerIns().Debugf("do Upgrade")

	hs, err := ws.Upgrade(readWrite{tmpReader, c})
	skipN := oldLen - tmpReader.Len()
	if err != nil {
		if err == io.EOF || errors.Is(err, io.ErrUnexpectedEOF) { //数据不完整
			return
		}
		buf.Next(skipN)
		logger.PhoenixLoggerIns().Debugf("conn[%v] [err=%v]", c.RemoteAddr().String(), err.Error())
		action = gnet.Close
		return
	}
	buf.Next(skipN)
	logger.PhoenixLoggerIns().Debugf("conn[%v] upgrade websocket protocol! Handshake: %v", c.RemoteAddr().String(), hs)
	if err != nil {
		logger.PhoenixLoggerIns().Errorf("conn[%v] [err=%v]", c.RemoteAddr().String(), err.Error())
		action = gnet.Close
		return
	}
	ok = true
	w.upgraded = true
	return
}

func (w *WsCodec) ReadBufferBytes(c gnet.Conn) gnet.Action {
	size := c.InboundBuffered()
	buf := make([]byte, size)
	read, err := c.Read(buf)
	if err != nil {
		logger.PhoenixLoggerIns().Errorf("read err! %w", err)
		return gnet.Close
	}
	if read < size {
		logger.PhoenixLoggerIns().Errorf("read bytes len err! size: %d read: %d", size, read)
		return gnet.Close
	}
	w.Buf.Write(buf)
	return gnet.None
}

func (w *WsCodec) Decode(c gnet.Conn) (outs []wsutil.Message, err error) {
	//fmt.Println("do Decode")
	messages, err := w.readWsMessages()
	if err != nil {
		logger.PhoenixLoggerIns().Errorf("Error reading message! %v", err)
		return nil, err
	}
	if messages == nil || len(messages) <= 0 { //没有读到完整数据 不处理
		return
	}
	for _, message := range messages {
		if message.OpCode.IsControl() {
			err = wsutil.HandleClientControlMessage(c, message)
			if err != nil {
				return
			}
			continue

		}
		if message.OpCode == ws.OpText || message.OpCode == ws.OpBinary {
			outs = append(outs, message)
			w.DataBuf.Write(message.Payload)
		}
	}
	return
}

func (w *WsCodec) DecodeMsg() (*garray.Array, error) {
	packets := garray.New()
	for {
		if w.PacketReader.ReadSize >= w.DataBuf.Len() {
			break
		}
		transformBuf := w.DataBuf.Bytes()[w.PacketReader.ReadSize:]
		if len(transformBuf) >= normal.HeaderSize {
			err, packet := w.PacketReader.Reader(transformBuf)
			if err != nil {
				logger.PhoenixLoggerIns().Errorf("Error reading message! %v", err)
				w.DataBuf.Reset()
				w.PacketReader.Reset()
				return nil, err
			}
			if packet != nil {
				packets.Append(packet)
			}
		}

	}
	w.DataBuf.Reset()
	w.PacketReader.Reset()
	return packets, nil
}

func (w *WsCodec) readWsMessages() (messages []wsutil.Message, err error) {
	msgBuf := &w.wsMsgBuf
	in := &w.Buf
	for {
		if msgBuf.curHeader == nil {
			if in.Len() < ws.MinHeaderSize { //头长度至少是2
				return
			}
			var head ws.Header
			if in.Len() >= ws.MaxHeaderSize {
				head, err = ws.ReadHeader(in)
				if err != nil {
					return messages, err
				}
			} else { //有可能不完整，构建新的 reader 读取 head 读取成功才实际对 in 进行读操作
				tmpReader := bytes.NewReader(in.Bytes())
				oldLen := tmpReader.Len()
				head, err = ws.ReadHeader(tmpReader)
				skipN := oldLen - tmpReader.Len()
				if err != nil {
					if err == io.EOF || err == io.ErrUnexpectedEOF { //数据不完整
						return messages, nil
					}
					in.Next(skipN)
					return nil, err
				}
				in.Next(skipN)
			}

			msgBuf.curHeader = &head
			err = ws.WriteHeader(&msgBuf.cachedBuf, head)
			if err != nil {
				return nil, err
			}
		}
		dataLen := (int)(msgBuf.curHeader.Length)
		if dataLen > 0 {
			if in.Len() >= dataLen {
				_, err = io.CopyN(&msgBuf.cachedBuf, in, int64(dataLen))
				if err != nil {
					return
				}
			} else { //数据不完整
				//fmt.Println(in.Len(), dataLen)
				logger.PhoenixLoggerIns().Debugf("incomplete data")
				return
			}
		}
		if msgBuf.curHeader.Fin { //当前 header 已经是一个完整消息
			messages, err = wsutil.ReadClientMessage(&msgBuf.cachedBuf, messages)
			if err != nil {
				return nil, err
			}
			msgBuf.cachedBuf.Reset()
		} else {
			logger.PhoenixLoggerIns().Debugf("The data is split into multiple frames")
		}
		msgBuf.curHeader = nil
	}
}
