// Package websocketProxyComponent -----------------------------
// @file      : webSocketProxyComponent.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/15 10:40
// @desc      :
// -------------------------------------------
package websocketProxyComponent

import (
	"context"
	"fmt"
	"github.com/panjf2000/gnet/v2"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/componets/proxyComponent"
	"phoenix/phoenix/componets/websocketProxyComponent/wsCodec"
	"phoenix/phoenix/entitys/instance/message"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/connectManager"
	"phoenix/phoenix/network/connects"
	"sync/atomic"
)

type WebSocketProxyComponent struct {
	proxyComponent.ProxyComponent
}

func NewWebSocketProxyComponent(r interfaces.IService, options config.ComponentOptions, ctx context.Context) (interfaces.IComponent, error) {
	return &WebSocketProxyComponent{
		ProxyComponent: proxyComponent.NewProxyComponentBase(r, options, ctx),
	}, nil
}

func (this *WebSocketProxyComponent) OnOpen(c gnet.Conn) ([]byte, gnet.Action) {
	conId := atomic.AddInt32(&this.ConnectId, 1)
	logger.PhoenixLoggerIns().Debugf("websocket proxy component OnOpen connect id: %d", conId)
	newCon := connects.NewWebSocketConnector(conId, c, this.Service)
	err := this.SelectGameServiceID(newCon)
	if err != nil {
		return nil, gnet.Close
	}
	logger.PhoenixLoggerIns().Debugf("websocket proxy component connect id: %d", conId)
	logger.PhoenixLoggerIns().Debugf("websocket proxy component connect id A: %d, %s", newCon.GetId(), newCon)
	connectManager.PhoenixManagerConnect().PushConnect(newCon)
	wsCoc := wsCodec.NewWsCodec(conId)
	wsCoc.ConnectId = conId
	c.SetContext(wsCoc)
	return nil, gnet.None
}

func (this *WebSocketProxyComponent) OnClose(c gnet.Conn, err error) gnet.Action {
	ws := c.Context().(*wsCodec.WsCodec)
	logger.PhoenixLoggerIns().Debugf("websocket proxy component OnClose connect id: %d", ws.ConnectId)
	connectManager.PhoenixManagerConnect().DeleteConnect(ws.ConnectId)
	return gnet.None
}

func (this *WebSocketProxyComponent) OnTraffic(c gnet.Conn) (action gnet.Action) {
	ws := c.Context().(*wsCodec.WsCodec)
	if ws.ReadBufferBytes(c) == gnet.Close {
		return gnet.Close
	}
	ok, action := ws.Upgrade(c)
	if !ok {
		return
	}

	if ws.Buf.Len() <= 0 {
		return gnet.None
	}
	messages, err := ws.Decode(c)
	if err != nil {
		return gnet.Close
	}
	if messages == nil {
		return
	}
	packets, err := ws.DecodeMsg()
	if err != nil {
		logger.PhoenixLoggerIns().Error(fmt.Sprintf("OnTraffic decode err %v", err))
		return gnet.Close
	}
	packets.Iterator(func(k int, v interface{}) bool {
		if p, ok := v.(*message.Packet); ok {
			connectManager.PhoenixManagerConnect().AddPackets(ws.ConnectId, p)
		}
		return true
	})
	return gnet.None
}

func (this *WebSocketProxyComponent) Start() error {
	go func() {
		logger.PhoenixLoggerIns().Info(fmt.Sprintf("WebSocketProxyComponent Start At %s", this.EndPoint.String()))
		err := gnet.Run(this, this.EndPoint.TcpFormat(), gnet.WithMulticore(this.Options.Options.Multicore),
			gnet.WithReusePort(true), gnet.WithTicker(true))
		if err != nil {
			logger.PhoenixLoggerIns().Errorf("WebSocketProxyComponent Start failed %v", err)
		}
	}()

	return nil
}
