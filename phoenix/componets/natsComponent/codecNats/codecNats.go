// Package codecNats -----------------------------
// @file      : codecNats.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/9 18:10
// @desc      :
// -------------------------------------------
package codecNats

import (
	"bytes"
	"github.com/gogf/gf/v2/container/garray"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/normal"
	"phoenix/phoenix/network/reader"
)

type CodecNats struct {
	Buf          bytes.Buffer         // 从实际socket中读取到的数据缓存
	PacketReader *reader.PacketReader // 包读取
}

func NewCodecNats() *CodecNats {
	return &CodecNats{
		PacketReader: reader.NewPacketReader(),
	}
}

func (this *CodecNats) ReadBufferBytes(datas []byte) {
	this.Buf.Write(datas)
}

func (this *CodecNats) Decode() (*garray.Array, error) {
	packets := garray.New()
	for {
		if this.PacketReader.ReadSize >= this.Buf.Len() {
			break
		}
		transformBuf := this.Buf.Bytes()[this.PacketReader.ReadSize:]
		if len(transformBuf) >= normal.HeaderSize {
			err, packet := this.PacketReader.ReaderNatsRpc(transformBuf)
			if err != nil {
				logger.PhoenixLoggerIns().Errorf("Error reading message! %v", err)
				this.Buf.Reset()
				this.PacketReader.Reset()
				return nil, err
			}
			if packet != nil {
				packets.Append(packet)
			}
		}

	}
	this.Buf.Reset()
	this.PacketReader.Reset()
	return packets, nil
}
