package natsComponent

import (
	"context"
	"fmt"
	"github.com/gogf/gf/v2/container/garray"
	"github.com/gogf/gf/v2/container/gmap"
	"github.com/nats-io/nats.go"
	"phoenix/phoenix/app"
	"phoenix/phoenix/base"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	error2 "phoenix/phoenix/defines/error"
	"phoenix/phoenix/defines/services"
	"phoenix/phoenix/entitys/instance/message"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/network"
	"phoenix/phoenix/network/protocol/nrpc/golang/natsRpc"
	"phoenix/phoenix/network/rpc"
	"phoenix/phoenix/utils"
	"time"
)

type NatsGameService struct {
	isOK             bool   // 服务是否已经准备好了
	serviceSubscribe string // nats-server 订阅的频道
}

type NatsComponent struct {
	base.BaseComponent
	// 连接地址
	endPoint *network.EndPoint
	// 连接超时时间
	Timeout time.Duration
	// 连接最大重连次数
	MaxReconnect int
	// 连接最大重连间隔
	ReconnectWait int64
	// 连接对象
	Conn *nats.Conn
	// 订阅消息
	natsName string
	// 消息处理
	natsRpc interfaces.INatsRpc
	// 发现的服务
	watchServicesInfo *gmap.StrAnyMap // 服务字典
	// 服务列表
	watchServicesSort *gmap.StrAnyMap // 服务排序列表,用于挑选服务
}

func (this *NatsComponent) Init() error {
	err := this.Connect()
	if err == nil {
		// 处理订阅
		logger.PhoenixLoggerIns().Infof("订阅消息, %s", this.natsName)
		_, err = this.Conn.Subscribe(this.natsName, func(msg *nats.Msg) {
			reErr := this.ReceiveMsg(msg)
			if reErr != nil {
				logger.PhoenixLoggerIns().Error(fmt.Sprintf("接收消息失败, %s", reErr.Error()))
				this.Stop()
			}
		})
		if err != nil {
			return err
		}
	} else {
		logger.PhoenixLoggerIns().Error(fmt.Sprintf("连接nats失败, %s", err.Error()))
		return err
	}
	this.natsRpc.StartWork()
	return nil
}

func (this *NatsComponent) Start() error {
	return nil
}

func (this *NatsComponent) ReceiveMsg(msg interface{}) error {
	if msg == nil {
		return nil
	}
	natMsg, ok := msg.(*nats.Msg)
	if !ok {
		logger.PhoenixLoggerIns().Error(fmt.Sprintf("订阅消息类型转换失败"))
		return nil
	}
	logger.PhoenixLoggerIns().Info(fmt.Sprintf("接收消息, %s", string(natMsg.Data)))
	this.natsRpc.ReadBufferBytes(natMsg.Data)
	packets, err := this.natsRpc.Decode()
	if err != nil {
		logger.PhoenixLoggerIns().Error(fmt.Sprintf("OnTraffic decode err %v", err))
		return err
	}
	packets.Iterator(func(k int, v interface{}) bool {
		if p, ok := v.(*message.NatsRpcPacket); ok {
			this.natsRpc.AddPacket(p)
		}
		return true
	})
	return nil
}

func (this *NatsComponent) SendNatsPacketWithCheck(serviceType string, serviceId string, p interfaces.IPacket) error {
	if this.watchServicesInfo.Contains(serviceType) {
		if v, ok := this.watchServicesInfo.Get(serviceType).(*gmap.StrAnyMap).Get(serviceId).(*NatsGameService); ok {
			if v.isOK {
				return this.SendNatsPacket(v.serviceSubscribe, p)
			} else {
				return utils.MakeError(error2.PHOENIX_ERROR_SERVICES_NOT_READY, "")
			}
		}
	}
	return utils.MakeError(error2.PHOENIX_ERROR_SERVICES_NOT_FIND, "")
}

func (this *NatsComponent) SendNatsPacket(subscribe string, p interfaces.IPacket) error {
	err, datas := this.natsRpc.EncodeNatsRpcPacket(p)
	if err != nil {
		return err
	}
	return this.SendNatsMsg(subscribe, datas)
}

func (this *NatsComponent) SendNatsMsg(subscribe string, data []byte) error {
	if this.Conn == nil {
		return nil
	}
	err := this.Conn.Publish(subscribe, data)
	if err != nil {
		return err
	}
	logger.PhoenixLoggerIns().Infof("nats 发送消息 to, %s", subscribe)
	return nil
}

func (this *NatsComponent) Stop() {
	this.Conn.Close()
	this.Conn = nil
}

func (this *NatsComponent) DiscoverService(serviceType string, serviceId string, serviceSubscribe string) {
	logger.PhoenixLoggerIns().Infof("NatsComponent DiscoverService, %s, %s, %s", serviceType, serviceId, serviceSubscribe)
	if !this.watchServicesInfo.Contains(serviceType) {
		this.watchServicesInfo.Set(serviceType, gmap.NewStrAnyMap(true))
	}
	s := &NatsGameService{
		isOK:             true,
		serviceSubscribe: serviceSubscribe,
	}
	this.watchServicesInfo.Get(serviceType).(*gmap.StrAnyMap).Set(serviceId, s)
	logger.PhoenixLoggerIns().Infof("send Hello msg, %s", serviceSubscribe)
	if !this.watchServicesSort.Contains(serviceType) {
		this.watchServicesSort.Set(serviceType, garray.NewSortedStrArray(true))
	}
	this.watchServicesSort.Get(serviceType).(*garray.SortedStrArray).Append(serviceId)
	//this.SendRpcHello(serviceSubscribe)
}

func (this *NatsComponent) OfflineService(serviceType string, serviceId string) {
	if this.watchServicesInfo.Contains(serviceType) {
		this.watchServicesInfo.Get(serviceType).(*gmap.StrAnyMap).Remove(serviceId)
	}
}
func (this *NatsComponent) CreateRpcBaseWithServiceId(serviceType string, serviceId string, channel interface{},
	connectorId dataType.PhoenixTypeConnectId) error {
	if c, ok := channel.(*natsRpc.RpcBase); ok {
		if this.watchServicesInfo.Contains(serviceType) {
			if v, ok := this.watchServicesInfo.Get(serviceType).(*gmap.StrAnyMap).Get(serviceId).(*NatsGameService); ok {
				if v.isOK {
					this.FillRpcBase(v.serviceSubscribe, c, connectorId)
					return nil
				} else {
					return utils.MakeError(error2.PHOENIX_ERROR_SERVICES_NOT_READY, "")
				}
			}
		}
	}
	return utils.MakeError(error2.PHOENIX_ERROR_SERVICES_NOT_FIND, "")
}

func (this *NatsComponent) FillRpcBase(ToSubscribe string, c *natsRpc.RpcBase, connectorId dataType.PhoenixTypeConnectId) {
	c.FromSubscribe = this.natsName
	c.FromServiceType = this.Service.GetType()
	c.FromServiceId = this.Service.GetId()
	c.ToSubscribe = ToSubscribe
	c.ConnectorId = connectorId
}

func (this *NatsComponent) SelectServiceRandom(serviceType string) (string, error) {
	randomIndex := utils.RandomInt(0, 100)
	return this.SelectService(serviceType, randomIndex)
}

func (this *NatsComponent) SelectService(serviceType string, indexId int) (string, error) {
	if !this.watchServicesSort.Contains(serviceType) {
		return "", utils.MakeError(error2.PHOENIX_ERROR_SERVICES_NOT_FIND, "")
	}
	s := this.watchServicesSort.Get(serviceType).(*garray.SortedStrArray)
	servicesCount := s.Len()
	if servicesCount == 0 {
		return "", utils.MakeError(error2.PHOENIX_ERROR_SERVICES_NOT_FIND, "")
	}
	serviceId, _ := s.Get(indexId % servicesCount)
	return serviceId, nil
}

func (this *NatsComponent) SelectGameService(accountId dataType.PhoenixTypeDataBaseId) (string, error) {
	return this.SelectService(services.PHOENIX_SERVICE_GAME, accountId)
}

func NewNatsComponent(r interfaces.IService, options config.ComponentOptions, ctx context.Context) (interfaces.IComponent, error) {
	phoenixOptions := app.PhoenixIns().GetOptions()
	endPoint := network.NewEndPoint(phoenixOptions.NatsOptions.Host, phoenixOptions.NatsOptions.Port)
	natsInstance := &NatsComponent{
		BaseComponent:     base.NewBaseComponent(r, options, ctx),
		endPoint:          endPoint,
		Timeout:           10,
		MaxReconnect:      10,
		ReconnectWait:     10,
		natsName:          r.GetId(),
		natsRpc:           rpc.NewNatsRpc(r),
		watchServicesInfo: gmap.NewStrAnyMap(true),
		watchServicesSort: gmap.NewStrAnyMap(true),
	}
	return natsInstance, nil
}

func (this *NatsComponent) Connect() error {
	url := fmt.Sprintf("%s", this.endPoint.String())
	phoenixOptions := app.PhoenixIns().GetOptions()
	nc, err := nats.Connect(url,
		nats.UserInfo(phoenixOptions.NatsOptions.User, phoenixOptions.NatsOptions.Pass),
		nats.Timeout(time.Second*this.Timeout),
		nats.MaxReconnects(this.MaxReconnect),
		nats.ReconnectWait(time.Duration(this.ReconnectWait)))
	if err != nil {
		logger.PhoenixLoggerIns().Error(fmt.Sprintf("连接nats服务器失败, %s", err.Error()))
		return err
	}
	this.Conn = nc
	return nil
}
