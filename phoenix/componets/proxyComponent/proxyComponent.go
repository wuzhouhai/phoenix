package proxyComponent

import (
	"context"
	"fmt"
	"github.com/panjf2000/gnet/v2"
	"phoenix/phoenix/base"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/common/logger"
	codec2 "phoenix/phoenix/componets/proxyComponent/codec"
	"phoenix/phoenix/defines/dataType"
	error2 "phoenix/phoenix/defines/error"
	"phoenix/phoenix/entitys/instance/message"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/managers/connectManager"
	"phoenix/phoenix/network"
	"phoenix/phoenix/network/connects"
	"phoenix/phoenix/utils"
	"sync/atomic"
	"time"
)

type ProxyComponent struct {
	base.BaseComponent
	gnet.BuiltinEventEngine
	Eng       gnet.Engine
	EndPoint  *network.EndPoint
	ConnectId dataType.PhoenixTypeConnectId
}

func NewProxyComponentBase(r interfaces.IService, options config.ComponentOptions, ctx context.Context) ProxyComponent {
	endPoint := network.NewEndPoint(options.Options.Proxy.Host, options.Options.Proxy.Port)
	return ProxyComponent{
		BaseComponent: base.NewBaseComponent(r, options, ctx),
		EndPoint:      endPoint,
	}
}

func NewProxyComponent(r interfaces.IService, options config.ComponentOptions, ctx context.Context) (interfaces.IComponent, error) {
	endPoint := network.NewEndPoint(options.Options.Proxy.Host, options.Options.Proxy.Port)
	return &ProxyComponent{
		BaseComponent: base.NewBaseComponent(r, options, ctx),
		EndPoint:      endPoint,
	}, nil
}

func (this *ProxyComponent) OnBoot(eng gnet.Engine) gnet.Action {
	this.Eng = eng
	return gnet.None
}

func (this *ProxyComponent) SelectGameServiceID(connector interfaces.IConnector) error {
	gameServiceId := this.Service.SelectGameService(dataType.PhoenixTypeDataBaseId(connector.GetId()))
	if gameServiceId == "" {
		logger.PhoenixLoggerIns().Errorf("ProxyComponent OnOpen no game service")
		return utils.MakeError(error2.PHOENIX_ERROR_SERVICES_NO_GAME, "no game service")
	}
	connector.SetGameServiceId(gameServiceId)
	return nil
}

func (this *ProxyComponent) OnOpen(c gnet.Conn) (out []byte, action gnet.Action) {
	atomic.AddInt32(&this.ConnectId, 1)
	newCon := connects.NewConnector(this.ConnectId, c, this.Service)
	connectManager.PhoenixManagerConnect().PushConnect(newCon)
	codec := codec2.NewCodec(this.ConnectId)
	c.SetContext(codec)
	return out, action
}

func (this *ProxyComponent) OnTraffic(c gnet.Conn) gnet.Action {
	codec := c.Context().(*codec2.Codec)
	if codec.ReadBufferBytes(c) == gnet.Close {
		return gnet.Close
	}
	if codec.Buf.Len() <= 0 {
		return gnet.None
	}
	packets, err := codec.Decode()
	if err != nil {
		logger.PhoenixLoggerIns().Error(fmt.Sprintf("OnTraffic decode err %v", err))
		return gnet.Close
	}
	packets.Iterator(func(k int, v interface{}) bool {
		if p, ok := v.(*message.Packet); ok {
			connectManager.PhoenixManagerConnect().AddPackets(codec.ConnectId, p)
		}
		return true
	})
	return gnet.None
}

func (this *ProxyComponent) OnTick() (delay time.Duration, action gnet.Action) {
	//logger.PhoenixLoggerIns().Info(fmt.Sprintf("[connected-count=%v]", atomic.LoadInt64(&this.connected)))
	return 3 * time.Second, gnet.None
}

func (this *ProxyComponent) OnClose(c gnet.Conn, err error) gnet.Action {
	if c.Context() != nil {
		codec := c.Context().(*codec2.Codec)
		logger.PhoenixLoggerIns().Debugf("socket proxy component OnClose connect id: %d", codec.ConnectId)
		connectManager.PhoenixManagerConnect().DeleteConnect(codec.ConnectId)
	}
	return gnet.None
}

func (this *ProxyComponent) Stop() {
	this.Eng.Stop(this.Ctx)
}

func (this *ProxyComponent) Init() error {
	return nil
}

func (this *ProxyComponent) Start() error {
	go func() {
		logger.PhoenixLoggerIns().Info(fmt.Sprintf("ProxyComponent Start At %s", this.EndPoint.String()))
		err := gnet.Run(this, this.EndPoint.TcpFormat(), gnet.WithMulticore(this.Options.Options.Multicore),
			gnet.WithReusePort(true), gnet.WithTicker(true))
		if err != nil {
			logger.PhoenixLoggerIns().Errorf("ProxyComponent Start err %v", err.Error())
		}
	}()
	return nil
}
