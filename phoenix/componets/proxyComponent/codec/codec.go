// Package codec -----------------------------
// @file      : codec.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/4/1 15:01
// @desc      :
// -------------------------------------------
package codec

import (
	"bytes"
	"github.com/gogf/gf/v2/container/garray"
	"github.com/panjf2000/gnet/v2"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/defines/dataType"
	"phoenix/phoenix/defines/normal"
	"phoenix/phoenix/network/reader"
)

type Codec struct {
	ConnectId    dataType.PhoenixTypeConnectId // 连接ID
	Buf          bytes.Buffer                  // 从实际socket中读取到的数据缓存
	PacketReader *reader.PacketReader          // 包读取
}

func NewCodec(conId dataType.PhoenixTypeConnectId) *Codec {
	return &Codec{
		ConnectId:    conId,
		PacketReader: reader.NewPacketReader(),
	}
}

func (this *Codec) ReadBufferBytes(c gnet.Conn) gnet.Action {
	size := c.InboundBuffered()
	buf := make([]byte, size)
	read, err := c.Read(buf)
	if err != nil {
		logger.PhoenixLoggerIns().Errorf("read err! %w", err)
		return gnet.Close
	}
	if read < size {
		logger.PhoenixLoggerIns().Errorf("read bytes len err! size: %d read: %d", size, read)
		return gnet.Close
	}
	this.Buf.Write(buf)
	return gnet.None
}

func (this *Codec) Decode() (*garray.Array, error) {
	packets := garray.New()
	for {
		if this.PacketReader.ReadSize >= this.Buf.Len() {
			break
		}
		transformBuf := this.Buf.Bytes()[this.PacketReader.ReadSize:]
		if len(transformBuf) >= normal.HeaderSize {
			err, packet := this.PacketReader.Reader(transformBuf)
			if err != nil {
				logger.PhoenixLoggerIns().Errorf("Error reading message! %v", err)
				this.Buf.Reset()
				this.PacketReader.Reset()
				return nil, err
			}
			if packet != nil {
				packets.Append(packet)
			}
		}

	}
	this.Buf.Reset()
	this.PacketReader.Reset()
	return packets, nil
}
