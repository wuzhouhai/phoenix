// Package etcdComponent -----------------------------
// @file      : etcdComponent.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/9/6 18:08
// @desc      :
// -------------------------------------------
package etcdComponent

import (
	"context"
	"github.com/gogf/gf/v2/util/gconv"
	"go.etcd.io/etcd/api/v3/mvccpb"
	clientv3 "go.etcd.io/etcd/client/v3"
	"phoenix/phoenix/app"
	"phoenix/phoenix/base"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/common/logger"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/network"
	"phoenix/phoenix/utils"
	"time"
)

type EtcdComponent struct {
	base.BaseComponent
	// 连接地址
	endPoint *network.EndPoint
	cli      *clientv3.Client // etcd client
	leaseId  clientv3.LeaseID // 租约ID
	//租约keepalieve相应chan
	keepAliveChan <-chan *clientv3.LeaseKeepAliveResponse
	serviceName   string // 服务名称
	natsName      string // nats服务名称
}

func (this *EtcdComponent) Init() error {
	client, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{this.endPoint.String()},
		DialTimeout: 2 * time.Second,
	})
	if err != nil {
		logger.PhoenixLoggerIns().Error("etcd client init failed")
		return err
	}
	this.cli = client
	timeOutCtx, cancel := context.WithTimeout(this.Ctx, 2*time.Second)
	defer cancel()
	_, err = client.Status(timeOutCtx, this.endPoint.String())
	if err != nil {
		logger.PhoenixLoggerIns().Error("etcd server connect failed")
		return err
	}
	// 设置租约时间
	leaseResp, err := client.Grant(this.Ctx, 5)
	// 注册服务
	_, err = client.Put(this.Ctx, this.serviceName, this.natsName, clientv3.WithLease(leaseResp.ID))
	if err != nil {
		logger.PhoenixLoggerIns().Errorf("register service  %s failed")
		return err
	}
	//设置续期
	this.keepAliveChan, err = client.KeepAlive(this.Ctx, leaseResp.ID)
	if err != nil {
		logger.PhoenixLoggerIns().Errorf("keepalive service %s failed", this.serviceName)
		return err
	}
	this.leaseId = leaseResp.ID
	go this.ListenLeaseRespChan()
	return nil
}

func (this *EtcdComponent) Start() error {
	logger.PhoenixLoggerIns().Debugf("%s etcd component start", this.serviceName)
	this.WatchServices()
	return nil
}

func (this *EtcdComponent) Stop() {
	if this.cli != nil {
		this.cli.Close()
	}
	this.cli = nil
}

func (this *EtcdComponent) WatchServices() {
	prefix := "phoenix/service/"
	// 根据前缀获取现有的keys
	getResp, err := this.cli.Get(this.Ctx, prefix, clientv3.WithPrefix())
	if err != nil {
		logger.PhoenixLoggerIns().Errorf("get keys with prefix %s failed", prefix)
		return
	}
	logger.PhoenixLoggerIns().Debugf("watch services %d", getResp.Count)
	for _, kv := range getResp.Kvs {
		//logger.PhoenixLoggerIns().Debugf("watch services key:%s, value:%s", string(kv.Key), string(kv.Value))
		this.AddService(string(kv.Key), string(kv.Value))
	}

	//监视前缀，修改变更的service
	go this.Watcher(prefix)
}

// 续约监控
func (this *EtcdComponent) ListenLeaseRespChan() {
	for {
		select {
		case <-this.keepAliveChan:
		}
	}
}

// Watcher watcher 监听前缀
func (this *EtcdComponent) Watcher(prefix string) {
	rch := this.cli.Watch(context.Background(), prefix, clientv3.WithPrefix())
	for wresp := range rch {
		for _, ev := range wresp.Events {
			switch ev.Type {
			case mvccpb.PUT: //修改或者新增
				this.AddService(string(ev.Kv.Key), string(ev.Kv.Value))
			case mvccpb.DELETE: //删除
				this.RemoveService(string(ev.Kv.Key))
			}
		}
	}
}

func (this *EtcdComponent) AddService(etcdKey string, etcdValue string) {
	logger.PhoenixLoggerIns().Debugf("%s find service etcdKey: %s, etcdValue : %s", this.serviceName, etcdKey, etcdValue)
	if etcdKey == this.serviceName {
		return
	}
	servicesInfo := utils.SplitString(etcdKey, "/")
	phoenixOptions := app.PhoenixIns().GetOptions()
	if len(servicesInfo) != 5 {
		return
	}
	if phoenixOptions.ServerID != gconv.Int(servicesInfo[2]) {
		return
	}
	serviceType := servicesInfo[3]
	serviceId := servicesInfo[4]
	this.Service.DiscoverService(serviceType, serviceId, etcdValue)
	logger.PhoenixLoggerIns().Debugf("add service Type: %s, serviceId : %s", serviceType, serviceId)
}

func (this *EtcdComponent) RemoveService(etcdKey string) {
	servicesInfo := utils.SplitString(etcdKey, "/")
	phoenixOptions := app.PhoenixIns().GetOptions()
	if len(servicesInfo) != 5 {
		return
	}
	if phoenixOptions.ServerID != gconv.Int(servicesInfo[2]) {
		return
	}
	serviceType := servicesInfo[3]
	serviceId := servicesInfo[4]
	this.Service.OfflineService(serviceType, serviceId)
}

func NewEtcdComponent(r interfaces.IService, options config.ComponentOptions, ctx context.Context) (interfaces.IComponent, error) {
	phoenixOptions := app.PhoenixIns().GetOptions()
	endPoint := network.NewEndPoint(phoenixOptions.EtcdOptions.Host, phoenixOptions.EtcdOptions.Port)
	c := &EtcdComponent{
		BaseComponent: base.NewBaseComponent(r, options, ctx),
		endPoint:      endPoint,
		serviceName: "phoenix/service/" + gconv.String(phoenixOptions.ServerID) + "/" + r.GetType() + "/" +
			r.GetId(),
		cli:           nil,
		leaseId:       0,
		keepAliveChan: nil,
		natsName:      r.GetId(),
	}
	return c, nil
}
