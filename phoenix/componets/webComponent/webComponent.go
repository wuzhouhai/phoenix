// Package webComponent -----------------------------
// @file      : webComponent.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/2 19:07
// @desc      :
// -------------------------------------------
package webComponent

import (
	"context"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
	"phoenix/phoenix/base"
	"phoenix/phoenix/common/config"
	"phoenix/phoenix/interfaces"
	"phoenix/phoenix/network/phttp/middleware"
	"phoenix/phoenix/register"
)

type WebComponent struct {
	base.BaseComponent
	webServer *ghttp.Server
}

func NewWebComponent(r interfaces.IService, options config.ComponentOptions, ctx context.Context) (interfaces.IComponent, error) {
	natsInstance := &WebComponent{
		BaseComponent: base.NewBaseComponent(r, options, ctx),
		webServer:     g.Server("webComponent"),
	}
	return natsInstance, nil
}

func (this *WebComponent) Init() error {
	configMap := make(map[string]interface{})
	for _, v := range this.Options.Options.Web.GServerOptions {
		configMap[v.ConfigKey] = v.ConfigValue
	}
	if len(configMap) > 0 {
		this.webServer.SetConfigWithMap(configMap)
	}
	// 注册前置中间件
	preLocalMiddlewares := register.GetLocalPreMiddlewares()
	for _, middleware := range preLocalMiddlewares {
		this.webServer.Use(middleware)
	}
	// 注册后置中间件
	postLocalMiddlewares := register.GetLocalPostMiddlewares()
	for _, middleware := range postLocalMiddlewares {
		this.webServer.Use(middleware)
	}
	// 这个是用来处理返回数据结构体的
	this.webServer.Use(middleware.PhoenixMiddlewareHandlerResponse)
	// 注册分组路由
	routerGroups := register.GetRouters()
	for _, routerGroup := range routerGroups {
		routerGroupCreate := register.GetRouterGroupCreateFunc(routerGroup)
		this.webServer.Group(routerGroup, func(group *ghttp.RouterGroup) {
			// 注册组前置中间件
			preGroupMiddlewares := register.GetPreMiddlewares(routerGroup)
			for _, middleware := range preGroupMiddlewares {
				group.Middleware(middleware)
			}
			// 注册组后置中间件
			postGroupMiddlewares := register.GetPostMiddlewares(routerGroup)
			for _, middleware := range postGroupMiddlewares {
				group.Middleware(middleware)
			}
			// 注册组路由
			group.Bind(routerGroupCreate())
		})
	}
	return nil
}

func (this *WebComponent) Start() error {
	err := this.webServer.Start()
	if err != nil {
		return err
	}
	return nil
}

func (this *WebComponent) Stop() {

}
