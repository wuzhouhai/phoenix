﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using log4net;

namespace Update
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private delegate void SetprogressBarHandle(int vaule);    //定义 代理函数 
        private delegate void SetprogressLabelHandle(string vaule);    //定义 代理函数 
        private delegate void SetUpdateTextBoxHandle(string vaule);    //定义 代理函数 
        private delegate void ShowBtnOkHandle();    //定义 代理函数 
        public MainWindow()
        {
            InitializeComponent();
            StartDowndUpdateIni();
        }

        public void downLoadThread(object o)
        {
            DownloadHelper dn = new DownloadHelper();
            dn.ShowDownloadPercent += this.downloadCallBackAction;
            dn.DownloadFile((string)o, "update.zip");

        }

        public void downloadCallBackAction(string fName, int percent)
        {
            ILog m_log = LogManager.GetLogger("log");
            m_log.Debug("下载进度" + percent);
            this.Dispatcher.Invoke(new SetprogressBarHandle(this.SetprogressBar), percent);
            // 下载完成,解压
            if (percent == 999)
            {
                string p1 = System.Environment.CurrentDirectory + "\\update.zip";   //待解压的文件
                string p2 = System.Environment.CurrentDirectory + "\\";//解压后放置的目标目录
                string[] FileProperties = new string[2];
                FileProperties[0] = p1;
                FileProperties[1] = p2;
                this.Dispatcher.Invoke(new SetprogressLabelHandle(this.SetprogressLabel), "下载完成开始解压文件.");
                UnZipClass UnZc = new UnZipClass();
                UnZc.UnZip(FileProperties);
                string filePath = System.Environment.CurrentDirectory + "\\" + "update.ini";
                IniFile inFile = new IniFile(filePath);
                int verNo = inFile.GetInt("version", "verNo", 1);
                string verInfo = inFile.GetString("version", "ver", "1.0.0.0");

                string fileVerPath = System.Environment.CurrentDirectory + "\\" + "version.ini";
                IniFile inFile2 = new IniFile(fileVerPath);
                inFile2.WriteInt("version", "verNo", verNo);
                inFile2.WriteString("version", "ver", verInfo);
                File.Delete("update.ini");
                File.Delete("update.zip");
                this.Dispatcher.Invoke(new SetprogressLabelHandle(this.SetprogressLabel), "更新完成,请点击确定重新启动程序.");
                this.Dispatcher.Invoke(new ShowBtnOkHandle(this.ShowBtnOk));
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //StartUpdate();
            Process.Start("GameTools.exe");
            Application.Current.Shutdown();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string p1 = System.Environment.CurrentDirectory + "\\update.zip";   //待解压的文件
            string p2 = System.Environment.CurrentDirectory + "\\";//解压后放置的目标目录
            string[] FileProperties = new string[2];
            FileProperties[0] = p1;
            FileProperties[1] = p2;
            UnZipClass UnZc = new UnZipClass();
            UnZc.UnZip(FileProperties);
        }
        
        public void StartUpdate()
        {
            //通过ParameterizedThreadStart创建线程
            Thread thread = new Thread(new ParameterizedThreadStart(this.downLoadThread));
            //给方法传值
            thread.Start("http://lod-us.game.gameark.cn/static/update.zip");
            updateTips.Content = "正在更新.......";
        }

        public void SetprogressBar(int percent)
        {
            dProgress.Value = percent;
            updateTips.Content = "正在更新.......  %" + percent;
        }

        public void SetprogressLabel(string info)
        {
            updateTips.Content = info;
        }

        public void SetUpdateTextBox(string info)
        {
            updateInfoBox.Text = info;
        }

        public void ShowBtnOk()
        {
            // 显示确定按钮
            this.Btn_OK.Visibility = Visibility.Visible;
        }
        //下载INI更新配置文件
        public void StartDowndUpdateIni()
        {
            //通过ParameterizedThreadStart创建线程
            Thread thread = new Thread(new ParameterizedThreadStart(this.DownLoadUpdateIniThread));
            //给方法传值
            thread.Start("http://lod-us.game.gameark.cn/static/update.ini");
        }
        public void DownLoadUpdateIniThread(object o)
        {
            DownloadHelper dn = new DownloadHelper();
            dn.ShowDownloadPercent += this.UpdateIniDownloadProgress;
            dn.DownloadFile((string)o, "update.ini");
        }

        public void UpdateIniDownloadProgress(string fName, int percent)
        {
            if(percent == 999)
            {
                string filePath = System.Environment.CurrentDirectory + "\\" + "update.ini";
                IniFile inFile = new IniFile(filePath);
                int verNo = inFile.GetInt("version", "verNo", 1);
                string verInfo = inFile.GetString("version", "ver", "1.0.0.0");
                string updateInfo = inFile.utf8ToUnicode(inFile.GetString("update", "info", "更新内容"));

                string fileVerPath = System.Environment.CurrentDirectory + "\\" + "version.ini";
                IniFile inFile2 = new IniFile(fileVerPath);
                int verNo2 = inFile2.GetInt("version", "verNo", 1);
                if(verNo != verNo2)
                {
                    this.Dispatcher.Invoke(new SetUpdateTextBoxHandle(this.SetUpdateTextBox), updateInfo);
                    StartUpdate();
                }
                else
                {
                    File.Delete("update.ini");
                    this.Dispatcher.Invoke(new SetprogressLabelHandle(this.SetprogressLabel), "当前已经是最新版本.");
                }
            }
        }
    }
}
