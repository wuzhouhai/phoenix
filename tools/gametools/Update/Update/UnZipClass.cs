﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Update
{
    class UnZipClass
    {
        /// <summary>
        /// 解压ZIP
        /// </summary>
        /// <param name="args">要解压的ZIP文件和解压后保存到的文件夹</param>
        public void UnZip(string[] args)
        {
            ZipInputStream s = new ZipInputStream(File.OpenRead(args[0]));  // 获取要解压的ZIP文件

            Encoding gbk = Encoding.GetEncoding("gbk");      // 防止中文名乱码
            ZipConstants.DefaultCodePage = gbk.CodePage;

            ZipEntry theEntry;
            while ((theEntry = s.GetNextEntry()) != null)
            {
                string directoryName = Path.GetDirectoryName(args[1]);   // 获取解压后保存到的文件夹
                string fileName = Path.GetFileName(theEntry.Name);
                
                if(fileName == String.Empty)
                {
                    string dirName = directoryName + "\\" + theEntry.Name;
                    Directory.CreateDirectory(dirName);
                    continue;
                }
                //生成解压目录
                Directory.CreateDirectory(directoryName);

                if (fileName != String.Empty)
                {
                    //解压文件到指定的目录
                    FileStream streamWriter = File.Create(args[1] + theEntry.Name);

                    int size = 2048;
                    byte[] data = new byte[2048];
                    while (true)       //  循环写入单个文件
                    {
                        size = s.Read(data, 0, data.Length);
                        if (size > 0)
                        {
                            streamWriter.Write(data, 0, size);     // 每次写入的文件大小
                        }
                        else
                        {
                            break;
                        }
                    }

                    streamWriter.Close();
                }
            }
            s.Close();
        }

    }
}
