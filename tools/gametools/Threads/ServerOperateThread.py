# -*- coding: utf-8 -*-
"""
@File       :  ServerOperateThread.py
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@arkgames.com
@Time       :  2022/6/23 14:53
@Description:  
"""

from PyQt5 import QtCore
from PyQt5.QtCore import *
from Common.ServerManager import ServerManager
from Common.CongfigZipManager import ConfigZipManager
from Common.HttpHelper import HttpHelper
from Common import Const


class ServerOperateThread(QtCore.QRunnable):

    def __init__(self, url, cmdType, communication):
        super(ServerOperateThread, self).__init__()
        self._communication = communication
        self._url = url
        self._cmdType = cmdType

    def run(self) -> None:
        requstDatas = {
            'cmdType': self._cmdType,
        }
        try:
            response = HttpHelper.PostFormData(self._url, requstDatas)
            if self._cmdType == Const.ServerOperateCmd_Start:
                self._communication.CmdStartSignal.emit({'back': response, 'err': None})
            elif self._cmdType == Const.ServerOperateCmd_Stop:
                self._communication.CmdStopSignal.emit({'back': response, 'err': None})
        except Exception as e:
            if self._cmdType == Const.ServerOperateCmd_Start:
                self._communication.CmdStartSignal.emit({'back': None, 'err': e})
            elif self._cmdType == Const.ServerOperateCmd_Stop:
                self._communication.CmdStopSignal.emit({'back': None, 'err': e})
