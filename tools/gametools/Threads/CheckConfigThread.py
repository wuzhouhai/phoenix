# -*- coding: utf-8 -*-
"""
@File       :  CheckConfigThread
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@arkgames.com
@Time       :  2022/12/15 10:34
@Description:  
"""

from PyQt5 import QtCore
from PyQt5.QtCore import *
from Common import Const
from Modules.AllSheetManager import AllSheetManager
from Excel.ExcelHelper import ExcelHelper
from Analysis.SheetAnalysis import SheetAnalysis
from Common.Utils import Utils
import re


class CheckConfigThread(QtCore.QRunnable):

    def __init__(self, allConfigs, mainApp):
        super(CheckConfigThread, self).__init__()
        self._allConfigs = allConfigs
        self._allSheetManager = AllSheetManager()
        self._mainApp = mainApp

    def getRuleConfig(self, ruleId, feildKey):
        return self._allSheetManager.GetSheetValue(Const.CheckConfigExcelSheetName, ruleId, feildKey)

    def checkTableMainKey(self, sheetName, mainKey):
        return self._allSheetManager.CheckSheetMainKey(sheetName, mainKey)

    def checkRuleTypeInConfig(self, checkType, checkValue, ruleValue):
        # 检测是否有正则
        checkValue: str = str(checkValue)
        if len(checkValue) == 0:
            return True, ""
        ruleRe = self.getRuleConfig(ruleValue, "regular")
        if ruleRe and len(ruleRe) > 0:
            if not Utils.CheckMatchRe(checkValue, ruleRe):
                return False, "正则效验错误"
        # 开始按照规则对检测字符进行拆分,最多支持三级拆分
        splitChar3 = self.getRuleConfig(ruleValue, "splitChar3")
        splitChar2 = self.getRuleConfig(ruleValue, "splitChar2")
        splitChar1 = self.getRuleConfig(ruleValue, "splitChar1")
        splitInfo = checkValue
        splitCounts = 0
        # 第三级拆分
        if splitChar3 and len(splitChar3):
            splitCounts += 1
            splitInfo = splitInfo.split(splitChar3)
        # 二级拆分
        if splitChar2 and len(splitChar2):
            splitCounts += 1
            if isinstance(splitInfo, list):
                tempInfo = list()
                # 遍历拆分
                for dInfo in splitInfo:
                    dSplitInfo = dInfo.split(splitChar2)
                    if dSplitInfo and len(dSplitInfo) > 0:
                        tempInfo.append(dSplitInfo)
                # 替换
                splitInfo = tempInfo
            elif isinstance(splitInfo, str):
                splitInfo = splitInfo.split(splitChar2)
        # 一级拆分
        if splitChar1 and len(splitChar1):
            splitCounts += 1
            if isinstance(splitInfo, list):
                tempInfo = list()
                # 遍历拆分
                for dInfo in splitInfo:
                    dSplitInfo = dInfo.split(splitChar1)
                    if dSplitInfo and len(dSplitInfo) > 0:
                        tempInfo.append(dSplitInfo)
                # 替换
                splitInfo = tempInfo
            elif isinstance(splitInfo, str):
                splitInfo = splitInfo.split(splitChar1)

        # 检测
        charRule1 = self.getRuleConfig(ruleValue, "charRule1")
        charRule2 = self.getRuleConfig(ruleValue, "charRule2")
        charRule3 = self.getRuleConfig(ruleValue, "charRule3")
        charRule4 = self.getRuleConfig(ruleValue, "charRule4")
        charRuleExtend2 = self.getRuleConfig(ruleValue, "charRuleExtend2")
        charRuleExtend3 = self.getRuleConfig(ruleValue, "charRuleExtend3")
        charRuleExtend4 = self.getRuleConfig(ruleValue, "charRuleExtend4")
        if isinstance(splitInfo, list):
            for cInfo in splitInfo:
                if isinstance(cInfo, list):
                    try:
                        # 判断长度
                        if len(cInfo) >= 1:
                            cValue = int(cInfo[0])
                            if charRule1 and len(charRule1) > 0:
                                ret, msg = self.checkSingleValue(cValue, charRule1)
                                if not ret:
                                    return ret, msg
                        if len(cInfo) >= 2:
                            cValue1 = int(cInfo[0])
                            cValue2 = int(cInfo[1])
                            if charRule2 and len(charRule2) > 0:
                                ret, msg = self.checkSingleValue(cValue2, charRule2)
                                if not ret:
                                    return ret, msg
                            if charRuleExtend2 and len(charRuleExtend2) > 0:
                                ret, msg = self.checkSingleValueExtend(cValue2, charRuleExtend2, cValue1)
                                if not ret:
                                    return ret, msg
                        if len(cInfo) >= 3:
                            cValue2 = int(cInfo[1])
                            cValue3 = int(cInfo[2])
                            if charRule3 and len(charRule3) > 0:
                                ret, msg = self.checkSingleValue(cValue3, charRule3)
                                if not ret:
                                    return ret, msg
                            if charRuleExtend3 and len(charRuleExtend3) > 0:
                                ret, msg = self.checkSingleValueExtend(cValue3, charRuleExtend3, cValue2)
                                if not ret:
                                    return ret, msg
                        if len(cInfo) >= 4:
                            cValue3 = int(cInfo[2])
                            cValue4 = int(cInfo[3])
                            if charRule4 and len(charRule4) > 0:
                                ret, msg = self.checkSingleValue(cValue4, charRule4)
                                if not ret:
                                    return ret, msg
                            if charRuleExtend4 and len(charRuleExtend4) > 0:
                                ret, msg = self.checkSingleValueExtend(cValue4, charRuleExtend4, cValue3)
                                if not ret:
                                    return ret, msg
                    except:
                        continue
                else:
                    try:
                        cValue = int(cInfo)
                        if charRule1 and len(charRule1) > 0:
                            ret, msg = self.checkSingleValue(cValue, charRule1)
                            if not ret:
                                return ret, msg
                    except:
                        continue
        else:
            if checkType == "number":
                try:
                    cValue = int(checkValue)
                    if charRule1 and len(charRule1) > 0:
                        ret, msg = self.checkSingleValue(cValue, charRule1)
                        if not ret:
                            return ret, msg
                except:
                    return False, "配置不是整形数据"
        return True, ""

    def checkSingleValueExtend(self, checkValue, ruleExtend, parentValue=None):
        if ruleExtend and len(ruleExtend) > 0:
            # 二级拆分
            ruleInfo = ruleExtend.split(";")
            for ruleSub in ruleInfo:
                # 一级拆分
                subRuleInfo = ruleSub.split(",")
                if subRuleInfo:
                    if len(subRuleInfo) == 2: # 规则数组有2,需要关心父值
                        pCheckValue = int(subRuleInfo[0])
                        subRule = subRuleInfo[1]
                        if parentValue and parentValue == pCheckValue:
                            ret, msg = self.checkSignleRule(checkValue, subRule)
                            if not ret:
                                return ret, msg
                    else:
                        return False, "配置检测逻辑信息无效 配置规则:%s" % subRuleInfo
        return True, ""

    def checkSingleValue(self, checkValue, rule):
        if rule and len(rule) > 0:
            # 二级拆分,用分号分割规则,必须全部满足
            ruleInfo = rule.split(";")
            for ruleSub in ruleInfo:
                checkResults = list()
                # 一级拆分,用逗号分割的规则,满足一项即可
                subRuleInfo = ruleSub.split(",")
                if subRuleInfo:
                    subErrList = list()
                    subRet = False
                    for subRule in subRuleInfo:
                        ret, msg = self.checkSignleRule(checkValue, subRule)
                        if ret:
                            subRet = True
                            checkResults.append(True)
                            subErrList.clear()
                            break
                        else:
                            subErrList.append(msg)
                    if not subRet:
                        retMsg = ""
                        for subErr in subErrList:
                            retMsg += subErr + "\n"
                        return False, subErrList
        return True, ""

    def checkSignleRule(self, checkValue, ruleInfo):
        try:
            if ruleInfo:
                # 看看是验证规则
                # 验证取值范围
                if ruleInfo.find("-") != -1 and Utils.CheckMatchRe(ruleInfo, r"\d+-\d+"):
                    valueInfo = ruleInfo.split("-")
                    if valueInfo:
                        if len(valueInfo) == 2:
                            minValue = int(valueInfo[0])
                            maxValue = int(valueInfo[1])
                            ret = self.checkValueRange(checkValue, minValue, maxValue, False)
                            if not ret:
                                return False, "配置区间范围检测失败,只能配置[%s-%s], 当前配置[%s]" % (str(minValue), str(maxValue), str(checkValue))
                # 是否有比较符号 <
                if ruleInfo.find("<") != -1 and Utils.CheckMatchRe(ruleInfo, r"<\d+"):
                    valueInfo = ruleInfo.replace('<','')
                    maxValue = int(valueInfo)
                    ret = self.checkValueRange(checkValue, None, maxValue, False)
                    if not ret:
                        return False, "配置区间范围检测失败,只能配置[<%s]" % str(maxValue)
                # 是否有比较符号 <=
                if ruleInfo.find("<=") != -1 and Utils.CheckMatchRe(ruleInfo, r"<=\d+"):
                    valueInfo = ruleInfo.replace('<=','')
                    maxValue = int(valueInfo)
                    ret = self.checkValueRange(checkValue, None, maxValue, True)
                    if not ret:
                        return False, "配置区间范围检测失败,只能配置[<=%s]" % str(maxValue)
                # 是否有比较符号 >
                if ruleInfo.find(">") != -1 and Utils.CheckMatchRe(ruleInfo, r">\d+"):
                    valueInfo = ruleInfo.replace('>', '')
                    minValue = int(valueInfo)
                    ret = self.checkValueRange(checkValue, minValue, None, False)
                    if not ret:
                        return False, "配置区间范围检测失败,只能配置[>%s]" % str(minValue)
                # 是否有比较符号 >=
                if ruleInfo.find(">=") != -1 and Utils.CheckMatchRe(ruleInfo, r">=\d+"):
                    valueInfo = ruleInfo.replace('>=', '')
                    minValue = int(valueInfo)
                    ret = self.checkValueRange(checkValue, minValue, None, True)
                    if not ret:
                        return False, "配置区间范围检测失败,只能配置[>=%s]" % str(minValue)
                # 是否有比较符号 ==
                if ruleInfo.find("==") != -1 and Utils.CheckMatchRe(ruleInfo, r"==\d+"):
                    valueInfo = ruleInfo.replace('==', '')
                    cValue = int(valueInfo)
                    if cValue != checkValue:
                        return False, "配置区间范围检测失败,只能配置[==%s]" % str(cValue)
                # 是否有符号@
                if ruleInfo.find("@") != -1 and Utils.CheckMatchRe(ruleInfo, r"@[a-z,A-Z]+"):
                    checkValue = int(checkValue)
                    valueInfo = ruleInfo.replace('@', '')
                    ret = self.checkTableMainKey(valueInfo, checkValue)
                    if not ret:
                        return False, "外链表主键检测失败, 表[%s]中不存在主键[%s]" % (valueInfo, str(checkValue))
        except:
            return False, "规则填写错误,规则配置[%s]" % ruleInfo
        return True, ""

    def checkValueRange(self, checkValue, minValue, maxValue, isOpen):
        if minValue is not None and maxValue is not None:
            if minValue > maxValue:
                minValue, maxValue = maxValue, minValue
            return minValue <= checkValue <= maxValue
        if minValue is not None:
            if isOpen:
                return checkValue >= minValue
            else:
                return checkValue > minValue
        if maxValue is not None:
            if isOpen:
                return checkValue <= maxValue
            else:
                return checkValue < maxValue

    def checkRuleTypeTable(self, checkValue, ruleValue):
        checkValue = int(checkValue)
        valueInfo = ruleValue.replace('@', '')
        ret = self.checkTableMainKey(valueInfo, checkValue)
        if not ret:
            return False, "外链表主键检测失败, 表[%s]中不存在主键[%s]" % (valueInfo, str(checkValue))
        return True, ""

    def makeErrCheckMsg(self, sheetName, checkDataKey, checkDataValue, msg, mainKeyValue):
        eMsg = "配置表[%s],主键[%s], 字段[%s], 配置[%s], 错误: %s" % (sheetName, str(mainKeyValue), checkDataKey, str(checkDataValue), msg)
        return eMsg

    def CheckSheet(self, sheet: SheetAnalysis):
        """检测表"""
        checkErr = list() # 错误信息内容
        # 遍历数据,看看哪些需要检测
        for i, typeData in enumerate(sheet.TypeList):
            tRule = typeData["rule"]
            # 如果需要验证规则
            if tRule:
                ruleType = tRule["ruleType"]
                ruleValue = tRule["ruleValue"]
                if ruleType and ruleValue:
                    if ruleType == Const.CheckRuleTypeInConfig:
                        self._mainApp.ThreadLogSignal.emit("使用规则表编号[%d]号规则,检测表[%s]字段[%s]" % (ruleValue, sheet.SheetName,
                                                           sheet.FeildList[i]), 0)
                    elif ruleType == Const.CheckRuleTypeTable:
                        self._mainApp.ThreadLogSignal.emit("检测表[%s]字段[%s], 外链表[%s]主键" % (sheet.SheetName, sheet.FeildList[i], str(ruleValue)), 0)
                    for data in sheet.AllDatas:
                        eMsg = None
                        # 遍历类型信息 看看有没有需要检测的内容
                        if ruleType == Const.CheckRuleTypeInConfig:
                            ret, msg = self.checkRuleTypeInConfig(typeData["iType"], data[i], ruleValue)
                            if not ret:
                                eMsg = self.makeErrCheckMsg(sheet.SheetName, sheet.FeildList[i], data[i], msg, data[sheet.MainKeyIndex])
                        elif ruleType == Const.CheckRuleTypeTable:
                            retA, msgA = self.checkRuleTypeTable(data[i], ruleValue)
                            if not retA:
                                eMsg = self.makeErrCheckMsg(sheet.SheetName, sheet.FeildList[i], data[i], msgA, data[sheet.MainKeyIndex])
                        if eMsg:
                            checkErr.append(eMsg)
        return checkErr

    def run(self) -> None:
        self._mainApp.ThreadLogSignal.emit("检测线程开始运行", 0)
        self._mainApp.ThreadLogSignal.emit("开始分析所有配置表", 0)
        for _, cnfFile in enumerate(self._allConfigs):
            exl = ExcelHelper(cnfFile)
            sheetNames = exl.GetAllSheetNames()
            for sheetName in sheetNames:
                if sheetName.find("@") != -1:
                    self._mainApp.ThreadLogSignal.emit("正在分析表[%s]" % sheetName, 0)
                    sheet = exl.GetSheetByName(sheetName)
                    if sheet:
                        sName = sheetName.lstrip('@')
                        ret = ExcelHelper.AnalysisConfig(sheet)
                        sheetAnalysis = SheetAnalysis(sName)
                        sheetAnalysis.Sheet = sheet
                        sheetAnalysis.DecList = ret["decList"]
                        sheetAnalysis.TypeList = ret["typeList"]
                        sheetAnalysis.FeildList = ret["feildList"]
                        sheetAnalysis.AllDatas = ret["allDatas"]
                        sheetAnalysis.MainKeyIndex = ret["mainKeyIndex"]
                        sheetAnalysis.AllItems = ret["dictDatas"]
                        self._allSheetManager.SetSheet(sName, sheetAnalysis)
        self._mainApp.ThreadLogSignal.emit("配置表分析完成", 0)
        self._mainApp.ThreadLogSignal.emit("开始检测配置表", 0)
        for name, cSheet in self._allSheetManager.AllSheet.items():
            if name.find(Const.CheckConfigExcelSheetName) != -1:
                continue
            errList = self.CheckSheet(cSheet)
            for errMsg in errList:
                self._mainApp.ThreadLogSignal.emit(errMsg, 1)
        self._mainApp.ThreadLogSignal.emit("检测配置表完成", 0)
        self._mainApp.CheckConfigFinishSignal.emit()