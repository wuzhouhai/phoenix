# -*- coding: utf-8 -*-
"""
@File       :  CheckOutExcelConfigThread
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@arkgames.com
@Time       :  2022/7/12 17:28
@Description:  
"""
import shutil
import svn.local
import svn.remote
from PyQt5 import QtCore
from Common import Const
from Common.Utils import Utils
import os
import stat


class SvnExcelConfigThread(QtCore.QRunnable):

    def __init__(self, operateType, svnUrl, localSvnPath, backLogSuc, backLogErr, communication):
        super(SvnExcelConfigThread, self).__init__()
        self._operateType = operateType
        self._communication = communication
        self._svnUrl = svnUrl
        self._localSvnPath = localSvnPath
        self._backLogSuc = backLogSuc
        self._backLogErr = backLogErr

    def run(self) -> None:
        if self._operateType == 1:      # checkout
            if Utils.CheckFilePath(self._localSvnPath):
                shutil.rmtree(self._localSvnPath, onerror=self.remove_readonly)
            try:
                r = svn.remote.RemoteClient(self._svnUrl)
                r.checkout(self._localSvnPath)
                if self._communication:
                    self._communication.ThreadLogSignal.emit(self._backLogSuc, 0)
            except Exception as e:
                if self._communication:
                    self._communication.ThreadLogSignal.emit(self._backLogErr, 1)
                    self._communication.ThreadLogSignal.emit(e.strerror + e.filename, 1)

    def remove_readonly(self, fun, path, _):
        os.chmod(path, stat.S_IWRITE)
        fun(path)