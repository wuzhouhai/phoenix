# -*- coding: utf-8 -*-
"""
@File       :  UpdateServerConfig
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@arkgames.com
@Time       :  2022/6/20 17:40
@Description:  
"""
from PyQt5 import QtCore
from PyQt5.QtCore import *
from Common.ServerManager import ServerManager
from Common.CongfigZipManager import ConfigZipManager
from Common.HttpHelper import HttpHelper
from Common import Const


class UpdateServerConfigThread(QtCore.QRunnable):

    def __init__(self, url, configPath, isRestartServer, communication):
        super(UpdateServerConfigThread, self).__init__()
        self._communication = communication
        self._url = url
        self._configPath = configPath
        self._isRestartServer = isRestartServer

    def run(self) -> None:
        fileName = self._configPath.split('\\')[-1]
        requstDatas = {
            'file': (fileName, open(self._configPath, 'rb').read(), 'application/zip'),
            'fileName': fileName,
            'ver': fileName.split("_")[1].split(".")[0],
            'sign': 'sign'
        }
        try:
            response = HttpHelper.PostFormData(self._url, requstDatas)
            self._communication.UpdateConfigSignal.emit({'back': response, 'err': None})
        except Exception as e:
            self._communication.UpdateConfigSignal.emit({'back': None, 'err': e})


