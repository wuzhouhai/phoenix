# -*- coding: utf-8 -*-
"""
@File       :  ItemAnalysisThread
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/9 11:47
@Description:  
"""
from PyQt5 import QtCore
from PyQt5.QtCore import *
from Modules.BaseManager import BaseManager


class AnalysisThread(QtCore.QThread):
    WorkFinishSignal = pyqtSignal(str)

    def __init__(self, bManager):
        super(AnalysisThread, self).__init__()
        self._iManager: BaseManager = bManager

    def run(self) -> None:
        print("AnalysisThread Start")
        self._iManager.analysisConfig()
        self.WorkFinishSignal.emit(self._iManager.GetModuleName())
        print("AnalysisThread Over")
