# -*- coding: utf-8 -*-
"""
@File       :  GetConfigVerThread
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@arkgames.com
@Time       :  2022/6/27 18:22
@Description:  
"""
from PyQt5 import QtCore
from PyQt5.QtCore import *
from Common.ServerManager import ServerManager
from Common.CongfigZipManager import ConfigZipManager
from Common.HttpHelper import HttpHelper
from Common import Const


class GetConfigVerThread(QtCore.QRunnable):

    def __init__(self, url, communication):
        super(GetConfigVerThread, self).__init__()
        self._communication = communication
        self._url = url

    def run(self) -> None:
        try:
            response = HttpHelper.PostFormData(self._url, {})
            self._communication.GetConfigVerSignal.emit({'back': response, 'err': None})
        except Exception as e:
            self._communication.GetConfigVerSignal.emit({'back': None, 'err': e})
