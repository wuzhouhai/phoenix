# -*- coding: utf-8 -*-
"""
@File       :  ReleaseConfigThread
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@arkgames.com
@Time       :  2022/7/13 14:34
@Description:  处理全配置发布到指定服务器
"""
import json
import shutil
import svn.local
import svn.remote
from OutPut.OutPutConfigWorkThread import OutPutConfigWorkThread
from Analysis.ConfigPathAnalysis import ConfigPathAnalysis
from Common.HttpHelper import HttpHelper
from PyQt5 import QtCore
from Common import Const
from Common.Utils import Utils
import os
import stat
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement
from xml.etree.ElementTree import ElementTree


class ReleaseConfigCmdThread(QtCore.QRunnable):

    def __init__(self):
        super(ReleaseConfigCmdThread, self).__init__()

    def run(self) -> None:
        re, e = self.UpdateSvn(Const.LoaclSvnExcelPathRelease)
        if not re:
            return
        re, e = self.UpdateSvn(Const.LocalSvnClientConfigsPathRelease)
        if not re:
            return
        re, e = self.UpdateSvn(Const.LocalServerOutPutPathRelease)
        if not re:
            return
        allConfigFiles, allConfigFileNames = ConfigPathAnalysis.FindAllConfigExcels(Const.LoaclSvnExcelPathRelease)
        for index, cnfFile in enumerate(allConfigFiles):
            # print("开始创建导出配置线程_A")
            fName = allConfigFileNames[index]
            # print("开始创建导出配置线程_B")
            wThread = OutPutConfigWorkThread(cnfFile, fName, Const.LocalSvnClientConfigsPathRelease, Const.LocalServerOutPutPathRelease, Const.OutPutType_All, False)
            # print("开始创建导出配置线程_C")
            wThread.start()
            wThread.wait()
        # 生成版本号
        Utils.CreateConfigServion("release_")
        verJson = {"id": 1, "ver": Utils.ConfigVersion}
        outputCFile = Const.LocalServerOutPutPathRelease + "/t_Ver.json"
        Utils.WriteFile(outputCFile, json.dumps(verJson))
        self.CreateVersionXmlInfo()
        verJson = {"id": 1, "ver": Utils.ConfigVersion}
        outputCFile = Const.LocalSvnClientConfigsPathRelease + "/t_Ver.json"
        Utils.WriteFile(outputCFile, json.dumps(verJson))
        # fileStrC = Const.FormatVerLuaStrClient % Utils.ConfigVersion
        # outputCFile = Const.LocalSvnClientConfigsPathRelease + "t_Ver.lua"
        # Utils.WriteFile(outputCFile, fileStrC)
        # 开始提交
        re, e = self.CommitSvn(Const.LocalServerOutPutPathRelease)
        if not re:
            return
        re, e = self.CommitSvn(Const.LocalSvnClientConfigsPathRelease)
        if not re:
            return
        print("CMD全量发布完成")

    def CreateVersionXmlInfo(self):
        """生成一份xml信息,保存版本和发布人"""
        root = Element('root')
        ver = SubElement(root, 'ver')
        ver.text = Utils.ConfigVersion
        author = SubElement(root, 'author')
        author.text = "cmd配置生成工具"
        tree = ElementTree(root)
        tree.write(Const.LocalServerOutPutPathRelease + 'ver.xml', encoding='utf-8')

    def revert(self, svnLocal, localPath):
        cmd = []
        # if revision is not None:
        cmd += ['-R', '.']
        # cmd += rel_filepaths
        svnLocal.run_command(
            'revert',
            cmd,
            wd=localPath)

    def clearPath(self, localPath):
        delList = os.listdir(localPath)
        for f in delList:
            if f == ".svn":
                continue
            filePath = os.path.join(localPath, f)
            if os.path.isfile(filePath):
                os.remove(filePath)
            elif os.path.isdir(filePath):
                shutil.rmtree(filePath, True)

    def UpdateSvn(self, localPath):
        try:
            svnLocal = svn.local.LocalClient(localPath)
            # self.revert(svnLocal, localPath)
            svnLocal.cleanup()
            self.clearPath(localPath)
            svnLocal.update()
        except Exception as e:
            return False, e
        return True, None

    def CommitSvn(self, localPath):
        try:
            svnLocal = svn.local.LocalClient(localPath)
            svnLocal.cleanup()
            svnLocal.commit("update configs by tools")
        except Exception as e:
            return False, e
        return True, None


