# -*- coding: utf-8 -*-
"""
@File       :  ReleaseConfigThread
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@arkgames.com
@Time       :  2022/7/13 14:34
@Description:  处理全配置发布到指定服务器
"""
import shutil
import svn.local
import svn.remote
from OutPut.OutPutConfigWorkThread import OutPutConfigWorkThread
from Analysis.ConfigPathAnalysis import ConfigPathAnalysis
from Common.HttpHelper import HttpHelper
from PyQt5 import QtCore
from Common import Const
from Common.Utils import Utils
import os
import stat
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement
from xml.etree.ElementTree import ElementTree


class ReleaseConfigThread(QtCore.QRunnable):

    def __init__(self, sIndex, operateSName, backLogSuc, backLogErr, isKaiFa, communication):
        super(ReleaseConfigThread, self).__init__()
        self._communication = communication
        self._backLogSuc = backLogSuc
        self._backLogErr = backLogErr
        self._sIndex = sIndex
        self._operateSName = operateSName
        self._configZipFiles = None
        self._isKaiFa = isKaiFa

    def run(self) -> None:
        # 更新策划配置
        self._communication.ThreadLogSignal.emit("开始全量发布", 0)
        if self._isKaiFa:
            re, e = self.UpdateSvn(Const.LoaclSvnExcelPath)
        else:
            re, e = self.UpdateSvn(Const.LoaclSvnExcelPathRelease)
        if not re:
            self._communication.ThreadLogSignal.emit(self._backLogErr, 1)
            self._communication.ThreadLogSignal.emit("更新策划配置SVN目录失败", 1)
            self._communication.ThreadLogSignal.emit(e.strerror + e.filename, 1)
            self._communication.ReleaseConfigFinishSignal.emit()
            return
        self._communication.ThreadLogSignal.emit("更新策划配置Excel完成", 0)
        if self._isKaiFa:
            re, e = self.UpdateSvn(Const.LocalSvnClientConfigsPath)
        else:
            re, e = self.UpdateSvn(Const.LocalSvnClientConfigsPathRelease)
        if not re:
            self._communication.ThreadLogSignal.emit(self._backLogErr, 1)
            self._communication.ThreadLogSignal.emit("更新客户端配置SVN目录失败", 1)
            self._communication.ThreadLogSignal.emit(e.strerror + e.filename, 1)
            self._communication.ReleaseConfigFinishSignal.emit()
            return
        self._communication.ThreadLogSignal.emit("更新客户端配置完成", 0)
        if self._isKaiFa:
            re, e = self.UpdateSvn(Const.LocalServerOutPutPath)
        else:
            re, e = self.UpdateSvn(Const.LocalServerOutPutPathRelease)
        if not re:
            self._communication.ThreadLogSignal.emit(self._backLogErr, 1)
            self._communication.ThreadLogSignal.emit("更新服务器配置SVN目录失败", 1)
            self._communication.ThreadLogSignal.emit(e.strerror + e.filename, 1)
            self._communication.ReleaseConfigFinishSignal.emit()
            return
        self._communication.ThreadLogSignal.emit("更新服务器配置完成", 0)
        # 开始导出配置
        self._communication.ThreadLogSignal.emit("开始生成配置", 0)
        if self._isKaiFa:
            allConfigFiles, allConfigFileNames = ConfigPathAnalysis.FindAllConfigExcels(Const.LoaclSvnExcelPath)
        else:
            allConfigFiles, allConfigFileNames = ConfigPathAnalysis.FindAllConfigExcels(Const.LoaclSvnExcelPathRelease)
        for index, cnfFile in enumerate(allConfigFiles):
            # print("开始创建导出配置线程_A")
            fName = allConfigFileNames[index]
            # print("开始创建导出配置线程_B")
            if self._isKaiFa:
                wThread = OutPutConfigWorkThread(cnfFile, fName, Const.LocalSvnClientConfigsPath, Const.LocalServerOutPutPath, Const.OutPutType_All)
            else:
                wThread = OutPutConfigWorkThread(cnfFile, fName, Const.LocalSvnClientConfigsPathRelease,
                                                 Const.LocalServerOutPutPathRelease, Const.OutPutType_All)
            wThread.WorkLogSignal.connect(self._communication.ConfigOutWorkLog)
            # print("开始创建导出配置线程_C")
            wThread.start()
            wThread.wait()
        self._communication.ThreadLogSignal.emit("生成配置完成", 0)
        # 开始复制关卡编辑器关卡配置
        self._communication.ThreadLogSignal.emit("复制关卡编辑配置", 0)
        self._communication.ThreadLogSignal.emit("复制客户端关卡编辑配置", 0)
        clientGuanQiaPath = self._communication.label_configPathClientGuanQia.text()
        if clientGuanQiaPath != "":
            clientGuanQiaFile = clientGuanQiaPath + "/" + Const.GuanQiaFileName
            if Utils.CheckFileIsExsit(clientGuanQiaFile):
                # 拷贝到对应配置目录
                if self._isKaiFa:
                    shutil.copy(clientGuanQiaFile, Const.LocalSvnClientConfigsPath + Const.GuanQiaFileName)
                else:
                    shutil.copy(clientGuanQiaFile, Const.LocalSvnClientConfigsPathRelease + Const.GuanQiaFileName)
                self._communication.ThreadLogSignal.emit("复制客户端关卡编辑配置完成", 0)
            else:
                self._communication.ThreadLogSignal.emit("没有找到客户端关卡配置文件", 0)
        else:
            self._communication.ThreadLogSignal.emit("没有设置客户端关卡编辑配置", 0)

        self._communication.ThreadLogSignal.emit("复制复制关卡编辑配置", 0)
        serverGuanQiaPath = self._communication.label_configPathServerGuanQia.text()
        if serverGuanQiaPath != "":
            serverGuanQiaFile = serverGuanQiaPath + "/" + Const.GuanQiaFileName
            if Utils.CheckFileIsExsit(serverGuanQiaFile):
                # 拷贝到对应配置目录
                if self._isKaiFa:
                    shutil.copy(serverGuanQiaFile, Const.LocalServerOutPutPath + Const.GuanQiaFileName)
                else:
                    shutil.copy(serverGuanQiaFile, Const.LocalServerOutPutPathRelease + Const.GuanQiaFileName)
                self._communication.ThreadLogSignal.emit("复制服务器关卡编辑配置完成", 0)
            else:
                self._communication.ThreadLogSignal.emit("没有找到服务器关卡配置文件", 0)
        else:
            self._communication.ThreadLogSignal.emit("没有设置服务器关卡编辑配置", 0)
        self._communication.ThreadLogSignal.emit("复制关卡编辑配置完成", 0)
        # 生成版本号
        self._communication.ThreadLogSignal.emit("生成版本号", 0)
        Utils.CreateConfigServion("release_")
        self._communication.ThreadLogSignal.emit("写入服务端版本号版本号:%s" % Utils.ConfigVersion, 0)
        fileStrS = Const.FormatVerLuaStrServer % Utils.ConfigVersion
        if self._isKaiFa:
            outputSFile = Const.LocalServerOutPutPath + "t_Ver.lua"
        else:
            outputSFile = Const.LocalServerOutPutPathRelease + "t_Ver.lua"
        Utils.WriteFile(outputSFile, fileStrS)
        self.CreateVersionXmlInfo()
        self._communication.ThreadLogSignal.emit("生成配置压缩包 版本号:%s" % Utils.ConfigVersion, 0)

        self._communication.ThreadLogSignal.emit("写入客户端版本号版本号:%s" % Utils.ConfigVersion, 0)
        fileStrC = Const.FormatVerLuaStrClient % Utils.ConfigVersion
        if self._isKaiFa:
            outputCFile = Const.LocalSvnClientConfigsPath + "t_Ver.lua"
        else:
            outputCFile = Const.LocalSvnClientConfigsPathRelease + "t_Ver.lua"
        Utils.WriteFile(outputCFile, fileStrC)
        # 开始提交
        self._communication.ThreadLogSignal.emit("准备提交配置", 0)
        self._communication.ThreadLogSignal.emit("提交服务器配置", 0)
        if self._isKaiFa:
            re, e = self.CommitSvn(Const.LocalServerOutPutPath)
        else:
            re, e = self.CommitSvn(Const.LocalServerOutPutPathRelease)
        if not re:
            self._communication.ThreadLogSignal.emit(self._backLogErr, 1)
            self._communication.ThreadLogSignal.emit("提交服务器配置SVN目录失败,请稍后再试", 1)
            self._communication.ThreadLogSignal.emit(e.strerror + e.filename, 1)
            self._communication.ReleaseConfigFinishSignal.emit()
            return
        self._communication.ThreadLogSignal.emit("提交服务器配置完成", 0)
        self._communication.ThreadLogSignal.emit("提交客户端配置", 0)
        if self._isKaiFa:
            re, e = self.CommitSvn(Const.LocalSvnClientConfigsPath)
        else:
            re, e = self.CommitSvn(Const.LocalSvnClientConfigsPathRelease)
        if not re:
            self._communication.ThreadLogSignal.emit(self._backLogErr, 1)
            self._communication.ThreadLogSignal.emit("提交客户端配置SVN目录失败,请稍后再试", 1)
            self._communication.ThreadLogSignal.emit(e.strerror + e.filename, 1)
            self._communication.ReleaseConfigFinishSignal.emit()
            return
        self._communication.ThreadLogSignal.emit("提交客户端配置完成", 0)
        # 开始打包配置
        self._communication.ThreadLogSignal.emit("开始打包服务器配置", 0)
        # 压缩
        if self._isKaiFa:
            zipFile = shutil.make_archive(
                Const.LocalServerOutPutZipPath + "serverConfigs_%s" % Utils.ConfigVersion, 'zip',
                Const.LocalServerOutPutPath)
        else:
            zipFile = shutil.make_archive(
                Const.LocalServerOutPutZipPathRelease + "serverConfigs_%s" % Utils.ConfigVersion, 'zip',
                Const.LocalServerOutPutPathRelease)
        if zipFile:
            self._configZipFiles = zipFile
            self._communication.ThreadLogSignal.emit("生成配置压缩包完成, 文件路径:%s" % self._configZipFiles, 0)
            self._communication.ThreadLogSignal.emit("打包服务器配置完成", 0)
        else:
            self._communication.ThreadLogSignal.emit(self._backLogErr, 1)
            self._communication.ThreadLogSignal.emit("打包服务器配置失败,请重新尝试全量发布", 1)
            self._communication.ReleaseConfigFinishSignal.emit()
            return
        # 上传配置到指定服务器
        if self._isKaiFa:
            self._communication.ThreadLogSignal.emit("开始上传服务器配置到[%s]" % self._operateSName, 0)
            sUrl = self._communication.serverManager.updateConfigUrl(self._sIndex)
            fileName = self._configZipFiles.split('\\')[-1]
            requstDatas = {
                'file': (fileName, open(self._configZipFiles, 'rb').read(), 'application/zip'),
                'fileName': fileName,
                'ver': fileName.split("_")[-1].split(".")[0],
                'sign': 'sign'
            }
            try:
                response = HttpHelper.PostFormData(sUrl, requstDatas)
                self._communication.ThreadLogSignal.emit("全量发布完成", 0)
                self._communication.UpdateConfigSignal.emit({'back': response, 'err': None})
            except Exception as e:
                self._communication.ThreadLogSignal.emit(self._backLogErr, 1)
                self._communication.ThreadLogSignal.emit("上传服务器配置失败,请禁用再启用网卡后, 重新尝试全量发布", 1)
                self._communication.ThreadLogSignal.emit("%s" % e, 1)
                self._communication.ReleaseConfigFinishSignal.emit()
                return
        if self._isKaiFa:
            self._communication.ThreadLogSignal.emit("开发版配置全量发布完成", 0)
        else:
            self._communication.ThreadLogSignal.emit("稳定版配置全量发布完成", 0)
            self._communication.ThreadLogSignal.emit("稳定版配置不会直接发布到服务器,请通知相关人员进行服务器更新。", 0)
        self._communication.ReleaseConfigFinishSignal.emit()

    def CreateVersionXmlInfo(self):
        """生成一份xml信息,保存版本和发布人"""
        root = Element('root')
        ver = SubElement(root, 'ver')
        ver.text = Utils.ConfigVersion
        author = SubElement(root, 'author')
        author.text = self._communication.configSignName
        tree = ElementTree(root)
        if self._isKaiFa:
            tree.write(Const.LocalServerOutPutPath + 'ver.xml', encoding='utf-8')
        else:
            tree.write(Const.LocalServerOutPutPathRelease + 'ver.xml', encoding='utf-8')

    def revert(self, svnLocal, localPath):
        cmd = []
        # if revision is not None:
        cmd += ['-R', '.']
        # cmd += rel_filepaths
        svnLocal.run_command(
            'revert',
            cmd,
            wd=localPath)

    def clearPath(self, localPath):
        delList = os.listdir(localPath)
        for f in delList:
            if f == ".svn":
                continue
            filePath = os.path.join(localPath, f)
            if os.path.isfile(filePath):
                os.remove(filePath)
            elif os.path.isdir(filePath):
                shutil.rmtree(filePath, True)

    def UpdateSvn(self, localPath):
        try:
            svnLocal = svn.local.LocalClient(localPath)
            # self.revert(svnLocal, localPath)
            svnLocal.cleanup()
            self.clearPath(localPath)
            svnLocal.update()
        except Exception as e:
            return False, e
        return True, None

    def CommitSvn(self, localPath):
        try:
            svnLocal = svn.local.LocalClient(localPath)
            svnLocal.cleanup()
            svnLocal.commit("配置工具提交(全量发布)")
        except Exception as e:
            return False, e
        return True, None


