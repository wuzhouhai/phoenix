import sys
from Threads.ReleaseConfigCmdThread import ReleaseConfigCmdThread
from Threads.SvnExcelConfigThread import SvnExcelConfigThread
from PyQt5.QtCore import *
import yaml
from Common import Const

pathYamlConfig = None
errCode = 0


def StartCheckOutConfigRelease():
    global pathYamlConfig, errCode
    try:
        if pathYamlConfig and pathYamlConfig['ConfigSvnRelease'] and pathYamlConfig['ConfigSvnRelease'] != "":
            tt = SvnExcelConfigThread(1, pathYamlConfig['ConfigSvnRelease'], Const.LoaclSvnExcelPathRelease, "检出数据表成功", "检出数据表失败", None)
            tt.setAutoDelete(True)
            QThreadPool.globalInstance().start(tt)
    except Exception as e:
        errCode = -102


def StartCheckOutClientConfigRelease():
    global pathYamlConfig, errCode
    try:
        if pathYamlConfig and pathYamlConfig['ClientConfigSvnRelease'] and pathYamlConfig['ClientConfigSvnRelease'] != "":
            tt = SvnExcelConfigThread(1, pathYamlConfig['ClientConfigSvnRelease'], Const.LocalSvnClientConfigsPathRelease, "检出客户端配置成功",
                                      "检出客户端配置失败", None)
            tt.setAutoDelete(True)
            QThreadPool.globalInstance().start(tt)
    except Exception as e:
        errCode = -101


def StartCheckOutServerConfigRelease():
    global pathYamlConfig, errCode
    try:
        if pathYamlConfig and pathYamlConfig['ServerConfigSvnRelease'] and pathYamlConfig['ServerConfigSvnRelease'] != "":
            tt = SvnExcelConfigThread(1, pathYamlConfig['ServerConfigSvnRelease'], Const.LocalServerOutPutPathRelease, "检出服务器配置成功",
                                      "检出服务器配置失败", None)
            tt.setAutoDelete(True)
            QThreadPool.globalInstance().start(tt)
    except Exception as e:
        errCode = -100


def readConf():
    global pathYamlConfig, errCode
    try:
        conf = open(file="cmd.yaml", mode='r', encoding="utf-8")
        pathYamlConfig = yaml.load(conf, Loader=yaml.FullLoader)
    except Exception as e:
        errCode = -99


if __name__ == '__main__':
    readConf()
    StartCheckOutConfigRelease()
    StartCheckOutClientConfigRelease()
    StartCheckOutServerConfigRelease()
    QThreadPool.globalInstance().waitForDone()
    t = ReleaseConfigCmdThread()
    QThreadPool.globalInstance().start(t)
    QThreadPool.globalInstance().waitForDone()
    sys.exit(errCode)
