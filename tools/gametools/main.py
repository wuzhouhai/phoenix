# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import sys
import MainWindow
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox
from App import App
from Common.Utils import Utils
from Common import Const
import subprocess
from PyQt5 import sip

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # app = QtWidgets.QApplication(sys.argv)
    # widget = AppMainWindow()
    # widget.show()
    myapp = QApplication(sys.argv)
    myDlg = App()
    # if Utils.GetServerVer():
    #     QMessageBox.information(myDlg, '提示信息', '有新的版本需要更新,请点击确定,开始更新。', QMessageBox.Ok)
    #     subprocess.Popen(Const.UpdateExe)
    #     sys.exit(0)
    # else:
    myDlg.show()
    sys.exit(myapp.exec_())

