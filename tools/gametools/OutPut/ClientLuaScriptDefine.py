# -*- coding: utf-8 -*-
"""
@File       :  ClientLuaScriptDefine
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/1 17:18
@Description:  
"""
from Common.Utils import Utils
from Common import Const


def FormatConfigTips(tName, colNameList, typeList, decList):
    strFormat = """ ---@class Config.%s
local t_%s_dec = {\n""" % (tName, tName)
    lastIndex = -1
    if colNameList:
        firstWrite = True
        for index, colName in enumerate(colNameList):
            if typeList[index]["ignore"] == 1:
                continue
            colStr = ""
            if not firstWrite:
                colStr += ",    --%s\n" % str(Utils.ReplaceChars(decList[lastIndex]))
            else:
                firstWrite = False
            if typeList[index]["iType"] == Const.RowType_String:
                colStr += "    %s = ''" % colName
            elif typeList[index]["iType"] == Const.RowType_Number:
                colStr += "    %s = 0" % colName
            elif typeList[index]["iType"] == Const.RowType_StringArray:
                colStr += "    ---@type string[]\n"
                colStr += "    %s = nil" % colName
            elif typeList[index]["iType"] == Const.RowType_StringArray2:
                colStr += "    ---@type string[][]\n"
                colStr += "    %s = nil" % colName
            elif typeList[index]["iType"] == Const.RowType_NumberArray:
                colStr += "    ---@type number[]\n"
                colStr += "    %s = nil" % colName
            elif typeList[index]["iType"] == Const.RowType_NumberArray2:
                colStr += "    ---@type number[][]\n"
                colStr += "    %s = nil" % colName
            elif typeList[index]["iType"] == Const.RowType_Dictionary:
                colStr += "    ---@type table<string, number>\n"
                colStr += "    %s = {}" % colName
            elif typeList[index]["iType"] == Const.RowType_LuaFormat:
                if typeList[index]["customType"]:
                    colStr += "    ---@type %s\n" % typeList[index]["customType"]
                else:
                    colStr += "    ---@type table\n"
                colStr += "    %s = {}" % colName
            lastIndex = index
            strFormat += colStr
    if lastIndex != -1:
        strFormat += "  --%s\n" % str(Utils.ReplaceChars(decList[lastIndex]))
    strFormat += "}\n"
    return strFormat


def FormatConfigDec(decList, typeList):
    """
    格式化注释行
    :param typeList:
    :param decList:
    :return:
    """
    strFormat = "--"
    firstWrite = True
    if decList:
        # lengh = len(decList)
        for i, dec in enumerate(decList):
            if typeList[i]["ignore"] == 1:
                continue
            if not firstWrite:
                strFormat += ","
            else:
                firstWrite = False
            strFormat += Utils.ReplaceChars(dec)
    strFormat += "\n"
    return strFormat


def FormatConfigCol(tName, colNameList, mainKeyIndex, typeList):
    """格式化列
    :param tName 表名
    :param typeList 类型信息
    :param colNameList 列字段信息
    :param mainKeyIndex 主键索引
    """
    strFormat = ""
    firstWrite = True
    if colNameList:
        strFormat += "local t_%s_col = {" % tName
        # length = len(colNameList)
        writeIndex = 0
        for index, colName in enumerate(colNameList):
            if typeList[index]["ignore"] == 1:
                continue
            colStr = ""
            if not firstWrite:
                colStr += ","
            else:
                firstWrite = False
            if index == mainKeyIndex:
                colStr += "id = %d" % (writeIndex + 1)
            else:
                colStr += colName + " = %d" % (writeIndex + 1)
            writeIndex += 1
            strFormat += colStr
        strFormat += "}\n"
    return strFormat


def FormatConfigData(tName, datas, mainKeyIndex, typeList):
    """
    格式化配置数据
    :param tName: 表名
    :param datas: 数据
    :param mainKeyIndex: 主键索引
    :param typeList: 类型列表
    :return: 格式化字符串
    """
    strFormat = ""
    if datas:
        strFormat += "local t_%s_tmp =\n{\n" % tName
        for data in datas:
            if data:
                dictKeyStr = "[%s] = {" % str(data[mainKeyIndex])
                firstWrite = True
                # length = len(data)
                for i, value in enumerate(data):
                    if typeList[i]["ignore"] == 1:
                        continue
                    if not firstWrite:
                        dictKeyStr += ","
                    else:
                        firstWrite = False
                    if typeList[i]["iType"] == Const.RowType_String:
                        if not value:
                            value = ""
                        dictKeyStr += '\"%s\"' % str(value)
                    elif typeList[i]["iType"] == Const.RowType_Number:
                        if not value or value == "":
                            value = 0
                        dictKeyStr += "%s" % str(value)
                    elif typeList[i]["iType"] == Const.RowType_StringArray:
                        if not value:
                            dictKeyStr += "{}"
                        else:
                            valueInfo = value.split(",")
                            tempFormat = "{"
                            if valueInfo and len(valueInfo) > 0:
                                for ii, v in enumerate(valueInfo):
                                    tempFormat += '\"%s\"' % str(v)
                                    if ii != len(valueInfo) - 1:
                                        tempFormat += ","
                            tempFormat += "}"
                            dictKeyStr += tempFormat
                    elif typeList[i]["iType"] == Const.RowType_StringArray2:
                        if not value:
                            dictKeyStr += "{}"
                        else:
                            tempFormat = ""
                            valueInfos = value.split(";")
                            if valueInfos:
                                tempFormat = "{"
                                for index, vInfos in enumerate(valueInfos):
                                    valueInfo = vInfos.split(",")
                                    if valueInfo:
                                        tempFormat += "{"
                                        for ii, v in enumerate(valueInfo):
                                            tempFormat += '\"%s\"' % str(v)
                                            if ii != len(valueInfo) - 1:
                                                tempFormat += ","
                                        tempFormat += "}"
                                    if index != len(valueInfos) - 1:
                                        tempFormat += ","
                                tempFormat += "}"
                            dictKeyStr += tempFormat
                    elif typeList[i]["iType"] == Const.RowType_NumberArray:
                        value = str(value)
                        if not value:
                            dictKeyStr += "{}"
                        else:
                            valueInfo = value.split(",")
                            tempFormat = ""
                            if valueInfo:
                                tempFormat = "{"
                                for ii, v in enumerate(valueInfo):
                                    tempFormat += '%s' % str(v)
                                    if ii != len(valueInfo) - 1:
                                        tempFormat += ","
                                tempFormat += "}"
                            dictKeyStr += tempFormat
                    elif typeList[i]["iType"] == Const.RowType_NumberArray2:
                        value = str(value)
                        if not value:
                            dictKeyStr += "{}"
                        else:
                            tempFormat = ""
                            valueInfos = value.split(";")
                            if valueInfos:
                                reviewValus = []
                                for vs in valueInfos:
                                    if vs != "":
                                        reviewValus.append(vs)
                                tempFormat = "{"
                                for index, vInfos in enumerate(reviewValus):
                                    valueInfo = vInfos.split(",")
                                    if valueInfo:
                                        tempFormat += "{"
                                        for ii, v in enumerate(valueInfo):
                                            tempFormat += '%s' % str(v)
                                            if ii != len(valueInfo) - 1:
                                                tempFormat += ","
                                        tempFormat += "}"
                                    if index != len(reviewValus) - 1:
                                        tempFormat += ","
                                tempFormat += "}"
                            dictKeyStr += tempFormat
                    elif typeList[i]["iType"] == Const.RowType_Dictionary:
                        if not value:
                            value = ""
                        tempFormat = "{"
                        dictTuples = value.split(";")
                        if dictTuples:
                            reviewValus = []
                            for vs in dictTuples:
                                if vs != "":
                                    reviewValus.append(vs)
                            for dIndex, dictTuple in enumerate(reviewValus):
                                dictTupleStrs = dictTuple.split(",")
                                if len(dictTupleStrs) == 2:
                                    dickKey = dictTupleStrs[0]
                                    dickValue = dictTupleStrs[1]
                                    tempFormat += '%s = %s' % (str(dickKey), str(dickValue))
                                    if dIndex != len(reviewValus) - 1:
                                        tempFormat += ","
                        tempFormat += "}"
                        dictKeyStr += tempFormat
                    elif typeList[i]["iType"] == Const.RowType_LuaFormat:
                        if not value:
                            dictKeyStr += "nil"
                        else:
                            dictKeyStr += '%s' % str(value)
                    else:  # 如果找不到类型,那么默认做字符串处理
                        if not value:
                            value = ""
                        dictKeyStr += '\"%s\"' % str(value)
                dictKeyStr += "},\n"
                strFormat += dictKeyStr
        strFormat += "}\n"
    return strFormat


def FormatEnd(tName):
    strFormat = """---@type table<number, Config.%s>
---@class Config.%s.Types
local t_%s = {}
local mt = {__index = function ( t, n ) local i = t_%s_col[n] if i then return t[i] end end}
setmetatable(t_%s, {__index = function(t, i) local rec = t_%s_tmp[i] if rec ~= nil then setmetatable(rec,mt) return rec end end})
---@return fun(t:table<number, Config.%s>, index?: number):number, Config.%s
function t_%s.iter() return function(t,k) local rk, rv if k then rk, rv = next(t,k) else rk, rv = next(t) end if rk then setmetatable(rv,mt)  end return rk, rv end, t_%s_tmp end
return t_%s""" % (tName, tName, tName, tName, tName, tName, tName, tName, tName, tName, tName)
    return strFormat
