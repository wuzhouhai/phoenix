# -*- coding: utf-8 -*-
"""
@File       :  OutPutConfigWorkThread
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/1 17:59
@Description:  
"""
import json

from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSignal
from Excel.ExcelHelper import ExcelHelper
from Common import Const
from Common.Utils import Utils
from OutPut import ClientLuaScriptDefine
from OutPut import ServerLuaScriptDefine


class OutPutConfigWorkThread(QtCore.QThread):

    # SimulatorStartSignal = pyqtSignal(int)
    WorkLogSignal = pyqtSignal(str, int)

    def __init__(self, filePath, fileName, outClientPath, outServerPath, outType, log=True):
        super(OutPutConfigWorkThread, self).__init__()
        self._filePath = filePath
        self._fileName = fileName
        self._outClientPath = outClientPath
        self._outServerPath = outServerPath
        self._exl = ExcelHelper(self._filePath)
        self._outType = outType
        self._mainKeyIndex = -1
        self._decList = list()
        self._typeList = list()
        self._feildList = list()
        self._allDatas = list()
        self._allSheets = list()
        self._errs = list()
        self._log = log
        self._isNeedChangeVersion = False

    def run(self) -> None:
        # print("导出配置线程开始运行")
        try:
            # exl = ExcelHelper(self._filePath)
            sheetNames = self.analysisConfigSheet()
            for sheetName in sheetNames:
                if sheetName.find("@") != -1:
                    sheet = self._exl.GetSheetByName(sheetName)
                    if sheet:
                        sName = sheetName.lstrip('@')
                        self.clearAnalysis()
                        self.outPutConfig(sName, sheet)
            if self._log:
                self.WorkLogSignal.emit("导出完成" + self._fileName, 0)
            # print("导出完成" + self._fileName)
        except Exception as e:
            print(e)

    def clearAnalysis(self):
        """清理数据,以便导出下张表"""
        self._decList = list()
        self._typeList = list()
        self._feildList = list()
        self._allDatas = list()
        self._errs = list()
        self._mainKeyIndex = -1

    def analysisConfigSheet(self):
        """分析需要导出数据的sheet"""
        return self._exl.GetAllSheetNames()

    def analysisConfig(self, sheet):
        """分析数据"""
        # 打开excel
        try:
            ret = ExcelHelper.AnalysisConfig(sheet)
            # print(ret)
            self._decList = ret["decList"]
            self._typeList = ret["typeList"]
            self._feildList = ret["feildList"]
            self._allDatas = ret["allDatas"]
            self._mainKeyIndex = ret["mainKeyIndex"]
            self._errs = ret["errs"]
        except Exception as e:
            print(e.__traceback__)

    def outPutConfig(self, sheetName, sheet):
        """导出配置"""
        self.analysisConfig(sheet)
        # 看看是不是有错误,注意,错误不会终止导出,只会提示
        if self._log:
            for err in self._errs:
                self.WorkLogSignal.emit("配置%s文件,表名%s,出现错误:[%s]" % (self._fileName, sheetName, err), 1)
        if self._outType == Const.OutPutType_Client or self._outType == Const.OutPutType_All:
            # 导出客户端
            self.outPutClientConfigToJson(sheetName)

        if self._outType == Const.OutPutType_Server or self._outType == Const.OutPutType_All:
            # 导出服务端
            self.outPutServerConfigToJson(sheetName)

    def outPutClientCofig(self, sheetName):
        """导出客户端配置"""
        fileStr = ClientLuaScriptDefine.FormatConfigTips(sheetName, self._feildList, self._typeList, self._decList)
        # fileStr = ClientLuaScriptDefine.FormatConfigDec(self._decList, self._typeList)
        fileStr += ClientLuaScriptDefine.FormatConfigCol(sheetName, self._feildList, self._mainKeyIndex, self._typeList)
        fileStr += ClientLuaScriptDefine.FormatConfigData(sheetName, self._allDatas, self._mainKeyIndex,
                                                          self._typeList)
        fileStr += ClientLuaScriptDefine.FormatEnd(sheetName)
        outputFile = self._outClientPath + "/t_%s.lua" % sheetName
        Utils.WriteFile(outputFile, fileStr)

    def outPutServerConfig(self, sheetName):
        """导出服务器配置"""
        fileStr = ServerLuaScriptDefine.FormatConfigTips(sheetName, self._feildList, self._typeList, self._decList)
        # fileStr = ServerLuaScriptDefine.FormatConfigDec(self._decList, self._typeList)
        fileStr += ServerLuaScriptDefine.FormatConfigCol(sheetName, self._feildList, self._mainKeyIndex, self._typeList)
        fileStr += ServerLuaScriptDefine.FormatConfigData(sheetName, self._allDatas, self._mainKeyIndex,
                                                          self._typeList)
        fileStr += ServerLuaScriptDefine.FormatEnd(sheetName)
        outputFile = self._outServerPath + "/t_%s.lua" % sheetName
        Utils.WriteFile(outputFile, fileStr)

    def formatJsonObject(self, data, dataType):
        """格式化json数据"""
        try:
            if dataType == Const.RowType_String or dataType == Const.RowType_LuaFormat:
                value = str(data)
            elif dataType == Const.RowType_Number:
                value = int(data)
            elif dataType == Const.RowType_StringArray:
                if data == "":
                    value = list()
                else:
                    value = Utils.SplitString(data, ",")
            elif dataType == Const.RowType_StringArray2:
                value = list()
                if len(data) > 0:
                    arrValues = Utils.SplitString(data, ";")
                    for arrDataStr in arrValues:
                        value.append(Utils.SplitString(arrDataStr, ","))
            elif dataType == Const.RowType_NumberArray:
                if data == "":
                    value = list()
                else:
                    value = list(map(int,Utils.SplitString(data, ",")))
            elif dataType == Const.RowType_NumberArray2:
                value = list()
                if len(data) > 0:
                    arrValues = Utils.SplitString(data, ";")
                    for arrDataStr in arrValues:
                        value.append(list(map(int, Utils.SplitString(arrDataStr, ","))))
            elif dataType == Const.RowType_Dictionary:
                value = dict()
                if len(data) > 0:
                    dictObjects = Utils.SplitString(data, ";")
                    for dictObject in dictObjects:
                        dictObject = Utils.SplitString(dictObject, ",")
                        key = dictObject[0]
                        cValue = dictObject[1]
                        _, k = Utils.IsNumber(key, False)
                        _, v = Utils.IsNumber(cValue)
                        value[k] = v
            else:
                value = str(data)
        except Exception as e:
            value = str(data)
        return value

    def outPutClientConfigToJson(self, sheetName):
        """导出客户端配置"""
        jsonDataList = list()
        for data in self._allDatas:
            jsonData = dict()
            for i, value in enumerate(data):
                value = self.formatJsonObject(value, self._typeList[i]['iType'])
                jsonData[self._feildList[i]] = value
            jsonDataList.append(jsonData)
        outputFile = self._outClientPath + "/t_%s.json" % sheetName
        Utils.WriteFile(outputFile, json.dumps(jsonDataList, indent=2, ensure_ascii=False))


    def outPutServerConfigToJson(self, sheetName):
        """导出服务器配置"""
        jsonDataList = list()
        for data in self._allDatas:
            jsonData = dict()
            for i, value in enumerate(data):
                value = self.formatJsonObject(value, self._typeList[i]['iType'])
                jsonData[self._feildList[i]] = value
            jsonDataList.append(jsonData)
        outputFile = self._outServerPath + "/t_%s.json" % sheetName
        Utils.WriteFile(outputFile, json.dumps(jsonDataList, indent=2, ensure_ascii=False))

