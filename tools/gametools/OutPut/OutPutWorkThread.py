# -*- coding: utf-8 -*-
"""
@File       :  OutPutWork
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/1 17:14
@Description:  
"""
from PyQt5 import QtCore
from PyQt5.QtCore import *
from OutPut.OutPutConfigWorkThread import OutPutConfigWorkThread
from Common.Utils import Utils
from Common import Const
import subprocess
import os


class OutPutWorkThread(QtCore.QThread):
    """导出线程类"""
    WorkFinishSignal = pyqtSignal()

    def __init__(self, files, fileNames, outClientPath, outServerPath, outType, luaCheckTool, uiCls):
        super(OutPutWorkThread, self).__init__()
        self._files = files
        self._fileNames = fileNames
        self._outClientPath = outClientPath
        self._outServerPath = outServerPath
        self._outType = outType
        self._uiCls = uiCls
        self._luaCheckTool = luaCheckTool
        self._workThreads = list()
        self.finishCounts = 0

    def run(self) -> None:
        print("导出线程开始运行")
        for index, cnfFile in enumerate(self._files):
            # print("开始创建导出配置线程_A")
            fName: str = self._fileNames[index]
            # 配置规则表不要导出
            if fName.find(Const.CheckConfigExcelFileName) != -1:
                continue
            # print("开始创建导出配置线程_B")
            wThread = OutPutConfigWorkThread(cnfFile, fName, self._outClientPath, self._outServerPath, self._outType)
            wThread.WorkLogSignal.connect(self._uiCls.ConfigOutWorkLog)
            # print("开始创建导出配置线程_C")
            wThread.start()
            wThread.wait()
        # self._uiCls.ThreadLogSignal.emit("启动效验配置进程", 0)
        # curPath = os.path.abspath('.')
        # if Utils.CheckFilePath(self._luaCheckTool):
        #     execCmd = "%s\\lua.exe %s\\checkConfig.lua " % (self._luaCheckTool, self._luaCheckTool) + self._outServerPath + "/"
        #     p = subprocess.Popen(execCmd, stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self._luaCheckTool)
        #     outinfo, _ = p.communicate()
        #     outinfo = outinfo.decode('gbk')
        #     outinfos = outinfo.split("\n")
        #     for out in outinfos:
        #         self._uiCls.ThreadLogSignal.emit(out, 1)
        # else:
        #     self._uiCls.ThreadLogSignal.emit("没有找到效验程序", 1)
        # errinfo = errinfo.decode('gbk')
        # print(outinfo)
        # print('------------------------------')
        # print(errinfo)
        # os.system(execCmd)
        self.WorkFinishSignal.emit()
        print("导出线程结束运行")