# -*- coding: utf-8 -*-
"""
@File       :  ItemObject
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/9 10:14
@Description:  物品
"""
import sys


class Item(object):
    class ItemError(TypeError):
        pass

    def __setattr__(self, key, value):
        if key in self.__dict__.keys():
            raise self.ItemError("Changing Item.%s" % key)
        else:
            self.__dict__[key] = value

    def __getattr__(self, key):
        if key in self.__dict__.keys():
            return self.key
        else:
            return None



