# -*- coding: utf-8 -*-
"""
@File       :  AllSheetManager
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@arkgames.com
@Time       :  2022/12/14 14:40
@Description:  
"""
from PyQt5.QtCore import *
from Analysis.SheetAnalysis import SheetAnalysis


class AllSheetManager(QObject):

    def __init__(self):
        super(AllSheetManager, self).__init__()
        self._allSheet = dict()  # 所有的表Sheet信息

    @property
    def AllSheet(self):
        return self._allSheet

    def SetSheet(self, name: str, sheet: SheetAnalysis):
        self._allSheet[name] = sheet

    def CheckSheetMainKey(self, name, mainKey):
        sheet: SheetAnalysis = self._allSheet.get(name, None)
        if sheet:
            return sheet.CheckMainKey(mainKey)
        return False

    def GetSheetValue(self, name, mainKey, feildKey):
        sheet: SheetAnalysis = self._allSheet.get(name, None)
        if sheet:
            return sheet.GetValueByMainKey(mainKey, feildKey)
        return None
