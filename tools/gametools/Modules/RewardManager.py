# -*- coding: utf-8 -*-
"""
@File       :  RewardManager
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/15 17:28
@Description:  奖励产出模块
"""
from PyQt5.QtCore import *
from Excel.ExcelHelper import ExcelHelper
from Common import ConfigTypes
from Modules.BaseManager import BaseManager
from Analysis.SheetAnalysis import SheetAnalysis


class RewardManager(BaseManager):
    """物品管理"""

    def __init__(self):
        super(RewardManager, self).__init__()
        self._module = "reward"
        self._treeViewData = dict()
        self._rewardGroups = dict()
        self._rewardLogicSheet = SheetAnalysis("rewardLogic")
        self._rewardSheet = SheetAnalysis("reward")

    @property
    def RewardGroups(self):
        return self._rewardGroups

    @property
    def TreeViewData(self):
        return self._treeViewData

    @property
    def RewardLogicSheeet(self):
        return self._rewardLogicSheet

    @property
    def RewardSheet(self):
        return self._rewardSheet

    def analysisConfig(self):
        """分析配置"""
        if self._exl:
            self._rewardLogicSheet.Sheet = self._exl.GetSheetByName("@RewardLogic")
            analysisInfo = ExcelHelper.AnalysisConfig(self._rewardLogicSheet.Sheet)
            self._rewardLogicSheet.DecList = analysisInfo["decList"]
            self._rewardLogicSheet.TypeList = analysisInfo["typeList"]
            self._rewardLogicSheet.FeildList = analysisInfo["feildList"]
            self._rewardLogicSheet.AllDatas = analysisInfo["allDatas"]
            self._rewardLogicSheet.MainKeyIndex = analysisInfo["mainKeyIndex"]
            self._rewardLogicSheet.AllItems = analysisInfo["dictDatas"]

            self.analysisRewardGroups()

            self._rewardSheet.Sheet = self._exl.GetSheetByName("@Reward")
            analysisInfo = ExcelHelper.AnalysisConfig(self._rewardSheet.Sheet)
            self._rewardSheet.DecList = analysisInfo["decList"]
            self._rewardSheet.TypeList = analysisInfo["typeList"]
            self._rewardSheet.FeildList = analysisInfo["feildList"]
            self._rewardSheet.AllDatas = analysisInfo["allDatas"]
            self._rewardSheet.MainKeyIndex = analysisInfo["mainKeyIndex"]
            self._rewardSheet.AllItems = analysisInfo["dictDatas"]

            self.DataIsOK = True

            self.AllSheets.append(self._rewardLogicSheet)
            self.AllSheets.append(self._rewardSheet)

    def analysisRewardGroups(self):
        for rLogic in self._rewardLogicSheet.AllDatas:
            groupIndex = self._rewardLogicSheet.GetConfigColIndex("group")
            group = rLogic[groupIndex]
            if group not in self._rewardGroups.keys():
                self._rewardGroups[group] = list()
            self._rewardGroups[group].append(rLogic[self._rewardLogicSheet.MainKeyIndex])

    def GetLogicData(self, rLogicId):
        iIndex = self._rewardLogicSheet.AllItems.get(rLogicId, None)
        if iIndex >= 0:
            return self._rewardLogicSheet.AllDatas[iIndex]

    def GetRewardGroups(self, groupId):
        return self._rewardGroups.get(groupId, [])

