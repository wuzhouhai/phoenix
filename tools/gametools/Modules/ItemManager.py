# -*- coding: utf-8 -*-
"""
@File       :  ItemManager
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/9 10:07
@Description:  物品管理类
"""
from Excel.ExcelHelper import ExcelHelper
from Common import ConfigTypes
from Modules.BaseManager import BaseManager
from Analysis.SheetAnalysis import SheetAnalysis


class ItemManager(BaseManager):
    """物品管理"""

    def __init__(self):
        super(ItemManager, self).__init__()
        self._sheetAnalysis = SheetAnalysis("item")
        self._module = "item"

    def analysisConfig(self):
        """分析配置"""
        if self._exl:
            self._sheetAnalysis.Sheet = self._exl.GetFirstSheet()
            itemAnalysisInfo = ExcelHelper.AnalysisConfig(self._sheetAnalysis.Sheet)
            self._sheetAnalysis.DecList = itemAnalysisInfo["decList"]
            self._sheetAnalysis.TypeList = itemAnalysisInfo["typeList"]
            self._sheetAnalysis.FeildList = itemAnalysisInfo["feildList"]
            self._sheetAnalysis.AllDatas = itemAnalysisInfo["allDatas"]
            self._sheetAnalysis.MainKeyIndex = itemAnalysisInfo["mainKeyIndex"]
            self._sheetAnalysis.AllItems = itemAnalysisInfo["dictDatas"]

            # 分析所有道具ID和道具名
            for iData in self._sheetAnalysis.AllDatas:
                idIndex = self.GetConfigColIndex("id")
                nameIndex = self.GetConfigColIndex("name")
                itemId = iData[idIndex]
                name = iData[nameIndex]
                self._sheetAnalysis.IDAndNames[itemId] = name

            self.DataIsOK = True

            self.AllSheets.append(self._sheetAnalysis)

    def GetAllData(self):
        return self._sheetAnalysis.AllDatas

    def GetData(self, index):
        if index < len(self._sheetAnalysis.AllDatas):
            return self._sheetAnalysis.AllDatas[index]
        return None

    def GetMainKeyIndex(self):
        return self._sheetAnalysis.MainKeyIndex

    def GetConfigColIndex(self, feildKey):
        """获取列名索引"""
        try:
            return self._sheetAnalysis.FeildList.index(feildKey)
        except Exception as e:
            return -1

    def GetValueType(self, tIndex):
        if tIndex < len(self._sheetAnalysis.TypeList):
            return self._sheetAnalysis.TypeList[tIndex]["iType"]
        return None

    def GetItemData(self, itemKindId):
        itemKindId = int(itemKindId)
        if itemKindId in self._sheetAnalysis.AllItems.keys():
            i = self._sheetAnalysis.AllItems[itemKindId]
            return self._sheetAnalysis.AllDatas[i]

    def GetItemKeyData(self, itemKindId, key):
        itemData = self.GetItemData(itemKindId)
        vIndex = self.GetConfigColIndex(key)
        if itemData and vIndex > -1:
            return itemData[vIndex]
        if key == 'name':
            return "未知道具"
        return None

    def UpdateItemKeyData(self, selectIndex, key, value):
        itemData = self._sheetAnalysis.AllDatas[selectIndex]
        vIndex = self.GetConfigColIndex(key)
        if itemData and vIndex > -1:
            if itemData[vIndex] is None and value == '' or value == 0:
                return
            if self.GetValueType(vIndex) == ConfigTypes.Number:
                try:
                    value = int(value)
                except Exception as e:
                    print(e)
                    return
            if itemData[vIndex] == value:
                return
            itemData[vIndex] = value
            self.AddUpdateList(selectIndex, key, value)

    def AddUpdateList(self, selectIndex, key, value):
        """增加变更记录"""
        dataRow = selectIndex + 4
        dataCel = self.GetConfigColIndex(key) + 1
        self._sheetAnalysis.UpdateList.append({"dataRow": dataRow, "dataCel": dataCel, "value": value})

    def SaveItemData(self):
        """保存数据"""
        if not self._sheetAnalysis.UpdateList:
            return True
        for updateInfo in self._sheetAnalysis.UpdateList:
            self._exl.UpdateValue(updateInfo, None)
        ret = self._exl.SaveExcel()
        return ret

    def CheckNewItemID(self, itemID):
        return int(itemID) in self._sheetAnalysis.IDAndNames.keys()

    def CheckNewItemName(self, itemName):
        return itemName in self._sheetAnalysis.IDAndNames.values()

    def AddNewItem(self, itemName, itemId):
        """新增道具"""
        self._sheetAnalysis.AllDatas.append(
            [None, None, None, None, None, None, None, None, None, None, None, None, None, None,
             None, None, None, None, None, None, None, None, None, None, None, None, None])
        newIndex = len(self._sheetAnalysis.AllDatas) - 1
        self._sheetAnalysis.AllItems[itemId] = newIndex
        self.UpdateItemKeyData(newIndex, "name", itemName)
        self.UpdateItemKeyData(newIndex, "id", itemId)
        self.UpdateItemKeyData(newIndex, "stack", 1)
        self.UpdateItemKeyData(newIndex, "page", 6)
        self.UpdateItemKeyData(newIndex, "sort1", itemId)

    def CloseFile(self):
        if self._exl:
            self._exl.Close()
