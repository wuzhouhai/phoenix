# -*- coding: utf-8 -*-
"""
@File       :  ConditionManager
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/19 11:15
@Description:  条件
"""
from PyQt5.QtCore import *
from Excel.ExcelHelper import ExcelHelper
from Common import ConfigTypes
from Modules.BaseManager import BaseManager
from Analysis.SheetAnalysis import SheetAnalysis


class ConditionManager(BaseManager):
    """条件管理"""

    def __init__(self):
        super(ConditionManager, self).__init__()
        self._module = "condition"
        self._conditonSheet = SheetAnalysis("condition")

    @property
    def ConditionSheet(self):
        return self._conditonSheet

    def analysisConfig(self):
        """分析配置"""
        if self._exl:
            self._conditonSheet.Sheet = self._exl.GetSheetByName("@Condition")
            analysisInfo = ExcelHelper.AnalysisConfig(self._conditonSheet.Sheet)
            self._conditonSheet.DecList = analysisInfo["decList"]
            self._conditonSheet.TypeList = analysisInfo["typeList"]
            self._conditonSheet.FeildList = analysisInfo["feildList"]
            self._conditonSheet.AllDatas = analysisInfo["allDatas"]
            self._conditonSheet.MainKeyIndex = analysisInfo["mainKeyIndex"]
            self._conditonSheet.AllItems = analysisInfo["dictDatas"]

            self.AllSheets.append(self._conditonSheet)