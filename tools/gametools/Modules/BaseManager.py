# -*- coding: utf-8 -*-
"""
@File       :  BaseManager
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/15 17:31
@Description:  模块基类,为了少写几行代码..
"""
from PyQt5.QtCore import *
from Excel.ExcelHelper import ExcelHelper


class BaseManager(QObject):
    """基类"""

    def __init__(self):
        super(BaseManager, self).__init__()
        self._isAnalysising = False
        self._filePath = None
        self._exl: ExcelHelper = None
        self._module = ""
        self._dataIsOK = False
        self._allSheets = list()

    @property
    def AllSheets(self):
        return self._allSheets

    @property
    def DataIsOK(self):
        return self._dataIsOK

    @DataIsOK.setter
    def DataIsOK(self, v):
        self._dataIsOK = v

    def GetModuleName(self):
        return self._module

    def SetSourceExcel(self, sourceExcel):
        """绑定配置文件"""
        self._filePath = sourceExcel
        self._exl = ExcelHelper(self._filePath)

    def SetAnalysis(self, v):
        self._isAnalysising = v

    def CheckIsAnalysis(self):
        return self._isAnalysising

    def analysisConfig(self):
        """分析配置"""
        pass

    def SaveSheets(self):
        for sheet in self.AllSheets:
            if not sheet.UpdateList:
                continue
            for updateInfo in sheet.UpdateList:
                self._exl.UpdateValue(updateInfo, sheet.Sheet)

    def SaveData(self):
        """保存数据"""
        ret = self._exl.SaveExcel()
        return ret