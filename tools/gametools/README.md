### GameTools
配置导出工具,采用python3 + pyqt5进行编写,需要安装pyqt5开发环境[安装教程](https://www.jb51.net/article/231003.htm)

python版本3.9
打包安装包使用pyinstaller[教程1](https://zhuanlan.zhihu.com/p/57435892),
[教程2](https://www.cnblogs.com/junge-mike/p/12761379.html)
###版本号
每次修改代需要更新发布,必须修改/Common/Utils.py中的版本号,往大数字修改
###完整发布
现有打包配置已经编写在main.spec下,
打包方式:切换到项目目录,输入命令pyinstaller -F main.spec
打包文件生成目录会在当前目录的dist目录中,将dist目录中GameTools目录直接打包即可








