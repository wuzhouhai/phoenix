# -*- coding: utf-8 -*-
"""
@File       :  ConfigTypes
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/9 11:19
@Description:  
"""

import sys


class ConfigTypes(object):
    class ConfigTypesError(TypeError):
        pass

    def __setattr__(self, key, value):
        if key in self.__dict__.keys():
            raise self.ConfigTypesError("Changing ConfigTypes.%s" % key)
        else:
            self.__dict__[key] = value

    def __getattr__(self, key):
        if key in self.__dict__.keys():
            return self.key
        else:
            return None


sys.modules[__name__] = ConfigTypes()

ConfigTypes.Number = "number"
ConfigTypes.String = "string"
