# -*- coding: utf-8 -*-
"""
@File       :  Utils
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/1 14:35
@Description:  
"""
import os
import time
from Common import Const
import requests
from configparser import ConfigParser
import uuid
import shortuuid
import hashlib
import re


class Utils:
    Ver = "0.0.0.1"

    ConfigVersion = ""

    @staticmethod
    def CheckFileIsExsit(file):
        return os.path.isfile(file)

    @staticmethod
    def CheckFilePath(filePath):
        return os.path.exists(filePath)

    @staticmethod
    def SearchFiles(root, target, contain=None):
        """遍历文件夹找所有文件"""
        items = os.listdir(root)
        allFiles = list()
        allFileName = list()
        for item in items:
            path = os.path.join(root, item)
            if os.path.isdir(path):
                continue
                # print('[-]', path)
                # files = Utils.SearchFiles(path, target)
                # allFiles.extend(files)
            if path.find('\\') != -1:
                fileName = path.split('\\')[-1]
            else:
                fileName = path.split('/')[-1]
            if contain and contain not in fileName:
                continue
            if fileName.find('~') != -1:
                continue
            if fileName.split('.')[-1] == target:
                allFiles.append(path)
                allFileName.append(fileName.split('.')[0])
        return allFiles, allFileName

    @staticmethod
    def WriteFile(fileName, strData):
        """写入文件"""
        try:
            with open(fileName, "w+", encoding="utf-8") as f:
                f.write(strData)
        except Exception as e:
            print(e)

    @staticmethod
    def GetNowTimeStr():
        return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

    @staticmethod
    def GetNowTimeStrConfigZip():
        return time.strftime("%Y%m%d%H%M%S", time.localtime())

    @staticmethod
    def ReplaceChars(s):
        """去除一些换行空格啥的"""
        separators = [' ', '\n', '\r', '\n\r']
        for separator in separators:
            s = s.replace(separator, '')
        return s

    @staticmethod
    def GetServerVer():
        """获取版本信息,看看是否需要更新"""
        # try:
        #     updateFileUrl = Const.UpdateServer + Const.UpdateVerFile
        #     r = requests.get(updateFileUrl)
        #     with open(Const.UpdateVerFile, 'wb') as f:
        #         f.write(r.content)
        #     cfg = ConfigParser()
        #     cfg.read(Const.UpdateVerFile)
        #     version = cfg.get("version", "ver")
        #     return version != Utils.Ver
        # except Exception as e:
        #     print(e.with_traceback())
        updateFileUrl = Const.UpdateServer + Const.UpdateVerFile
        r = requests.get(updateFileUrl)
        with open(Const.UpdateVerFile, 'wb') as f:
            f.write(r.content)
        cfg = ConfigParser()
        cfg.read(Const.UpdateVerFile, encoding='UTF-8')
        version = cfg.get("version", "ver")
        return version != Utils.Ver
        # return False

    @staticmethod
    def CreateConfigServion(prefix):
        """生成版本号"""
        # uuid 保证每次生成版本号唯一
        Utils.ConfigVersion = prefix + shortuuid.ShortUUID().random(length=12) + "#" + Utils.GetNowTimeStrConfigZip()

    @staticmethod
    def MD5Str(context):
        m = hashlib.md5()
        m.update(context)
        m.hexdigest()
        md5str = m.hexdigest()
        return md5str

    @staticmethod
    def CheckMatchRe(checkValue, reFormat):
        if reFormat and len(reFormat) > 0:
            pattern = re.compile(reFormat)
            if not pattern.match(checkValue):
                return False
            return True
        return False

    @staticmethod
    def SplitString(strRes, sep):
        return strRes.split(sep)

    @staticmethod
    def IsNumber(s, canFloat=True):
        if canFloat and s.find('.') != -1:
            try:
                v = float(s)
                return True, v
            except ValueError:
                pass
        try:
            import unicodedata
            v = unicodedata.decimal(s)
            return True, v
        except (TypeError, ValueError):
            pass

        return False, s
