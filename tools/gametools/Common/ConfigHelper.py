# -*- coding: utf-8 -*-
"""
@File       :  SqlLiteHepler
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/2 17:51
@Description:  
"""

import sqlite3

DefaultCnfFile = "cnf.db"


class Config:

    def __init__(self):
        self._conn = sqlite3.connect(DefaultCnfFile)
        self._allConfigs = dict()
        self._allTranslatePath = list()
        self.createConfigTable()
        self.loadConfig()

    def __del__(self):
        self._conn.commit()
        self._conn.close()

    def createConfigTable(self):
        cursor = self._conn.cursor()
        cursor.execute('create table IF NOT EXISTS user (id varchar(50) primary key, name varchar(200))')
        cursor.close()

    def createTranslateTable(self):
        cursor = self._conn.cursor()
        cursor.execute('create table IF NOT EXISTS translate (name varchar(200))')
        cursor.close()

    def addTranslatePath(self, path):
        if path not in self._allTranslatePath:
            cursor = self._conn.cursor()
            cursor.execute("insert into translate (name) values (\'%s\')" % (str(path)))
            cursor.close()
            self._allTranslatePath.append(path)

    def delTranslatePath(self, path):
        if path in self._allTranslatePath:
            cursor = self._conn.cursor()
            cursor.execute("delete from translate where name = \'%s\'" % (str(path)))
            cursor.close()
            self._allTranslatePath.remove(path)

    def updateConfig(self, key, value):
        if key in self._allConfigs.keys():
            cursor = self._conn.cursor()
            cursor.execute("update user set name = \'%s\' where  id = \'%s\'" % (str(value), str(key)))
            cursor.close()
        else:
            cursor = self._conn.cursor()
            cursor.execute("insert into user (id, name) values (\'%s\', \'%s\')" % (str(key), str(value)))
            cursor.close()
        self._allConfigs[key] = value

    def loadConfig(self):
        cursor = self._conn.cursor()
        cursor.execute("select * from user")
        values = cursor.fetchall()
        for value in values:
            self._allConfigs[value[0]] = value[1]

    def getConfig(self, key):
        return self._allConfigs.get(key, "")


