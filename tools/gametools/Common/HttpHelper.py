# -*- coding: utf-8 -*-
"""
@File       :  HttpHelper
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@arkgames.com
@Time       :  2022/2/17 18:14
@Description:  
"""
import json
# from urllib import request
import urllib3
from urllib3 import encode_multipart_formdata


class HttpHelper:

    def __init__(self):
        pass

    # @staticmethod
    # def Post(url: str, postdata: object):
    #     http = urllib3.PoolManager()
    #     params = json.dumps(postdata)
    #     headers = {'Accept-Charset': 'utf-8', 'Content-Type': 'application/json'}
    #     params = bytes(params, 'utf8')
    #     req = request.Request(url=url, data=params, headers=headers, method='POST')
    #     response = request.urlopen(req).read()
    #     response = str(response, 'utf-8')
    #     return response

    @staticmethod
    def PostFormData(url: str, postdata: object):
        http = urllib3.PoolManager()
        # encode_data = encode_multipart_formdata(postdata)
        # data = encode_data[0]
        # header = {'Content-Type': encode_data[1]}
        req = http.request('POST', url, fields=postdata)
        # req = request.Request(url=url, data=data, headers=header, method='POST')
        # response = request.urlopen(req).read()
        response = req.data.decode('utf-8')
        return response
