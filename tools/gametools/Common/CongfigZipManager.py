# -*- coding: utf-8 -*-
"""
@File       :  CongfigZipManager
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@arkgames.com
@Time       :  2022/6/21 18:19
@Description:  
"""
from PyQt5.QtCore import *
from Common.Utils import Utils
import functools


class ConfigZipManager(QObject):

    @staticmethod
    def StaticSoitCongfigVersions(config1, config2):
        config1Time = int(config1.get('fName').split("#")[1])
        config2Time = int(config2.get('fName').split("#")[1])
        if config1Time > config2Time:
            return -1
        elif config1Time < config2Time:
            return 1
        return 0

    def __init__(self):
        super(ConfigZipManager, self).__init__()
        self._allConfings = None

    def loadAllConfigs(self, path):
        """加载所有ZIP配置"""
        self._allConfings = list()
        allFiles, allFileNames = Utils.SearchFiles(path, 'zip', "serverConfigs_")
        # print(allFile)
        for i, fName in enumerate(allFileNames):
            self._allConfings.append({'fName': fName, 'filePath': allFiles[i]})

        self._allConfings.sort(key=functools.cmp_to_key(ConfigZipManager.StaticSoitCongfigVersions))

    def addNewConfig(self, path):
        fileName = path.split('\\')[-1].split(".")[0]
        self._allConfings.append({'fName': fileName, 'filePath': path})
        self._allConfings.sort(key=functools.cmp_to_key(ConfigZipManager.StaticSoitCongfigVersions))

    def allConfigs(self):
        return self._allConfings

    def getConfigPathByIndex(self, i):
        if i > len(self._allConfings) - 1:
            return None
        return self._allConfings[i]


