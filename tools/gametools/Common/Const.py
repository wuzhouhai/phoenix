# -*- coding: utf-8 -*-
"""
@File       :  Const
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/1 18:03
@Description:  
"""
import sys


class Const(object):
    class ConstError(TypeError):
        pass

    def __setattr__(self, key, value):
        if key in self.__dict__.keys():
            raise self.ConstError("Changing const.%s" % key)
        else:
            self.__dict__[key] = value

    def __getattr__(self, key):
        if key in self.__dict__.keys():
            return self.key
        else:
            return None


sys.modules[__name__] = Const()

Const.FormatVerLuaStrServer = """
 ---@class Config.Ver
local t_Ver_dec = {
	id = 0,
    version = '',    --版本号
}
local t_Ver_col = {id=1, version = 2}
local t_Ver_tmp =
{
[0] = {"%s"},
}
---@type table<number, Config.Ver>
T_Ver = {}
local mt = {__index = function ( t, n ) local i = t_Ver_col[n] if i then return t[i] end end}
setmetatable(T_Ver, {__index = function(t, i) local rec = t_Ver_tmp[i] if rec ~= nil then setmetatable(rec,mt) return rec end end})
function T_Ver.iter() return function(t,k) local rk, rv if k then rk, rv = next(t,k) else rk, rv = next(t) end if rk then setmetatable(rv,mt)  end return rk, rv end, t_Ver_tmp end
"""

Const.FormatVerLuaStrClient = """
 ---@class Config.Ver
local t_Ver_dec = {
    id = 0,    --状态Id
    version = ''    --版本号
}
local t_Ver_col = {id = 1,version = 2}
local t_Ver_tmp =
{
[0] = {"%s"},
}
---@type table<number, Config.Ver>
local t_Ver = {}
local mt = {__index = function ( t, n ) local i = t_Ver_col[n] if i then return t[i] end end}
setmetatable(t_Ver, {__index = function(t, i) local rec = t_Ver_tmp[i] if rec ~= nil then setmetatable(rec,mt) return rec end end})
function t_Ver.iter() return function(t,k) local rk, rv if k then rk, rv = next(t,k) else rk, rv = next(t) end if rk then setmetatable(rv,mt)  end return rk, rv end, t_Ver_tmp end
return t_Ver
"""

Const.UpdateConfigKey = 'XbOB5t8YPcuD6XPJ'

Const.OutPutType_All = 3
Const.OutPutType_Client = 1
Const.OutPutType_Server = 2

Const.ConfigSvn = r"https://svn-trans-forward.gameark.cn:9999/svn/LegendDragons/project/trunk/Excel/C-%E7%AD%96%E5%88%92%E9%85%8D%E7%BD%AE%E8%A1%A8"
Const.ClientConfigSvn = r"https://svn-trans-forward.gameark.cn:9999/svn/LegendDragons/project/trunk/res_url/share_assets/android/lua/GameConfig"
Const.ServerConfigSvn = r"https://svn-trans-forward.gameark.cn:9999/svn/LegendDragons/project/trunk/Excel/ServerConfig"

Const.ClientConfigSvnRelease = r"https://svn-trans-forward.gameark.cn:9999/svn/LegendDragons/project/release/res_url/share_assets/android/lua/GameConfig"
Const.ConfigSvnRelease = r"https://svn-trans-forward.gameark.cn:9999/svn/LegendDragons/project/release/Excel/C-%E7%AD%96%E5%88%92%E9%85%8D%E7%BD%AE%E8%A1%A8"
Const.ServerConfigSvnRelease = r"https://svn-trans-forward.gameark.cn:9999/svn/LegendDragons/project/release/Excel/ServerConfig"


Const.LoaclSvnExcelPath = r"resource/excels/"
Const.LocalSvnClientConfigsPath = r"resource/cLuaConfigs/"
Const.LocalServerOutPutPath = r"resource/sLuaConfigs/"
Const.LocalServerOutPutZipPath = r"resource/sZipConfigs/"

Const.LoaclSvnExcelPathRelease = r"resource/excels_release/"
Const.LocalSvnClientConfigsPathRelease = r"resource/cLuaConfigs_release/"
Const.LocalServerOutPutPathRelease = r"resource/sLuaConfigs_release/"
Const.LocalServerOutPutZipPathRelease = r"resource/sZipConfigs_release/"

Const.GuanQiaFileName = "t_CampaignMapNew.lua"

Const.UpdateServer = "http://lod-us-game.arkgames.com/static/"                    # 更新服务器地址
Const.UpdateVerFile = "update.ini"                              # 更新服务器文件
Const.UpdateExe = "Update.exe"

Const.RewardTypeResource = 1
Const.RewardTypeItem = 2
Const.RewardTypeDragon = 3

Const.RewardType = {1: '资源', 2: '道具'}
Const.RewardResource = {1: '等级', 2: '经验', 3: '钻石', 4: '粮食', 5: '木材', 6: '石材', 7: 'vip经验', 8: 'vip等级',
                        9: '能量', 10: '探索能量', 11: '公会积分'}
Const.TranslateType = {'English': {'id': 1, 'fileExt': 'EN', 'transColName': None},
                       'Portugal': {'id': 2, 'fileExt': 'PT', 'transColName': 'Portugal'}}
Const.TranslateType_En = 'English'
Const.TranslateType_Pt = 'Portugal'

Const.RowType_String = "string"                     # 字符串类型
Const.RowType_Number = "number"                     # 数值类型
Const.RowType_StringArray = "stringArray"           # 一维字符数组
Const.RowType_StringArray2 = "stringArray2"         # 二维字符数组
Const.RowType_NumberArray = "numberArray"           # 一维数值数组
Const.RowType_NumberArray2 = "numberArray2"         # 二维数值数组
Const.RowType_Dictionary = "mapStrNumber"             # 字典
Const.RowType_LuaFormat = "luaFormat"               # lua代码

Const.AllRowTypes = [Const.RowType_String, Const.RowType_Number, Const.RowType_StringArray, Const.RowType_StringArray2,
                     Const.RowType_NumberArray, Const.RowType_NumberArray2, Const.RowType_Dictionary, Const.RowType_LuaFormat]

Const.ServerOperateCmd_Start = 9527
Const.ServerOperateCmd_Stop = 9528
Const.ServerOperateCmd_SafeStop = 9529

Const.CheckConfigExcelFileName = "J-检测配置规则"
Const.CheckConfigExcelSheetName = "ConfigRuleCheck"

Const.CheckRuleTypeInConfig = 1  # 规则检测类型读取配置
Const.CheckRuleTypeTable = 2    # 规则检测类型检测关联其他表主键




