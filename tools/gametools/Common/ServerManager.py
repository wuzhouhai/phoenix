# -*- coding: utf-8 -*-
"""
@File       :  ServerManager
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@arkgames.com
@Time       :  2022/6/20 17:55
@Description:  服务器信息管理
"""
from PyQt5.QtCore import *
import yaml


class ServerManager(QObject):

    def __init__(self):
        super(ServerManager, self).__init__()
        self.file_name = r'servers.yaml'
        self.defaulteTestIndex = -1;
        self.servers = list()
        self.serversOperate = dict()

    def readConf(self):
        conf = open(file=self.file_name, mode='r', encoding="utf-8")
        serversData = yaml.load(conf, Loader=yaml.FullLoader)
        self.servers = serversData['servers']
        if not self.servers:
            return
        for index, s in enumerate(self.servers):
            if s['name'] == "一般测试服1(s9915)":
                self.defaulteTestIndex = index
            self.serversOperate[s['id']] = {'inStart': 0, 'inStop': 0}

    def getServers(self):
        return self.servers

    def getServerByIndex(self, i):
        return self.servers[i] or None

    def queryConfigVerUrl(self, i):
        serverInfo = self.getServerByIndex(i)
        if serverInfo:
            if int(serverInfo['port']) == 80:
                return "http://%s/%s" % (serverInfo['host'], serverInfo['configUrl'])
            else:
                return "http://%s:%s/%s" % (serverInfo['host'], serverInfo['port'], serverInfo['configUrl'])
        return None

    def updateConfigUrl(self, i):
        serverInfo = self.getServerByIndex(i)
        if serverInfo:
            if int(serverInfo['port']) == 80:
                return "http://%s/%s" % (serverInfo['host'], serverInfo['url'])
            else:
                return "http://%s:%s/%s" % (serverInfo['host'], serverInfo['port'], serverInfo['url'])
        return None

    def cmdUrl(self, i):
        serverInfo = self.getServerByIndex(i)
        if serverInfo:
            if int(serverInfo['port']) == 80:
                return "http://%s/%s" % (serverInfo['host'], serverInfo['cmdUrl'])
            else:
                return "http://%s:%s/%s" % (serverInfo['host'], serverInfo['port'], serverInfo['cmdUrl'])
        return None

    def checkServerCanOperateStart(self, i):
        serverInfo = self.getServerByIndex(i)
        if serverInfo:
            if self.serversOperate.get(serverInfo['id'], {}).get('inStart', -1) == 0:
                return True
            return False
        return False

    def checkServerCanOperateStop(self, i):
        serverInfo = self.getServerByIndex(i)
        if serverInfo:
            if self.serversOperate.get(serverInfo['id'], {}).get('inStop', -1) == 0:
                return True
            return False
        return False