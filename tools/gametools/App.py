# -*- coding: utf-8 -*-
"""
@File       :  App
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/1 9:31
@Description:  
"""

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from MainWindow import Ui_MainWindow
from Analysis.ConfigPathAnalysis import ConfigPathAnalysis
from Common.Utils import Utils
from Common.ConfigHelper import Config
from OutPut.OutPutWorkThread import OutPutWorkThread
from Common import Const
from Modules.ItemManager import ItemManager
from Modules.RewardManager import RewardManager
from Modules.ConditionManager import ConditionManager
from Threads.AnalysisThread import AnalysisThread
from Translate.OutPutTranslateThread import OutPutTranslateThread
import shutil
import os
from Common.ServerManager import ServerManager
from Common.CongfigZipManager import ConfigZipManager
from Threads.UpdateServerConfigThread import UpdateServerConfigThread
from Threads.ServerOperateThread import ServerOperateThread
from Threads.GetConfigVerThread import GetConfigVerThread
from Threads.SvnExcelConfigThread import SvnExcelConfigThread
from Threads.ReleaseConfigThread import ReleaseConfigThread
from Threads.CheckConfigThread import CheckConfigThread
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement
from xml.etree.ElementTree import ElementTree
import json
import pysvn
import svn.local, svn.remote
import pprint


class App(Ui_MainWindow, QMainWindow):
    UpdateConfigSignal = pyqtSignal(object)
    CmdStartSignal = pyqtSignal(object)
    CmdStopSignal = pyqtSignal(object)
    GetConfigVerSignal = pyqtSignal(object)
    ThreadLogSignal = pyqtSignal(str, int)
    ReleaseConfigFinishSignal = pyqtSignal()
    CheckConfigFinishSignal = pyqtSignal()

    def __init__(self):
        super(App, self).__init__()
        self._allConfigFiles = None
        self._allConfigFileNames = None
        self._workThread = None
        self._workThreadTrans = None
        self._daoJuExcelFile = None
        self._rewardExcelFile = None
        self._conditiExcelFile = None
        self._outPutErr = False
        self._luaCheckConfigToolPath = None
        self._selectItemIndex = -1
        self._selectRewardLogicId = -1
        self._itemManager = ItemManager()
        self._rewardManager = RewardManager()
        self._conditionManager = ConditionManager()
        self._cnf = Config()
        self._treeViewRewardModel = QStandardItemModel()
        self._rewardLogicCondionsA = list()
        self._rewardLogicCondionsB = list()
        self._rewardSelectIndex = -1
        self._selectTranslatPathIndex = -1
        self._configZipFiles = None
        self._configSignName = ""
        self._workReleseConfig = False
        self._workCheckConfig = False
        self._serverManager = ServerManager()
        self._configZipManager = ConfigZipManager()
        self._taskThreads = QThreadPool()
        self._taskThreads.setMaxThreadCount(10)
        self.itemNames = list()
        self.rewardNames = list()
        self.itemSelectLiftDropGroups = list()
        self.signalBind()
        self.setupUi(self)
        # self.QueryServerConfigVer()

    @property
    def AllConfigFiles(self):
        return self._allConfigFiles

    def closeEvent(self, event):
        pass
        # if self._itemManager:
        #     self._itemManager.SaveItemData()
        #     self._itemManager.CloseFile()

    def setupUi(self, MainWindow):
        super(App, self).setupUi(MainWindow)

        self.setWindowTitle("PhonexConfig-V" + str(Utils.Ver))
        self.tableWidget_Config.setColumnCount(2)
        self.tableWidget_Config.setColumnWidth(1, 50)
        self.tableWidget_Config.setHorizontalHeaderLabels(['配置路径', '导出'])
        self.tableWidget_Config.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)  # 所有列自动拉伸，充满界面
        self.tableWidget_Config.setEditTriggers(QTableView.NoEditTriggers)
        self.tableWidget_Config.cellChanged.connect(self.ConfigCellChanged)

        self.tableWidget_ConfigTranslta.setColumnCount(1)
        self.tableWidget_ConfigTranslta.setColumnWidth(1, 100)
        self.tableWidget_ConfigTranslta.setHorizontalHeaderLabels(['翻译路径'])
        self.tableWidget_ConfigTranslta.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)  # 所有列自动拉伸，充满界面

        self.tableWidgetItemLift.setColumnCount(4)
        self.tableWidgetItemLift.setColumnWidth(0, 70)
        self.tableWidgetItemLift.setColumnWidth(1, 70)
        self.tableWidgetItemLift.setColumnWidth(2, 65)
        self.tableWidgetItemLift.setColumnWidth(3, 48)
        self.tableWidgetItemLift.setHorizontalHeaderLabels(['道具/资源', 'ID', '数目', '掉落组'])
        # self.tableWidgetItemLift.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)  # 所有列自动拉伸，充满界面
        self.tableWidgetItemLift.setEditTriggers(QTableView.NoEditTriggers)

        self.tableWidgetItemLiftSource.setColumnCount(4)
        self.tableWidgetItemLiftSource.setColumnWidth(0, 70)
        self.tableWidgetItemLiftSource.setColumnWidth(1, 70)
        self.tableWidgetItemLiftSource.setColumnWidth(2, 65)
        self.tableWidgetItemLiftSource.setColumnWidth(3, 48)
        self.tableWidgetItemLiftSource.setHorizontalHeaderLabels(['道具/资源', 'ID', '数目', '掉落组'])
        # self.tableWidgetItemLift.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)  # 所有列自动拉伸，充满界面
        self.tableWidgetItemLiftSource.setEditTriggers(QTableView.NoEditTriggers)

        self.tableWidgetRewardSolid.setColumnCount(3)
        self.tableWidgetRewardSolid.setColumnWidth(0, 50)
        self.tableWidgetRewardSolid.setColumnWidth(1, 148)
        self.tableWidgetRewardSolid.setHorizontalHeaderLabels(['类型', '名称', '数目'])
        # self.tableWidgetRewardSolid.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)  # 所有列自动拉伸，充满界面
        self.tableWidgetRewardSolid.setEditTriggers(QTableView.NoEditTriggers)

        self.tableWidgetRewardRandom.setColumnCount(4)
        self.tableWidgetRewardRandom.setColumnWidth(0, 40)
        self.tableWidgetRewardRandom.setColumnWidth(1, 150)
        self.tableWidgetRewardRandom.setColumnWidth(2, 50)
        self.tableWidgetRewardRandom.setColumnWidth(3, 50)
        self.tableWidgetRewardRandom.setHorizontalHeaderLabels(['类型', '名称', '数目', '权值'])
        self.tableWidgetRewardRandom.setEditTriggers(QTableView.NoEditTriggers)

        self.tableWidgetCondition1.setColumnCount(2)
        self.tableWidgetCondition1.setColumnWidth(0, 50)
        self.tableWidgetCondition1.setColumnWidth(1, 118)
        self.tableWidgetCondition1.setHorizontalHeaderLabels(['ID', '描述'])
        self.tableWidgetCondition1.setEditTriggers(QTableView.NoEditTriggers)

        self.tableWidgetCondition2.setColumnCount(2)
        self.tableWidgetCondition2.setColumnWidth(0, 50)
        self.tableWidgetCondition2.setColumnWidth(1, 118)
        self.tableWidgetCondition2.setHorizontalHeaderLabels(['ID', '描述'])
        self.tableWidgetCondition2.setEditTriggers(QTableView.NoEditTriggers)

        reg = QRegExp('^\d{1,}$')
        validator = QRegExpValidator(self)
        validator.setRegExp(reg)
        self.lineEditItemID.setValidator(validator)
        self.lineEditItemStack.setValidator(validator)
        self.lineEditItemSort1.setValidator(validator)
        self.lineEditItemSort2.setValidator(validator)
        self.lineEditItemDecompose.setValidator(validator)
        self.lineEditItemSortExchange.setValidator(validator)
        self.lineEditItemCompose.setValidator(validator)
        self.lineEditItemEquipId.setValidator(validator)
        self.lineEditItemDragonId.setValidator(validator)
        self.lineEditItemIslandId.setValidator(validator)
        self.lineEditItemTowerId.setValidator(validator)
        self.lineEditItemPeriod.setValidator(validator)
        self.lineEditItemGuildId.setValidator(validator)
        self.lineEditItemPage.setValidator(validator)
        self.lineEditNewItemID.setValidator(validator)
        self.lineEditRewardLogicId.setValidator(validator)
        self.lineEditRewardLogicGroup.setValidator(validator)
        self.lineEditRewardWeight.setValidator(validator)
        self.lineEditRewardLogicGNew.setValidator(validator)
        self.lineEditRewardLogicNew.setValidator(validator)
        self.lineEditRewardSolidCounts.setValidator(validator)
        self.lineEditRewardRandomCounts.setValidator(validator)
        self.lineEditRewardRandomWeight.setValidator(validator)
        self.lineEditRewardId.setValidator(validator)
        self.lineEditNewRewardId.setValidator(validator)

        self.treeViewRewardLogic.setModel(self._treeViewRewardModel)

        # 加载配置
        openPath = self._cnf.getConfig("configPath")
        if openPath != "" and Utils.CheckFilePath(openPath):
            self.label_configPath.setText(openPath)
            self.FillConfigs()

        clientPath = self._cnf.getConfig("clientPath")
        if clientPath != "" and Utils.CheckFilePath(clientPath):
            self.label_clientOutPath.setText(clientPath)

        serverPath = self._cnf.getConfig("serverPath")
        if serverPath != "" and Utils.CheckFilePath(serverPath):
            self.label_serverOutPath.setText(serverPath)

        serverZipPath = self._cnf.getConfig("serverZipPath")
        if serverZipPath != "" and Utils.CheckFilePath(serverZipPath):
            self.label_serverOutZipPath.setText(serverZipPath)
            self._configZipManager.loadAllConfigs(serverZipPath)
            self.FillZipConfigVersions()

        clientGuanQiaPath = self._cnf.getConfig("clientGuanQiaPath")
        if clientGuanQiaPath != "" and Utils.CheckFilePath(clientGuanQiaPath):
            self.label_configPathClientGuanQia.setText(clientGuanQiaPath)

        serverGuanQiaPath = self._cnf.getConfig("serverGuanQiaPath")
        if serverGuanQiaPath != "" and Utils.CheckFilePath(serverGuanQiaPath):
            self.label_configPathServerGuanQia.setText(serverGuanQiaPath)

        translatePath = self._cnf.getConfig("translatePath")
        if translatePath != "" and Utils.CheckFilePath(translatePath):
            self.label_configPath_transOut.setText(translatePath)

        self._daoJuExcelFile = self._cnf.getConfig("daoJuExcelPath")
        if not Utils.CheckFileIsExsit(self._daoJuExcelFile):
            self._daoJuExcelFile = ""
        self.label_DaoJuExcel.setText(self._daoJuExcelFile)
        self.StartAnalysisItem()

        self._rewardExcelFile = self._cnf.getConfig("rewardExcelPath")
        if not Utils.CheckFileIsExsit(self._rewardExcelFile):
            self._rewardExcelFile = ""
        self.label_RewardExcel.setText(self._rewardExcelFile)
        self.StartAnalysisReward()

        self._conditiExcelFile = self._cnf.getConfig("conditionExcelPath")
        if not Utils.CheckFileIsExsit(self._conditiExcelFile):
            self._conditiExcelFile = ""
        self.label_ConditionExcel.setText(self._conditiExcelFile)
        self.StartAnalysisCondition()

        self._configSignName = self._cnf.getConfig("configSignName")
        if len(self._configSignName) > 0:
            self.lineEdit_ConfigSign.setText(self._configSignName)
            self.lineEdit_ConfigSign.setDisabled(True)

        # self._luaCheckConfigToolPath = self._cnf.getConfig("configPathLuaCheckTool")
        # self._luaCheckConfigToolPath = ""
        # if not Utils.CheckFilePath(self._luaCheckConfigToolPath):
        #     if openPath != "" and Utils.CheckFilePath(openPath):
        #         self._luaCheckConfigToolPath = os.path.abspath(os.path.dirname(openPath)) + "\\TableCheck\\"
        # self.label_configPath_Check_Lua.setText(self._luaCheckConfigToolPath)

        self.makeResourceDir()
        self._serverManager.readConf()
        self.FillServers()
        # self.CheckOutConfigSvn()
        # self.CheckOutClientConfigSvn()
        # self.CheckOutServerConfigSvn()
        # self.CheckOutConfigSvnRelease()
        # self.CheckOutClientConfigSvnRelease()
        # self.CheckOutServerConfigSvnRelease()

    def signalBind(self):
        """绑定信号"""
        self.UpdateConfigSignal.connect(self.UpdateConfigSignalBack)
        self.CmdStartSignal.connect(self.CmdStartSignalBack)
        self.CmdStopSignal.connect(self.CmdStopSignalBack)
        self.GetConfigVerSignal.connect(self.GetConifgVerBack)
        self.ThreadLogSignal.connect(self.ThreadLogSignalBack)
        self.ReleaseConfigFinishSignal.connect(self.ReleaseConfigFinishSignalBack)
        self.CheckConfigFinishSignal.connect(self.CheckConfigFinishSignalBack)

    def CheckConfigFinishSignalBack(self):
        self._workCheckConfig = False

    def ReleaseConfigFinishSignalBack(self):
        self._workReleseConfig = False

    def ThreadLogSignalBack(self, logStr, level):
        self.ShowLog(logStr, level)

    def GetConifgVerBack(self, datas):
        if datas['back']:
            try:
                cVer = json.loads(datas['back'])
                self.ShowLog("获取选中服务器配置版本成功")
                self.label_ServerConfigVer.setText(cVer["ver"])
                self.label_ServerConfigAuthor.setText(cVer["author"])
            except Exception as e:
                self.ShowLog("获取选中服务器配置版本失败")
        else:
            self.ShowLog("获取选中服务器配置版本失败")

    def UpdateConfigSignalBack(self, datas):
        if datas['back']:
            try:
                backCode = int(datas['back'])
                if backCode == 0:
                    self.ShowLog("上传完成")
                    self.QueryServerConfigVer()
                    QMessageBox.warning(self, '提示信息', '亲,配置发布成功,重启服务器才能生效哦。', QMessageBox.Yes)
                else:
                    self.ShowLog("上传失败")
            except Exception as e:
                self.ShowLog("上传失败")
                self.ShowLog(datas['back'])
        else:
            self.ShowLog("上传失败")
            if datas['err']:
                self.ShowLog(datas['err'])

    def CmdStartSignalBack(self, datas):
        if datas['back']:
            try:
                backCode = int(datas['back'])
                if backCode == 0:
                    self.ShowLog("启动完成")
                else:
                    self.ShowLog("启动失败")
            except Exception as e:
                self.ShowLog("启动失败")
                self.ShowLog(datas['back'])
        else:
            self.ShowLog("启动失败")
            if datas['err']:
                self.ShowLog(datas['err'])

    def CmdStopSignalBack(self, datas):
        # print(datas)
        if datas['back']:
            try:
                backCode = int(datas['back'])
                if backCode == 0:
                    self.ShowLog("关闭完成")
                else:
                    self.ShowLog("关闭失败")
            except Exception as e:
                self.ShowLog("关闭失败")
                self.ShowLog(datas['back'])
        else:
            self.ShowLog("关闭失败")
            if datas['err']:
                self.ShowLog(datas['err'])

    def OpenConfigLuaCheckToolPath(self):
        pathName = QFileDialog.getExistingDirectory(self, '选择Lua检测配置工具路径', '/')
        if pathName:
            self.label_configPath_Check_Lua.setText(pathName)
            self._cnf.updateConfig("configPathLuaCheckTool", pathName)
            self._luaCheckConfigToolPath = pathName

    def OpenConfigPath(self):
        print("testBtnClick")
        pathName = QFileDialog.getExistingDirectory(self, '选择配置文件路径', '/')
        if pathName:
            self.label_configPath.setText(pathName)
            self._cnf.updateConfig("configPath", pathName)
            self.FillConfigs()

    def OpenClientOutPath(self):
        pathName = QFileDialog.getExistingDirectory(self, '选择客户端配置导出目录', '/')
        if pathName:
            self._cnf.updateConfig("clientPath", pathName)
            self.label_clientOutPath.setText(pathName)

    def OpenServerOutPath(self):
        pathName = QFileDialog.getExistingDirectory(self, '选择服务器配置导出目录', '/')
        if pathName:
            if pathName == self.label_serverOutZipPath.text():
                QMessageBox.warning(self, '提示信息', '本路径不能与[[服务器版本路径]]相同', QMessageBox.Yes)
                return
            self._cnf.updateConfig("serverPath", pathName)
            self.label_serverOutPath.setText(pathName)

    def OpenConfigOutZipPath(self):
        pathName = QFileDialog.getExistingDirectory(self, '选择服务器配置ZIP生成目录', '/')
        if pathName:
            if self.label_serverOutPath.text() == pathName:
                QMessageBox.warning(self, '提示信息', '本路径不能与[[服务器输出路径]]相同', QMessageBox.Yes)
                return
            self._cnf.updateConfig("serverZipPath", pathName)
            self.label_serverOutZipPath.setText(pathName)
            self._configZipManager.loadAllConfigs(pathName)
            self.FillZipConfigVersions()

    def OpenClientGuanQiaPath(self):
        ret = QMessageBox.warning(self, '提示信息', '请与关卡编辑器导出目录设置一致!如果亲不负责关卡,请不要设置此路径。',
                                  QMessageBox.Yes | QMessageBox.No)
        if ret == QMessageBox.No:
            return
        pathName = QFileDialog.getExistingDirectory(self, '选择客户端关卡配置文件所在目录', '/')
        if pathName:
            if self.label_configPathServerGuanQia.text() == pathName:
                QMessageBox.warning(self, '提示信息', '本路径不能与[[服务器关卡配置文件目录]相同', QMessageBox.Yes)
                return
            self._cnf.updateConfig("clientGuanQiaPath", pathName)
            self.label_configPathClientGuanQia.setText(pathName)

    def OpenServerGuanQiaPath(self):
        ret = QMessageBox.warning(self, '提示信息', '请与关卡编辑器导出目录设置一致!如果亲不负责关卡,请不要设置此路径。',
                                  QMessageBox.Yes | QMessageBox.No)
        if ret == QMessageBox.No:
            return
        pathName = QFileDialog.getExistingDirectory(self, '选择服务器关卡配置文件所在目录', '/')
        if pathName:
            if self.label_configPathClientGuanQia.text() == pathName:
                QMessageBox.warning(self, '提示信息', '本路径不能与[[客户端关卡配置文件目录]相同', QMessageBox.Yes)
                return
            self._cnf.updateConfig("serverGuanQiaPath", pathName)
            self.label_configPathServerGuanQia.setText(pathName)

    def OpenRewardConfigPath(self):
        fileInfo = QFileDialog.getOpenFileName(self, '选择文件', '.', 'Excel files(*.xlsx)')
        print(fileInfo[0])
        if fileInfo:
            try:
                self._rewardExcelFile = fileInfo[0]
                self.label_RewardExcel.setText(self._rewardExcelFile)
                self._cnf.updateConfig("rewardExcelPath", self._rewardExcelFile)
                self.StartAnalysisReward()
            except Exception as e:
                print(e)

    def OpenConditionConfigPath(self):
        fileInfo = QFileDialog.getOpenFileName(self, '选择文件', '.', 'Excel files(*.xlsx)')
        if fileInfo:
            self._conditiExcelFile = fileInfo[0]
            self.label_ConditionExcel.setText(self._conditiExcelFile)
            self._cnf.updateConfig("conditionExcelPath", self._conditiExcelFile)
            self.StartAnalysisCondition()

    def FillServers(self):
        """填充可以操作的服务器"""
        self.comboBox_Servers.clear()
        ss = self._serverManager.getServers()
        if not ss:
            return
        for i, ser in enumerate(ss):
            self.comboBox_Servers.addItem(ser['name'], i)

    def FillZipConfigVersions(self):
        self.comboBox_Configs.clear()
        allConfigs = self._configZipManager.allConfigs()
        for i, configInfo in enumerate(allConfigs):
            splitNameInfo = configInfo['fName'].split("_")
            if splitNameInfo:
                self.comboBox_Configs.addItem(splitNameInfo[1].split(".")[0], i)

    def FillConfigs(self):
        """填充配置文件列表"""
        self._allConfigFiles, self._allConfigFileNames = ConfigPathAnalysis.FindAllConfigExcels(
            self.label_configPath.text())
        # print(len(self._allConfigFiles))
        length = len(self._allConfigFiles)
        self.tableWidget_Config.setRowCount(length + 1)
        # 遍历填充
        for index, file in enumerate(self._allConfigFiles):
            newItem = QTableWidgetItem(file)
            self.tableWidget_Config.setItem(index, 0, newItem)
            check = QTableWidgetItem()
            check.setCheckState(Qt.Unchecked)  # 把checkBox设为未选中状态
            self.tableWidget_Config.setItem(index, 1, check)  # 在(x,y)添加checkBox

        # 最后一行添加一个全选
        check = QTableWidgetItem(u"全选")
        check.setCheckState(Qt.Unchecked)  # 把checkBox设为未选中状态
        self.tableWidget_Config.setItem(length, 1, check)  # 在(x,y)添加checkBox

    def ConfigCellChanged(self, row, col):
        """列变动"""
        # print("1111111111111111" + str(row) + " " + str(col))
        if row == len(self._allConfigFiles) and col == 1:
            isCheck = self.tableWidget_Config.item(row, col).checkState()
            # 状态传染
            for index in range(len(self._allConfigFiles)):
                self.tableWidget_Config.item(index, 1).setCheckState(isCheck)

    def QueryServerConfigVer(self):
        if self.comboBox_Servers.count() > 0:
            sIndex = self.comboBox_Servers.currentIndex()
            sUrl = self._serverManager.queryConfigVerUrl(sIndex)
            t = GetConfigVerThread(sUrl, self)
            t.setAutoDelete(True)
            self._taskThreads.start(t)

    def PushZipConfig(self):
        if len(self._configSignName) <= 0:
            QMessageBox.warning(self, '提示信息', '请先设置发布人', QMessageBox.Yes)
            return
        if self.comboBox_Servers.count() > 0 and self.comboBox_Configs.count() > 0:
            reply = QMessageBox.warning(self, '提示信息', '亲,确定要发布配置版本[%s]到服务器[%s],此操作不可恢复?' % (
                self.comboBox_Configs.currentText(), self.comboBox_Servers.currentText()),
                                        QMessageBox.Yes | QMessageBox.No)
            if reply == QMessageBox.No:
                return
            sIndex = self.comboBox_Servers.currentIndex()
            cIndex = self.comboBox_Configs.currentIndex()
            sUrl = self._serverManager.updateConfigUrl(sIndex)
            configInfo = self._configZipManager.getConfigPathByIndex(cIndex)
            if sUrl and configInfo:
                self.ShowLog("开始上传:" + self.comboBox_Configs.currentText())
                t = UpdateServerConfigThread(sUrl, configInfo['filePath'], False, self)
                t.setAutoDelete(True)
                self._taskThreads.start(t)

    def StartGameServer(self):
        """启动游戏服务器"""
        if self.comboBox_Servers.count() > 0:
            sIndex = self.comboBox_Servers.currentIndex()
            sUrl = self._serverManager.cmdUrl(sIndex)
            if sUrl:
                self.ShowLog("开始启动:" + self.comboBox_Servers.currentText())
                t = ServerOperateThread(sUrl, Const.ServerOperateCmd_Start, self)
                t.setAutoDelete(True)
                self._taskThreads.start(t)

    def StopGameServer(self):
        """停止游戏服务器"""
        if self.comboBox_Servers.count() > 0:
            if not self.checkBox_safeClose.isChecked():
                reply = QMessageBox.warning(self, '提示信息',
                                            '亲,停止游戏服务器:[%s]?(注意,操作为非安全关闭,有可能丢失数据,仅供开发使用)' % self.comboBox_Servers.currentText(),
                                            QMessageBox.Yes | QMessageBox.No)
                if reply == QMessageBox.No:
                    return
                sIndex = self.comboBox_Servers.currentIndex()
                sUrl = self._serverManager.cmdUrl(sIndex)
                if sUrl:
                    self.ShowLog("开始停止:" + self.comboBox_Servers.currentText())
                    t = ServerOperateThread(sUrl, Const.ServerOperateCmd_Stop, self)
                    t.setAutoDelete(True)
                    self._taskThreads.start(t)
            else:
                reply = QMessageBox.warning(self, '提示信息',
                                            '亲,停止游戏服务器:[%s]?(注意,操作为安全关闭,关闭需等待一定时间(1-10)分钟)' % self.comboBox_Servers.currentText(),
                                            QMessageBox.Yes | QMessageBox.No)
                if reply == QMessageBox.No:
                    return
                sIndex = self.comboBox_Servers.currentIndex()
                sUrl = self._serverManager.cmdUrl(sIndex)
                if sUrl:
                    self.ShowLog("开始停止:" + self.comboBox_Servers.currentText())
                    t = ServerOperateThread(sUrl, Const.ServerOperateCmd_SafeStop, self)
                    t.setAutoDelete(True)
                    self._taskThreads.start(t)

    def StartOutPutConfig(self):
        """开始导出"""
        # 开始验证规则
        if self._workThread:
            QMessageBox.warning(self, '提示信息', '导出正在进行中', QMessageBox.Yes)
            return
        # 有没有源地址
        if self.label_configPath.text() == "":
            QMessageBox.warning(self, '提示信息', '请选择配置目录', QMessageBox.Yes)
            return
        if self.label_clientOutPath.text() == "" and self.label_serverOutPath.text() == "":
            QMessageBox.warning(self, '提示信息', '导出目录至少需要指定一个', QMessageBox.Yes)
            return
        # if self.label_serverOutZipPath.text() == "" and self.label_serverOutPath.text() != "":
        #     QMessageBox.warning(self, '提示信息', '请选择服务器版本目录', QMessageBox.Yes)
        #     return
        # if len(self._configSignName) <= 0:
        #     QMessageBox.warning(self, '提示信息', '请先设置发布人', QMessageBox.Yes)
        #     return
        # 判断导出类型
        outPutType = Const.OutPutType_All
        if self.label_clientOutPath.text() != "" and self.label_serverOutPath.text() != "":
            outPutType = Const.OutPutType_All
        else:
            if self.label_clientOutPath.text() != "":
                outPutType = Const.OutPutType_Client
            elif self.label_serverOutPath.text() != "":
                outPutType = Const.OutPutType_Server
        self._outPutErr = False
        outPutFiles = list()
        outPutFileNames = list()
        # 判断需要导出的文件
        length = len(self._allConfigFiles)
        for index in range(length):
            if self.tableWidget_Config.item(index, 1).checkState() == Qt.Checked:
                outPutFiles.append(self._allConfigFiles[index])
                outPutFileNames.append(self._allConfigFileNames[index])
        if len(outPutFiles) == 0:
            QMessageBox.warning(self, '提示信息', '当前没有选中任何导出文件', QMessageBox.Yes)
            return
        # 启动导出线程
        # log = "准备启动导出线程:outPutFiles:%d,outPutFileNames:%d" % (len(outPutFiles), len(outPutFileNames))
        # print(log)
        self.ShowLog("开始导出")
        # 产生新版本号
        Utils.CreateConfigServion("")
        # self._luaCheckConfigToolPath = os.path.abspath(os.path.dirname(self.label_configPath.text())) + "\\TableCheck\\"
        self._workThread = OutPutWorkThread(outPutFiles, outPutFileNames, self.label_clientOutPath.text(),
                                            self.label_serverOutPath.text(), outPutType, None, self)
        self._workThread.finished.connect(self.WorkFinished)
        self._workThread.start()

    def ClearLog(self):
        """清理日志"""
        self.textEdit_ConfigOut.clear()
        self.textEdit_ConfigOutErr.clear()

    def ClearServerConfig(self):
        if self.label_serverOutPath.text() != "":
            reply = QMessageBox.warning(self, '提示信息', '亲,确定要清理服务器配置文件,此操作不可恢复?',
                                        QMessageBox.Yes | QMessageBox.No)
            if reply == QMessageBox.No:
                return
            shutil.rmtree(self.label_serverOutPath.text())
            os.mkdir(self.label_serverOutPath.text())
            QMessageBox.warning(self, '提示信息', '清理完成', QMessageBox.Yes)

    def ClearClientConfig(self):
        if self.label_clientOutPath.text() != "":
            reply = QMessageBox.warning(self, '提示信息', '亲,确定要清理客户端配置文件,此操作不可恢复?',
                                        QMessageBox.Yes | QMessageBox.No)
            if reply == QMessageBox.No:
                return
            shutil.rmtree(self.label_clientOutPath.text())
            os.mkdir(self.label_clientOutPath.text())
            QMessageBox.warning(self, '提示信息', '清理完成', QMessageBox.Yes)

    def ClearServerConfigZip(self):
        if self.label_serverOutZipPath.text() != "":
            reply = QMessageBox.warning(self, '提示信息', '亲,确定要清理服务器配置打包目录,此操作不可恢复?',
                                        QMessageBox.Yes | QMessageBox.No)
            if reply == QMessageBox.No:
                return
            shutil.rmtree(self.label_serverOutZipPath.text())
            os.mkdir(self.label_serverOutZipPath.text())
            self._configZipManager.loadAllConfigs(self.label_serverOutZipPath.text())
            self.FillZipConfigVersions()
            QMessageBox.warning(self, '提示信息', '清理完成', QMessageBox.Yes)

    def ShowLog(self, msg, level=0):
        """输入日志"""
        if level == 1:
            self.ShowErrLog(msg)
        else:
            self.textEdit_ConfigOut.moveCursor(QTextCursor.End)
            self.textEdit_ConfigOut.append("<font color = \"#339900\">[%s]: %s </font>" % (Utils.GetNowTimeStr(), msg))

    def ShowErrLog(self, msg):
        self.textEdit_ConfigOutErr.moveCursor(QTextCursor.End)
        self.textEdit_ConfigOutErr.append("<font color = \"#FF0000\">[%s]: %s </font>" % (Utils.GetNowTimeStr(), msg))

    def ConfigOutWorkLog(self, logMsg, level):
        if level == 1:
            self._outPutErr = True
        self.ShowLog(logMsg, level)

    def CreateVersionXmlInfo(self):
        """生成一份xml信息,保存版本和发布人"""
        root = Element('root')
        ver = SubElement(root, 'ver')
        ver.text = Utils.ConfigVersion
        author = SubElement(root, 'author')
        author.text = self._configSignName
        tree = ElementTree(root)
        tree.write(self.label_serverOutPath.text() + '\\ver.xml', encoding='utf-8')

    def WorkFinished(self):
        # if self._workThread.isFinished():
        self._workThread.wait()
        self._workThread = None
        self.ShowLog("导出完成")
        if self._outPutErr:
            QMessageBox.warning(self, '提示信息', '本次导出出现错误,请查看导出日志', QMessageBox.Yes)
            return
        # 生成版本号
        self.ShowLog("生成版本号")
        Utils.CreateConfigServion("")

        if self.label_serverOutPath.text() != "":
            self.ShowLog("写入服务端版本号版本号:%s" % Utils.ConfigVersion)
            # fileStrS = Const.FormatVerLuaStrServer % Utils.ConfigVersion
            verJson = {"id": 1, "ver": Utils.ConfigVersion}
            outputSFile = self.label_serverOutPath.text() + "/t_Ver.json"
            Utils.WriteFile(outputSFile, json.dumps(verJson))
            self.CreateVersionXmlInfo()
            self.ShowLog("生成配置压缩包 版本号:%s" % Utils.ConfigVersion)
            # 压缩
            if self.label_serverOutZipPath.text() != "":
                zipFile = shutil.make_archive(
                    self.label_serverOutZipPath.text() + "\\serverConfigs_%s" % Utils.ConfigVersion, 'zip',
                    self.label_serverOutPath.text())
                if zipFile:
                    self._configZipFiles = zipFile
                    self._configZipManager.addNewConfig(zipFile)
                    self.FillZipConfigVersions()
                    self.ShowLog("生成配置压缩包完成, 文件路径:%s" % self._configZipFiles)

        if self.label_clientOutPath.text() != "":
            self.ShowLog("写入客户端版本号 版本号:%s" % Utils.ConfigVersion)
            verJson = {"id": 1, "ver": Utils.ConfigVersion}
            outputCFile = self.label_clientOutPath.text() + "/t_Ver.json"
            Utils.WriteFile(outputCFile, json.dumps(verJson))

    def TransWorkFinished(self):
        # if self._workThread.isFinished():
        self._workThreadTrans.wait()
        self._workThreadTrans = None
        self.ShowLog("翻译导出完成")

    def OpenDaoJuConfigPath(self):
        """设置道具表路径"""
        fileInfo = QFileDialog.getOpenFileName(self, '选择文件', '.', 'Excel files(*.xlsx)')
        print(fileInfo[0])
        if fileInfo:
            self._daoJuExcelFile = fileInfo[0]
            self.label_DaoJuExcel.setText(self._daoJuExcelFile)
            self._cnf.updateConfig("daoJuExcelPath", self._daoJuExcelFile)
            self.StartAnalysisItem()

    def StartAnalysisItem(self):
        """开始分析道具"""
        if self._itemManager and self._daoJuExcelFile and not self._itemManager.CheckIsAnalysis():
            self._itemManager.SetSourceExcel(self._daoJuExcelFile)
            self._itemManager.SetAnalysis(True)
            t = AnalysisThread(self._itemManager)
            t.WorkFinishSignal.connect(self.AnalysisFinish)
            t.start()
            t.wait()

    def StartAnalysisReward(self):
        """分析奖励产出表"""
        if self._rewardManager and self._rewardExcelFile and not self._rewardManager.CheckIsAnalysis():
            self._rewardManager.SetSourceExcel(self._rewardExcelFile)
            self._rewardManager.SetAnalysis(True)
            t = AnalysisThread(self._rewardManager)
            t.WorkFinishSignal.connect(self.AnalysisFinish)
            t.start()
            t.wait()

    def StartAnalysisCondition(self):
        """分析条件表"""
        if self._conditionManager and self._conditiExcelFile and not self._conditionManager.CheckIsAnalysis():
            self._conditionManager.SetSourceExcel(self._conditiExcelFile)
            self._conditionManager.SetAnalysis(True)
            t = AnalysisThread(self._conditionManager)
            t.WorkFinishSignal.connect(self.AnalysisFinish)
            t.start()
            t.wait()

    def AnalysisFinish(self, moduleName):
        """物品数据加载完成"""
        if moduleName == self._itemManager.GetModuleName():
            self._itemManager.SetAnalysis(False)
            self._itemManager.DataIsOK = True
            self.FillItemList()
        elif moduleName == self._rewardManager.GetModuleName():
            self._rewardManager.SetAnalysis(False)
            self._rewardManager.DataIsOK = True
            self.FillselTreeViewRewardLogic()
            self.FillRewardListView()
            self.FillItemSelectTable()
            self.ChangeRewardComboBoxRewardSolid()
            self.ChangeRewardComboBoxRewardRandom()
        elif moduleName == self._conditionManager.GetModuleName():
            self._conditionManager.SetAnalysis(False)
            self._conditionManager.DataIsOK = True
            self.FillRewardConditionCombox()

    def FillRewardConditionCombox(self):
        allData = self._conditionManager.ConditionSheet.AllDatas
        if allData:
            self.comboBoxRewardLogicCon1.clear()
            self.comboBoxRewardLogicCon2.clear()
            for i, data in enumerate(allData):
                dMainId = data[self._conditionManager.ConditionSheet.MainKeyIndex]
                conditionDec = self._conditionManager.ConditionSheet.GetValue(i, "describe")
                self.comboBoxRewardLogicCon1.addItem("%s_%s" % (str(dMainId), conditionDec), dMainId)
                self.comboBoxRewardLogicCon2.addItem("%s_%s" % (str(dMainId), conditionDec), dMainId)

    def FillItemList(self, isSelectLast=False):
        allDatas = self._itemManager.GetAllData()
        nameDataIndex = self._itemManager.GetConfigColIndex("name")
        mainkKeyIndex = self._itemManager.GetMainKeyIndex()
        if nameDataIndex != -1:
            self.itemNames.clear()
            for data in allDatas:
                iName = "[%s]" % str(data[mainkKeyIndex]) + data[nameDataIndex]
                self.itemNames.append(iName)
                # self.comboBoxSelectItem.addItem(iName, data[mainkKeyIndex])
            slm = QStringListModel()
            slm.setStringList(self.itemNames)
            self.listViewDaoJu.setModel(slm)
            if isSelectLast:
                viewIndex = slm.index(len(self.itemNames) - 1)
                self.listViewDaoJu.setCurrentIndex(viewIndex)
                self._selectItemIndex = len(self.itemNames) - 1

    def ItemClick(self, qModelIndex):
        """选中物品"""
        # QMessageBox.information(self, 'ListWidget', '你选择了：' + self.itemNames[qModelIndex.row()])
        if self._selectItemIndex != qModelIndex.row():
            self._selectItemIndex = qModelIndex.row()
        else:
            pass
        self.FillItemInfo()

    def FillItemInfo(self):
        """填充物品信息"""
        if self._itemManager:
            iData = self._itemManager.GetData(self._selectItemIndex)
            if iData:
                # 填充文本
                self.FillItemEdit(self._itemManager, "name", self.lineEditItemName, iData)
                self.FillItemEdit(self._itemManager, "id", self.lineEditItemID, iData)
                self.FillItemEdit(self._itemManager, "sort1", self.lineEditItemSort1, iData)
                self.FillItemEdit(self._itemManager, "sort2", self.lineEditItemSort2, iData)
                self.FillItemEdit(self._itemManager, "decompose", self.lineEditItemDecompose, iData)
                self.FillItemEdit(self._itemManager, "exchange", self.lineEditItemSortExchange, iData)
                self.FillItemEdit(self._itemManager, "compose", self.lineEditItemCompose, iData)
                self.FillItemEdit(self._itemManager, "equipId", self.lineEditItemEquipId, iData)
                self.FillItemEdit(self._itemManager, "dragonId", self.lineEditItemDragonId, iData)
                self.FillItemEdit(self._itemManager, "islandId", self.lineEditItemIslandId, iData)
                self.FillItemEdit(self._itemManager, "towerId", self.lineEditItemTowerId, iData)
                self.FillItemEdit(self._itemManager, "period", self.lineEditItemPeriod, iData)
                self.FillItemEdit(self._itemManager, "guildId", self.lineEditItemGuildId, iData)
                # self.FillItemEdit(self._itemManager, "dropId", self.lineEditItemDrop, iData)
                self.FillItemEdit(self._itemManager, "page", self.lineEditItemPage, iData)
                self.FillItemEdit(self._itemManager, "icon1", self.lineEditItemIconRes1, iData)
                self.FillItemEdit(self._itemManager, "icon2", self.lineEditItemIconRes2, iData)
                self.FillItemEdit(self._itemManager, "icon3", self.lineEditItemIconRes3, iData)
                self.FillItemEdit(self._itemManager, "titleNum", self.lineEditItemtitleNum, iData)
                self.FillItemEdit(self._itemManager, "jump", self.lineEditItemJump, iData)
                self.FillItemEdit(self._itemManager, "decMini", self.lineEditDecMini, iData)
                self.FillItemEdit(self._itemManager, "dec", self.textEditItemDec, iData)
                self.FillItemEdit(self._itemManager, "stack", self.lineEditItemStack, iData)

                # 填充类型选项
                self.FillItemComboBox(self._itemManager, "type", self.comboBoxItemType, iData)
                self.FillItemComboBox(self._itemManager, "quality", self.comboBoxItemQuality, iData)

                # 红点
                self.FillCheckBox(self._itemManager, "redPoint", self.checkBoxRedPoint, iData, 0, 0)

                # 可选道具填充
                dataIndex = self._itemManager.GetConfigColIndex("selectItem")
                if dataIndex != -1:
                    vData = iData[dataIndex]
                    self.AnalysisSelectItemAndFill(vData)

    def AnalysisSelectItemAndFill(self, vData: str):
        # pass
        self.tableWidgetItemLift.setRowCount(0)
        if vData:
            try:
                items = vData.split(";")
                if items:
                    for i, item in enumerate(items):
                        dropInfo = item.split(",")
                        if len(dropInfo) == 4:
                            rowCount = self.tableWidgetItemLift.rowCount()
                            self.tableWidgetItemLift.insertRow(rowCount)
                            typeName = ""
                            if int(dropInfo[0]) == Const.RewardTypeResource:
                                typeName += Const.RewardResource.get(int(dropInfo[1]), "未知资源")
                            elif int(dropInfo[0]) == Const.RewardTypeItem:
                                typeName += self._itemManager.GetItemKeyData(int(dropInfo[1]), "name")
                            else:
                                continue
                            newItem = QTableWidgetItem(typeName)
                            newItem.setData(99, int(dropInfo[0]))
                            self.tableWidgetItemLift.setItem(rowCount, 0, newItem)
                            newItem0 = QTableWidgetItem(dropInfo[1])
                            self.tableWidgetItemLift.setItem(rowCount, 1, newItem0)
                            newItem2 = QTableWidgetItem(dropInfo[2])
                            self.tableWidgetItemLift.setItem(rowCount, 2, newItem2)
                            newItem3 = QTableWidgetItem(dropInfo[3])
                            self.tableWidgetItemLift.setItem(rowCount, 3, newItem3)
            except Exception as e:
                print(e)

    # def FillAddSelectItem(self, itemKindId, itemNum, dropId, row):
    #     if self._selectItemIndex:
    #         itemName = self._itemManager.GetItemKeyData(itemKindId, "name")
    #         itemKey = self._itemManager.GetItemKeyData(itemKindId, "id")
    #         # itemName = "[%s]%s" % (itemKey, itemName)
    #         if itemName:
    #             newItem = QTableWidgetItem(itemName)
    #             self.tableWidgetItemSelect.setItem(row, 0, newItem)
    #             newItem0 = QTableWidgetItem(itemKey)
    #             self.tableWidgetItemSelect.setItem(row, 1, newItem0)
    #             newItem2 = QTableWidgetItem(itemNum)
    #             self.tableWidgetItemSelect.setItem(row, 2, newItem2)
    #             newItem3 = QTableWidgetItem(dropId)
    #             self.tableWidgetItemSelect.setItem(row, 3, newItem3)

    def FillCheckBox(self, iManager, configKey, checkBox, iData, checkValue, defaulte):
        dataIndex = iManager.GetConfigColIndex(configKey)
        if dataIndex == -1:
            return
        vData = iData[dataIndex]
        if vData is None:
            vData = defaulte
        checkBox.setChecked(vData == checkValue)

    def FillItemComboBox(self, iManager, configKey, comboBox, iData, deviation=1):
        dataIndex = iManager.GetConfigColIndex(configKey)
        if dataIndex == -1:
            return
        vData = iData[dataIndex]
        if vData is None:
            vData = 1
        comboBox.setCurrentIndex(vData - deviation)

    def FillItemEdit(self, iManager, configKey, lineEdit, iData):
        # 开始填充数据
        dataIndex = iManager.GetConfigColIndex(configKey)
        if dataIndex == -1:
            return
        vData = iData[dataIndex]
        if vData is None:
            vData = ""
        lineEdit.setText(str(vData))

    def FillListVew(self, iManager, configKey, listView, iData, viewList):
        viewList.clear()
        dataIndex = iManager.GetConfigColIndex(configKey)
        if dataIndex == -1:
            return
        vData = iData[dataIndex]
        if vData is None:
            vData = ""
        vData = str(vData)
        if vData:
            allConditions = vData.split(",")
            for condition in allConditions:
                describe = self._conditionManager.ConditionSheet.GetValueByMainKey(int(condition), "describe")
                viewList.append("%s_%s" % (condition, describe))
            # vData = str(vData) + "_%s" % dec
            # viewList.extend(vData.split(","))
        slm = QStringListModel()
        slm.setStringList(viewList)
        listView.setModel(slm)

    def SaveItemChange(self):
        """保存道具修改到内存"""
        if self._itemManager and self._selectItemIndex >= 0:
            iData = self._itemManager.GetData(self._selectItemIndex)
            if iData:
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "name", self.lineEditItemName.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "id", self.lineEditItemID.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "sort1", self.lineEditItemSort1.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "sort2", self.lineEditItemSort2.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "decompose",
                                                    self.lineEditItemDecompose.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "exchange",
                                                    self.lineEditItemSortExchange.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "compose", self.lineEditItemCompose.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "equipId", self.lineEditItemEquipId.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "dragonId", self.lineEditItemDragonId.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "islandId", self.lineEditItemIslandId.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "towerId", self.lineEditItemTowerId.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "period", self.lineEditItemPeriod.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "guildId", self.lineEditItemGuildId.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "dropId",
                                                    self.comboBoxItemDrop.currentData())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "page", self.lineEditItemPage.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "icon1", self.lineEditItemIconRes1.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "icon2", self.lineEditItemIconRes2.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "icon3", self.lineEditItemIconRes3.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "titleNum", self.lineEditItemtitleNum.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "jump", self.lineEditItemJump.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "decMini", self.lineEditDecMini.text())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "dec", self.textEditItemDec.toPlainText())
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "stack", self.lineEditItemStack.text())

                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "type",
                                                    self.comboBoxItemType.currentIndex() + 1)
                self._itemManager.UpdateItemKeyData(self._selectItemIndex, "quality",
                                                    self.comboBoxItemQuality.currentIndex() + 1)

                if self.checkBoxRedPoint.isChecked():
                    self._itemManager.UpdateItemKeyData(self._selectItemIndex, "redPoint", 0)
                else:
                    self._itemManager.UpdateItemKeyData(self._selectItemIndex, "redPoint", 1)

                self.SaveSelectItemInfo()

    def SaveSelectItemInfo(self):
        """保存可选"""
        if self._selectItemIndex > -1:
            seletFormat = ""
            rCounts = self.tableWidgetItemLift.rowCount()
            for i in range(rCounts):
                qitem: QTableWidgetItem = self.tableWidgetItemLift.item(i, 0)
                typeData = qitem.data(99)
                valueId = self.tableWidgetItemLift.item(i, 1).text()
                nums = self.tableWidgetItemLift.item(i, 2).text()
                group = self.tableWidgetItemLift.item(i, 3).text()
                seletFormat += "%s,%s,%s,%s" % (typeData, valueId, nums, group)
                if i != rCounts - 1:
                    seletFormat += ";"
            self._itemManager.UpdateItemKeyData(self._selectItemIndex, "selectItem", seletFormat)

    def SaveItemToConfig(self):
        """保存道具到文件"""
        ret = self._itemManager.SaveItemData()
        if ret:
            QMessageBox.warning(self, '提示信息', '保存成功', QMessageBox.Yes)
        else:
            QMessageBox.warning(self, '提示信息', '保存失败', QMessageBox.Yes)

    def AddNewItem(self):
        """新增道具"""
        itemId = self.lineEditNewItemID.text()
        itemName = self.lineEditNewItemName.text()
        if itemName == "":
            QMessageBox.warning(self, '提示信息', '请输入道具名称', QMessageBox.Yes)
            return
        if itemId == "":
            QMessageBox.warning(self, '提示信息', '请输入道具ID', QMessageBox.Yes)
            return
        if self._itemManager.CheckNewItemID(itemId):
            QMessageBox.warning(self, '提示信息', '道具ID重复', QMessageBox.Yes)
            return
        if self._itemManager.CheckNewItemName(itemName):
            reply = QMessageBox.warning(self, '提示信息', '道具名称重复,确定要创建道具吗?',
                                        QMessageBox.Yes | QMessageBox.No)
            if reply == QMessageBox.No:
                return
        self._itemManager.AddNewItem(itemName, itemId)
        self.FillItemList(True)
        self.FillItemInfo()
        self.lineEditNewItemID.setText("")
        self.lineEditNewItemName.setText("")
        QMessageBox.warning(self, '提示信息', '新增成功', QMessageBox.Yes)

    def FillselTreeViewRewardLogic(self, groupidDefault=0, defauleLogicId=0):
        self._treeViewRewardModel.clear()
        self.comboBoxItemDrop.clear()
        self.comboBoxSelectDrop.clear()
        groupDatas = self._rewardManager.RewardGroups
        defaultIndex = -1
        i = 0
        for groupId, group in groupDatas.items():
            if groupId == groupidDefault:
                defaultIndex = 0
            groupName = self._rewardManager.RewardLogicSheeet.GetValueByMainKey(group[0], "name")
            root = self._treeViewRewardModel.invisibleRootItem()
            root.appendRow([QStandardItem("%s#%d" % (groupName, groupId))])
            if groupId not in self._rewardManager.TreeViewData.keys():
                self._rewardManager.TreeViewData[groupId] = dict()
            self._rewardManager.TreeViewData[groupId][-1] = root.rowCount() - 1
            parent = root.child(root.rowCount() - 1)
            parent.setData(groupId, 9999)
            self.comboBoxItemDrop.addItem("[%d]" % groupId + groupName, groupId)
            self.comboBoxSelectDrop.addItem("[%d]" % groupId + groupName, groupId)
            for rLogicId in group:
                parent.appendRow([QStandardItem("产出逻辑_%d" % rLogicId)])
                iRoot = parent.child(parent.rowCount() - 1)
                iRoot.setData(groupId * 10000000 + rLogicId, 9999)
                self._rewardManager.TreeViewData[groupId][root.rowCount() - 1] = rLogicId
            i += 1
        self.treeViewRewardLogic.expandAll()
        if groupidDefault:
            rowCounts = self._treeViewRewardModel.item(defaultIndex).rowCount()
            idx = self._treeViewRewardModel.indexFromItem(
                self._treeViewRewardModel.item(defaultIndex).child(rowCounts - 1))
            self.treeViewRewardLogic.setCurrentIndex(idx)
            self._selectRewardLogicId = defauleLogicId
            self.FillRewardLogic()

    def SelectDropChange(self, curIndex):
        group = self.comboBoxSelectDrop.currentData()
        self.FillItemSelectTable(group)

    def SelectConfigServerChange(self, curIndex):
        self.QueryServerConfigVer()

    def FillItemSelectTable(self, groupId=-1):
        if groupId == -1:
            groupId = self.comboBoxSelectDrop.itemData(0)
        self.tableWidgetItemLiftSource.setRowCount(0)
        # self.tableWidgetItemLiftSource.insertRow()
        allGroupLogicIds = self._rewardManager.RewardGroups.get(groupId, None)
        if allGroupLogicIds:
            for groupLogicId in allGroupLogicIds:
                rewardId = self._rewardManager.RewardLogicSheeet.GetValueByMainKey(groupLogicId, "rewardId")
                if rewardId:
                    silodDrop = self._rewardManager.RewardSheet.GetValueByMainKey(rewardId, "solidDrop")
                    if silodDrop:
                        drops = silodDrop.split(";")
                        for drop in drops:
                            dropInfo = drop.split(",")
                            if len(dropInfo) == 3:
                                rowCount = self.tableWidgetItemLiftSource.rowCount()
                                self.tableWidgetItemLiftSource.insertRow(rowCount)
                                typeName = ""
                                if int(dropInfo[0]) == Const.RewardTypeResource:
                                    typeName += Const.RewardResource.get(int(dropInfo[1]), "未知资源")
                                elif int(dropInfo[0]) == Const.RewardTypeItem:
                                    typeName += self._itemManager.GetItemKeyData(int(dropInfo[1]), "name")
                                else:
                                    continue
                                newItem = QTableWidgetItem(typeName)
                                newItem.setData(99, int(dropInfo[0]))
                                self.tableWidgetItemLiftSource.setItem(rowCount, 0, newItem)
                                newItem0 = QTableWidgetItem(dropInfo[1])
                                self.tableWidgetItemLiftSource.setItem(rowCount, 1, newItem0)
                                newItem2 = QTableWidgetItem(dropInfo[2])
                                self.tableWidgetItemLiftSource.setItem(rowCount, 2, newItem2)
                                newItem3 = QTableWidgetItem(str(groupId))
                                self.tableWidgetItemLiftSource.setItem(rowCount, 3, newItem3)

    def FillRewardListView(self, isSelectLast=False):
        allDatas = self._rewardManager.RewardSheet.AllDatas
        nameDataIndex = self._rewardManager.RewardSheet.GetConfigColIndex("name")
        mainkKeyIndex = self._rewardManager.RewardSheet.MainKeyIndex
        if nameDataIndex != -1:
            self.rewardNames.clear()
            for data in allDatas:
                name = data[nameDataIndex]
                if name is None:
                    name = ""
                iName = "[%s]" % str(data[mainkKeyIndex]) + name
                self.rewardNames.append(iName)
                self.comboBoxRewardLogicRID.addItem(iName, data[mainkKeyIndex])
            slm = QStringListModel()
            slm.setStringList(self.rewardNames)
            self.listViewReward.setModel(slm)
            if isSelectLast:
                viewIndex = slm.index(len(self.rewardNames) - 1)
                self.listViewReward.setCurrentIndex(viewIndex)
                self._rewardSelectIndex = len(self.rewardNames) - 1
                self.FillRewardInfo()

    def RewardListViewSelect(self, qModelIndex: QModelIndex):
        if self._rewardSelectIndex != qModelIndex.row():
            self._rewardSelectIndex = qModelIndex.row()
        else:
            pass
        self.FillRewardInfo()

    def FillRewardInfo(self):
        if self._rewardSelectIndex > -1:
            iData = self._rewardManager.RewardSheet.AllDatas[self._rewardSelectIndex]
            if iData:
                self.FillItemEdit(self._rewardManager.RewardSheet, "id", self.lineEditRewardId, iData)
                self.FillItemEdit(self._rewardManager.RewardSheet, "name", self.lineEditRewardName, iData)

                vDataRewardSolid = self._rewardManager.RewardSheet.GetValue(self._rewardSelectIndex, "solidDrop")
                self.AnalysisRewardSolidAndFill(vDataRewardSolid)

                vDataRewardRandom = self._rewardManager.RewardSheet.GetValue(self._rewardSelectIndex, "randomDrop")
                self.AnalysisRewardRandomAndFill(vDataRewardRandom)

    def AnalysisRewardSolidAndFill(self, vData: str):
        self.tableWidgetRewardSolid.setRowCount(0)
        if vData:
            try:
                items = vData.split(";")
                if items:
                    self.tableWidgetRewardSolid.setRowCount(len(items))
                    for i, item in enumerate(items):
                        itemInfo = item.split(",")
                        typeName = Const.RewardType.get(int(itemInfo[0]), "未知")
                        resourceName = ""
                        if int(itemInfo[0]) == Const.RewardTypeResource:
                            resourceName = Const.RewardResource.get(int(itemInfo[1]), "未知")
                        elif int(itemInfo[0]) == Const.RewardTypeItem:
                            if self._itemManager:
                                name = self._itemManager.GetItemKeyData(int(itemInfo[1]), "name")
                                if name:
                                    resourceName += "[%s]" % itemInfo[1] + name
                        newItem = QTableWidgetItem(typeName)
                        newItem.setData(99, int(itemInfo[0]))
                        self.tableWidgetRewardSolid.setItem(i, 0, newItem)
                        newItem0 = QTableWidgetItem(resourceName)
                        newItem0.setData(99, int(itemInfo[1]))
                        self.tableWidgetRewardSolid.setItem(i, 1, newItem0)
                        newItem2 = QTableWidgetItem(itemInfo[2])
                        self.tableWidgetRewardSolid.setItem(i, 2, newItem2)
            except Exception as e:
                print(e)

    def AnalysisRewardRandomAndFill(self, vData: str):
        self.tableWidgetRewardRandom.setRowCount(0)
        if vData:
            try:
                items = vData.split(";")
                if items:
                    self.tableWidgetRewardRandom.setRowCount(len(items))
                    for i, item in enumerate(items):
                        itemInfo = item.split(",")
                        typeName = Const.RewardType.get(int(itemInfo[0]), "未知")
                        resourceName = ""
                        if int(itemInfo[0]) == Const.RewardTypeResource:
                            resourceName = Const.RewardResource.get(int(itemInfo[1]), "未知")
                        elif int(itemInfo[0]) == Const.RewardTypeItem:
                            if self._itemManager:
                                name = self._itemManager.GetItemKeyData(int(itemInfo[1]), "name")
                                if name:
                                    resourceName += "[%s]" % itemInfo[1] + name
                        newItem = QTableWidgetItem(typeName)
                        newItem.setData(99, int(itemInfo[0]))
                        self.tableWidgetRewardRandom.setItem(i, 0, newItem)
                        newItem0 = QTableWidgetItem(resourceName)
                        newItem0.setData(99, int(itemInfo[1]))
                        self.tableWidgetRewardRandom.setItem(i, 1, newItem0)
                        newItem2 = QTableWidgetItem(itemInfo[2])
                        self.tableWidgetRewardRandom.setItem(i, 2, newItem2)
                        newItem3 = QTableWidgetItem(itemInfo[3])
                        self.tableWidgetRewardRandom.setItem(i, 3, newItem3)
            except Exception as e:
                print(e)

    def FillRewardLogic(self):
        if self._selectRewardLogicId > -1:
            iData = self._rewardManager.GetLogicData(self._selectRewardLogicId)
            if iData:
                self.FillItemEdit(self._rewardManager.RewardLogicSheeet, "id", self.lineEditRewardLogicId, iData)
                self.FillItemEdit(self._rewardManager.RewardLogicSheeet, "group", self.lineEditRewardLogicGroup, iData)
                self.FillItemEdit(self._rewardManager.RewardLogicSheeet, "name", self.lineEditRewardLogicName, iData)

                self.FillItemComboBox(self._rewardManager.RewardLogicSheeet, "weightType",
                                      self.comboBoxRewardLogicWType, iData, 0)
                self.FillItemEdit(self._rewardManager.RewardLogicSheeet, "weight", self.lineEditRewardWeight, iData)

                self.tableWidgetCondition1.setRowCount(0)
                self.tableWidgetCondition2.setRowCount(0)

                indexCondition1 = self._rewardManager.RewardLogicSheeet.GetConfigColIndex("condition1")
                if iData[indexCondition1]:
                    condition1 = str(iData[indexCondition1])
                    conditions1 = condition1.split(";")
                    for condition in conditions1:
                        describe = self._conditionManager.ConditionSheet.GetValueByMainKey(int(condition), "describe")
                        rowCount = self.tableWidgetCondition1.rowCount()
                        self.tableWidgetCondition1.insertRow(rowCount)
                        newItem = QTableWidgetItem(condition)
                        newItem.setData(99, int(condition))
                        self.tableWidgetCondition1.setItem(rowCount, 0, newItem)
                        newItem0 = QTableWidgetItem(describe)
                        self.tableWidgetCondition1.setItem(rowCount, 1, newItem0)

                indexCondition2 = self._rewardManager.RewardLogicSheeet.GetConfigColIndex("condition2")
                if iData[indexCondition2]:
                    condition2 = str(iData[indexCondition2])
                    conditions2 = condition2.split(";")
                    for condition in conditions2:
                        describe = self._conditionManager.ConditionSheet.GetValueByMainKey(int(condition), "describe")
                        rowCount = self.tableWidgetCondition2.rowCount()
                        self.tableWidgetCondition2.insertRow(rowCount)
                        newItem = QTableWidgetItem(condition)
                        newItem.setData(99, int(condition))
                        self.tableWidgetCondition2.setItem(rowCount, 0, newItem)
                        newItem0 = QTableWidgetItem(describe)
                        self.tableWidgetCondition2.setItem(rowCount, 1, newItem0)

    def RewardLogicTreeSelect(self, qModelIndex: QModelIndex):
        sData = qModelIndex.data(9999)
        if sData < 10000000:
            self._selectRewardLogicId = -1
            return
        rLogicId = sData % 10000000
        self._selectRewardLogicId = rLogicId
        # print(rLogicId)
        self.FillRewardLogic()

    def AddLiftSelectItems(self):
        if self._selectItemIndex == -1:
            QMessageBox.warning(self, '提示信息', '请先选中一个道具', QMessageBox.Yes)
            return
        row = self.tableWidgetItemLiftSource.currentIndex().row()
        if row == -1:
            QMessageBox.warning(self, '提示信息', '当前没有选中项', QMessageBox.Yes)
            return
        qitem: QTableWidgetItem = self.tableWidgetItemLiftSource.item(row, 0)
        typeName = qitem.text()
        typeData = qitem.data(99)
        valueId = self.tableWidgetItemLiftSource.item(row, 1).text()
        nums = self.tableWidgetItemLiftSource.item(row, 2).text()
        group = self.tableWidgetItemLiftSource.item(row, 3).text()
        rowCount = self.tableWidgetItemLift.rowCount()
        self.tableWidgetItemLift.insertRow(rowCount)
        newItem = QTableWidgetItem(typeName)
        newItem.setData(99, typeData)
        self.tableWidgetItemLift.setItem(rowCount, 0, newItem)
        newItem0 = QTableWidgetItem(valueId)
        self.tableWidgetItemLift.setItem(rowCount, 1, newItem0)
        newItem2 = QTableWidgetItem(nums)
        self.tableWidgetItemLift.setItem(rowCount, 2, newItem2)
        newItem3 = QTableWidgetItem(group)
        self.tableWidgetItemLift.setItem(rowCount, 3, newItem3)
        # print()

    def DelLiftSelectItems(self):
        if self._selectItemIndex == -1:
            QMessageBox.warning(self, '提示信息', '请先选中一个道具', QMessageBox.Yes)
            return
        row = self.tableWidgetItemLift.currentIndex().row()
        if row == -1:
            QMessageBox.warning(self, '提示信息', '当前没有选中项', QMessageBox.Yes)
            return
        ret = QMessageBox.warning(self, '提示信息', '是否确认删除当前选项', QMessageBox.Yes | QMessageBox.No)
        if ret == QMessageBox.Yes:
            self.tableWidgetItemLift.removeRow(row)

    def RewardLogic1Add(self):
        if self._selectRewardLogicId == -1:
            QMessageBox.warning(self, '提示信息', '请先选中一个逻辑', QMessageBox.Yes)
            return
        cData = self.comboBoxRewardLogicCon1.currentData()
        describe = self._conditionManager.ConditionSheet.GetValueByMainKey(cData, "describe")
        rowCount = self.tableWidgetCondition1.rowCount()
        self.tableWidgetCondition1.insertRow(rowCount)
        newItem = QTableWidgetItem(str(cData))
        newItem.setData(99, cData)
        self.tableWidgetCondition1.setItem(rowCount, 0, newItem)
        newItem0 = QTableWidgetItem(describe)
        self.tableWidgetCondition1.setItem(rowCount, 1, newItem0)

    def RewardLogic1Del(self):
        if self._selectRewardLogicId == -1:
            QMessageBox.warning(self, '提示信息', '请先选中一个逻辑', QMessageBox.Yes)
            return
        if self.tableWidgetCondition1.rowCount():
            row = self.tableWidgetCondition1.currentRow()
            if row == -1:
                QMessageBox.warning(self, '提示信息', '请先选中一个删除选项', QMessageBox.Yes)
                return
            self.tableWidgetCondition1.removeRow(row)

    def RewardLogic2Add(self):
        if self._selectRewardLogicId == -1:
            QMessageBox.warning(self, '提示信息', '请先选中一个逻辑', QMessageBox.Yes)
            return
        cData = self.comboBoxRewardLogicCon2.currentData()
        describe = self._conditionManager.ConditionSheet.GetValueByMainKey(cData, "describe")
        rowCount = self.tableWidgetCondition2.rowCount()
        self.tableWidgetCondition2.insertRow(rowCount)
        newItem = QTableWidgetItem(str(cData))
        newItem.setData(99, cData)
        self.tableWidgetCondition2.setItem(rowCount, 0, newItem)
        newItem0 = QTableWidgetItem(describe)
        self.tableWidgetCondition2.setItem(rowCount, 1, newItem0)

    def RewardLogic2Del(self):
        if self._selectRewardLogicId == -1:
            QMessageBox.warning(self, '提示信息', '请先选中一个逻辑', QMessageBox.Yes)
            return
        if self.tableWidgetCondition2.rowCount():
            row = self.tableWidgetCondition2.currentRow()
            if row == -1:
                QMessageBox.warning(self, '提示信息', '请先选中一个删除选项', QMessageBox.Yes)
                return
            self.tableWidgetCondition2.removeRow(row)

    def ChangeRewardComboBoxRewardSolid(self, curIndex=None):
        self.comboBoxRewardSolidId.clear()
        if curIndex is None:
            curIndex = self.comboBoxRewardSolidT.currentIndex()
        selectType = curIndex + 1
        if selectType == Const.RewardTypeResource:
            for reType, reDec in Const.RewardResource.items():
                self.comboBoxRewardSolidId.addItem(reDec, reType)
        elif selectType == Const.RewardTypeDragon:
            self.comboBoxRewardSolidId.addItem("战士龙", 10001)
            self.comboBoxRewardSolidId.addItem("猎人龙", 20001)
            self.comboBoxRewardSolidId.addItem("法师龙", 30001)
            self.comboBoxRewardSolidId.addItem("辅助龙", 40001)
            self.comboBoxRewardSolidId.addItem("赛季龙", 50001)
        else:
            allItemDatas = self._itemManager.GetAllData()
            for itemData in allItemDatas:
                mainKeyId = itemData[self._itemManager.GetMainKeyIndex()]
                nameIndex = self._itemManager.GetConfigColIndex("name")
                name = itemData[nameIndex]
                mergeName = "[%s]%s" % (str(mainKeyId), name)
                self.comboBoxRewardSolidId.addItem(mergeName, mainKeyId)

    def ChangeRewardComboBoxRewardRandom(self, curIndex=None):
        self.comboBoxRewardRandomId.clear()
        if curIndex is None:
            curIndex = self.comboBoxRewardSolidT.currentIndex()
        selectType = curIndex + 1
        if selectType == Const.RewardTypeResource:
            for reType, reDec in Const.RewardResource.items():
                self.comboBoxRewardRandomId.addItem(reDec, reType)
        elif selectType == Const.RewardTypeDragon:
            self.comboBoxRewardSolidId.addItem("战士龙", 10001)
            self.comboBoxRewardSolidId.addItem("猎人龙", 20001)
            self.comboBoxRewardSolidId.addItem("法师龙", 30001)
            self.comboBoxRewardSolidId.addItem("辅助龙", 40001)
            self.comboBoxRewardSolidId.addItem("赛季龙", 50001)
        else:
            allItemDatas = self._itemManager.GetAllData()
            for itemData in allItemDatas:
                mainKeyId = itemData[self._itemManager.GetMainKeyIndex()]
                nameIndex = self._itemManager.GetConfigColIndex("name")
                name = itemData[nameIndex]
                mergeName = "[%s]%s" % (str(mainKeyId), name)
                self.comboBoxRewardRandomId.addItem(mergeName, mainKeyId)

    def RewardSolidAdd(self):
        if self._rewardSelectIndex == -1:
            QMessageBox.warning(self, '提示信息', '当前没有选中项', QMessageBox.Yes)
            return
        counts = self.lineEditRewardSolidCounts.text()
        if counts == "":
            QMessageBox.warning(self, '提示信息', '请填写数量', QMessageBox.Yes)
            return
        selectType = self.comboBoxRewardSolidT.currentIndex() + 1
        reName = self.comboBoxRewardSolidId.currentText()
        reType = self.comboBoxRewardSolidId.currentData()
        if selectType == Const.RewardTypeResource:
            row = self.tableWidgetRewardSolid.rowCount()
            self.tableWidgetRewardSolid.insertRow(row)
            newItem = QTableWidgetItem("资源")
            newItem.setData(99, Const.RewardTypeResource)
            self.tableWidgetRewardSolid.setItem(row, 0, newItem)
            newItem0 = QTableWidgetItem(reName)
            newItem0.setData(99, reType)
            self.tableWidgetRewardSolid.setItem(row, 1, newItem0)
            newItem2 = QTableWidgetItem(counts)
            self.tableWidgetRewardSolid.setItem(row, 2, newItem2)
        elif selectType == Const.RewardTypeDragon:
            row = self.tableWidgetRewardSolid.rowCount()
            self.tableWidgetRewardSolid.insertRow(row)
            newItem = QTableWidgetItem("龙")
            newItem.setData(99, Const.RewardTypeDragon)
            self.tableWidgetRewardSolid.setItem(row, 0, newItem)
            newItem0 = QTableWidgetItem(reName)
            newItem0.setData(99, reType)
            self.tableWidgetRewardSolid.setItem(row, 1, newItem0)
            newItem2 = QTableWidgetItem(counts)
            self.tableWidgetRewardSolid.setItem(row, 2, newItem2)
        else:
            row = self.tableWidgetRewardSolid.rowCount()
            self.tableWidgetRewardSolid.insertRow(row)
            newItem = QTableWidgetItem("道具")
            newItem.setData(99, Const.RewardTypeItem)
            self.tableWidgetRewardSolid.setItem(row, 0, newItem)
            newItem0 = QTableWidgetItem(reName)
            newItem0.setData(99, reType)
            self.tableWidgetRewardSolid.setItem(row, 1, newItem0)
            newItem2 = QTableWidgetItem(counts)
            self.tableWidgetRewardSolid.setItem(row, 2, newItem2)

    def RewardSolidDel(self):
        if self._rewardSelectIndex == -1:
            QMessageBox.warning(self, '提示信息', '当前没有选中项', QMessageBox.Yes)
            return
        row = self.tableWidgetRewardSolid.currentRow()
        if row == -1:
            QMessageBox.warning(self, '提示信息', '请选中需要的删除项', QMessageBox.Yes)
            return
        self.tableWidgetRewardSolid.removeRow(row)

    def RewardRandomAdd(self):
        if self._rewardSelectIndex == -1:
            QMessageBox.warning(self, '提示信息', '当前没有选中项', QMessageBox.Yes)
            return
        counts = self.lineEditRewardRandomCounts.text()
        if counts == "":
            QMessageBox.warning(self, '提示信息', '请填写数量', QMessageBox.Yes)
            return
        weight = self.lineEditRewardRandomWeight.text()
        if weight == "":
            QMessageBox.warning(self, '提示信息', '请填写权值', QMessageBox.Yes)
            return
        selectType = self.comboBoxRewardRandomT.currentIndex() + 1
        reName = self.comboBoxRewardRandomId.currentText()
        reType = self.comboBoxRewardRandomId.currentData()
        if selectType == Const.RewardTypeResource:
            row = self.tableWidgetRewardRandom.rowCount()
            self.tableWidgetRewardRandom.insertRow(row)
            newItem = QTableWidgetItem("资源")
            newItem.setData(99, Const.RewardTypeResource)
            self.tableWidgetRewardRandom.setItem(row, 0, newItem)
            newItem0 = QTableWidgetItem(reName)
            newItem0.setData(99, reType)
            self.tableWidgetRewardRandom.setItem(row, 1, newItem0)
            newItem2 = QTableWidgetItem(counts)
            self.tableWidgetRewardRandom.setItem(row, 2, newItem2)
            newItem3 = QTableWidgetItem(weight)
            self.tableWidgetRewardRandom.setItem(row, 3, newItem3)
        elif selectType == Const.RewardTypeDragon:
            row = self.tableWidgetRewardSolid.rowCount()
            self.tableWidgetRewardSolid.insertRow(row)
            newItem = QTableWidgetItem("龙")
            newItem.setData(99, Const.RewardTypeDragon)
            self.tableWidgetRewardSolid.setItem(row, 0, newItem)
            newItem0 = QTableWidgetItem(reName)
            newItem0.setData(99, reType)
            self.tableWidgetRewardSolid.setItem(row, 1, newItem0)
            newItem2 = QTableWidgetItem(counts)
            self.tableWidgetRewardSolid.setItem(row, 2, newItem2)
        else:
            row = self.tableWidgetRewardRandom.rowCount()
            self.tableWidgetRewardRandom.insertRow(row)
            newItem = QTableWidgetItem("道具")
            newItem.setData(99, Const.RewardTypeItem)
            self.tableWidgetRewardRandom.setItem(row, 0, newItem)
            newItem0 = QTableWidgetItem(reName)
            newItem0.setData(99, reType)
            self.tableWidgetRewardRandom.setItem(row, 1, newItem0)
            newItem2 = QTableWidgetItem(counts)
            self.tableWidgetRewardRandom.setItem(row, 2, newItem2)
            newItem3 = QTableWidgetItem(weight)
            self.tableWidgetRewardRandom.setItem(row, 3, newItem3)

    def RewardRandomDel(self):
        if self._rewardSelectIndex == -1:
            QMessageBox.warning(self, '提示信息', '当前没有选中项', QMessageBox.Yes)
            return
        row = self.tableWidgetRewardRandom.currentRow()
        if row == -1:
            QMessageBox.warning(self, '提示信息', '请选中需要的删除项', QMessageBox.Yes)
            return
        self.tableWidgetRewardRandom.removeRow(row)

    def SaveRewardLogic(self):
        """保存奖励逻辑"""
        if self._selectRewardLogicId == -1:
            return
        self._rewardManager.RewardLogicSheeet.UpdateKeyDataByMainkey(self._selectRewardLogicId, "id",
                                                                     self.lineEditRewardLogicId.text())
        self._rewardManager.RewardLogicSheeet.UpdateKeyDataByMainkey(self._selectRewardLogicId, "group",
                                                                     self.lineEditRewardLogicGroup.text())
        self._rewardManager.RewardLogicSheeet.UpdateKeyDataByMainkey(self._selectRewardLogicId, "weightType",
                                                                     self.comboBoxRewardLogicWType.currentIndex())
        self._rewardManager.RewardLogicSheeet.UpdateKeyDataByMainkey(self._selectRewardLogicId, "weight",
                                                                     self.lineEditRewardWeight.text())
        changeName = self.lineEditRewardLogicName.text()
        oldName = self._rewardManager.RewardLogicSheeet.GetValueByMainKey(self._selectRewardLogicId, "name")
        if changeName != oldName:
            self._rewardManager.RewardLogicSheeet.UpdateKeyDataByMainkey(self._selectRewardLogicId, "name",
                                                                         self.lineEditRewardLogicName.text())
            # 名字要整组去改
            groups = self._rewardManager.RewardGroups
            for g in groups:
                self._rewardManager.RewardLogicSheeet.UpdateKeyDataByMainkey(g, "name",
                                                                             self.lineEditRewardLogicName.text())
        self._rewardManager.RewardLogicSheeet.UpdateKeyDataByMainkey(self._selectRewardLogicId, "rewardId",
                                                                     self.comboBoxRewardLogicRID.currentData())

        formatStr = ""
        rowCondition1 = self.tableWidgetCondition1.rowCount()
        if rowCondition1 > 0:
            for row in range(rowCondition1):
                cid = self.tableWidgetCondition1.item(row, 0)
                formatStr += "%s" % cid.text()
                if row != rowCondition1 - 1:
                    formatStr += ";"
        self._rewardManager.RewardLogicSheeet.UpdateKeyDataByMainkey(self._selectRewardLogicId, "condition1",
                                                                     formatStr)

        formatStr2 = ""
        rowCondition2 = self.tableWidgetCondition2.rowCount()
        if rowCondition2 > 0:
            for row in range(rowCondition2):
                cid = self.tableWidgetCondition2.item(row, 0)
                formatStr2 += "%s" % cid.text()
                if row != rowCondition2 - 1:
                    formatStr2 += ";"
        self._rewardManager.RewardLogicSheeet.UpdateKeyDataByMainkey(self._selectRewardLogicId, "condition2",
                                                                     formatStr2)
        self.FillselTreeViewRewardLogic()

    def SaveRewardInfo(self):
        """保存奖励信息"""
        if self._rewardSelectIndex == -1:
            return
        self._rewardManager.RewardSheet.UpdateKeyData(self._rewardSelectIndex, "id", self.lineEditRewardId.text())
        self._rewardManager.RewardSheet.UpdateKeyData(self._rewardSelectIndex, "name", self.lineEditRewardName.text())

        formatStr = ""
        solidRowCounts = self.tableWidgetRewardSolid.rowCount()
        if solidRowCounts > 0:
            for row in range(solidRowCounts):
                typeItem = self.tableWidgetRewardSolid.item(row, 0)
                typeId = typeItem.data(99)
                reTypeItem = self.tableWidgetRewardSolid.item(row, 1)
                reTypeId = reTypeItem.data(99)
                counts = self.tableWidgetRewardSolid.item(row, 2).text()
                formatStr += "%s,%s,%s" % (str(typeId), str(reTypeId), counts)
                if row != solidRowCounts - 1:
                    formatStr += ";"
        self._rewardManager.RewardSheet.UpdateKeyData(self._rewardSelectIndex, "solidDrop", formatStr)

        formatStr2 = ""
        randomRowCounts = self.tableWidgetRewardRandom.rowCount()
        if randomRowCounts > 0:
            for row in range(randomRowCounts):
                typeItem = self.tableWidgetRewardRandom.item(row, 0)
                typeId = typeItem.data(99)
                reTypeItem = self.tableWidgetRewardRandom.item(row, 1)
                reTypeId = reTypeItem.data(99)
                counts = self.tableWidgetRewardRandom.item(row, 2).text()
                weight = self.tableWidgetRewardRandom.item(row, 3).text()
                formatStr2 += "%s,%s,%s,%s" % (str(typeId), str(reTypeId), counts, weight)
                if row != randomRowCounts - 1:
                    formatStr2 += ";"
        self._rewardManager.RewardSheet.UpdateKeyData(self._rewardSelectIndex, "randomDrop", formatStr2)
        self.FillRewardListView()

    def SaveRewardFile(self):
        self._rewardManager.SaveSheets()
        ret = self._rewardManager.SaveData()
        if ret:
            QMessageBox.warning(self, '提示信息', '保存成功', QMessageBox.Yes)

    def NewRewardLogic(self):
        newDataGroup = self.lineEditRewardLogicGNew.text()
        if newDataGroup == "":
            QMessageBox.warning(self, '提示信息', '组ID不能为空', QMessageBox.Yes)
            return
        newDataLogicId = self.lineEditRewardLogicNew.text()
        if newDataLogicId == "":
            QMessageBox.warning(self, '提示信息', '主键ID不能为空', QMessageBox.Yes)
            return

        # 验证逻辑ID
        iNewDataLogicId = int(newDataLogicId)
        if iNewDataLogicId in self._rewardManager.RewardLogicSheeet.AllItems.keys():
            QMessageBox.warning(self, '提示信息', '主键ID重复', QMessageBox.Yes)
            return
        self._rewardManager.RewardLogicSheeet.AllDatas.append([None, None, None, None, None, None, None, None])
        newIndex = len(self._rewardManager.RewardLogicSheeet.AllDatas) - 1
        iNewDataGroup = int(newDataGroup)
        name = ""
        if iNewDataGroup not in self._rewardManager.RewardGroups.keys():
            self._rewardManager.RewardGroups[iNewDataGroup] = list()
            name = "产出组" + newDataGroup
        else:
            mainkey = self._rewardManager.RewardGroups[iNewDataGroup][0]
            name = self._rewardManager.RewardLogicSheeet.GetValueByMainKey(mainkey, "name")
        self._rewardManager.RewardGroups[iNewDataGroup].append(int(newDataLogicId))
        self._rewardManager.RewardLogicSheeet.AllItems[int(newDataLogicId)] = newIndex
        self._rewardManager.RewardLogicSheeet.UpdateKeyData(newIndex, "id", newDataLogicId)
        self._rewardManager.RewardLogicSheeet.UpdateKeyData(newIndex, "name", name)
        self._rewardManager.RewardLogicSheeet.UpdateKeyData(newIndex, "group", newDataGroup)
        self._rewardManager.RewardLogicSheeet.UpdateKeyData(newIndex, "weight", 0)
        self._rewardManager.RewardLogicSheeet.UpdateKeyData(newIndex, "weightType", 0)
        self.FillselTreeViewRewardLogic(iNewDataGroup, int(newDataLogicId))

        self.lineEditRewardLogicGNew.setText("")
        self.lineEditRewardLogicNew.setText("")
        QMessageBox.warning(self, '提示信息', '新增成功', QMessageBox.Yes)

    def NewReward(self):
        newDataId = self.lineEditNewRewardId.text()
        if newDataId == "":
            QMessageBox.warning(self, '提示信息', '主键ID不能为空', QMessageBox.Yes)
            return
        newDataName = self.lineEditNewRewardName.text()
        if newDataName == "":
            QMessageBox.warning(self, '提示信息', '名称描述不能为空', QMessageBox.Yes)
            return
        iNewDataId = int(newDataId)
        if iNewDataId in self._rewardManager.RewardSheet.AllItems.keys():
            QMessageBox.warning(self, '提示信息', '主键ID重复', QMessageBox.Yes)
            return
        self._rewardManager.RewardSheet.AllDatas.append([None, None, None, None, None, None])
        newIndex = len(self._rewardManager.RewardSheet.AllDatas) - 1
        self._rewardManager.RewardSheet.AllItems[int(newDataId)] = newIndex
        self._rewardManager.RewardSheet.UpdateKeyData(newIndex, "id", newDataId)
        self._rewardManager.RewardSheet.UpdateKeyData(newIndex, "name", newDataName)
        self.FillRewardListView(True)

        self.lineEditNewRewardId.setText("")
        self.lineEditNewRewardName.setText("")
        QMessageBox.warning(self, '提示信息', '新增成功', QMessageBox.Yes)

    def OpenTranslateOutPath(self):
        pathName = QFileDialog.getExistingDirectory(self, '选择翻译配置导出目录', '/')
        if pathName:
            self._cnf.updateConfig("translatePath", pathName)
            self.label_configPath_transOut.setText(pathName)

    def CreateTransTempleteConfig(self):
        """生成翻译模板文件"""
        # 有没有源地址
        if self.label_configPath.text() == "":
            QMessageBox.warning(self, '提示信息', '请选择配置目录', QMessageBox.Yes)
            return
        if self.label_configPath_transOut.text() == "":
            QMessageBox.warning(self, '提示信息', '请选导出目录', QMessageBox.Yes)
            return
        outPutFiles = list()
        outPutFileNames = list()
        # 判断需要导出的文件
        length = len(self._allConfigFiles)
        for index in range(length):
            if self.tableWidget_Config.item(index, 1).checkState() == Qt.Checked:
                outPutFiles.append(self._allConfigFiles[index])
                outPutFileNames.append(self._allConfigFileNames[index])
        if len(outPutFiles) == 0:
            QMessageBox.warning(self, '提示信息', '当前没有选中任何文件', QMessageBox.Yes)
            return
        translateTypes = list()
        # 判断要导出的类型
        if self.checkBoxTransEN.isChecked():
            translateTypes.append(Const.TranslateType_En)
        if self.checkBoxTransPT.isChecked():
            translateTypes.append(Const.TranslateType_Pt)
        if len(translateTypes) == 0:
            QMessageBox.warning(self, '提示信息', '当前没有选中任何导出语言', QMessageBox.Yes)
            return
        isClearAll = self.checkBoxTransClear.isChecked()
        if isClearAll:
            ret = QMessageBox.warning(self, '提示信息', '此次操作会重新生成翻译文件,是否继续',
                                      QMessageBox.Yes | QMessageBox.No)
            if ret == QMessageBox.No:
                return
        self.ShowLog("开始导出")
        self._workThreadTrans = OutPutTranslateThread(outPutFiles, "translate", self.label_configPath_transOut.text(),
                                                      translateTypes, isClearAll, self)
        self._workThreadTrans.finished.connect(self.TransWorkFinished)
        self._workThreadTrans.start()

    def TranslateItemClicked(self, qModelIndex):
        if self._selectTranslatPathIndex != qModelIndex.row():
            self._selectTranslatPathIndex = qModelIndex.row()

    def AddTranslatePath(self):
        pathName = QFileDialog.getExistingDirectory(self, '选择翻译配置导出目录', '/')
        if pathName:
            self.tableWidget_ConfigTranslta.setRowCount(self.tableWidget_ConfigTranslta.rowCount() + 1)
            newItem = QTableWidgetItem(pathName)
            self.tableWidget_ConfigTranslta.setItem(0, 0, newItem)

    def DelTranslatePath(self):
        if self._selectTranslatPathIndex != -1:
            self.tableWidget_ConfigTranslta.removeRow(self._selectTranslatPathIndex)
            self._selectTranslatPathIndex = -1
        else:
            QMessageBox.warning(self, '提示信息', '当前没有选中条目', QMessageBox.Yes)

    def SaveConfigSignName(self):
        if len(self._configSignName) > 0:
            return
        if self.lineEdit_ConfigSign.text() == "请输入发布人" or len(self.lineEdit_ConfigSign.text()) == 0:
            QMessageBox.warning(self, '提示信息', '请输入有效发布人', QMessageBox.Yes)
            return
        self._configSignName = self.lineEdit_ConfigSign.text()
        self._cnf.updateConfig("configSignName", self._configSignName)
        self.lineEdit_ConfigSign.setDisabled(True)
        QMessageBox.warning(self, '提示信息', '保存成功', QMessageBox.Yes)

    def TestSVN(self):
        r = svn.local.LocalClient(self.label_clientOutPath.text())
        try:
            r.info()
            # info =
            # pprint.pprint(info)
            self.ShowLog("客户端配置输出路径可用")
        except Exception as e:
            self.ShowLog("您指定的客户端配置导出路径不是SVN路径,将无法正常完成客户端配置上传", 1)

    def TestCheckOutSVN(self):
        r = svn.remote.RemoteClient(
            r"https://svn-trans-forward.gameark.cn:9999/svn/LegendDragons/project/trunk/Excel/C-%E7%AD%96%E5%88%92%E9%85%8D%E7%BD%AE%E8%A1%A8")
        re = r.checkout(r'excelConfig')
        pprint.pprint(re)

    def makeResourceDir(self):
        if not Utils.CheckFilePath(r'resource'):
            os.mkdir(r'resource')
        if not Utils.CheckFilePath(Const.LocalServerOutPutZipPath):
            os.mkdir(Const.LocalServerOutPutZipPath)
        if not Utils.CheckFilePath(Const.LocalServerOutPutZipPathRelease):
            os.mkdir(Const.LocalServerOutPutZipPathRelease)

    def CheckOutConfigSvn(self):
        if Utils.CheckFilePath(Const.LoaclSvnExcelPath):
            return
        self.StartCheckOutConfig()

    def CheckOutClientConfigSvn(self):
        if Utils.CheckFilePath(Const.LocalSvnClientConfigsPath):
            return
        self.StartCheckOutClientConfig()

    def CheckOutServerConfigSvn(self):
        if Utils.CheckFilePath(Const.LocalServerOutPutPath):
            return
        self.StartCheckOutServerConfig()

    def CheckOutConfigSvnRelease(self):
        if Utils.CheckFilePath(Const.LoaclSvnExcelPathRelease):
            return
        self.StartCheckOutConfigRelease()

    def CheckOutClientConfigSvnRelease(self):
        if Utils.CheckFilePath(Const.LocalSvnClientConfigsPathRelease):
            return
        self.StartCheckOutClientConfigRelease()

    def CheckOutServerConfigSvnRelease(self):
        if Utils.CheckFilePath(Const.LocalServerOutPutPathRelease):
            return
        self.StartCheckOutServerConfigRelease()

    def StartCheckOutConfig(self):
        self.ShowLog("开始SVN检出数据表")
        t = SvnExcelConfigThread(1, Const.ConfigSvn, Const.LoaclSvnExcelPath, "检出数据表成功", "检出数据表失败", self)
        t.setAutoDelete(True)
        self._taskThreads.start(t)

    def StartCheckOutClientConfig(self):
        self.ShowLog("开始SVN检出客户端LUA配置")
        t = SvnExcelConfigThread(1, Const.ClientConfigSvn, Const.LocalSvnClientConfigsPath, "检出客户端配置成功",
                                 "检出客户端配置失败", self)
        t.setAutoDelete(True)
        self._taskThreads.start(t)

    def StartCheckOutServerConfig(self):
        self.ShowLog("开始SVN检服务器LUA配置")
        t = SvnExcelConfigThread(1, Const.ServerConfigSvn, Const.LocalServerOutPutPath, "检出服务器配置成功",
                                 "检出服务器配置失败", self)
        t.setAutoDelete(True)
        self._taskThreads.start(t)

    def StartCheckOutConfigRelease(self):
        self.ShowLog("开始SVN检出稳定服数据表")
        t = SvnExcelConfigThread(1, Const.ConfigSvnRelease, Const.LoaclSvnExcelPathRelease, "检出数据表成功",
                                 "检出数据表失败", self)
        t.setAutoDelete(True)
        self._taskThreads.start(t)

    def StartCheckOutClientConfigRelease(self):
        self.ShowLog("开始SVN检出客户端稳定服LUA配置")
        t = SvnExcelConfigThread(1, Const.ClientConfigSvnRelease, Const.LocalSvnClientConfigsPathRelease,
                                 "检出客户端配置成功", "检出客户端配置失败", self)
        t.setAutoDelete(True)
        self._taskThreads.start(t)

    def StartCheckOutServerConfigRelease(self):
        self.ShowLog("开始SVN检服务器稳定服LUA配置")
        t = SvnExcelConfigThread(1, Const.ServerConfigSvnRelease, Const.LocalServerOutPutPathRelease,
                                 "检出服务器配置成功", "检出服务器配置失败", self)
        t.setAutoDelete(True)
        self._taskThreads.start(t)

    def ReleaseAllConfigs(self):
        if self._workReleseConfig:
            QMessageBox.warning(self, '提示信息', '正在全量发布中,请等待本次操作结束后再试', QMessageBox.Ok)
            return
        if self.label_configPathClientGuanQia.text() == "":
            retA = QMessageBox.warning(self, '提示信息',
                                       '没有配置客户端关卡配置文件所在路径,本次全量发布将不会更新客户端关卡配置文件,是否继续',
                                       QMessageBox.Yes | QMessageBox.No)
            if retA == QMessageBox.No:
                return

        if self.label_configPathServerGuanQia.text() == "":
            retB = QMessageBox.warning(self, '提示信息',
                                       '没有配置服务器关卡配置文件所在路径,本次全量发布将不会更新服务器关卡配置文件,是否继续',
                                       QMessageBox.Yes | QMessageBox.No)
            if retB == QMessageBox.No:
                return

        ret = QMessageBox.warning(self, '提示信息', '此次操作会发布所有SVN配置到服务器[%s],此操作不可逆,是否继续' %
                                  "一般测试服1(s9915)", QMessageBox.Yes | QMessageBox.No)
        if ret == QMessageBox.No:
            return
        t = ReleaseConfigThread(self._serverManager.defaulteTestIndex, "一般测试服1(s9915)", "开发服发布配置成功",
                                "开发服发布配置失败", True, self)
        t.setAutoDelete(True)
        self._taskThreads.start(t)
        self._workReleseConfig = True

    def ReleaseAllConfigsWenDing(self):
        if self._workReleseConfig:
            QMessageBox.warning(self, '提示信息', '正在全量发布中,请等待本次操作结束后再试', QMessageBox.Ok)
            return
        if self.label_configPathClientGuanQia.text() == "":
            retA = QMessageBox.warning(self, '提示信息',
                                       '没有配置客户端关卡配置文件所在路径,本次全量发布将不会更新客户端关卡配置文件,是否继续',
                                       QMessageBox.Yes | QMessageBox.No)
            if retA == QMessageBox.No:
                return

        if self.label_configPathServerGuanQia.text() == "":
            retB = QMessageBox.warning(self, '提示信息',
                                       '没有配置服务器关卡配置文件所在路径,本次全量发布将不会更新服务器关卡配置文件,是否继续',
                                       QMessageBox.Yes | QMessageBox.No)
            if retB == QMessageBox.No:
                return

        ret = QMessageBox.warning(self, '提示信息',
                                  '此次操作会发布所有稳定服SVN配置表到稳定服相关LUA配置SVN,不会发布到服务器,是否继续',
                                  QMessageBox.Yes | QMessageBox.No)
        if ret == QMessageBox.No:
            return
        t = ReleaseConfigThread(self.comboBox_Servers.currentIndex(), self.comboBox_Servers.currentText(),
                                "稳定服发布配置成功",
                                "稳定服发布配置失败", False, self)
        t.setAutoDelete(True)
        self._taskThreads.start(t)
        self._workReleseConfig = True

    @property
    def configSignName(self):
        return self._configSignName

    @property
    def serverManager(self):
        return self._serverManager

    def StartCheckConfig(self):
        if self._workCheckConfig:
            QMessageBox.warning(self, '提示信息', '正在检测配置表,请等待本次操作结束后再试', QMessageBox.Ok)
            return
        self.ShowLog("开始对数据表配置进行检测")
        t = CheckConfigThread(self._allConfigFiles, self)
        t.setAutoDelete(True)
        self._taskThreads.start(t)
        self._workCheckConfig = True
