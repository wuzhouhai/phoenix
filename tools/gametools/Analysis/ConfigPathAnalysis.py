# -*- coding: utf-8 -*-
"""
@File       :  ConfigPathAnalysis
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/1 14:32
@Description:  
"""
from Common.Utils import Utils


class ConfigPathAnalysis:
    """找出所有的配置文件"""

    @staticmethod
    def FindAllConfigExcels(path):
        allFiles, allFileNames = Utils.SearchFiles(path, 'xlsx')
        # print(allFile)
        return allFiles, allFileNames
