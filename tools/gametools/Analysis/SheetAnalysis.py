# -*- coding: utf-8 -*-
"""
@File       :  SheetAnlysis
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/15 18:00
@Description:  表一个sheet的分析数据
"""
from PyQt5.QtCore import *
from Common import ConfigTypes


class SheetAnalysis(QObject):

    def __init__(self, sheetName):
        super(SheetAnalysis, self).__init__()
        self._decList = list()
        self._typeList = list()
        self._feildList = list()
        self._allDatas = list()
        self._mainKeyIndex = -1
        self._allItems = dict()  # 配置ID到_allDatas列表的索引
        self._idAndNames = dict()
        self._sheet = None
        self._updateList = list()
        self._sheetName = sheetName

    @property
    def DecList(self):
        return self._decList

    @DecList.setter
    def DecList(self, v):
        self._decList = v

    @property
    def TypeList(self):
        return self._typeList

    @TypeList.setter
    def TypeList(self, v):
        self._typeList = v

    @property
    def FeildList(self):
        return self._feildList

    @FeildList.setter
    def FeildList(self, v):
        self._feildList = v

    @property
    def AllDatas(self):
        return self._allDatas

    @AllDatas.setter
    def AllDatas(self, v):
        self._allDatas = v

    @property
    def MainKeyIndex(self):
        return self._mainKeyIndex

    @MainKeyIndex.setter
    def MainKeyIndex(self, v):
        self._mainKeyIndex = v

    @property
    def AllItems(self):
        return self._allItems

    @AllItems.setter
    def AllItems(self, v):
        self._allItems = v

    @property
    def IDAndNames(self):
        return self._idAndNames

    @IDAndNames.setter
    def IDAndNames(self, v):
        self._idAndNames = v

    @property
    def Sheet(self):
        return self._sheet

    @Sheet.setter
    def Sheet(self, v):
        self._sheet = v

    @property
    def UpdateList(self):
        return self._updateList

    @UpdateList.setter
    def UpdateList(self, v):
        self._updateList = v

    @property
    def SheetName(self):
        return self._sheetName

    @SheetName.setter
    def SheetName(self, v):
        self._sheetName = v

    def GetData(self, index):
        if index < len(self.AllDatas):
            return self.AllDatas[index]
        return None

    def GetMainKeyIndex(self):
        return self.MainKeyIndex

    def GetConfigColIndex(self, feildKey):
        """获取列名索引"""
        try:
            return self.FeildList.index(feildKey)
        except Exception as e:
            return -1

    def GetValueType(self, tIndex):
        if tIndex < len(self.TypeList):
            return self.TypeList[tIndex]["iType"]
        return None

    def GetValue(self, iIndex, feildKey):
        data = self.GetData(iIndex)
        if data:
            i = self.GetConfigColIndex(feildKey)
            if i != -1:
                return data[i]
        return None

    def CheckMainKey(self, mainKey):
        iIndex = self.AllItems.get(mainKey, None)
        if iIndex is not None:
            return True
        return False

    def GetValueByMainKey(self, mainKey, feildKey):
        iIndex = self.AllItems.get(mainKey, None)
        if iIndex is not None:
            return self.GetValue(iIndex, feildKey)
        return None

    def UpdateKeyDataByMainkey(self, mainKey, key, value):
        iIndex = self.AllItems.get(mainKey, None)
        if iIndex is None:
            return
        self.UpdateKeyData(iIndex, key, value)

    def UpdateKeyData(self, selectIndex, key, value):
        itemData = self.AllDatas[selectIndex]
        vIndex = self.GetConfigColIndex(key)
        if itemData and vIndex > -1:
            # if not isNew and itemData[vIndex] is None and value == '' or value == 0:
            #     return
            if self.GetValueType(vIndex) == ConfigTypes.Number:
                try:
                    if value == '':
                        value = 0
                    value = int(value)
                except Exception as e:
                    print(e)
                    return
            if itemData[vIndex] == value:
                return
            itemData[vIndex] = value
            self.AddUpdateList(selectIndex, key, value)

    def AddUpdateList(self, selectIndex, key, value):
        """增加变更记录"""
        dataRow = selectIndex + 4
        dataCel = self.GetConfigColIndex(key) + 1
        self.UpdateList.append({"dataRow": dataRow, "dataCel": dataCel, "value": value})
