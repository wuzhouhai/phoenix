#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：pythonProjects 
@File    ：OutPutTranslateThread.py
@IDE     ：PyCharm 
@Author  ：zhouhai.wu@gameark.cn
@Date    ：2021/12/1 17:53 
"""

from PyQt5 import QtCore
from PyQt5.QtCore import *
from Excel.ExcelHelper import ExcelHelper
from Common import Const


class OutPutTranslateThread(QtCore.QThread):
    """翻译文件导出类"""
    WorkFinishSignal = pyqtSignal()

    def __init__(self, files, outFile, outPath, translateTypes, isClearAll, uiCls):
        super(OutPutTranslateThread, self).__init__()
        self._files = files
        self._uiCls = uiCls
        self._outFile = outPath + "/" + outFile
        self._isClearAll = isClearAll
        self._translateTypes = translateTypes
        self._translatSourceList = list()   # 所有需要翻译的文字

    def run(self) -> None:
        """
        """
        print("翻译导出线程开始运行")
        for configFile in self._files:
            self.AnalysisConfigExcel(configFile)
        for translateType in self._translateTypes:
            if translateType in Const.TranslateType.keys():
                outFile = self._outFile + "_" + Const.TranslateType[translateType]["fileExt"] + ".xlsx"
                self.OutPutTranslateConfigFile(outFile, Const.TranslateType[translateType]["transColName"])
        self.WorkFinishSignal.emit()
        print("翻译导出线程结束运行")

    def AnalysisConfigExcel(self, configExcelFile) -> None:
        """
        分析单个表需要翻译的内容
        :param configExcelFile:
        """
        try:
            configFileExl = ExcelHelper(configExcelFile)
            sheetNames = configFileExl.GetAllSheetNames()
            for sheetName in sheetNames:
                if sheetName.find("@") != -1:
                    translatSourceFeildIndex = list()
                    sheet = configFileExl.GetSheetByName(sheetName)
                    if sheet:
                        sName = sheetName.lstrip('@')
                        sheetAnalysis = ExcelHelper.AnalysisConfig(sheet)
                        # 遍历类型,看那些字段是需要翻译的
                        for i, typeInfo in enumerate(sheetAnalysis["typeList"]):
                            if typeInfo["trans"] == 1 and typeInfo["isString"] == 1:
                                translatSourceFeildIndex.append(i)
                        # 遍历字段生成需要翻译的数据
                        for configData in sheetAnalysis["allDatas"]:
                            for i in translatSourceFeildIndex:
                                if configData[i] not in self._translatSourceList:
                                    self._translatSourceList.append(configData[i])
        except Exception as e:
            print(e)

    def OutPutTranslateConfigFile(self, outFile: str, transColName: str) -> None:
        """
        导出需要翻译的文件
        :return:
        """
        try:
            outTranslateExl = ExcelHelper(outFile)
            ws = outTranslateExl.GetFirstSheet()
            inOutTranslateList = list()
            outPutInfoList = list()
            if self._isClearAll:
                outTranslateExl.ClearSheetAll(ws)
                outPutInfoList.append({"dataRow": 1, "dataCel": 1, "value": "Translate"})
                outPutInfoList.append({"dataRow": 1, "dataCel": 2, "value": "English"})
                if transColName is not None:
                    outPutInfoList.append({"dataRow": 1, "dataCel": 3, "value": transColName})
            else:
                # 遍历获取所有翻译源数据
                for index, row in enumerate(ws.rows):
                    if index > 0:
                        inOutTranslateList.append(row[0].value)
            startRow = ws.max_row + 1
            for i, translateInfo in enumerate(self._translatSourceList):
                if translateInfo in inOutTranslateList:
                    continue
                outPutInfoList.append({"dataRow": startRow + i, "dataCel": 1, "value": translateInfo})
            for updateData in outPutInfoList:
                outTranslateExl.UpdateValue(updateData)
            outTranslateExl.SaveExcel()
        except Exception as e:
            print(e)

