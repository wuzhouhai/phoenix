# -*- coding: utf-8 -*-
"""
@File       :  ExcelHelper
@Author     :  wuzhouhai
@Email      :  zhouhai.wu@gameark.cn
@Time       :  2021/7/1 13:48
@Description:  
"""
import openpyxl
import os
from Common import Const


class ExcelHelper:

    @staticmethod
    def AnalysisConfig(sheet):
        ret = {"decList": list(), "typeList": list(), "feildList": list(), "allDatas": list(), "mainKeyIndex": -1,
               "dictDatas": dict(), "customTypeList": list(), "errs": list()}
        try:
            mainKeyList = list()
            index = 0
            # 读取数据逐行
            for row in sheet.rows:
                if index == 0:  # 注释行
                    for i, cell in enumerate(row):
                        if cell.value is None:
                            cell.value = ""
                        ret["decList"].append(cell.value)
                elif index == 1:
                    # 读取类型数据
                    for i, cell in enumerate(row):
                        tInfo = {"notNull": 0, "iType": "", "trans": 0, "ignore": 0,
                                 "customType": None, "rule": None}  # 是不是允许空,如果允许空的,会自动填充对应类型导出
                        # 检测是不是主key
                        if type(cell.value) is str:
                            if cell.value.find("~") != -1 or cell.value == "":
                                tInfo["ignore"] = 1
                            else:
                                if cell.value.find("@") != -1:
                                    customTInfo = cell.value.split("@")
                                    if len(customTInfo) > 1:
                                        tInfo["customType"] = customTInfo[1]
                                    valueStr = customTInfo[0]
                                else:
                                    valueStr = cell.value
                                # 是否有验证规则
                                if valueStr.find("#") != -1:
                                    valueInfo = valueStr.split("#")
                                    if len(valueInfo) > 1:
                                        rule = valueInfo[1]
                                        # 尝试转换整形,能转换的话说明是rule表里面的,不能的话,就代表是表名
                                        try:
                                            ruleValue = int(rule)
                                            tInfo["rule"] = {"ruleType": Const.CheckRuleTypeInConfig, "ruleValue": ruleValue}
                                        except:
                                            tInfo["rule"] = {"ruleType": Const.CheckRuleTypeTable, "ruleValue": rule}
                                    valueStr = valueInfo[0]

                                typeInfo = valueStr.split("_")
                                if len(typeInfo) == 2:
                                    # 找到主键
                                    if typeInfo[1].find("!") != -1:
                                        ret["mainKeyIndex"] = i
                                    # 找到必填标志
                                    if typeInfo[1].find(".") != -1:
                                        tInfo["notNull"] = 1
                                    # 此字段是不是需要翻译
                                    if typeInfo[1].find("$") != -1:
                                        tInfo["trans"] = 1
                                if typeInfo[0] not in Const.AllRowTypes:
                                    tInfo["ignore"] = 1
                                else:
                                    tInfo["iType"] = typeInfo[0]
                        else:
                            tInfo["ignore"] = 1
                        ret["typeList"].append(tInfo)
                elif index == 2:
                    # 添加字段
                    for i, cell in enumerate(row):
                        if cell.value is None:
                            cell.value = "undefine_" + str(i)
                        ret["feildList"].append(cell.value)
                else:  # 处理数据
                    if ret["mainKeyIndex"] > -1 and row[ret["mainKeyIndex"]].value:
                        if row[ret["mainKeyIndex"]].value not in mainKeyList:
                            mainKeyList.append(row[ret["mainKeyIndex"]].value)
                        else:
                            ret["errs"].append("%s 主键重复" % str(row[ret["mainKeyIndex"]].value))
                            continue
                        data = []
                        for i, cell in enumerate(row):
                            cellValue = cell.value
                            # 把所有数据存起来
                            if cellValue is None:
                                if ret["typeList"][i]["iType"] == Const.RowType_Number:
                                    cellValue = 0
                                else:
                                    cellValue = ""
                            if ret["typeList"][i]["iType"] == Const.RowType_String or ret["typeList"][i][
                                "iType"] == Const.RowType_StringArray or ret["typeList"][i][
                                "iType"] == Const.RowType_StringArray2:
                                if type(cellValue) is not str:
                                    cellValue = str(cellValue)
                                if cellValue.find('"') != -1:
                                    ret["errs"].append(
                                        "%s 配置错误, 字符串 [%s] 不能含有双引号" % (str(row[ret["mainKeyIndex"]].value), cellValue))
                                    continue
                            if ret["typeList"][i]["iType"] == Const.RowType_Number:
                                try:
                                    int(cellValue)
                                except Exception as e:
                                    ret["errs"].append("%s 配置类型是整形, 字符串 [%s] 不能为字符, 该配置已被修改为默认值0" % (
                                        str(row[ret["mainKeyIndex"]].value), cellValue))
                                    cellValue = 0
                            data.append(cellValue)
                        ret["allDatas"].append(data)
                        ret["dictDatas"][row[ret["mainKeyIndex"]].value] = len(ret["allDatas"]) - 1
                index += 1
        except Exception as e:
            print("数据表分析出错 表:[%s], 异常:[%s]" % (sheet.title, e))
        return ret

    def __init__(self, cnfFilePath):
        self._cnfFilePath = cnfFilePath
        self.book = None
        if os.path.exists(cnfFilePath):
            self.book = openpyxl.load_workbook(cnfFilePath)  # 打开文件
        else:
            self.book = openpyxl.Workbook()

    def Close(self):
        if self.book:
            self.book.close()

    def GetFirstSheet(self):
        """拿第一张表"""
        sheetNames = self.book.get_sheet_names()
        ws = self.book.get_sheet_by_name(sheetNames[0])
        return ws

    def GetAllSheetNames(self):
        sheetNames = self.book.get_sheet_names()
        return sheetNames

    def GetSheetByName(self, sheetName):
        """通过sheet名称获取sheet"""
        ws = self.book.get_sheet_by_name(sheetName)
        return ws

    def ClearSheet(self, sheet):
        """删除sheet数据"""
        sheet.delete_rows(4, sheet.max_row)

    def ClearSheetAll(self, sheet):
        """删除所有数据"""
        sheet.delete_rows(0, sheet.max_row)
        sheet.delete_cols(1, sheet.max_column)

    def UpdateValue(self, updateInfo, sheet=None):
        """更新"""
        if not sheet:
            sheet = self.GetFirstSheet()
        sheet.cell(updateInfo["dataRow"], updateInfo["dataCel"]).value = updateInfo["value"]

    def SaveExcel(self):
        """保存文件"""
        try:
            if self.book:
                self.book.save(self._cnfFilePath)
                return True
        except:
            return False
