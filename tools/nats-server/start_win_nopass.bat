chcp 65001
@echo off
::获取当前路径
set currentPath=%~dp0
echo %currentPath%
set natsExe=%currentPath%windows/nats-server.exe
echo %natsExe%
::判断程序是否存在
if not exist %natsExe% (
    echo nats-server.exe not found
    exit 1
    )
echo 启动nats_server
::判断程序是否运行,不存在就允许
tasklist | find /i "nats-server.exe" && taskkill /im nats-server.exe /f || start /b %natsExe%
