#!/bin/bash
currentPath=$(pwd)
echo "${currentPath}"
natsExe=${currentPath}/linux/nats-server
echo "${natsExe}"
#判断程序是否存在
if [ ! -f "${natsExe}" ]; then
    echo "nats-server not exist"
    exit 1
fi
#判断程序是否运行,没有运行就杀死程序
if [ -f ${currentPath}/linux/nats-server.pid ]; then
    # shellcheck disable=SC2046
    kill -SIGTERM $(cat ${currentPath}/linux/nats-server.pid)
fi
#启动程序 将日志输出到log文件中
nohup ${natsExe} -P ${currentPath}/linux/nats-server.pid > ${currentPath}/../../logs/nats-server.log 2>&1 &


