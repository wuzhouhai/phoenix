@echo off
set local_path=%CD%
set app_path=%local_path%\..\..\apps\examples\phoenix_app_dwys
set protoc_path=%local_path%\..\protobuf\protoc-25.1-win64\bin
set proto_path=%local_path%\..\..\phoenix\network\protocol\protobufs
set proto_app_path=%app_path%\protocol\protobufs
set output_path=%app_path%\protocol\golang
set output_path_python=%app_path%\protocol\python
echo "Generating protobufs..."
echo %local_path%
echo %protoc_path%"
if not exist %protoc_path% (
	echo "protoc.exe not found, please download it from https://github.com/google/protobuf/releases"
	pause
	exit
)
%protoc_path%\protoc.exe --proto_path=%proto_path% --proto_path=%proto_app_path% --go_out=%output_path% %proto_path%\*.proto %proto_app_path%\*.proto
%protoc_path%\protoc.exe --proto_path=%proto_path% --proto_path=%proto_app_path% --python_out=%output_path_python% %proto_path%\*.proto %proto_app_path%\*.proto
echo "Done"
