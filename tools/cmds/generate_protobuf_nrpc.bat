@echo off
set local_path=%CD%
set protoc_path=%local_path%\..\protobuf\protoc-25.1-win64\bin
set proto_path=%local_path%\..\..\phoenix\network\protocol\nrpc
set proto_path_base=%local_path%\..\..\phoenix\network\protocol\nrpc\protobufs
set output_path=%local_path%\..\..\..
set output_path_nrpc=%local_path%\..\..\phoenix\network\protocol\nrpc\golang
echo "Generating nrpc protobufs..."
echo %local_path%
echo %protoc_path%"
if not exist %protoc_path% (
	echo "protoc.exe not found, please download it from https://github.com/google/protobuf/releases"
	pause
	exit
)
rem %protoc_path%\protoc.exe  --proto_path=%proto_path_base%  --proto_path=%proto_path% --go_out=%output_path% %proto_path_base%\*.proto %proto_path%\*.proto
%protoc_path%\protoc.exe  --proto_path=%proto_path_base%  --go_out=%output_path% %proto_path_base%\*.proto
echo "Done"
