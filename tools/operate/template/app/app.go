package main

import (
	"phoenix/phoenix"
)

func main() {
	ph := phoenix.PhoenixApp()
	ph.PhoenixStart(NewGServer())
}
