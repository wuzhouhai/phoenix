// Package hello -----------------------------
// @file      : hello.go
// @author    : WuZhouHai
// @contact   : wzh200x@126.com
// @time      : 2024/1/3 19:04
// @desc      :
// -------------------------------------------
package hello

import (
	"context"
	"fmt"
	"github.com/gogf/gf/v2/frame/g"
)

type HelloReq struct {
	g.Meta `path:"/hello" method:"get"`
	Name   string `v:"required" dc:"Your name"`
}
type HelloRes struct {
	Reply string `json:"result"`
}

type Hello struct{}

func (Hello) HelloHandler(ctx context.Context, req *HelloReq) (res *HelloRes, err error) {
	g.Log().Debugf(ctx, `receive say: %+v`, req)
	res = &HelloRes{
		Reply: fmt.Sprintf(`Hi %s`, req.Name),
	}
	return
}

func NewHello() interface{} {
	return &Hello{}
}
