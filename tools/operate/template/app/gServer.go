package main

type GServer struct {
}

func (this *GServer) OnInit() {
}

func (this *GServer) OnAfterInit() {

}

func (this *GServer) OnStart() {

}

func NewGServer() *GServer {
	return &GServer{}
}
