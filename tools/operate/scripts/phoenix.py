import sys
from operate import Operate

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python phoenix.py operateType operateValue")
        sys.exit(1)
    op = Operate(sys.argv)
    msg = op.DoOperate()
    print(msg)