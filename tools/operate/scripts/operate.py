import os
import re
import shutil
from pathlib import Path


class Operate(object):

    def __init__(self, argv):
        self.argv = argv

    def DoOperate(self):
        phoenixRoot = os.environ.get("PHOENIX_ROOT")
        if phoenixRoot is None:
            return "未设置PHOENIX_ROOT环境变量"
        operateType = self.argv[1]
        operateValue = self.argv[2]
        if operateType == "new":
            return self.CreateNewObject(operateValue)
        else:
            return "未知操作"

    def CreateNewObject(self, name):
        print("创建应用: " + name)
        phoenixRoot = os.environ.get("PHOENIX_ROOT")
        appPath = phoenixRoot + "/apps"
        templatePath = phoenixRoot + "/tools/operate/template/app"
        appPath += "/" + name
        if os.path.exists(appPath):
            return "应用已存在"
        self.CopyPath(templatePath, appPath)
        initFilePath = appPath + "/registerInit.template"
        initFilePathGo = appPath + "/registerInit.go"
        # 读取文件内容
        with open(initFilePath, 'r', encoding='utf-8') as file:
            content = file.read()
        # 进行搜索和替换
        updated_content = re.sub(r'%projectName%', name, content)
        # 写回文件
        with open(initFilePath, 'w', encoding='utf-8') as file:
            file.write(updated_content)
        os.rename(initFilePath, initFilePathGo)
        return "应用 [" + name + "]创建成功"

    def CopyPath(self, srcFolder, destFolder):
        # 创建目标文件夹如果它不存在
        Path(destFolder).mkdir(parents=True, exist_ok=True)

        # 遍历源文件夹中的所有文件和子文件夹
        for item in os.listdir(srcFolder):
            src_item = os.path.join(srcFolder, item)
            dest_item = os.path.join(destFolder, f"{item}")

            if os.path.isdir(src_item):
                # 如果是目录，递归调用自身
                self.CopyPath(src_item, dest_item)
            else:
                # 如果是文件，就复制它
                shutil.copy2(src_item, dest_item)  # 使用copy2来保留元数据