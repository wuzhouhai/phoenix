chcp 65001
@echo off
::获取当前路径
set currentPath=%~dp0
echo %currentPath%
set etcdExe=%currentPath%windows/etcd.exe
echo %etcdExe%
::判断程序是否存在
if not exist %etcdExe% (
    echo nats-server.exe not found
    exit 1
    )
echo 启动etcd
::判断程序是否运行,不存在就允许
tasklist | find /i "etcd.exe" && taskkill /im etcd.exe /f || start /b %etcdExe%
